using System;
using System.Data;
using System.Drawing;
using System.Collections.Specialized;
using System.Globalization;
using System.Web;
using System.Linq;

using System.Collections.Generic;
using System.Text;
using System.Net.Mail;

using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;

using Orchestrator.Globals;

namespace Orchestrator.Reports.Invoicing.Groupage
{

    public class rptOriginalGroupageInvoice : GroupageReportBase
    {

        #region Report Variables

        private bool _currentPageHasData = false;
        float? _deliveryAddressWidth = null;

        //# The total amount of extras to apply fuel surcharge to 
        private decimal m_AmountForFuelSurcharge = 0;
        private decimal m_ForeignAmountForFuelSurcharge = 0;
        private Label lblYourVatNo;
        private TextBox txtPurchaseOrderReference;
        private Label lblPurchaseOrderReference;
        private Label lblHelpsWithFormattingDontRemove;
        private SubReport sbSurcharges;
        private TextBox txtYourVatNo;
        private Line lineOrderFooter;
        private TextBox txtPageHeaderInvoiceNo;
        private Label lblPageHeaderInvoiceNo;
        private RichTextBox txtCustomNotes;
        private SubReport subGoodsRefused;
        private TextBox lblCases;
        private TextBox txtCases;
        private TextBox lblVehicleRegNo;
        private TextBox txtVehicleRegNo;
        private GroupHeader groupHeader1;
        private GroupFooter groupFooter1;
        private TextBox GoodsTypelbl;
        private TextBox GoodsTypeTxt;
        List<decimal> _fuelSurchargePercentages = new List<decimal>();
        private string exchangeRates = string.Empty;

        //Used to accumulate the sub totals per Nominal Code
        //Dictionary<string, Entities.InvoiceNominalCodeSubTotal> m_NominalCodeSubTotal = new Dictionary<string, Entities.InvoiceNominalCodeSubTotal>();

        #endregion

        #region Override Header

        protected override void CreateReportHeader()
        {
        }

        private void ReportBase_BeforePrint(object sender, EventArgs e)
        {
            TextBox txtPageNumber = (TextBox)this.Sections["PageFooter"].Controls["ftr_txtPageNumber"];
            ((Label)this.Sections["PageFooter"].Controls["ftr_lblPage"]).Text = "Page " + txtPageNumber.Text;
        }

        protected override void CreatePageFooter()
        {
            Section PageFooter = Sections["PageFooter"];

            if (PageFooter == null)
            {
                // Insert the page footer
                Sections.InsertPageHF();
                PageFooter = Sections["PageFooter"];
            }

            this.Sections["PageFooter"].BeforePrint += new EventHandler(ReportBase_BeforePrint);

            // Page Count TextBox
            TextBox txtPageNumber = new TextBox();
            txtPageNumber.Name = "ftr_txtPageNumber";
            txtPageNumber.Text = "##";
            txtPageNumber.Visible = false;
            txtPageNumber.SummaryType = SummaryType.PageCount;
            txtPageNumber.SummaryRunning = SummaryRunning.All;

            // Page Count Label
            Label lblPage = new Label();
            lblPage.Name = "ftr_lblPage";
            lblPage.Width = 5F;
            lblPage.Top = 0.1F;
            lblPage.Left = PrintWidth - lblPage.Width;
            lblPage.Alignment = TextAlignment.Right;

            //Add the controls to the footer
            if (PageFooter != null)
                PageFooter.Controls.AddRange(new ARControl[] { lblPage, txtPageNumber });

            if (PageFooter != null && !this.showInvoicePageFooter)
            {
                // make the footer smaller for the standard footer to avoid big gaps in the report.
                PageFooter.Height = 0.2F;
                this.srFooter.Height = 0.2F;
            }
            if (this.invoicePageFooterOnLastPageOnly)
                this.srFooter.Visible = false;
        }

        #endregion

        public rptOriginalGroupageInvoice()
        {
            InitializeComponent();

            rptHeader rpt = new rptHeader();
            this.srHeader.Report = rpt;

            rptPageHeader rptPage = new rptPageHeader();
            this.srPageHeader.Report = rptPage;

            rptFooter rpt_ftr = new rptFooter();
            this.srFooter.Report = rpt_ftr;

            lblTradingAs.Text = System.Web.Configuration.WebConfigurationManager.AppSettings["TradingAs"];
        }

        #region Report Events

        private void ReportHeader_Format(object sender, EventArgs e)
        {
            //-------------------------------------------------------------------------------------	
            //							Client Account & Address Details Section
            //-------------------------------------------------------------------------------------	
            Facade.IOrganisation facOrganisation = new Facade.Organisation();

            string vatNo = facOrganisation.GetVATNumberForIdentityID(this.PreInvoice.IdentityID);
            // set the client Vat number
            if (String.IsNullOrEmpty(vatNo))
            {
                this.txtYourVatNo.Visible = false;
                this.lblYourVatNo.Visible = false;
            }
            else if (vatNo.ToLower().Trim() == "not set")
            {
                this.txtYourVatNo.Visible = false;
                this.lblYourVatNo.Visible = false;
            }
            else
            {
                this.txtYourVatNo.Visible = true;
                this.lblYourVatNo.Visible = true;
                this.txtYourVatNo.Text = vatNo;
            }
            // Client Name
            txtCustomerName.Text = facOrganisation.GetDisplayNameForIdentityId(this.PreInvoice.IdentityID);

            // Client Account Details
            txtAccountNo.Text = facOrganisation.GetAccountCodeForIdentityId(this.PreInvoice.IdentityID);

            Facade.IAddress facAddress = new Facade.Point();
            Entities.Address address = facAddress.GetForAddressID(this.PreInvoice.GenerationParameters.AddressID);
            if (address != null)
            {
                // Client Address Details
                txtAddress.Text = BuildAddressInformation(address);
            }

            DataSet dsDefaults = facOrganisation.GetDefaultsForIdentityId(this.PreInvoice.IdentityID);

            if (dsDefaults.Tables[0].Rows.Count > 0)
                this.lblInvoiceTerms.Text = dsDefaults.Tables[0].Rows[0]["PaymentTerms"].ToString();

            // Configure the invoice date and client reference on the report.
            txtInvoiceDate.Text = PreInvoice.GenerationParameters.InvoiceDate.ToString("dd/MM/yy");
            if (PreInvoice.GenerationParameters.ClientInvoiceReference.Length > 0)
            {
                lblYourReference.Visible = true;
                txtYourReference.Visible = true;
                txtYourReference.Text = PreInvoice.GenerationParameters.ClientInvoiceReference;
            }
            else
            {
                lblYourReference.Visible = false;
                txtYourReference.Visible = false;
            }

            if (PreInvoice.GenerationParameters.PurchaseOrderReference.Length > 0)
            {
                lblPurchaseOrderReference.Visible = true;
                txtPurchaseOrderReference.Visible = true;
                txtPurchaseOrderReference.Text = PreInvoice.GenerationParameters.PurchaseOrderReference;
            }
            else
            {
                lblPurchaseOrderReference.Visible = false;
                txtPurchaseOrderReference.Visible = false;
            }

            if (PreInvoice.InvoiceID > 0)
            {
                // Set the invoice number.
                txtInvoiceNo.Text = Configuration.InvoicingInvoicePrefix + PreInvoice.InvoiceID.ToString();
                txtPageHeaderInvoiceNo.Text = Configuration.InvoicingInvoicePrefix + PreInvoice.InvoiceID.ToString();
            }
            // Show either the "this is a preview of the invoice" or the "this is a real invoice" text label.
            lblIsPreInvoice.Visible = PreInvoice.InvoiceID == 0;
            lblIsInvoice.Visible = PreInvoice.InvoiceID > 0;

            // Show VAT Number
            bool showHeader = false;
            bool.TryParse(System.Configuration.ConfigurationManager.AppSettings["ShowInvoiceHeader"], out showHeader);

            // this can now be turned off on a per print basis.
            if (PreInvoice.GenerationParameters.Options.UseHeadedPaper)
            {
                this.srFooter.Visible = false;
                this.srHeader.Visible = false;
                this.srPageHeader.Visible = false;
            }

            lblOurVatNo.Visible = !showHeader;
            txtOurVatNo.Visible = !showHeader;
            txtOurVatNo.Text = System.Configuration.ConfigurationManager.AppSettings["VatNo"];
        }

        private void PageHeader_Format(object sender, EventArgs e)
        {
            this._currentPageHasData = false;

            PageHeader ph = sender as PageHeader;

            if (this.PageNumber == 1 || !this.showInvoicePageHeader)
            {
                this.srPageHeader.Visible = false;

                // On any page other that the first page, make give the page header more height.
                ph.Height -= 1.800f;
                foreach (ARControl control in ph.Controls)
                    control.Top -= 1.800f;
            }
            else
                this.srPageHeader.Visible = true;

            if (this.PageNumber == 1)
            {
                this.lblPageHeaderInvoiceNo.Visible = false;
                this.txtPageHeaderInvoiceNo.Visible = false;
            }
            else
            {
                this.lblPageHeaderInvoiceNo.Top += 1.800f;
                this.lblPageHeaderInvoiceNo.Visible = true;
                this.txtPageHeaderInvoiceNo.Top += 1.800f;
                this.txtPageHeaderInvoiceNo.Visible = true;
            }
        }

        private void PageHeader_BeforePrint(object sender, EventArgs e)
        {
            if (!this._currentPageHasData)
                lblDate.Visible = lblOrderId.Visible = lblCollectionAddress.Visible = lblDeliveryAddress.Visible = lblPallets.Visible = lblWeight.Visible = lblPalletSpaces.Visible = lblCases.Visible = lblRef.Visible = lblValue.Visible = false;
        }

        decimal fuelSurchargeAmount = 0m;
        decimal foreignFuelSurchargeAmount = 0m;

        void grpOrder_Format(object sender, EventArgs e)
        {
            this._currentPageHasData = true;

            switch (txtPalletSpaces.Text)
            {
                case "0.25":
                    txtPalletSpaces.Text = "�";
                    break;
                case "0.50":
                    txtPalletSpaces.Text = "�";
                    break;
                default:
                    decimal output;

                    if (decimal.TryParse(txtPalletSpaces.Text, out output))
                        txtPalletSpaces.Text = string.Format("{0:f0}", output);

                    if (output == 0)
                        txtPalletSpaces.Text = string.Empty;

                    break;
            }

            int orderId = int.Parse(txtOrderID.Text);
            DataRow orderRow = (this as IInvoiceReportWithDataSet).GenerationData.Tables[0].Select(string.Format("OrderId = {0}", orderId))[0];
            int orderGroupID = int.Parse(txtOrderGroupID.Text);

            // Keep the Total and Nominal Code Sub Totals running

            //// Parse the rates from the order row, not the textboxes, because they are currency formatted.
            decimal rate = (decimal)orderRow["Rate"];
            decimal foreignRate = (decimal)orderRow["ForeignRate"];
            decimal exchangeRate = 0;

            if (orderRow["ExchangeRate"] != System.DBNull.Value)
                exchangeRate = (decimal)orderRow["ExchangeRate"];

            //Fuel Surcharge is always applied to the order rate so add the order rate to the 
            //sum that will be used to calculate the Fuel Surcharge
            m_AmountForFuelSurcharge += (decimal)orderRow["FuelSurchargeAmount"]; ;
            m_ForeignAmountForFuelSurcharge += (decimal)orderRow["FuelSurchargeForeignAmount"];

            //Start calculating the order's fuel surcharge sub total
            //It will be shown in the footer
            fuelSurchargeAmount = (decimal)orderRow["FuelSurchargeAmount"];
            foreignFuelSurchargeAmount = (decimal)orderRow["FuelSurchargeForeignAmount"];

            //If Surcharges are NOT to be shown then they need to be added to the rate
            if (!Orchestrator.Globals.Configuration.ShowSurchargesInInvoice)
            {
                decimal surchargeTotal = 0m, surchargeFuelSurchargeTotal = 0m;
                decimal foreignSurchargeTotal = 0m, foreignSurchargeFuelSurchargeTotal = 0m;

                GetAndSumSurcharges(orderGroupID, orderId, out surchargeTotal, out surchargeFuelSurchargeTotal, out foreignSurchargeTotal, out foreignSurchargeFuelSurchargeTotal);

                //Add surcharge fuel surcharge for the order fuel surcharge sub total
                fuelSurchargeAmount += surchargeFuelSurchargeTotal;
                foreignFuelSurchargeAmount += foreignSurchargeFuelSurchargeTotal;

                //Add surcharge fuel surcharge for the reports total fuel surcharge
                m_AmountForFuelSurcharge += surchargeFuelSurchargeTotal;
                m_ForeignAmountForFuelSurcharge += foreignSurchargeFuelSurchargeTotal;

                //Add the surcharges to the rate
                rate += surchargeTotal;
                foreignRate += foreignSurchargeTotal;

                //Modify the rate text to include the surcharges
                txtRate.Text = rate.ToString("C", CultureInfo.InvariantCulture.NumberFormat);
            }

            //Always set the Foreign currency so that the formatting is applied
            //surcharges may have been added in to the rate
            txtForeignRate.Text = foreignRate.ToString("C");

            //Add to the running total to show as the Sub total 
            //(may also incude the surcharges)
            ItemTotal += rate;
            ForeignItemTotal += foreignRate;

            // Show all distinct exchange rates used by orders on this invoice
            if (exchangeRate != 0 && !exchangeRates.Contains(exchangeRate.ToString()))
                exchangeRates += exchangeRate.ToString() + " ";      

            OrderGroupHandlingRecord record =
                        _handledOrderGroups.Find(delegate (OrderGroupHandlingRecord handlingRecord)
                        {
                            return
                                handlingRecord.OrderGroupID ==
                                orderGroupID;
                        });

            if (record != null)
            {
                this.lineOrderFooter.Visible = false;
            }
            else
            {
                this.lineOrderFooter.Visible = true;
                this.lineOrderFooter.LineStyle = LineStyle.Solid;
                this.lineOrderFooter.LineWeight = 1f;
            }

            // Signal that the order group has been handled
            if (orderGroupID > 0)
                _handledOrderGroups.Add(new OrderGroupHandlingRecord(orderGroupID)); // Ensure the total for this order group will not be processed again.

            ((GroupHeader)sender).Visible = true;



            #region Customer References field

            // Always show customer order number (or 'none' if there is no customer order number).
            // NEXT, if Include References is true and there is a delivery order number, then show the delivery order number.
            // NEXT, if Include Run Id is true, then show the delivery run id.
            // NEXT, if Include Service Level is true then show the service level
            // NEXT, for each custom reference that has DisplayOnInvoice == true and Reference != null then show the Reference field.


            List<DataRow> customerReferences = new List<DataRow>();
            string sOrderId = orderRow["OrderID"].ToString();

            // Get customer order number row
            DataRow customerOrderNumberRow = (this as IInvoiceReportWithDataSet).GenerationData.Tables[1].NewRow();
            customerOrderNumberRow["OrderID"] = sOrderId;
            customerOrderNumberRow["Reference"] = orderRow["CustomerOrderNumber"].ToString();

            // Get delivery order number
            DataRow deliveryOrderNumberRow = null;
            if ((this as IInvoiceReport).PreInvoice.GenerationParameters.Options.IncludeReferences)
            {
                if (this.txtDeliveryOrderNumber.Text.Length > 0)
                {
                    deliveryOrderNumberRow = (this as IInvoiceReportWithDataSet).GenerationData.Tables[1].NewRow();
                    deliveryOrderNumberRow["OrderID"] = sOrderId;
                    deliveryOrderNumberRow["Reference"] = orderRow["DeliveryOrderNumber"].ToString();
                }
            }

            // Get the run id
            DataRow runIdRow = null;
            if ((this as IInvoiceReport).PreInvoice.GenerationParameters.Options.IncludeInvoiceRunId)
            {
                runIdRow = (this as IInvoiceReportWithDataSet).GenerationData.Tables[1].NewRow();
                runIdRow["OrderID"] = sOrderId;
                runIdRow["Reference"] = orderRow["LastRunId"].ToString();
            }

            // Get service order number
            DataRow serviceLevelRow = null;
            if ((this as IInvoiceReport).PreInvoice.GenerationParameters.Options.IncludeServiceLevel)
            {
                serviceLevelRow = (this as IInvoiceReportWithDataSet).GenerationData.Tables[1].NewRow();
                serviceLevelRow["OrderID"] = sOrderId;
                serviceLevelRow["Reference"] = orderRow["OrderServiceLevel"].ToString();
            }

            // Get custom references
            DataRow[] customReferencesDataRows = (this as IInvoiceReportWithDataSet).GenerationData.Tables[1].Select("OrderID = " + sOrderId);
            List<DataRow> customReferencesList = new List<DataRow>();
            foreach (DataRow customReference in customReferencesDataRows)
            {
                if (Convert.ToBoolean(customReference["DisplayOnInvoice"]) == true && customReference["Reference"].ToString().Equals(String.Empty) == false)
                {
                    customReferencesList.Add(customReference);
                }
            }

            // Add customer references list of rows as appropriate 
            customerReferences.Insert(customerReferences.Count, customerOrderNumberRow);
            if (deliveryOrderNumberRow != null)
                customerReferences.Insert(customerReferences.Count, deliveryOrderNumberRow);
            if (runIdRow != null)
                customerReferences.Insert(customerReferences.Count, runIdRow);
            if (serviceLevelRow != null)
                customerReferences.Insert(customerReferences.Count, serviceLevelRow);
            customerReferences.InsertRange(customerReferences.Count, customReferencesList);

            // Convert the list of reference data rows into an array of reference data rows
            DataRow[] referencesForReport;
            referencesForReport = new DataRow[customerReferences.Count];
            customerReferences.CopyTo(referencesForReport);

            // Assign the array of reference data rows to subReferences.Report.DataSource
            this.subReferences.Report = new SubReports.References();
            this.subReferences.Report.DataSource = referencesForReport;
            this.subReferences.Report.DataMember = "Table";

            #endregion


            this.subGoodsRefused.Visible = false;

            if ((this as IInvoiceReport).PreInvoice.GenerationParameters.Options.IncludeGoodsRefusals)
            {
                DataRow[] goodsRefusals = (this as IInvoiceReportWithDataSet).GenerationData.Tables[3].Select("OrderID = " + this.txtOrderID.Text);

                if (goodsRefusals.Length > 0)
                {
                    this.subGoodsRefused.Visible = true;
                    this.subGoodsRefused.Report = new SubReports.GoodsRefused();
                    this.subGoodsRefused.Report.DataSource = goodsRefusals;
                    this.subGoodsRefused.Report.DataMember = "Table";
                }
            }

            //vehidleRegNo
            var showRegNo = (this as IInvoiceReport).PreInvoice.GenerationParameters.Options.ShowVehicleRegInline;

            this.groupFooter1.Visible = false;
            this.groupFooter1.Height = 0;

            if (showRegNo)
            {
                lblVehicleRegNo.Visible = txtVehicleRegNo.Visible = true;
                lblVehicleRegNo.Text = "RegNo:";
                if (txtVehicleRegNo.Text.Trim() == "")
                {
                    txtVehicleRegNo.Text = "None Loaded";
                }
            }
            else
            {
                lblVehicleRegNo.Text = "";
            }

            var includeGoodsType = (this as IInvoiceReport).PreInvoice.GenerationParameters.Options.IncludeGoodsType;
            if (includeGoodsType)
            {
                GoodsTypelbl.Visible = GoodsTypeTxt.Visible = true;
                GoodsTypelbl.Text = "Goods Type:";
                if (GoodsTypeTxt.Text.Trim() == "")
                {
                    GoodsTypeTxt.Text = "None";
                }
                if (!showRegNo)
                {
                    GoodsTypelbl.Left = lblVehicleRegNo.Left;
                    GoodsTypeTxt.Left = txtVehicleRegNo.Left;
                }
            }
            else
            {
                GoodsTypelbl.Text = "";
            }

            if (includeGoodsType || showRegNo)
            {
                this.groupHeader1.Visible = true;
                this.groupHeader1.Height = 0.1979167F;
            }
            else
            {
                lblVehicleRegNo.Text = "";
                GoodsTypelbl.Text = "";
                this.groupHeader1.Visible = false;
                this.groupHeader1.Height = 0;
            }

            // pallets column display
            var includePallets = (this as IInvoiceReport).PreInvoice.GenerationParameters.Options.IncludePallets;
            txtPallets.Visible = includePallets;

            // weight column display
            var includeWeight = (this as IInvoiceReport).PreInvoice.GenerationParameters.Options.IncludeWeights;
            txtWeight.Visible = includeWeight;

            var includeSpaces = (this as IInvoiceReport).PreInvoice.GenerationParameters.Options.IncludeSpaces;
            txtPalletSpaces.Visible = includeSpaces;

            var includeCases = (this as IInvoiceReport).PreInvoice.GenerationParameters.Options.IncludeCases;
            txtCases.Visible = includeCases;

            // resize delivery address and reposition pallets column where required if columns are hidden
            if (!_deliveryAddressWidth.HasValue)
            {
                _deliveryAddressWidth = lblDeliveryAddress.Width;

                if (!includePallets)
                {
                    _deliveryAddressWidth += lblPallets.Width;
                    lblPallets.Width = txtPallets.Width = 0;
                }

                if (!includeWeight)
                {
                    _deliveryAddressWidth += lblWeight.Width;
                    lblWeight.Width = txtWeight.Width = 0;
                }

                if (!includeSpaces)
                {
                    _deliveryAddressWidth += lblPalletSpaces.Width;
                    lblPalletSpaces.Width = txtPalletSpaces.Width = 0;
                }

                if (!includeCases)
                {
                    _deliveryAddressWidth += lblCases.Width;
                    lblCases.Width = txtCases.Width = 0;
                }
            }
            float lastpos = 0;
            txtDeliveryAddress.Width = _deliveryAddressWidth.Value;

            lastpos = txtDeliveryAddress.Left + txtDeliveryAddress.Width;
            float lastwidth = 0;

            if (includePallets)
            {
                txtPallets.Left = lastpos;
                lastwidth = txtPallets.Width;
            }
            if (includeWeight)
            {
                txtWeight.Left = lastpos = lastpos + lastwidth;
                lastwidth = txtWeight.Width;
            }
            if (includeSpaces)
            {
                txtPalletSpaces.Left = lastpos = lastpos + lastwidth;
                lastwidth = txtPalletSpaces.Width;
            }
            if (includeCases)
            {
                txtCases.Left = lastpos = lastpos + lastwidth;
                lastwidth = txtCases.Width;
            }



        }

        private void grpOrderFooter_Format(object sender, System.EventArgs eArgs)
        {
            int orderGroupID = int.Parse(txtOrderGroupIDFooter.Text);
            int orderID = int.Parse(txtOrderIDFooter.Text);
            DataRow orderRow = (this as IInvoiceReportWithDataSet).GenerationData.Tables[0].Select(string.Format("OrderId = {0}", orderID))[0];

            IInvoiceReportWithDataSet invoiceReport = this as IInvoiceReportWithDataSet;
            GroupFooter thisFooter = sender as GroupFooter;

            Label lblJobFuelSurcharge = thisFooter.Controls["lblJobFuelSurcharge"] as Label;
            TextBox txtJobFuelSurchargeRate = thisFooter.Controls["txtJobFuelSurchargeRate"] as TextBox;
            TextBox txtJobFuelSurchargeAmount = thisFooter.Controls["txtJobFuelSurchargeAmount"] as TextBox;

            if (orderGroupID == 0 || !OrderGroupHasBeenHandledWithExtras(orderGroupID))
            {
                ((GroupFooter)sender).Visible = true;

                sbSurcharges.Visible = false;

                //If Surcharges are to be shown
                if (Orchestrator.Globals.Configuration.ShowSurchargesInInvoice)
                {
                    decimal surchargeTotal = 0m, surchargeFuelSurchargeTotal = 0m;
                    decimal foreignSurchargeTotal = 0m, foreignSurchargeFuelSurchargeTotal = 0m;

                    DataRow[] surcharges = GetAndSumSurcharges(orderGroupID, orderID, out surchargeTotal, out surchargeFuelSurchargeTotal, out foreignSurchargeTotal, out foreignSurchargeFuelSurchargeTotal);

                    //Add surcharge fuel surcharge for order fuel surcharge sub total
                    //(The order's fuel surcharge has already been added in grpOrder_Format -
                    //it is splt between the two events to prevent the need for GetAndSumSurcharges 
                    //from being called twice as the reporrts order field cannot be set here)
                    fuelSurchargeAmount += surchargeFuelSurchargeTotal;
                    foreignFuelSurchargeAmount += foreignSurchargeFuelSurchargeTotal;

                    //If there is fuel surcharge on extras add to the amount used for fuel surcharge calculations
                    m_AmountForFuelSurcharge += surchargeFuelSurchargeTotal;
                    m_ForeignAmountForFuelSurcharge += foreignSurchargeFuelSurchargeTotal;

                    if (surcharges != null && surcharges.Length > 0)
                    {
                        sbSurcharges.Visible = true;
                        sbSurcharges.Report = new SubReports.Surcharges();
                        sbSurcharges.Report.DataSource = surcharges;
                        sbSurcharges.Report.DataMember = "Table";

                        //Surcharges are being Add to the report's extras total
                        invoiceReport.ExtrasTotal += surchargeTotal;
                        invoiceReport.ForeignExtrasTotal += foreignSurchargeTotal;

                    }
                }

                //#12987 JBS Reintroduce the Extras per Order
                decimal extrasTotal = 0m, extrasFuelSurchargeTotal = 0m;
                decimal foreignExtrasTotal = 0m, foreignExtrasFuelSurchargeTotal = 0m;

                CalculateAndShowJobExtras(thisFooter, orderGroupID, orderID, out extrasTotal, out extrasFuelSurchargeTotal, out foreignExtrasTotal, out foreignExtrasFuelSurchargeTotal);

                //Add to the report's extras total
                invoiceReport.ExtrasTotal += extrasTotal;
                invoiceReport.ForeignExtrasTotal += foreignExtrasTotal;

                //Default to not showing fuel surcharge per order
                lblJobFuelSurcharge.Visible = false;
                txtJobFuelSurchargeRate.Visible = false;
                txtJobFuelSurchargeAmount.Visible = false;

                //Add extras fuel surcharge for order fuel surcharge sub total
                fuelSurchargeAmount += extrasFuelSurchargeTotal;
                foreignFuelSurchargeAmount += foreignExtrasFuelSurchargeTotal;

                //Maintain a list of unique fuel surcharge percentages for displaying in
                //the report footer
                decimal fuelSurchargePercentage = (orderRow["FuelSurchargePercentage"] == DBNull.Value) ? decimal.Zero : (decimal)orderRow["FuelSurchargePercentage"];
                if (!_fuelSurchargePercentages.Contains(fuelSurchargePercentage))
                    _fuelSurchargePercentages.Add(fuelSurchargePercentage);

                //TODO With the FS changes is this required?
                //#15566 Add to the extras total for fuel surcharge 
                m_AmountForFuelSurcharge += extrasFuelSurchargeTotal;
                m_ForeignAmountForFuelSurcharge += foreignExtrasFuelSurchargeTotal;

                //If Fuel Surcharge is to be shown and it is to be shown per order
                //make the relevant field visible and display the values
                if (invoiceReport.PreInvoice.GenerationParameters.Options.ApplyFuelSurcharge
                    && invoiceReport.PreInvoice.GenerationParameters.Options.FuelSurchargeDisplayMethod == eInvoiceDisplayMethod.PerOrder)
                {
                    lblJobFuelSurcharge.Visible = true;
                    txtJobFuelSurchargeRate.Visible = true;
                    txtJobFuelSurchargeAmount.Visible = true;

                    txtJobFuelSurchargeRate.Text = fuelSurchargePercentage.ToString("F2") + "%";
                    txtJobFuelSurchargeAmount.Text = foreignFuelSurchargeAmount.ToString("C");
                }
                //Hide if not VatToal checked bjc
                if (invoiceReport.PreInvoice.GenerationParameters.Options.IncludeVatTotal)
                {
                    lblVAT.Visible = true;
                    txtVATPercentage.Visible = true;
                    txtVATAmount.Visible = true;
                }
                else
                {
                    lblVAT.Visible = false;
                    txtVATPercentage.Visible = false;
                    txtVATAmount.Visible = false;
                }

                //Reposition the controls here instead of in the BeforePrint event 
                //as setting the height of the footer has no effect there
                RepositionOrderFooterControls(thisFooter);
            }
            else
            {
                // This order group has already been handled - don't reprocess everything and hide this row.
                ((GroupFooter)sender).Visible = false;
                return;
            }
        }

        private void Detail_Format(object sender, EventArgs e)
        {
            this._currentPageHasData = true;

            lblDeliveryAddress.Width = txtDeliveryAddress.Width;
            lblPallets.Left = txtPallets.Left;
            lblPallets.Visible = txtPallets.Visible;
            lblWeight.Left = txtWeight.Left;
            lblWeight.Visible = txtWeight.Visible;
            lblPalletSpaces.Left = txtPalletSpaces.Left;
            lblPalletSpaces.Visible = txtPalletSpaces.Visible;
            lblCases.Left = txtCases.Left;
            lblCases.Visible = txtCases.Visible;
            lblVehicleRegNo.Visible = txtVehicleRegNo.Visible;
        }

        private void grpOrderFooter_BeforePrint(object sender, System.EventArgs eArgs)
        {
            //Reposition the controls in the Format event instead of here as setting 
            //the height of the footer has no effect here
            //RepositionOrderFooterControls(sender as GroupFooter);
        }

        private void PageFooter_Format(object sender, EventArgs e)
        {
            PageFooter pf = sender as PageFooter;

            if (pf != null)
                pf.Visible = true;
        }

        private void ReportFooter_Format(object sender, EventArgs e)
        {
            IInvoiceReportWithDataSet invoiceReport = this as IInvoiceReportWithDataSet;
            ReportFooter reportFooter = sender as ReportFooter;

            SubReport extrasSubReport = reportFooter.Controls["sbExtras"] as SubReport;
            Label lblExtras = reportFooter.Controls["lblExtras"] as Label;
            TextBox txtExtrasTotal = reportFooter.Controls["txtExtrasTotal"] as TextBox;

            if (this.invoicePageFooterOnLastPageOnly)
            {
                sbClientCustomerReportFooter.Report = rptFooter.GetClientPageFooterSubReport(System.Configuration.ConfigurationManager.AppSettings["FooterReportName"].ToLower());
                if (sbClientCustomerReportFooter.Report != null)
                    sbClientCustomerReportFooter.Visible = true;
            }
            else
            {
                sbClientCustomerReportFooter.Visible = false;
                ReportHeader.Height -= sbClientCustomerReportFooter.Height;
            }

            // If the extras are displayed per invoice get all the extras, otherwise only get those that have not already been displayed.
            // Exclude surcharges, i.e. IsDisplayedOnAddUpdateOrder=1
            DataRow[] extrasToDisplay = null;
            if (invoiceReport.PreInvoice.GenerationParameters.Options.ExtraDisplayMethod == eInvoiceDisplayMethod.PerInvoice)
                extrasToDisplay = invoiceReport.GenerationData.Tables[2].Select("IsDisplayedOnAddUpdateOrder=0 AND IsRedeliveryExtra=0"); // Redelivery Extras as always shown with the order.
            else
                extrasToDisplay = invoiceReport.GenerationData.Tables[2].Select("IsDisplayedOnAddUpdateOrder=0 AND OrderID NOT IN (" + Entities.Utilities.GetCSV(invoiceReport.PreInvoice.ItemIDs) + ")");

            extrasSubReport.Visible = extrasToDisplay != null && extrasToDisplay.Length > 0;
            lblExtras.Visible = extrasToDisplay != null && extrasToDisplay.Length > 0;
            txtExtrasTotal.Visible = extrasToDisplay != null && extrasToDisplay.Length > 0;
            lblFuelSurcharge.Visible = !string.IsNullOrEmpty(txtFuelSurchargeRate.Text);

            if (extrasToDisplay != null && extrasToDisplay.Length > 0)
            {
                decimal extrasToDisplayTotal = 0, extrasForFuelSurchargeTotal = 0;
                decimal foreignExtrasToDisplayTotal = 0, foreignExtrasForFuelSurchargeTotal = 0;

                foreach (DataRow row in extrasToDisplay)
                {
                    extrasToDisplayTotal += (decimal)row["ExtraAmount"];
                    foreignExtrasToDisplayTotal += (decimal)row["ForeignAmount"];

                    extrasForFuelSurchargeTotal += (decimal)row["FuelSurchargeAmount"];
                    foreignExtrasForFuelSurchargeTotal += (decimal)row["FuelSurchargeForeignAmount"];
                }

                txtExtrasTotal.Text = foreignExtrasToDisplayTotal.ToString("C");

                invoiceReport.ExtrasTotal += extrasToDisplayTotal;
                invoiceReport.ForeignExtrasTotal += foreignExtrasToDisplayTotal;

                m_AmountForFuelSurcharge += extrasForFuelSurchargeTotal;
                m_ForeignAmountForFuelSurcharge += foreignExtrasForFuelSurchargeTotal;

                // Display the sub-report.
                extrasSubReport.Report = new SubReports.Extras();
                extrasSubReport.Report.DataSource = extrasToDisplay;
                extrasSubReport.Report.DataMember = "Table";

                // Calculate the fuel surcharge to apply to the extras on the invoice.
                //#12987 JBS Make Fuel Surcharge on Extras conditional
                lblFuelSurcharge.Visible = invoiceReport.PreInvoice.GenerationParameters.Options.ApplyFuelSurcharge && foreignExtrasForFuelSurchargeTotal > 0m;
                txtFuelSurchargeRate.Visible = lblFuelSurcharge.Visible;
                txtFuelSurchareAmount.Visible = lblFuelSurcharge.Visible;

                //Unfortunately only one percentage is currently expected but each order can now have its own.
                //However, it is likely that only one will bve used... hopefully
                txtFuelSurchargeRate.Text = string.Empty;
                foreach (decimal percentage in _fuelSurchargePercentages)
                    txtFuelSurchargeRate.Text += percentage.ToString("F2") + "% ";

                txtFuelSurchareAmount.Text = foreignExtrasForFuelSurchargeTotal.ToString("C");
            }

            Label lblInvoiceFuelCharge = reportFooter.Controls["lblInvoiceFuelCharge"] as Label;
            TextBox txtInvoiceFuelSurchargeRate = reportFooter.Controls["txtInvoiceFuelSurchargeRate"] as TextBox;
            TextBox txtFuelChargeTotal = reportFooter.Controls["txtFuelChargeTotal"] as TextBox;

            // Control the visibility of the fuel surcharge totals depending on the application of any surcharge.
            lblInvoiceFuelCharge.Visible = invoiceReport.PreInvoice.GenerationParameters.Options.ApplyFuelSurcharge;
            txtInvoiceFuelSurchargeRate.Visible = invoiceReport.PreInvoice.GenerationParameters.Options.ApplyFuelSurcharge;
            txtFuelChargeTotal.Visible = invoiceReport.PreInvoice.GenerationParameters.Options.ApplyFuelSurcharge;

            // Display the totals;
            txtSubTotal.Text = ForeignItemTotal.ToString("C");
            txtExtraTotal.Text = ForeignExtrasTotal.ToString("C");
            txtInvoiceSubTotal.Text = (ForeignItemTotal + ForeignExtrasTotal).ToString("C");

            txtInvoiceFuelSurchargeRate.Text = string.Empty;
            foreach (decimal percentage in _fuelSurchargePercentages)
                txtInvoiceFuelSurchargeRate.Text += percentage.ToString("F2") + "% ";

            (this as IInvoiceReport).FuelSurcharge = m_AmountForFuelSurcharge;
            (this as IInvoiceReport).ForeignFuelSurcharge = m_ForeignAmountForFuelSurcharge;


            txtFuelChargeTotal.Text = (this as IInvoiceReport).ForeignFuelSurcharge.ToString("C");
            txtVATPercentage.Text = (this as IInvoiceReport).PreInvoice.GenerationParameters.TaxRate.ToString("F2") + "%";

            System.Globalization.CultureInfo uk = new System.Globalization.CultureInfo("en-GB");

            bool IsFuelInTotals = PreInvoice.GenerationParameters.Options.IncludeFuelSurchargeInTotals;

            if (PreInvoice.GenerationParameters.Overrides.OverrideAmounts)
            {
                (this as IInvoiceReport).ItemTotal = PreInvoice.GenerationParameters.Overrides.NetAmount;
                txtTotalAmount.Text = PreInvoice.GenerationParameters.Overrides.NetAmount.ToString("C");
                (this as IInvoiceReport).ItemTotal = decimal.Parse(txtTotalAmount.Text, NumberStyles.Any);

                (this as IInvoiceReport).TaxTotal = PreInvoice.GenerationParameters.Overrides.TaxAmount;
                txtVATAmount.Text = PreInvoice.GenerationParameters.Overrides.TaxAmount.ToString("C");
                (this as IInvoiceReport).TaxTotal = decimal.Parse(txtVATAmount.Text, NumberStyles.Any);

                (this as IInvoiceReport).GrossTotal = PreInvoice.GenerationParameters.Overrides.TotalAmount;
                txtInvoiceTotal.Text = PreInvoice.GenerationParameters.Overrides.TotalAmount.ToString("C");
                (this as IInvoiceReport).GrossTotal = decimal.Parse(txtInvoiceTotal.Text, NumberStyles.Any);
            }
            else
            {
                if (IsFuelInTotals)
                {
                    (this as IInvoiceReport).ForeignItemTotal = decimal.Round((ForeignItemTotal + ForeignExtrasTotal + ForeignFuelSurcharge), 2, MidpointRounding.AwayFromZero);
                    (this as IInvoiceReport).ItemTotal = decimal.Round((ItemTotal + ExtrasTotal + FuelSurcharge), 2, MidpointRounding.AwayFromZero);
                }
                else
                {
                    (this as IInvoiceReport).ForeignItemTotal = decimal.Round((ForeignItemTotal + ForeignExtrasTotal), 2, MidpointRounding.AwayFromZero);
                    (this as IInvoiceReport).ItemTotal = decimal.Round((ItemTotal + ExtrasTotal), 2, MidpointRounding.AwayFromZero);
                }


                txtTotalAmount.Text = (this as IInvoiceReport).ForeignItemTotal.ToString("C");

                (this as IInvoiceReport).ForeignTaxTotal = decimal.Round(((this as IInvoiceReport).ForeignItemTotal / 100) * PreInvoice.GenerationParameters.TaxRate, 2, MidpointRounding.AwayFromZero);
                (this as IInvoiceReport).TaxTotal = decimal.Round(((this as IInvoiceReport).ItemTotal / 100) * PreInvoice.GenerationParameters.TaxRate, 2, MidpointRounding.AwayFromZero);
                txtVATAmount.Text = (this as IInvoiceReport).ForeignTaxTotal.ToString("C");

                if (!System.Threading.Thread.CurrentThread.CurrentCulture.Equals(uk) && (this as IInvoiceReport).PreInvoice.GenerationParameters.TaxRate > 0.00M)
                    txtVATPercentage.Text += " " + exchangeRates + " GBP " + (this as IInvoiceReport).TaxTotal.ToString();

                (this as IInvoiceReport).ForeignGrossTotal = decimal.Round(((this as IInvoiceReport).ForeignItemTotal + (this as IInvoiceReport).ForeignTaxTotal), 2, MidpointRounding.AwayFromZero);
                (this as IInvoiceReport).GrossTotal = decimal.Round((this as IInvoiceReport).ItemTotal + (this as IInvoiceReport).TaxTotal, 2, MidpointRounding.AwayFromZero);
                txtInvoiceTotal.Text = (this as IInvoiceReport).ForeignGrossTotal.ToString("C");
            }

            (this as IInvoiceReport).ExtrasTotal = decimal.Round((this as IInvoiceReport).ExtrasTotal, 2, MidpointRounding.AwayFromZero);
            (this as IInvoiceReport).FuelSurcharge = decimal.Round((this as IInvoiceReport).FuelSurcharge, 2, MidpointRounding.AwayFromZero);

            (this as IInvoiceReport).ForeignExtrasTotal = decimal.Round((this as IInvoiceReport).ForeignExtrasTotal, 2, MidpointRounding.AwayFromZero);
            txtExtrasTotal.Text = (this as IInvoiceReport).ForeignExtrasTotal.ToString("C");

            (this as IInvoiceReport).ForeignFuelSurcharge = decimal.Round((this as IInvoiceReport).ForeignFuelSurcharge, 2, MidpointRounding.AwayFromZero);

            //Check that the Nominal Code Subtotals match the totals calculated for the report
            CheckNominalCodeSubTotals();

            if (Globals.Configuration.InvoiceMessageForTypes.IndexOf("rptGroupageInvoice") > -1
                && Globals.Configuration.ShowInvoiceMessage)
            {
                this.txtCustomNotes.Text = String.Empty;
                this.txtCustomNotes.Html = Globals.Configuration.CustomInvoiceMessage;
            }
            else
            {
                this.txtCustomNotes.Html = String.Empty;
                this.txtCustomNotes.Text = String.Empty;
            }
        }

        private void CheckNominalCodeSubTotals()
        {
            if ((this as IInvoiceReport).PreInvoice.InvoiceID == 0)
            {
                decimal ncTotal = this.PreInvoice.InvoiceNominalCodeSubTotals.Sum(nc => nc.NetAmount);
                decimal invoiceTotal = (this as IInvoiceReport).ItemTotal;

                if (Math.Abs(ncTotal - invoiceTotal) > 0.02m)
                {
                    string msg = string.Format("The sum of NominalCodeSubTotals {0} does not match the invoice total {1} for PreInvoice {2}.\n", ncTotal, invoiceTotal, (this as IInvoiceReport).PreInvoice.PreInvoiceID);
                    msg += "NC debugInfo: " + this.PreInvoice.debugInfo;
                    try
                    {
                        System.Diagnostics.EventLog log = new System.Diagnostics.EventLog("Application");
                        log.WriteEntry(msg);
                    }
                    catch { }
                    try
                    {
                        EmailProblem(msg);
                    }
                    catch { };
                }
            }
        }

        private void EmailProblem(string text)
        {
            text = "From " + Globals.Configuration.InstallationName + "\n" + text;

            System.Net.Mail.MailMessage mailMessage = new System.Net.Mail.MailMessage();

            mailMessage.To.Add("support@orchestrator.co.uk");
            mailMessage.Subject = "Nominal Code Calculation Problem";
            mailMessage.Sender = new MailAddress("support@p1tp.com");
            mailMessage.From = new MailAddress("support@p1tp.com");
            mailMessage.Body = text;

            System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();
            smtp.Host = Globals.Configuration.MailServer;
            smtp.Port = Globals.Configuration.MailPort;
            smtp.Credentials = new System.Net.NetworkCredential(Globals.Configuration.MailUsername,
                Globals.Configuration.MailPassword);

            smtp.Send(mailMessage);
            mailMessage.Dispose();
        }


        private void ReportFooter_BeforePrint(object sender, System.EventArgs eArgs)
        {
            // Adjust the positioning of items in the report footer based on the heights and visibility of those items.
            ReportFooter reportFooter = sender as ReportFooter;

            float requiredY = 0F;
            float gap = 0.188F;

            lblExtrasHeading.Visible = sbExtras.Visible;
            if (sbExtras.Visible)
            {
                requiredY += sbExtras.Top + sbExtras.Height + gap;
            }

            if (lblExtras.Visible)
            {
                lblExtras.Top = requiredY;
                txtExtrasTotal.Top = requiredY;

                requiredY += gap;
            }

            if (txtFuelSurchareAmount.Visible)
            {
                lblFuelSurcharge.Top = requiredY;
                txtFuelSurchargeRate.Top = requiredY;
                txtFuelSurchareAmount.Top = requiredY;

                requiredY += gap;
            }

            gap = 0.100F;
            reportFooterLine.Y1 = reportFooterLine.Y2 = requiredY;

            lblSubTotal.Top = txtSubTotal.Top = requiredY;
            requiredY += txtSubTotal.Height;

            lblExtraTotal.Top = txtExtraTotal.Top = requiredY;
            requiredY += txtExtraTotal.Height;

            lblInvoiceSubTotal.Top = txtInvoiceSubTotal.Top = requiredY;
            requiredY += txtInvoiceSubTotal.Height;

            if (lblInvoiceFuelCharge.Visible)
            {
                lblInvoiceFuelCharge.Top = txtInvoiceFuelSurchargeRate.Top = txtFuelChargeTotal.Top = requiredY;
                requiredY += txtFuelChargeTotal.Height;
            }

            lblTotalAmount.Top = txtTotalAmount.Top = requiredY;
            requiredY += txtTotalAmount.Height;

            lblVAT.Top = txtVATPercentage.Top = txtVATAmount.Top = requiredY;
            requiredY += txtVATAmount.Height + gap;

            invoiceTotalTopLine.Y1 = invoiceTotalTopLine.Y2 = lblInvoiceTotal.Top = txtInvoiceTotal.Top = requiredY;
            requiredY += txtInvoiceTotal.Height;
            invoiceTotalBaseLine.Y1 = invoiceTotalBaseLine.Y2 = requiredY;
            requiredY += gap;

            lblInvoiceTerms.Top = requiredY;
            requiredY += gap;

            lblTradingAs.Top = requiredY;
            this.txtCustomNotes.Top = this.lblSubTotal.Top;
        }

        private void rptGroupageInvoice_ReportStart(object sender, System.EventArgs eArgs)
        {

        }

        void rptGroupageInvoice_ReportEnd(object sender, EventArgs e)
        {

        }

        #endregion

        private void RepositionOrderFooterControls(GroupFooter orderFooter)
        {
            // Adjust the positioning of items in the order footer based on the heights and visibility of those items.

            SubReport subGoodsRefused = orderFooter.Controls["subGoodsRefused"] as SubReport;
            SubReport sbSurcharges = orderFooter.Controls["sbSurcharges"] as SubReport;
            SubReport sbJobExtras = orderFooter.Controls["sbJobExtras"] as SubReport;
            Label lblJobFuelSurcharge = orderFooter.Controls["lblJobFuelSurcharge"] as Label;
            TextBox txtJobFuelSurchargeRate = orderFooter.Controls["txtJobFuelSurchargeRate"] as TextBox;
            TextBox txtJobFuelSurchargeAmount = orderFooter.Controls["txtJobFuelSurchargeAmount"] as TextBox;

            float requiredY = subGoodsRefused.Top;

            if (subGoodsRefused.Visible || sbSurcharges.Visible)
            {
                requiredY += 0.063F;

                if (subGoodsRefused.Visible)
                    requiredY += subGoodsRefused.Height;

                if (sbSurcharges.Visible)
                {
                    if (subGoodsRefused.Visible)
                        sbSurcharges.Top = requiredY;

                    requiredY += sbSurcharges.Height;
                }
            }

            if (sbJobExtras.Visible)
            {
                sbJobExtras.Top = requiredY;
                requiredY += sbJobExtras.Height;
            }

            if (lblJobExtraTotal.Visible)
            {
                lblJobExtraTotal.Top = requiredY;
                txtJobExtrasTotal.Top = requiredY;

                requiredY += Math.Max(lblJobExtraTotal.Height, txtJobExtrasTotal.Height);
            }

            if (lblJobFuelSurcharge.Visible)
            {
                lblJobFuelSurcharge.Top = requiredY;
                txtJobFuelSurchargeRate.Top = requiredY;
                txtJobFuelSurchargeAmount.Top = requiredY;

                requiredY += Math.Max(lblJobFuelSurcharge.Height, Math.Max(txtJobFuelSurchargeRate.Height, txtJobFuelSurchargeAmount.Height));
            }

        }


        #region ActiveReports Designer generated code
        private DataDynamics.ActiveReports.ReportHeader ReportHeader = null;
        private DataDynamics.ActiveReports.SubReport srHeader = null;
        private DataDynamics.ActiveReports.Label Label18 = null;
        private DataDynamics.ActiveReports.Label Label19 = null;
        private DataDynamics.ActiveReports.Label Label20 = null;
        private DataDynamics.ActiveReports.TextBox txtInvoiceNo = null;
        private DataDynamics.ActiveReports.TextBox txtInvoiceDate = null;
        private DataDynamics.ActiveReports.TextBox txtAccountNo = null;
        private DataDynamics.ActiveReports.TextBox txtCustomerName = null;
        private DataDynamics.ActiveReports.TextBox txtAddress = null;
        private DataDynamics.ActiveReports.Label lblIsPreInvoice = null;
        private DataDynamics.ActiveReports.Label lblIsInvoice = null;
        private DataDynamics.ActiveReports.Label lblOurVatNo = null;
        private DataDynamics.ActiveReports.TextBox txtOurVatNo = null;
        private DataDynamics.ActiveReports.Label lblYourReference = null;
        private DataDynamics.ActiveReports.TextBox txtYourReference = null;
        private DataDynamics.ActiveReports.PageHeader PageHeader = null;
        private DataDynamics.ActiveReports.SubReport srPageHeader = null;
        private DataDynamics.ActiveReports.TextBox lblDate = null;
        private DataDynamics.ActiveReports.TextBox lblOrderId = null;
        private DataDynamics.ActiveReports.TextBox lblCollectionAddress = null;
        private DataDynamics.ActiveReports.TextBox lblDeliveryAddress = null;
        private DataDynamics.ActiveReports.TextBox lblPallets = null;
        private DataDynamics.ActiveReports.TextBox lblWeight = null;
        private DataDynamics.ActiveReports.TextBox lblPalletSpaces = null;
        private DataDynamics.ActiveReports.TextBox lblRef = null;
        private DataDynamics.ActiveReports.TextBox lblValue = null;
        private DataDynamics.ActiveReports.GroupHeader grpOrder = null;
        private DataDynamics.ActiveReports.TextBox txtRate = null;
        private DataDynamics.ActiveReports.TextBox txtForeignRate = null;
        private DataDynamics.ActiveReports.TextBox txtCustomerRef = null;
        private DataDynamics.ActiveReports.TextBox txtDeliveryOrderNumber = null;
        private DataDynamics.ActiveReports.SubReport subReferences = null;
        private DataDynamics.ActiveReports.TextBox txtOrderServiceLevel = null;
        private DataDynamics.ActiveReports.TextBox txtPalletSpaces = null;
        private DataDynamics.ActiveReports.TextBox txtWeight = null;
        private DataDynamics.ActiveReports.TextBox txtPallets = null;
        private DataDynamics.ActiveReports.TextBox txtDeliveryAddress = null;
        private DataDynamics.ActiveReports.TextBox txtCollectionAddress = null;
        private DataDynamics.ActiveReports.TextBox txtOrderID = null;
        private DataDynamics.ActiveReports.TextBox txtOrderGroupID = null;
        private DataDynamics.ActiveReports.TextBox txtDeliveryDate = null;
        private DataDynamics.ActiveReports.Detail Detail = null;
        private DataDynamics.ActiveReports.GroupFooter grpOrderFooter = null;
        private DataDynamics.ActiveReports.TextBox txtJobFuelSurchargeAmount = null;
        private DataDynamics.ActiveReports.TextBox txtJobFuelSurchargeRate = null;
        private DataDynamics.ActiveReports.Label lblJobFuelSurcharge = null;
        private DataDynamics.ActiveReports.TextBox txtJobExtrasTotal = null;
        private DataDynamics.ActiveReports.Label lblJobExtraTotal = null;
        private DataDynamics.ActiveReports.SubReport sbJobExtras = null;
        private DataDynamics.ActiveReports.TextBox txtOrderIDFooter = null;
        private DataDynamics.ActiveReports.TextBox txtOrderGroupIDFooter = null;
        private DataDynamics.ActiveReports.PageFooter PageFooter = null;
        private DataDynamics.ActiveReports.SubReport srFooter = null;
        private DataDynamics.ActiveReports.ReportFooter ReportFooter = null;
        private DataDynamics.ActiveReports.Label lblTotalAmount = null;
        private DataDynamics.ActiveReports.Label lblVAT = null;
        private DataDynamics.ActiveReports.Label lblInvoiceTotal = null;
        private DataDynamics.ActiveReports.TextBox txtTotalAmount = null;
        private DataDynamics.ActiveReports.TextBox txtVATAmount = null;
        private DataDynamics.ActiveReports.TextBox txtVATPercentage = null;
        private DataDynamics.ActiveReports.TextBox txtInvoiceTotal = null;
        private DataDynamics.ActiveReports.Label lblInvoiceFuelCharge = null;
        private DataDynamics.ActiveReports.TextBox txtFuelChargeTotal = null;
        private DataDynamics.ActiveReports.TextBox txtInvoiceFuelSurchargeRate = null;
        private DataDynamics.ActiveReports.Label lblInvoiceTerms = null;
        private DataDynamics.ActiveReports.Line invoiceTotalTopLine = null;
        private DataDynamics.ActiveReports.Line invoiceTotalBaseLine = null;
        private DataDynamics.ActiveReports.Label lblExtraTotal = null;
        private DataDynamics.ActiveReports.TextBox txtExtraTotal = null;
        private DataDynamics.ActiveReports.SubReport sbExtras = null;
        private DataDynamics.ActiveReports.SubReport sbClientCustomerReportFooter = null;
        private DataDynamics.ActiveReports.Label lblSubTotal = null;
        private DataDynamics.ActiveReports.TextBox txtSubTotal = null;
        private DataDynamics.ActiveReports.Label lblInvoiceSubTotal = null;
        private DataDynamics.ActiveReports.TextBox txtInvoiceSubTotal = null;
        private DataDynamics.ActiveReports.Label lblTradingAs = null;
        private DataDynamics.ActiveReports.Label lblExtras = null;
        private DataDynamics.ActiveReports.TextBox txtExtrasTotal = null;
        private DataDynamics.ActiveReports.Line reportFooterLine = null;
        private DataDynamics.ActiveReports.Label lblFuelSurcharge = null;
        private DataDynamics.ActiveReports.TextBox txtFuelSurchargeRate = null;
        private DataDynamics.ActiveReports.TextBox txtFuelSurchareAmount = null;
        private DataDynamics.ActiveReports.Label lblExtrasHeading = null;
        public void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptOriginalGroupageInvoice));
            this.Detail = new DataDynamics.ActiveReports.Detail();
            this.ReportHeader = new DataDynamics.ActiveReports.ReportHeader();
            this.lblIsInvoice = new DataDynamics.ActiveReports.Label();
            this.srHeader = new DataDynamics.ActiveReports.SubReport();
            this.Label18 = new DataDynamics.ActiveReports.Label();
            this.Label19 = new DataDynamics.ActiveReports.Label();
            this.Label20 = new DataDynamics.ActiveReports.Label();
            this.txtInvoiceNo = new DataDynamics.ActiveReports.TextBox();
            this.txtInvoiceDate = new DataDynamics.ActiveReports.TextBox();
            this.txtAccountNo = new DataDynamics.ActiveReports.TextBox();
            this.txtCustomerName = new DataDynamics.ActiveReports.TextBox();
            this.txtAddress = new DataDynamics.ActiveReports.TextBox();
            this.lblIsPreInvoice = new DataDynamics.ActiveReports.Label();
            this.lblOurVatNo = new DataDynamics.ActiveReports.Label();
            this.txtOurVatNo = new DataDynamics.ActiveReports.TextBox();
            this.lblYourReference = new DataDynamics.ActiveReports.Label();
            this.txtYourReference = new DataDynamics.ActiveReports.TextBox();
            this.lblYourVatNo = new DataDynamics.ActiveReports.Label();
            this.txtYourVatNo = new DataDynamics.ActiveReports.TextBox();
            this.txtPurchaseOrderReference = new DataDynamics.ActiveReports.TextBox();
            this.lblPurchaseOrderReference = new DataDynamics.ActiveReports.Label();
            this.lblHelpsWithFormattingDontRemove = new DataDynamics.ActiveReports.Label();
            this.ReportFooter = new DataDynamics.ActiveReports.ReportFooter();
            this.lblTotalAmount = new DataDynamics.ActiveReports.Label();
            this.lblVAT = new DataDynamics.ActiveReports.Label();
            this.lblInvoiceTotal = new DataDynamics.ActiveReports.Label();
            this.txtTotalAmount = new DataDynamics.ActiveReports.TextBox();
            this.txtVATAmount = new DataDynamics.ActiveReports.TextBox();
            this.txtVATPercentage = new DataDynamics.ActiveReports.TextBox();
            this.txtInvoiceTotal = new DataDynamics.ActiveReports.TextBox();
            this.lblInvoiceFuelCharge = new DataDynamics.ActiveReports.Label();
            this.txtFuelChargeTotal = new DataDynamics.ActiveReports.TextBox();
            this.txtInvoiceFuelSurchargeRate = new DataDynamics.ActiveReports.TextBox();
            this.lblInvoiceTerms = new DataDynamics.ActiveReports.Label();
            this.invoiceTotalTopLine = new DataDynamics.ActiveReports.Line();
            this.invoiceTotalBaseLine = new DataDynamics.ActiveReports.Line();
            this.lblExtraTotal = new DataDynamics.ActiveReports.Label();
            this.txtExtraTotal = new DataDynamics.ActiveReports.TextBox();
            this.sbExtras = new DataDynamics.ActiveReports.SubReport();
            this.sbClientCustomerReportFooter = new DataDynamics.ActiveReports.SubReport();
            this.lblSubTotal = new DataDynamics.ActiveReports.Label();
            this.txtSubTotal = new DataDynamics.ActiveReports.TextBox();
            this.lblInvoiceSubTotal = new DataDynamics.ActiveReports.Label();
            this.txtInvoiceSubTotal = new DataDynamics.ActiveReports.TextBox();
            this.lblTradingAs = new DataDynamics.ActiveReports.Label();
            this.lblExtras = new DataDynamics.ActiveReports.Label();
            this.txtExtrasTotal = new DataDynamics.ActiveReports.TextBox();
            this.reportFooterLine = new DataDynamics.ActiveReports.Line();
            this.lblFuelSurcharge = new DataDynamics.ActiveReports.Label();
            this.txtFuelSurchargeRate = new DataDynamics.ActiveReports.TextBox();
            this.txtFuelSurchareAmount = new DataDynamics.ActiveReports.TextBox();
            this.lblExtrasHeading = new DataDynamics.ActiveReports.Label();
            this.txtCustomNotes = new DataDynamics.ActiveReports.RichTextBox();
            this.PageHeader = new DataDynamics.ActiveReports.PageHeader();
            this.srPageHeader = new DataDynamics.ActiveReports.SubReport();
            this.lblDate = new DataDynamics.ActiveReports.TextBox();
            this.lblOrderId = new DataDynamics.ActiveReports.TextBox();
            this.lblCollectionAddress = new DataDynamics.ActiveReports.TextBox();
            this.lblDeliveryAddress = new DataDynamics.ActiveReports.TextBox();
            this.lblPallets = new DataDynamics.ActiveReports.TextBox();
            this.lblWeight = new DataDynamics.ActiveReports.TextBox();
            this.lblPalletSpaces = new DataDynamics.ActiveReports.TextBox();
            this.lblRef = new DataDynamics.ActiveReports.TextBox();
            this.lblValue = new DataDynamics.ActiveReports.TextBox();
            this.lblPageHeaderInvoiceNo = new DataDynamics.ActiveReports.Label();
            this.txtPageHeaderInvoiceNo = new DataDynamics.ActiveReports.TextBox();
            this.lblCases = new DataDynamics.ActiveReports.TextBox();
            this.PageFooter = new DataDynamics.ActiveReports.PageFooter();
            this.srFooter = new DataDynamics.ActiveReports.SubReport();
            this.txtPalletSpaces = new DataDynamics.ActiveReports.TextBox();
            this.grpOrder = new DataDynamics.ActiveReports.GroupHeader();
            this.txtOrderGroupID = new DataDynamics.ActiveReports.TextBox();
            this.txtRate = new DataDynamics.ActiveReports.TextBox();
            this.txtCustomerRef = new DataDynamics.ActiveReports.TextBox();
            this.txtDeliveryOrderNumber = new DataDynamics.ActiveReports.TextBox();
            this.subReferences = new DataDynamics.ActiveReports.SubReport();
            this.txtOrderServiceLevel = new DataDynamics.ActiveReports.TextBox();
            this.txtWeight = new DataDynamics.ActiveReports.TextBox();
            this.txtPallets = new DataDynamics.ActiveReports.TextBox();
            this.txtDeliveryAddress = new DataDynamics.ActiveReports.TextBox();
            this.txtCollectionAddress = new DataDynamics.ActiveReports.TextBox();
            this.txtOrderID = new DataDynamics.ActiveReports.TextBox();
            this.txtDeliveryDate = new DataDynamics.ActiveReports.TextBox();
            this.txtForeignRate = new DataDynamics.ActiveReports.TextBox();
            this.lineOrderFooter = new DataDynamics.ActiveReports.Line();
            this.txtCases = new DataDynamics.ActiveReports.TextBox();
            this.lblVehicleRegNo = new DataDynamics.ActiveReports.TextBox();
            this.txtVehicleRegNo = new DataDynamics.ActiveReports.TextBox();
            this.grpOrderFooter = new DataDynamics.ActiveReports.GroupFooter();
            this.txtJobFuelSurchargeAmount = new DataDynamics.ActiveReports.TextBox();
            this.txtJobFuelSurchargeRate = new DataDynamics.ActiveReports.TextBox();
            this.lblJobFuelSurcharge = new DataDynamics.ActiveReports.Label();
            this.txtJobExtrasTotal = new DataDynamics.ActiveReports.TextBox();
            this.lblJobExtraTotal = new DataDynamics.ActiveReports.Label();
            this.sbJobExtras = new DataDynamics.ActiveReports.SubReport();
            this.txtOrderIDFooter = new DataDynamics.ActiveReports.TextBox();
            this.txtOrderGroupIDFooter = new DataDynamics.ActiveReports.TextBox();
            this.sbSurcharges = new DataDynamics.ActiveReports.SubReport();
            this.subGoodsRefused = new DataDynamics.ActiveReports.SubReport();
            this.groupHeader1 = new DataDynamics.ActiveReports.GroupHeader();
            this.GoodsTypelbl = new DataDynamics.ActiveReports.TextBox();
            this.GoodsTypeTxt = new DataDynamics.ActiveReports.TextBox();
            this.groupFooter1 = new DataDynamics.ActiveReports.GroupFooter();
            ((System.ComponentModel.ISupportInitialize)(this.lblIsInvoice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvoiceNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvoiceDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustomerName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblIsPreInvoice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblOurVatNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOurVatNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblYourReference)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYourReference)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblYourVatNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYourVatNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPurchaseOrderReference)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPurchaseOrderReference)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHelpsWithFormattingDontRemove)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotalAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVAT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInvoiceTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVATAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVATPercentage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvoiceTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInvoiceFuelCharge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFuelChargeTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvoiceFuelSurchargeRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInvoiceTerms)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblExtraTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExtraTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSubTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInvoiceSubTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvoiceSubTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTradingAs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblExtras)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExtrasTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFuelSurcharge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFuelSurchargeRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFuelSurchareAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblExtrasHeading)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblOrderId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCollectionAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDeliveryAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPallets)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblWeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPalletSpaces)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRef)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPageHeaderInvoiceNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageHeaderInvoiceNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCases)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPalletSpaces)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOrderGroupID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustomerRef)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDeliveryOrderNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOrderServiceLevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPallets)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDeliveryAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCollectionAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOrderID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDeliveryDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtForeignRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCases)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVehicleRegNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVehicleRegNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtJobFuelSurchargeAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtJobFuelSurchargeRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblJobFuelSurcharge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtJobExtrasTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblJobExtraTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOrderIDFooter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOrderGroupIDFooter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GoodsTypelbl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GoodsTypeTxt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Height = 0.04166667F;
            this.Detail.Name = "Detail";
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            // 
            // ReportHeader
            // 
            this.ReportHeader.CanShrink = true;
            this.ReportHeader.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.lblIsInvoice,
            this.srHeader,
            this.Label18,
            this.Label19,
            this.Label20,
            this.txtInvoiceNo,
            this.txtInvoiceDate,
            this.txtAccountNo,
            this.txtCustomerName,
            this.txtAddress,
            this.lblIsPreInvoice,
            this.lblOurVatNo,
            this.txtOurVatNo,
            this.lblYourReference,
            this.txtYourReference,
            this.lblYourVatNo,
            this.txtYourVatNo,
            this.txtPurchaseOrderReference,
            this.lblPurchaseOrderReference,
            this.lblHelpsWithFormattingDontRemove});
            this.ReportHeader.Height = 3.354167F;
            this.ReportHeader.Name = "ReportHeader";
            this.ReportHeader.Format += new System.EventHandler(this.ReportHeader_Format);
            // 
            // lblIsInvoice
            // 
            this.lblIsInvoice.Border.BottomColor = System.Drawing.Color.Black;
            this.lblIsInvoice.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblIsInvoice.Border.LeftColor = System.Drawing.Color.Black;
            this.lblIsInvoice.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblIsInvoice.Border.RightColor = System.Drawing.Color.Black;
            this.lblIsInvoice.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblIsInvoice.Border.TopColor = System.Drawing.Color.Black;
            this.lblIsInvoice.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblIsInvoice.Height = 0.2F;
            this.lblIsInvoice.HyperLink = null;
            this.lblIsInvoice.Left = 0.0625F;
            this.lblIsInvoice.Name = "lblIsInvoice";
            this.lblIsInvoice.Style = "ddo-char-set: 1; text-align: center; font-weight: bold; font-size: 14pt; ";
            this.lblIsInvoice.Text = "INVOICE";
            this.lblIsInvoice.Top = 3.0625F;
            this.lblIsInvoice.Width = 7.375F;
            // 
            // srHeader
            // 
            this.srHeader.Border.BottomColor = System.Drawing.Color.Black;
            this.srHeader.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.srHeader.Border.LeftColor = System.Drawing.Color.Black;
            this.srHeader.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.srHeader.Border.RightColor = System.Drawing.Color.Black;
            this.srHeader.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.srHeader.Border.TopColor = System.Drawing.Color.Black;
            this.srHeader.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.srHeader.CloseBorder = false;
            this.srHeader.Height = 1.8125F;
            this.srHeader.Left = 0F;
            this.srHeader.Name = "srHeader";
            this.srHeader.Report = null;
            this.srHeader.ReportName = "rptHeader";
            this.srHeader.Top = 0F;
            this.srHeader.Width = 7.5F;
            // 
            // Label18
            // 
            this.Label18.Border.BottomColor = System.Drawing.Color.Black;
            this.Label18.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label18.Border.LeftColor = System.Drawing.Color.Black;
            this.Label18.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label18.Border.RightColor = System.Drawing.Color.Black;
            this.Label18.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label18.Border.TopColor = System.Drawing.Color.Black;
            this.Label18.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label18.Height = 0.188F;
            this.Label18.HyperLink = null;
            this.Label18.Left = 4.875F;
            this.Label18.Name = "Label18";
            this.Label18.Style = "ddo-char-set: 1; font-weight: bold; ";
            this.Label18.Text = "Invoice No:";
            this.Label18.Top = 1.875F;
            this.Label18.Width = 0.875F;
            // 
            // Label19
            // 
            this.Label19.Border.BottomColor = System.Drawing.Color.Black;
            this.Label19.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label19.Border.LeftColor = System.Drawing.Color.Black;
            this.Label19.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label19.Border.RightColor = System.Drawing.Color.Black;
            this.Label19.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label19.Border.TopColor = System.Drawing.Color.Black;
            this.Label19.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label19.Height = 0.188F;
            this.Label19.HyperLink = null;
            this.Label19.Left = 4.875F;
            this.Label19.Name = "Label19";
            this.Label19.Style = "ddo-char-set: 1; font-weight: bold; ";
            this.Label19.Text = "Invoice Date:";
            this.Label19.Top = 2.0625F;
            this.Label19.Width = 0.875F;
            // 
            // Label20
            // 
            this.Label20.Border.BottomColor = System.Drawing.Color.Black;
            this.Label20.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label20.Border.LeftColor = System.Drawing.Color.Black;
            this.Label20.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label20.Border.RightColor = System.Drawing.Color.Black;
            this.Label20.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label20.Border.TopColor = System.Drawing.Color.Black;
            this.Label20.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label20.Height = 0.188F;
            this.Label20.HyperLink = null;
            this.Label20.Left = 4.875F;
            this.Label20.Name = "Label20";
            this.Label20.Style = "ddo-char-set: 1; font-weight: bold; ";
            this.Label20.Text = "Account No:";
            this.Label20.Top = 2.25F;
            this.Label20.Width = 0.875F;
            // 
            // txtInvoiceNo
            // 
            this.txtInvoiceNo.Border.BottomColor = System.Drawing.Color.Black;
            this.txtInvoiceNo.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtInvoiceNo.Border.LeftColor = System.Drawing.Color.Black;
            this.txtInvoiceNo.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtInvoiceNo.Border.RightColor = System.Drawing.Color.Black;
            this.txtInvoiceNo.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtInvoiceNo.Border.TopColor = System.Drawing.Color.Black;
            this.txtInvoiceNo.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtInvoiceNo.Height = 0.1875F;
            this.txtInvoiceNo.Left = 5.8125F;
            this.txtInvoiceNo.MultiLine = false;
            this.txtInvoiceNo.Name = "txtInvoiceNo";
            this.txtInvoiceNo.Style = "color: Black; ";
            this.txtInvoiceNo.Text = "To Be Issued";
            this.txtInvoiceNo.Top = 1.875F;
            this.txtInvoiceNo.Width = 1.625F;
            // 
            // txtInvoiceDate
            // 
            this.txtInvoiceDate.Border.BottomColor = System.Drawing.Color.Black;
            this.txtInvoiceDate.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtInvoiceDate.Border.LeftColor = System.Drawing.Color.Black;
            this.txtInvoiceDate.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtInvoiceDate.Border.RightColor = System.Drawing.Color.Black;
            this.txtInvoiceDate.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtInvoiceDate.Border.TopColor = System.Drawing.Color.Black;
            this.txtInvoiceDate.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtInvoiceDate.Height = 0.1875F;
            this.txtInvoiceDate.Left = 5.8125F;
            this.txtInvoiceDate.MultiLine = false;
            this.txtInvoiceDate.Name = "txtInvoiceDate";
            this.txtInvoiceDate.Style = "";
            this.txtInvoiceDate.Text = null;
            this.txtInvoiceDate.Top = 2.0625F;
            this.txtInvoiceDate.Width = 1.625F;
            // 
            // txtAccountNo
            // 
            this.txtAccountNo.Border.BottomColor = System.Drawing.Color.Black;
            this.txtAccountNo.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtAccountNo.Border.LeftColor = System.Drawing.Color.Black;
            this.txtAccountNo.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtAccountNo.Border.RightColor = System.Drawing.Color.Black;
            this.txtAccountNo.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtAccountNo.Border.TopColor = System.Drawing.Color.Black;
            this.txtAccountNo.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtAccountNo.Height = 0.1875F;
            this.txtAccountNo.Left = 5.8125F;
            this.txtAccountNo.MultiLine = false;
            this.txtAccountNo.Name = "txtAccountNo";
            this.txtAccountNo.Style = "vertical-align: top; ";
            this.txtAccountNo.Text = null;
            this.txtAccountNo.Top = 2.25F;
            this.txtAccountNo.Width = 1.625F;
            // 
            // txtCustomerName
            // 
            this.txtCustomerName.Border.BottomColor = System.Drawing.Color.Black;
            this.txtCustomerName.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtCustomerName.Border.LeftColor = System.Drawing.Color.Black;
            this.txtCustomerName.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtCustomerName.Border.RightColor = System.Drawing.Color.Black;
            this.txtCustomerName.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtCustomerName.Border.TopColor = System.Drawing.Color.Black;
            this.txtCustomerName.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtCustomerName.DataField = "OrganisationName";
            this.txtCustomerName.Height = 0.1875F;
            this.txtCustomerName.Left = 0.1875F;
            this.txtCustomerName.Name = "txtCustomerName";
            this.txtCustomerName.Style = "ddo-char-set: 1; font-weight: bold; ";
            this.txtCustomerName.Text = null;
            this.txtCustomerName.Top = 1.875F;
            this.txtCustomerName.Width = 3.4375F;
            // 
            // txtAddress
            // 
            this.txtAddress.Border.BottomColor = System.Drawing.Color.Black;
            this.txtAddress.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtAddress.Border.LeftColor = System.Drawing.Color.Black;
            this.txtAddress.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtAddress.Border.RightColor = System.Drawing.Color.Black;
            this.txtAddress.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtAddress.Border.TopColor = System.Drawing.Color.Black;
            this.txtAddress.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtAddress.Height = 0.9375F;
            this.txtAddress.Left = 0.1875F;
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Style = "ddo-char-set: 1; font-weight: bold; ";
            this.txtAddress.Text = null;
            this.txtAddress.Top = 2.0625F;
            this.txtAddress.Width = 2.1875F;
            // 
            // lblIsPreInvoice
            // 
            this.lblIsPreInvoice.Border.BottomColor = System.Drawing.Color.Black;
            this.lblIsPreInvoice.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblIsPreInvoice.Border.LeftColor = System.Drawing.Color.Black;
            this.lblIsPreInvoice.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblIsPreInvoice.Border.RightColor = System.Drawing.Color.Black;
            this.lblIsPreInvoice.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblIsPreInvoice.Border.TopColor = System.Drawing.Color.Black;
            this.lblIsPreInvoice.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblIsPreInvoice.Height = 0.25F;
            this.lblIsPreInvoice.HyperLink = null;
            this.lblIsPreInvoice.Left = 0.0625F;
            this.lblIsPreInvoice.Name = "lblIsPreInvoice";
            this.lblIsPreInvoice.Style = "color: Red; text-align: center; ";
            this.lblIsPreInvoice.Text = "This invoice has not yet been saved, add invoice to allocate Invoice No.";
            this.lblIsPreInvoice.Top = 3.0625F;
            this.lblIsPreInvoice.Width = 4.5F;
            // 
            // lblOurVatNo
            // 
            this.lblOurVatNo.Border.BottomColor = System.Drawing.Color.Black;
            this.lblOurVatNo.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblOurVatNo.Border.LeftColor = System.Drawing.Color.Black;
            this.lblOurVatNo.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblOurVatNo.Border.RightColor = System.Drawing.Color.Black;
            this.lblOurVatNo.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblOurVatNo.Border.TopColor = System.Drawing.Color.Black;
            this.lblOurVatNo.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblOurVatNo.Height = 0.188F;
            this.lblOurVatNo.HyperLink = null;
            this.lblOurVatNo.Left = 4.875F;
            this.lblOurVatNo.Name = "lblOurVatNo";
            this.lblOurVatNo.Style = "ddo-char-set: 1; font-weight: bold; ";
            this.lblOurVatNo.Text = "Our VAT No:";
            this.lblOurVatNo.Top = 2.8125F;
            this.lblOurVatNo.Width = 0.875F;
            // 
            // txtOurVatNo
            // 
            this.txtOurVatNo.Border.BottomColor = System.Drawing.Color.Black;
            this.txtOurVatNo.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtOurVatNo.Border.LeftColor = System.Drawing.Color.Black;
            this.txtOurVatNo.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtOurVatNo.Border.RightColor = System.Drawing.Color.Black;
            this.txtOurVatNo.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtOurVatNo.Border.TopColor = System.Drawing.Color.Black;
            this.txtOurVatNo.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtOurVatNo.Height = 0.1875F;
            this.txtOurVatNo.Left = 5.8125F;
            this.txtOurVatNo.MultiLine = false;
            this.txtOurVatNo.Name = "txtOurVatNo";
            this.txtOurVatNo.Style = "vertical-align: top; ";
            this.txtOurVatNo.Text = null;
            this.txtOurVatNo.Top = 2.8125F;
            this.txtOurVatNo.Width = 1.625F;
            // 
            // lblYourReference
            // 
            this.lblYourReference.Border.BottomColor = System.Drawing.Color.Black;
            this.lblYourReference.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblYourReference.Border.LeftColor = System.Drawing.Color.Black;
            this.lblYourReference.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblYourReference.Border.RightColor = System.Drawing.Color.Black;
            this.lblYourReference.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblYourReference.Border.TopColor = System.Drawing.Color.Black;
            this.lblYourReference.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblYourReference.Height = 0.188F;
            this.lblYourReference.HyperLink = null;
            this.lblYourReference.Left = 4.875F;
            this.lblYourReference.Name = "lblYourReference";
            this.lblYourReference.Style = "ddo-char-set: 1; font-weight: bold; ";
            this.lblYourReference.Text = "Self Bill Ref:";
            this.lblYourReference.Top = 2.4375F;
            this.lblYourReference.Width = 0.875F;
            // 
            // txtYourReference
            // 
            this.txtYourReference.Border.BottomColor = System.Drawing.Color.Black;
            this.txtYourReference.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtYourReference.Border.LeftColor = System.Drawing.Color.Black;
            this.txtYourReference.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtYourReference.Border.RightColor = System.Drawing.Color.Black;
            this.txtYourReference.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtYourReference.Border.TopColor = System.Drawing.Color.Black;
            this.txtYourReference.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtYourReference.Height = 0.1875F;
            this.txtYourReference.Left = 5.8125F;
            this.txtYourReference.MultiLine = false;
            this.txtYourReference.Name = "txtYourReference";
            this.txtYourReference.Style = "vertical-align: top; ";
            this.txtYourReference.Text = null;
            this.txtYourReference.Top = 2.4375F;
            this.txtYourReference.Width = 1.625F;
            // 
            // lblYourVatNo
            // 
            this.lblYourVatNo.Border.BottomColor = System.Drawing.Color.Black;
            this.lblYourVatNo.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblYourVatNo.Border.LeftColor = System.Drawing.Color.Black;
            this.lblYourVatNo.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblYourVatNo.Border.RightColor = System.Drawing.Color.Black;
            this.lblYourVatNo.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblYourVatNo.Border.TopColor = System.Drawing.Color.Black;
            this.lblYourVatNo.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblYourVatNo.Height = 0.188F;
            this.lblYourVatNo.HyperLink = null;
            this.lblYourVatNo.Left = 4.875F;
            this.lblYourVatNo.Name = "lblYourVatNo";
            this.lblYourVatNo.Style = "ddo-char-set: 1; font-weight: bold; ";
            this.lblYourVatNo.Text = "Your VAT No:";
            this.lblYourVatNo.Top = 3F;
            this.lblYourVatNo.Width = 0.875F;
            // 
            // txtYourVatNo
            // 
            this.txtYourVatNo.Border.BottomColor = System.Drawing.Color.Black;
            this.txtYourVatNo.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtYourVatNo.Border.LeftColor = System.Drawing.Color.Black;
            this.txtYourVatNo.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtYourVatNo.Border.RightColor = System.Drawing.Color.Black;
            this.txtYourVatNo.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtYourVatNo.Border.TopColor = System.Drawing.Color.Black;
            this.txtYourVatNo.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtYourVatNo.Height = 0.1875F;
            this.txtYourVatNo.Left = 5.8125F;
            this.txtYourVatNo.MultiLine = false;
            this.txtYourVatNo.Name = "txtYourVatNo";
            this.txtYourVatNo.Style = "vertical-align: top; ";
            this.txtYourVatNo.Text = null;
            this.txtYourVatNo.Top = 3F;
            this.txtYourVatNo.Width = 1.625F;
            // 
            // txtPurchaseOrderReference
            // 
            this.txtPurchaseOrderReference.Border.BottomColor = System.Drawing.Color.Black;
            this.txtPurchaseOrderReference.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtPurchaseOrderReference.Border.LeftColor = System.Drawing.Color.Black;
            this.txtPurchaseOrderReference.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtPurchaseOrderReference.Border.RightColor = System.Drawing.Color.Black;
            this.txtPurchaseOrderReference.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtPurchaseOrderReference.Border.TopColor = System.Drawing.Color.Black;
            this.txtPurchaseOrderReference.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtPurchaseOrderReference.Height = 0.1875F;
            this.txtPurchaseOrderReference.Left = 5.8125F;
            this.txtPurchaseOrderReference.MultiLine = false;
            this.txtPurchaseOrderReference.Name = "txtPurchaseOrderReference";
            this.txtPurchaseOrderReference.Style = "vertical-align: top; ";
            this.txtPurchaseOrderReference.Text = null;
            this.txtPurchaseOrderReference.Top = 2.625F;
            this.txtPurchaseOrderReference.Width = 1.625F;
            // 
            // lblPurchaseOrderReference
            // 
            this.lblPurchaseOrderReference.Border.BottomColor = System.Drawing.Color.Black;
            this.lblPurchaseOrderReference.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblPurchaseOrderReference.Border.LeftColor = System.Drawing.Color.Black;
            this.lblPurchaseOrderReference.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblPurchaseOrderReference.Border.RightColor = System.Drawing.Color.Black;
            this.lblPurchaseOrderReference.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblPurchaseOrderReference.Border.TopColor = System.Drawing.Color.Black;
            this.lblPurchaseOrderReference.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblPurchaseOrderReference.Height = 0.1875F;
            this.lblPurchaseOrderReference.HyperLink = null;
            this.lblPurchaseOrderReference.Left = 4.875F;
            this.lblPurchaseOrderReference.Name = "lblPurchaseOrderReference";
            this.lblPurchaseOrderReference.Style = "ddo-char-set: 1; font-weight: bold; ";
            this.lblPurchaseOrderReference.Text = "P/Order No:";
            this.lblPurchaseOrderReference.Top = 2.625F;
            this.lblPurchaseOrderReference.Width = 0.875F;
            // 
            // lblHelpsWithFormattingDontRemove
            // 
            this.lblHelpsWithFormattingDontRemove.Border.BottomColor = System.Drawing.Color.Black;
            this.lblHelpsWithFormattingDontRemove.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblHelpsWithFormattingDontRemove.Border.LeftColor = System.Drawing.Color.Black;
            this.lblHelpsWithFormattingDontRemove.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblHelpsWithFormattingDontRemove.Border.RightColor = System.Drawing.Color.Black;
            this.lblHelpsWithFormattingDontRemove.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblHelpsWithFormattingDontRemove.Border.TopColor = System.Drawing.Color.Black;
            this.lblHelpsWithFormattingDontRemove.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblHelpsWithFormattingDontRemove.Height = 0.1979167F;
            this.lblHelpsWithFormattingDontRemove.HyperLink = null;
            this.lblHelpsWithFormattingDontRemove.Left = 2.75F;
            this.lblHelpsWithFormattingDontRemove.Name = "lblHelpsWithFormattingDontRemove";
            this.lblHelpsWithFormattingDontRemove.Style = "";
            this.lblHelpsWithFormattingDontRemove.Text = "";
            this.lblHelpsWithFormattingDontRemove.Top = 2.8125F;
            this.lblHelpsWithFormattingDontRemove.Width = 1F;
            // 
            // ReportFooter
            // 
            this.ReportFooter.CanShrink = true;
            this.ReportFooter.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.lblTotalAmount,
            this.lblVAT,
            this.lblInvoiceTotal,
            this.txtTotalAmount,
            this.txtVATAmount,
            this.txtVATPercentage,
            this.txtInvoiceTotal,
            this.lblInvoiceFuelCharge,
            this.txtFuelChargeTotal,
            this.txtInvoiceFuelSurchargeRate,
            this.lblInvoiceTerms,
            this.invoiceTotalTopLine,
            this.invoiceTotalBaseLine,
            this.lblExtraTotal,
            this.txtExtraTotal,
            this.sbExtras,
            this.sbClientCustomerReportFooter,
            this.lblSubTotal,
            this.txtSubTotal,
            this.lblInvoiceSubTotal,
            this.txtInvoiceSubTotal,
            this.lblTradingAs,
            this.lblExtras,
            this.txtExtrasTotal,
            this.reportFooterLine,
            this.lblFuelSurcharge,
            this.txtFuelSurchargeRate,
            this.txtFuelSurchareAmount,
            this.lblExtrasHeading,
            this.txtCustomNotes});
            this.ReportFooter.Height = 3.947917F;
            this.ReportFooter.KeepTogether = true;
            this.ReportFooter.Name = "ReportFooter";
            this.ReportFooter.PrintAtBottom = true;
            this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
            this.ReportFooter.BeforePrint += new System.EventHandler(this.ReportFooter_BeforePrint);
            // 
            // lblTotalAmount
            // 
            this.lblTotalAmount.Border.BottomColor = System.Drawing.Color.Black;
            this.lblTotalAmount.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblTotalAmount.Border.LeftColor = System.Drawing.Color.Black;
            this.lblTotalAmount.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblTotalAmount.Border.RightColor = System.Drawing.Color.Black;
            this.lblTotalAmount.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblTotalAmount.Border.TopColor = System.Drawing.Color.Black;
            this.lblTotalAmount.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblTotalAmount.Height = 0.1875F;
            this.lblTotalAmount.HyperLink = null;
            this.lblTotalAmount.Left = 4F;
            this.lblTotalAmount.Name = "lblTotalAmount";
            this.lblTotalAmount.Style = "ddo-char-set: 1; font-weight: bold; ";
            this.lblTotalAmount.Text = "TOTAL AMOUNT";
            this.lblTotalAmount.Top = 1.375F;
            this.lblTotalAmount.Width = 1.5625F;
            // 
            // lblVAT
            // 
            this.lblVAT.Border.BottomColor = System.Drawing.Color.Black;
            this.lblVAT.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblVAT.Border.LeftColor = System.Drawing.Color.Black;
            this.lblVAT.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblVAT.Border.RightColor = System.Drawing.Color.Black;
            this.lblVAT.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblVAT.Border.TopColor = System.Drawing.Color.Black;
            this.lblVAT.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblVAT.Height = 0.2F;
            this.lblVAT.HyperLink = null;
            this.lblVAT.Left = 4F;
            this.lblVAT.Name = "lblVAT";
            this.lblVAT.Style = "ddo-char-set: 1; font-weight: bold; ";
            this.lblVAT.Text = "VAT @";
            this.lblVAT.Top = 1.5625F;
            this.lblVAT.Width = 0.5625F;
            // 
            // lblInvoiceTotal
            // 
            this.lblInvoiceTotal.Border.BottomColor = System.Drawing.Color.Black;
            this.lblInvoiceTotal.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblInvoiceTotal.Border.LeftColor = System.Drawing.Color.Black;
            this.lblInvoiceTotal.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblInvoiceTotal.Border.RightColor = System.Drawing.Color.Black;
            this.lblInvoiceTotal.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblInvoiceTotal.Border.TopColor = System.Drawing.Color.Black;
            this.lblInvoiceTotal.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblInvoiceTotal.Height = 0.2F;
            this.lblInvoiceTotal.HyperLink = null;
            this.lblInvoiceTotal.Left = 4.012153F;
            this.lblInvoiceTotal.Name = "lblInvoiceTotal";
            this.lblInvoiceTotal.Style = "ddo-char-set: 1; font-weight: bold; ";
            this.lblInvoiceTotal.Text = "INVOICE TOTAL";
            this.lblInvoiceTotal.Top = 1.875F;
            this.lblInvoiceTotal.Width = 1.25F;
            // 
            // txtTotalAmount
            // 
            this.txtTotalAmount.Border.BottomColor = System.Drawing.Color.Black;
            this.txtTotalAmount.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtTotalAmount.Border.LeftColor = System.Drawing.Color.Black;
            this.txtTotalAmount.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtTotalAmount.Border.RightColor = System.Drawing.Color.Black;
            this.txtTotalAmount.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtTotalAmount.Border.TopColor = System.Drawing.Color.Black;
            this.txtTotalAmount.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtTotalAmount.Height = 0.1875F;
            this.txtTotalAmount.Left = 6.75F;
            this.txtTotalAmount.Name = "txtTotalAmount";
            this.txtTotalAmount.OutputFormat = resources.GetString("txtTotalAmount.OutputFormat");
            this.txtTotalAmount.Style = "text-align: right; ";
            this.txtTotalAmount.Text = null;
            this.txtTotalAmount.Top = 1.375F;
            this.txtTotalAmount.Width = 0.75F;
            // 
            // txtVATAmount
            // 
            this.txtVATAmount.Border.BottomColor = System.Drawing.Color.Black;
            this.txtVATAmount.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtVATAmount.Border.LeftColor = System.Drawing.Color.Black;
            this.txtVATAmount.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtVATAmount.Border.RightColor = System.Drawing.Color.Black;
            this.txtVATAmount.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtVATAmount.Border.TopColor = System.Drawing.Color.Black;
            this.txtVATAmount.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtVATAmount.Height = 0.2F;
            this.txtVATAmount.Left = 6.75F;
            this.txtVATAmount.Name = "txtVATAmount";
            this.txtVATAmount.OutputFormat = resources.GetString("txtVATAmount.OutputFormat");
            this.txtVATAmount.Style = "text-align: right; ";
            this.txtVATAmount.Text = null;
            this.txtVATAmount.Top = 1.5625F;
            this.txtVATAmount.Width = 0.75F;
            // 
            // txtVATPercentage
            // 
            this.txtVATPercentage.Border.BottomColor = System.Drawing.Color.Black;
            this.txtVATPercentage.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtVATPercentage.Border.LeftColor = System.Drawing.Color.Black;
            this.txtVATPercentage.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtVATPercentage.Border.RightColor = System.Drawing.Color.Black;
            this.txtVATPercentage.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtVATPercentage.Border.TopColor = System.Drawing.Color.Black;
            this.txtVATPercentage.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtVATPercentage.Height = 0.1875F;
            this.txtVATPercentage.Left = 4.5625F;
            this.txtVATPercentage.Name = "txtVATPercentage";
            this.txtVATPercentage.OutputFormat = resources.GetString("txtVATPercentage.OutputFormat");
            this.txtVATPercentage.Style = "ddo-char-set: 1; font-weight: bold; ";
            this.txtVATPercentage.Text = null;
            this.txtVATPercentage.Top = 1.5625F;
            this.txtVATPercentage.Width = 1.9375F;
            // 
            // txtInvoiceTotal
            // 
            this.txtInvoiceTotal.Border.BottomColor = System.Drawing.Color.Black;
            this.txtInvoiceTotal.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtInvoiceTotal.Border.LeftColor = System.Drawing.Color.Black;
            this.txtInvoiceTotal.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtInvoiceTotal.Border.RightColor = System.Drawing.Color.Black;
            this.txtInvoiceTotal.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtInvoiceTotal.Border.TopColor = System.Drawing.Color.Black;
            this.txtInvoiceTotal.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtInvoiceTotal.Height = 0.2F;
            this.txtInvoiceTotal.Left = 6.75F;
            this.txtInvoiceTotal.Name = "txtInvoiceTotal";
            this.txtInvoiceTotal.OutputFormat = resources.GetString("txtInvoiceTotal.OutputFormat");
            this.txtInvoiceTotal.Style = "text-align: right; ";
            this.txtInvoiceTotal.Text = null;
            this.txtInvoiceTotal.Top = 1.875F;
            this.txtInvoiceTotal.Width = 0.7621534F;
            // 
            // lblInvoiceFuelCharge
            // 
            this.lblInvoiceFuelCharge.Border.BottomColor = System.Drawing.Color.Black;
            this.lblInvoiceFuelCharge.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblInvoiceFuelCharge.Border.LeftColor = System.Drawing.Color.Black;
            this.lblInvoiceFuelCharge.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblInvoiceFuelCharge.Border.RightColor = System.Drawing.Color.Black;
            this.lblInvoiceFuelCharge.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblInvoiceFuelCharge.Border.TopColor = System.Drawing.Color.Black;
            this.lblInvoiceFuelCharge.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblInvoiceFuelCharge.Height = 0.1875F;
            this.lblInvoiceFuelCharge.HyperLink = null;
            this.lblInvoiceFuelCharge.Left = 4F;
            this.lblInvoiceFuelCharge.Name = "lblInvoiceFuelCharge";
            this.lblInvoiceFuelCharge.Style = "ddo-char-set: 1; font-weight: bold; ";
            this.lblInvoiceFuelCharge.Text = "FUEL SURCHARGE @";
            this.lblInvoiceFuelCharge.Top = 1.1875F;
            this.lblInvoiceFuelCharge.Width = 1.5625F;
            // 
            // txtFuelChargeTotal
            // 
            this.txtFuelChargeTotal.Border.BottomColor = System.Drawing.Color.Black;
            this.txtFuelChargeTotal.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtFuelChargeTotal.Border.LeftColor = System.Drawing.Color.Black;
            this.txtFuelChargeTotal.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtFuelChargeTotal.Border.RightColor = System.Drawing.Color.Black;
            this.txtFuelChargeTotal.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtFuelChargeTotal.Border.TopColor = System.Drawing.Color.Black;
            this.txtFuelChargeTotal.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtFuelChargeTotal.Height = 0.1875F;
            this.txtFuelChargeTotal.Left = 6.75F;
            this.txtFuelChargeTotal.Name = "txtFuelChargeTotal";
            this.txtFuelChargeTotal.OutputFormat = resources.GetString("txtFuelChargeTotal.OutputFormat");
            this.txtFuelChargeTotal.Style = "text-align: right; ";
            this.txtFuelChargeTotal.Text = null;
            this.txtFuelChargeTotal.Top = 1.1875F;
            this.txtFuelChargeTotal.Width = 0.75F;
            // 
            // txtInvoiceFuelSurchargeRate
            // 
            this.txtInvoiceFuelSurchargeRate.Border.BottomColor = System.Drawing.Color.Black;
            this.txtInvoiceFuelSurchargeRate.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtInvoiceFuelSurchargeRate.Border.LeftColor = System.Drawing.Color.Black;
            this.txtInvoiceFuelSurchargeRate.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtInvoiceFuelSurchargeRate.Border.RightColor = System.Drawing.Color.Black;
            this.txtInvoiceFuelSurchargeRate.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtInvoiceFuelSurchargeRate.Border.TopColor = System.Drawing.Color.Black;
            this.txtInvoiceFuelSurchargeRate.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtInvoiceFuelSurchargeRate.Height = 0.2F;
            this.txtInvoiceFuelSurchargeRate.Left = 5.5625F;
            this.txtInvoiceFuelSurchargeRate.Name = "txtInvoiceFuelSurchargeRate";
            this.txtInvoiceFuelSurchargeRate.OutputFormat = resources.GetString("txtInvoiceFuelSurchargeRate.OutputFormat");
            this.txtInvoiceFuelSurchargeRate.Style = "text-align: left; ";
            this.txtInvoiceFuelSurchargeRate.Text = null;
            this.txtInvoiceFuelSurchargeRate.Top = 1.1875F;
            this.txtInvoiceFuelSurchargeRate.Width = 0.9322917F;
            // 
            // lblInvoiceTerms
            // 
            this.lblInvoiceTerms.Border.BottomColor = System.Drawing.Color.Black;
            this.lblInvoiceTerms.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblInvoiceTerms.Border.LeftColor = System.Drawing.Color.Black;
            this.lblInvoiceTerms.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblInvoiceTerms.Border.RightColor = System.Drawing.Color.Black;
            this.lblInvoiceTerms.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblInvoiceTerms.Border.TopColor = System.Drawing.Color.Black;
            this.lblInvoiceTerms.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblInvoiceTerms.Height = 0.2F;
            this.lblInvoiceTerms.HyperLink = null;
            this.lblInvoiceTerms.Left = 0.0625F;
            this.lblInvoiceTerms.Name = "lblInvoiceTerms";
            this.lblInvoiceTerms.Style = "color: DimGray; ddo-char-set: 1; text-align: center; font-weight: bold; font-styl" +
                "e: normal; font-size: 8.25pt; ";
            this.lblInvoiceTerms.Text = "Strictly 30 Days Nett.";
            this.lblInvoiceTerms.Top = 2.0625F;
            this.lblInvoiceTerms.Width = 7.4375F;
            // 
            // invoiceTotalTopLine
            // 
            this.invoiceTotalTopLine.Border.BottomColor = System.Drawing.Color.Black;
            this.invoiceTotalTopLine.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.invoiceTotalTopLine.Border.LeftColor = System.Drawing.Color.Black;
            this.invoiceTotalTopLine.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.invoiceTotalTopLine.Border.RightColor = System.Drawing.Color.Black;
            this.invoiceTotalTopLine.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.invoiceTotalTopLine.Border.TopColor = System.Drawing.Color.Black;
            this.invoiceTotalTopLine.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.invoiceTotalTopLine.Height = 0F;
            this.invoiceTotalTopLine.Left = 4.027778F;
            this.invoiceTotalTopLine.LineWeight = 1F;
            this.invoiceTotalTopLine.Name = "invoiceTotalTopLine";
            this.invoiceTotalTopLine.Top = 1.875F;
            this.invoiceTotalTopLine.Width = 3.472222F;
            this.invoiceTotalTopLine.X1 = 4.027778F;
            this.invoiceTotalTopLine.X2 = 7.5F;
            this.invoiceTotalTopLine.Y1 = 1.875F;
            this.invoiceTotalTopLine.Y2 = 1.875F;
            // 
            // invoiceTotalBaseLine
            // 
            this.invoiceTotalBaseLine.Border.BottomColor = System.Drawing.Color.Black;
            this.invoiceTotalBaseLine.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.invoiceTotalBaseLine.Border.LeftColor = System.Drawing.Color.Black;
            this.invoiceTotalBaseLine.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.invoiceTotalBaseLine.Border.RightColor = System.Drawing.Color.Black;
            this.invoiceTotalBaseLine.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.invoiceTotalBaseLine.Border.TopColor = System.Drawing.Color.Black;
            this.invoiceTotalBaseLine.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.invoiceTotalBaseLine.Height = 0F;
            this.invoiceTotalBaseLine.Left = 4.028F;
            this.invoiceTotalBaseLine.LineWeight = 1F;
            this.invoiceTotalBaseLine.Name = "invoiceTotalBaseLine";
            this.invoiceTotalBaseLine.Top = 2.078125F;
            this.invoiceTotalBaseLine.Width = 3.472F;
            this.invoiceTotalBaseLine.X1 = 4.028F;
            this.invoiceTotalBaseLine.X2 = 7.5F;
            this.invoiceTotalBaseLine.Y1 = 2.078125F;
            this.invoiceTotalBaseLine.Y2 = 2.078125F;
            // 
            // lblExtraTotal
            // 
            this.lblExtraTotal.Border.BottomColor = System.Drawing.Color.Black;
            this.lblExtraTotal.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblExtraTotal.Border.LeftColor = System.Drawing.Color.Black;
            this.lblExtraTotal.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblExtraTotal.Border.RightColor = System.Drawing.Color.Black;
            this.lblExtraTotal.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblExtraTotal.Border.TopColor = System.Drawing.Color.Black;
            this.lblExtraTotal.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblExtraTotal.Height = 0.1875F;
            this.lblExtraTotal.HyperLink = null;
            this.lblExtraTotal.Left = 4F;
            this.lblExtraTotal.Name = "lblExtraTotal";
            this.lblExtraTotal.Style = "font-weight: bold; ";
            this.lblExtraTotal.Text = "EXTRA TOTAL";
            this.lblExtraTotal.Top = 0.8125F;
            this.lblExtraTotal.Width = 1.5625F;
            // 
            // txtExtraTotal
            // 
            this.txtExtraTotal.Border.BottomColor = System.Drawing.Color.Black;
            this.txtExtraTotal.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtExtraTotal.Border.LeftColor = System.Drawing.Color.Black;
            this.txtExtraTotal.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtExtraTotal.Border.RightColor = System.Drawing.Color.Black;
            this.txtExtraTotal.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtExtraTotal.Border.TopColor = System.Drawing.Color.Black;
            this.txtExtraTotal.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtExtraTotal.Height = 0.1875F;
            this.txtExtraTotal.Left = 6.75F;
            this.txtExtraTotal.Name = "txtExtraTotal";
            this.txtExtraTotal.Style = "text-align: right; ";
            this.txtExtraTotal.Text = null;
            this.txtExtraTotal.Top = 0.8125F;
            this.txtExtraTotal.Width = 0.75F;
            // 
            // sbExtras
            // 
            this.sbExtras.Border.BottomColor = System.Drawing.Color.Black;
            this.sbExtras.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.sbExtras.Border.LeftColor = System.Drawing.Color.Black;
            this.sbExtras.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.sbExtras.Border.RightColor = System.Drawing.Color.Black;
            this.sbExtras.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.sbExtras.Border.TopColor = System.Drawing.Color.Black;
            this.sbExtras.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.sbExtras.CloseBorder = false;
            this.sbExtras.Height = 0.063F;
            this.sbExtras.Left = 0.25F;
            this.sbExtras.Name = "sbExtras";
            this.sbExtras.Report = null;
            this.sbExtras.Top = 0.1875F;
            this.sbExtras.Visible = false;
            this.sbExtras.Width = 7.25F;
            // 
            // sbClientCustomerReportFooter
            // 
            this.sbClientCustomerReportFooter.Border.BottomColor = System.Drawing.Color.Black;
            this.sbClientCustomerReportFooter.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.sbClientCustomerReportFooter.Border.LeftColor = System.Drawing.Color.Black;
            this.sbClientCustomerReportFooter.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.sbClientCustomerReportFooter.Border.RightColor = System.Drawing.Color.Black;
            this.sbClientCustomerReportFooter.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.sbClientCustomerReportFooter.Border.TopColor = System.Drawing.Color.Black;
            this.sbClientCustomerReportFooter.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.sbClientCustomerReportFooter.CloseBorder = false;
            this.sbClientCustomerReportFooter.Height = 1.3125F;
            this.sbClientCustomerReportFooter.Left = 0F;
            this.sbClientCustomerReportFooter.Name = "sbClientCustomerReportFooter";
            this.sbClientCustomerReportFooter.Report = null;
            this.sbClientCustomerReportFooter.Top = 2.5F;
            this.sbClientCustomerReportFooter.Visible = false;
            this.sbClientCustomerReportFooter.Width = 7.5625F;
            // 
            // lblSubTotal
            // 
            this.lblSubTotal.Border.BottomColor = System.Drawing.Color.Black;
            this.lblSubTotal.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblSubTotal.Border.LeftColor = System.Drawing.Color.Black;
            this.lblSubTotal.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblSubTotal.Border.RightColor = System.Drawing.Color.Black;
            this.lblSubTotal.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblSubTotal.Border.TopColor = System.Drawing.Color.Black;
            this.lblSubTotal.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblSubTotal.Height = 0.1875F;
            this.lblSubTotal.HyperLink = null;
            this.lblSubTotal.Left = 4F;
            this.lblSubTotal.Name = "lblSubTotal";
            this.lblSubTotal.Style = "font-weight: bold; ";
            this.lblSubTotal.Text = "SUB TOTAL";
            this.lblSubTotal.Top = 0.625F;
            this.lblSubTotal.Width = 1.5625F;
            // 
            // txtSubTotal
            // 
            this.txtSubTotal.Border.BottomColor = System.Drawing.Color.Black;
            this.txtSubTotal.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtSubTotal.Border.LeftColor = System.Drawing.Color.Black;
            this.txtSubTotal.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtSubTotal.Border.RightColor = System.Drawing.Color.Black;
            this.txtSubTotal.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtSubTotal.Border.TopColor = System.Drawing.Color.Black;
            this.txtSubTotal.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtSubTotal.Height = 0.1875F;
            this.txtSubTotal.Left = 6.75F;
            this.txtSubTotal.Name = "txtSubTotal";
            this.txtSubTotal.Style = "text-align: right; ";
            this.txtSubTotal.Text = null;
            this.txtSubTotal.Top = 0.625F;
            this.txtSubTotal.Width = 0.75F;
            // 
            // lblInvoiceSubTotal
            // 
            this.lblInvoiceSubTotal.Border.BottomColor = System.Drawing.Color.Black;
            this.lblInvoiceSubTotal.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblInvoiceSubTotal.Border.LeftColor = System.Drawing.Color.Black;
            this.lblInvoiceSubTotal.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblInvoiceSubTotal.Border.RightColor = System.Drawing.Color.Black;
            this.lblInvoiceSubTotal.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblInvoiceSubTotal.Border.TopColor = System.Drawing.Color.Black;
            this.lblInvoiceSubTotal.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblInvoiceSubTotal.Height = 0.1875F;
            this.lblInvoiceSubTotal.HyperLink = null;
            this.lblInvoiceSubTotal.Left = 4F;
            this.lblInvoiceSubTotal.Name = "lblInvoiceSubTotal";
            this.lblInvoiceSubTotal.Style = "font-weight: bold; ";
            this.lblInvoiceSubTotal.Text = "SUB TOTAL";
            this.lblInvoiceSubTotal.Top = 1F;
            this.lblInvoiceSubTotal.Width = 1.5625F;
            // 
            // txtInvoiceSubTotal
            // 
            this.txtInvoiceSubTotal.Border.BottomColor = System.Drawing.Color.Black;
            this.txtInvoiceSubTotal.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtInvoiceSubTotal.Border.LeftColor = System.Drawing.Color.Black;
            this.txtInvoiceSubTotal.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtInvoiceSubTotal.Border.RightColor = System.Drawing.Color.Black;
            this.txtInvoiceSubTotal.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtInvoiceSubTotal.Border.TopColor = System.Drawing.Color.Black;
            this.txtInvoiceSubTotal.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtInvoiceSubTotal.Height = 0.1875F;
            this.txtInvoiceSubTotal.Left = 6.75F;
            this.txtInvoiceSubTotal.Name = "txtInvoiceSubTotal";
            this.txtInvoiceSubTotal.Style = "text-align: right; ";
            this.txtInvoiceSubTotal.Text = null;
            this.txtInvoiceSubTotal.Top = 1F;
            this.txtInvoiceSubTotal.Width = 0.75F;
            // 
            // lblTradingAs
            // 
            this.lblTradingAs.Border.BottomColor = System.Drawing.Color.Black;
            this.lblTradingAs.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblTradingAs.Border.LeftColor = System.Drawing.Color.Black;
            this.lblTradingAs.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblTradingAs.Border.RightColor = System.Drawing.Color.Black;
            this.lblTradingAs.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblTradingAs.Border.TopColor = System.Drawing.Color.Black;
            this.lblTradingAs.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblTradingAs.Height = 0.1875F;
            this.lblTradingAs.HyperLink = null;
            this.lblTradingAs.Left = 0.0625F;
            this.lblTradingAs.Name = "lblTradingAs";
            this.lblTradingAs.Style = "color: DimGray; ddo-char-set: 0; text-align: center; font-weight: bold; font-styl" +
                "e: italic; font-size: 8.25pt; ";
            this.lblTradingAs.Text = "";
            this.lblTradingAs.Top = 2.25F;
            this.lblTradingAs.Width = 7.375F;
            // 
            // lblExtras
            // 
            this.lblExtras.Border.BottomColor = System.Drawing.Color.Black;
            this.lblExtras.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblExtras.Border.LeftColor = System.Drawing.Color.Black;
            this.lblExtras.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblExtras.Border.RightColor = System.Drawing.Color.Black;
            this.lblExtras.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblExtras.Border.TopColor = System.Drawing.Color.Black;
            this.lblExtras.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblExtras.Height = 0.2F;
            this.lblExtras.HyperLink = null;
            this.lblExtras.Left = 4.125F;
            this.lblExtras.Name = "lblExtras";
            this.lblExtras.Style = "ddo-char-set: 1; font-weight: bold; ";
            this.lblExtras.Text = "Extras :";
            this.lblExtras.Top = 0.1875F;
            this.lblExtras.Visible = false;
            this.lblExtras.Width = 1.4375F;
            // 
            // txtExtrasTotal
            // 
            this.txtExtrasTotal.Border.BottomColor = System.Drawing.Color.Black;
            this.txtExtrasTotal.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtExtrasTotal.Border.LeftColor = System.Drawing.Color.Black;
            this.txtExtrasTotal.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtExtrasTotal.Border.RightColor = System.Drawing.Color.Black;
            this.txtExtrasTotal.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtExtrasTotal.Border.TopColor = System.Drawing.Color.Black;
            this.txtExtrasTotal.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtExtrasTotal.Height = 0.1875F;
            this.txtExtrasTotal.Left = 6.75F;
            this.txtExtrasTotal.Name = "txtExtrasTotal";
            this.txtExtrasTotal.OutputFormat = resources.GetString("txtExtrasTotal.OutputFormat");
            this.txtExtrasTotal.Style = "text-align: right; vertical-align: top; ";
            this.txtExtrasTotal.Text = null;
            this.txtExtrasTotal.Top = 0.1875F;
            this.txtExtrasTotal.Visible = false;
            this.txtExtrasTotal.Width = 0.7604167F;
            // 
            // reportFooterLine
            // 
            this.reportFooterLine.Border.BottomColor = System.Drawing.Color.Black;
            this.reportFooterLine.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.reportFooterLine.Border.LeftColor = System.Drawing.Color.Black;
            this.reportFooterLine.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.reportFooterLine.Border.RightColor = System.Drawing.Color.Black;
            this.reportFooterLine.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.reportFooterLine.Border.TopColor = System.Drawing.Color.Black;
            this.reportFooterLine.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.reportFooterLine.Height = 0F;
            this.reportFooterLine.Left = 0F;
            this.reportFooterLine.LineWeight = 1F;
            this.reportFooterLine.Name = "reportFooterLine";
            this.reportFooterLine.Top = 0.625F;
            this.reportFooterLine.Width = 7.5F;
            this.reportFooterLine.X1 = 0F;
            this.reportFooterLine.X2 = 7.5F;
            this.reportFooterLine.Y1 = 0.625F;
            this.reportFooterLine.Y2 = 0.625F;
            // 
            // lblFuelSurcharge
            // 
            this.lblFuelSurcharge.Border.BottomColor = System.Drawing.Color.Black;
            this.lblFuelSurcharge.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblFuelSurcharge.Border.LeftColor = System.Drawing.Color.Black;
            this.lblFuelSurcharge.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblFuelSurcharge.Border.RightColor = System.Drawing.Color.Black;
            this.lblFuelSurcharge.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblFuelSurcharge.Border.TopColor = System.Drawing.Color.Black;
            this.lblFuelSurcharge.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblFuelSurcharge.Height = 0.1875F;
            this.lblFuelSurcharge.HyperLink = null;
            this.lblFuelSurcharge.Left = 4.125F;
            this.lblFuelSurcharge.Name = "lblFuelSurcharge";
            this.lblFuelSurcharge.Style = "ddo-char-set: 1; font-weight: bold; ";
            this.lblFuelSurcharge.Text = "Fuel Surcharge @";
            this.lblFuelSurcharge.Top = 0.375F;
            this.lblFuelSurcharge.Visible = false;
            this.lblFuelSurcharge.Width = 1.4375F;
            // 
            // txtFuelSurchargeRate
            // 
            this.txtFuelSurchargeRate.Border.BottomColor = System.Drawing.Color.Black;
            this.txtFuelSurchargeRate.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtFuelSurchargeRate.Border.LeftColor = System.Drawing.Color.Black;
            this.txtFuelSurchargeRate.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtFuelSurchargeRate.Border.RightColor = System.Drawing.Color.Black;
            this.txtFuelSurchargeRate.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtFuelSurchargeRate.Border.TopColor = System.Drawing.Color.Black;
            this.txtFuelSurchargeRate.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtFuelSurchargeRate.Height = 0.1875F;
            this.txtFuelSurchargeRate.Left = 5.5625F;
            this.txtFuelSurchargeRate.Name = "txtFuelSurchargeRate";
            this.txtFuelSurchargeRate.OutputFormat = resources.GetString("txtFuelSurchargeRate.OutputFormat");
            this.txtFuelSurchargeRate.Style = "text-align: left; vertical-align: top; ";
            this.txtFuelSurchargeRate.Text = null;
            this.txtFuelSurchargeRate.Top = 0.375F;
            this.txtFuelSurchargeRate.Width = 0.9375F;
            // 
            // txtFuelSurchareAmount
            // 
            this.txtFuelSurchareAmount.Border.BottomColor = System.Drawing.Color.Black;
            this.txtFuelSurchareAmount.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtFuelSurchareAmount.Border.LeftColor = System.Drawing.Color.Black;
            this.txtFuelSurchareAmount.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtFuelSurchareAmount.Border.RightColor = System.Drawing.Color.Black;
            this.txtFuelSurchareAmount.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtFuelSurchareAmount.Border.TopColor = System.Drawing.Color.Black;
            this.txtFuelSurchareAmount.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtFuelSurchareAmount.Height = 0.1875F;
            this.txtFuelSurchareAmount.Left = 6.75F;
            this.txtFuelSurchareAmount.Name = "txtFuelSurchareAmount";
            this.txtFuelSurchareAmount.OutputFormat = resources.GetString("txtFuelSurchareAmount.OutputFormat");
            this.txtFuelSurchareAmount.Style = "text-align: right; ";
            this.txtFuelSurchareAmount.Text = null;
            this.txtFuelSurchareAmount.Top = 0.375F;
            this.txtFuelSurchareAmount.Width = 0.75F;
            // 
            // lblExtrasHeading
            // 
            this.lblExtrasHeading.Border.BottomColor = System.Drawing.Color.Black;
            this.lblExtrasHeading.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblExtrasHeading.Border.LeftColor = System.Drawing.Color.Black;
            this.lblExtrasHeading.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblExtrasHeading.Border.RightColor = System.Drawing.Color.Black;
            this.lblExtrasHeading.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblExtrasHeading.Border.TopColor = System.Drawing.Color.Black;
            this.lblExtrasHeading.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblExtrasHeading.Height = 0.1875F;
            this.lblExtrasHeading.HyperLink = null;
            this.lblExtrasHeading.Left = 0.25F;
            this.lblExtrasHeading.Name = "lblExtrasHeading";
            this.lblExtrasHeading.Style = "ddo-char-set: 1; font-weight: bold; ";
            this.lblExtrasHeading.Text = "Included Extras";
            this.lblExtrasHeading.Top = 0F;
            this.lblExtrasHeading.Visible = false;
            this.lblExtrasHeading.Width = 1.4375F;
            // 
            // txtCustomNotes
            // 
            this.txtCustomNotes.AutoReplaceFields = true;
            this.txtCustomNotes.BackColor = System.Drawing.Color.Transparent;
            this.txtCustomNotes.Border.BottomColor = System.Drawing.Color.Black;
            this.txtCustomNotes.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtCustomNotes.Border.LeftColor = System.Drawing.Color.Black;
            this.txtCustomNotes.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtCustomNotes.Border.RightColor = System.Drawing.Color.Black;
            this.txtCustomNotes.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtCustomNotes.Border.TopColor = System.Drawing.Color.Black;
            this.txtCustomNotes.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtCustomNotes.CanShrink = true;
            this.txtCustomNotes.Font = new System.Drawing.Font("Arial", 10F);
            this.txtCustomNotes.ForeColor = System.Drawing.Color.Black;
            this.txtCustomNotes.Height = 0.1875F;
            this.txtCustomNotes.Left = 0.125F;
            this.txtCustomNotes.Name = "txtCustomNotes";
            this.txtCustomNotes.RTF = resources.GetString("txtCustomNotes.RTF");
            this.txtCustomNotes.Top = 0.6875F;
            this.txtCustomNotes.Width = 3.75F;
            // 
            // PageHeader
            // 
            this.PageHeader.CanShrink = true;
            this.PageHeader.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.srPageHeader,
            this.lblDate,
            this.lblOrderId,
            this.lblCollectionAddress,
            this.lblDeliveryAddress,
            this.lblPallets,
            this.lblWeight,
            this.lblPalletSpaces,
            this.lblRef,
            this.lblValue,
            this.lblPageHeaderInvoiceNo,
            this.txtPageHeaderInvoiceNo,
            this.lblCases});
            this.PageHeader.Height = 2.28125F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
            this.PageHeader.BeforePrint += new System.EventHandler(this.PageHeader_BeforePrint);
            // 
            // srPageHeader
            // 
            this.srPageHeader.Border.BottomColor = System.Drawing.Color.Black;
            this.srPageHeader.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.srPageHeader.Border.LeftColor = System.Drawing.Color.Black;
            this.srPageHeader.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.srPageHeader.Border.RightColor = System.Drawing.Color.Black;
            this.srPageHeader.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.srPageHeader.Border.TopColor = System.Drawing.Color.Black;
            this.srPageHeader.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.srPageHeader.CloseBorder = false;
            this.srPageHeader.Height = 1.875F;
            this.srPageHeader.Left = 0F;
            this.srPageHeader.Name = "srPageHeader";
            this.srPageHeader.Report = null;
            this.srPageHeader.ReportName = "";
            this.srPageHeader.Top = 0.1875F;
            this.srPageHeader.Width = 7.5F;
            // 
            // lblDate
            // 
            this.lblDate.Border.BottomColor = System.Drawing.Color.Black;
            this.lblDate.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblDate.Border.LeftColor = System.Drawing.Color.Black;
            this.lblDate.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblDate.Border.RightColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.lblDate.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblDate.Border.TopColor = System.Drawing.Color.Black;
            this.lblDate.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblDate.Height = 0.2F;
            this.lblDate.Left = 0F;
            this.lblDate.Name = "lblDate";
            this.lblDate.Style = "color: White; text-align: center; background-color: Black; ";
            this.lblDate.Text = "Date";
            this.lblDate.Top = 2.0625F;
            this.lblDate.Width = 0.6875F;
            // 
            // lblOrderId
            // 
            this.lblOrderId.Border.BottomColor = System.Drawing.Color.Black;
            this.lblOrderId.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblOrderId.Border.LeftColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.lblOrderId.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblOrderId.Border.RightColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.lblOrderId.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblOrderId.Border.TopColor = System.Drawing.Color.Black;
            this.lblOrderId.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblOrderId.Height = 0.2F;
            this.lblOrderId.Left = 0.6875F;
            this.lblOrderId.Name = "lblOrderId";
            this.lblOrderId.Style = "color: White; text-align: center; background-color: Black; ";
            this.lblOrderId.Text = "Order Id";
            this.lblOrderId.Top = 2.0625F;
            this.lblOrderId.Width = 0.5625F;
            // 
            // lblCollectionAddress
            // 
            this.lblCollectionAddress.Border.BottomColor = System.Drawing.Color.Black;
            this.lblCollectionAddress.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblCollectionAddress.Border.LeftColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.lblCollectionAddress.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblCollectionAddress.Border.RightColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.lblCollectionAddress.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblCollectionAddress.Border.TopColor = System.Drawing.Color.Black;
            this.lblCollectionAddress.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblCollectionAddress.Height = 0.2F;
            this.lblCollectionAddress.Left = 1.25F;
            this.lblCollectionAddress.Name = "lblCollectionAddress";
            this.lblCollectionAddress.Style = "color: White; text-align: center; background-color: Black; ";
            this.lblCollectionAddress.Text = "Collection Address";
            this.lblCollectionAddress.Top = 2.0625F;
            this.lblCollectionAddress.Width = 1.375F;
            // 
            // lblDeliveryAddress
            // 
            this.lblDeliveryAddress.Border.BottomColor = System.Drawing.Color.Black;
            this.lblDeliveryAddress.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblDeliveryAddress.Border.LeftColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.lblDeliveryAddress.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblDeliveryAddress.Border.RightColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.lblDeliveryAddress.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblDeliveryAddress.Border.TopColor = System.Drawing.Color.Black;
            this.lblDeliveryAddress.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblDeliveryAddress.Height = 0.1875F;
            this.lblDeliveryAddress.Left = 2.625F;
            this.lblDeliveryAddress.Name = "lblDeliveryAddress";
            this.lblDeliveryAddress.Style = "color: White; text-align: center; background-color: Black; ";
            this.lblDeliveryAddress.Text = "Delivery Address";
            this.lblDeliveryAddress.Top = 2.0625F;
            this.lblDeliveryAddress.Width = 1.0625F;
            // 
            // lblPallets
            // 
            this.lblPallets.Border.BottomColor = System.Drawing.Color.Black;
            this.lblPallets.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblPallets.Border.LeftColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.lblPallets.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblPallets.Border.RightColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.lblPallets.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblPallets.Border.TopColor = System.Drawing.Color.Black;
            this.lblPallets.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblPallets.Height = 0.2F;
            this.lblPallets.Left = 3.6875F;
            this.lblPallets.Name = "lblPallets";
            this.lblPallets.Style = "color: White; text-align: center; background-color: Black; ";
            this.lblPallets.Text = "Pallets";
            this.lblPallets.Top = 2.0625F;
            this.lblPallets.Width = 0.4375F;
            // 
            // lblWeight
            // 
            this.lblWeight.Border.BottomColor = System.Drawing.Color.Black;
            this.lblWeight.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblWeight.Border.LeftColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.lblWeight.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblWeight.Border.RightColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.lblWeight.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblWeight.Border.TopColor = System.Drawing.Color.Black;
            this.lblWeight.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblWeight.Height = 0.2F;
            this.lblWeight.Left = 4.125F;
            this.lblWeight.Name = "lblWeight";
            this.lblWeight.Style = "color: White; text-align: center; background-color: Black; ";
            this.lblWeight.Text = "Weight(kg)";
            this.lblWeight.Top = 2.0625F;
            this.lblWeight.Width = 0.75F;
            // 
            // lblPalletSpaces
            // 
            this.lblPalletSpaces.Border.BottomColor = System.Drawing.Color.Black;
            this.lblPalletSpaces.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblPalletSpaces.Border.LeftColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.lblPalletSpaces.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblPalletSpaces.Border.RightColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.lblPalletSpaces.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblPalletSpaces.Border.TopColor = System.Drawing.Color.Black;
            this.lblPalletSpaces.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblPalletSpaces.CanGrow = false;
            this.lblPalletSpaces.Height = 0.2F;
            this.lblPalletSpaces.Left = 4.875F;
            this.lblPalletSpaces.Name = "lblPalletSpaces";
            this.lblPalletSpaces.Style = "color: White; text-align: left; background-color: Black; white-space: nowrap; ";
            this.lblPalletSpaces.Text = "Spaces";
            this.lblPalletSpaces.Top = 2.0625F;
            this.lblPalletSpaces.Width = 0.5F;
            // 
            // lblRef
            // 
            this.lblRef.Border.BottomColor = System.Drawing.Color.Black;
            this.lblRef.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblRef.Border.LeftColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.lblRef.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblRef.Border.RightColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.lblRef.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblRef.Border.TopColor = System.Drawing.Color.Black;
            this.lblRef.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblRef.Height = 0.1875F;
            this.lblRef.Left = 5.8125F;
            this.lblRef.Name = "lblRef";
            this.lblRef.Style = "color: White; text-align: center; background-color: Black; ";
            this.lblRef.Text = "Customer Ref.";
            this.lblRef.Top = 2.0625F;
            this.lblRef.Width = 0.9375F;
            // 
            // lblValue
            // 
            this.lblValue.Border.BottomColor = System.Drawing.Color.Black;
            this.lblValue.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblValue.Border.LeftColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.lblValue.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblValue.Border.RightColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.lblValue.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblValue.Border.TopColor = System.Drawing.Color.Black;
            this.lblValue.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblValue.Height = 0.2F;
            this.lblValue.Left = 6.75F;
            this.lblValue.Name = "lblValue";
            this.lblValue.Style = "color: White; text-align: center; background-color: Black; ";
            this.lblValue.Text = "Value";
            this.lblValue.Top = 2.0625F;
            this.lblValue.Width = 0.75F;
            // 
            // lblPageHeaderInvoiceNo
            // 
            this.lblPageHeaderInvoiceNo.Border.BottomColor = System.Drawing.Color.Black;
            this.lblPageHeaderInvoiceNo.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblPageHeaderInvoiceNo.Border.LeftColor = System.Drawing.Color.Black;
            this.lblPageHeaderInvoiceNo.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblPageHeaderInvoiceNo.Border.RightColor = System.Drawing.Color.Black;
            this.lblPageHeaderInvoiceNo.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblPageHeaderInvoiceNo.Border.TopColor = System.Drawing.Color.Black;
            this.lblPageHeaderInvoiceNo.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblPageHeaderInvoiceNo.Height = 0.1875F;
            this.lblPageHeaderInvoiceNo.HyperLink = null;
            this.lblPageHeaderInvoiceNo.Left = 5.9375F;
            this.lblPageHeaderInvoiceNo.Name = "lblPageHeaderInvoiceNo";
            this.lblPageHeaderInvoiceNo.Style = "ddo-char-set: 1; font-weight: bold; ";
            this.lblPageHeaderInvoiceNo.Text = "Invoice No:";
            this.lblPageHeaderInvoiceNo.Top = 0F;
            this.lblPageHeaderInvoiceNo.Width = 0.75F;
            // 
            // txtPageHeaderInvoiceNo
            // 
            this.txtPageHeaderInvoiceNo.Border.BottomColor = System.Drawing.Color.Black;
            this.txtPageHeaderInvoiceNo.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtPageHeaderInvoiceNo.Border.LeftColor = System.Drawing.Color.Black;
            this.txtPageHeaderInvoiceNo.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtPageHeaderInvoiceNo.Border.RightColor = System.Drawing.Color.Black;
            this.txtPageHeaderInvoiceNo.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtPageHeaderInvoiceNo.Border.TopColor = System.Drawing.Color.Black;
            this.txtPageHeaderInvoiceNo.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtPageHeaderInvoiceNo.Height = 0.1875F;
            this.txtPageHeaderInvoiceNo.Left = 6.6875F;
            this.txtPageHeaderInvoiceNo.MultiLine = false;
            this.txtPageHeaderInvoiceNo.Name = "txtPageHeaderInvoiceNo";
            this.txtPageHeaderInvoiceNo.Style = "color: Black; text-align: right; ";
            this.txtPageHeaderInvoiceNo.Text = "To Be Issued";
            this.txtPageHeaderInvoiceNo.Top = 0F;
            this.txtPageHeaderInvoiceNo.Width = 0.8125F;
            // 
            // lblCases
            // 
            this.lblCases.Border.BottomColor = System.Drawing.Color.Black;
            this.lblCases.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblCases.Border.LeftColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.lblCases.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblCases.Border.RightColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.lblCases.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblCases.Border.TopColor = System.Drawing.Color.Black;
            this.lblCases.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblCases.Height = 0.1875F;
            this.lblCases.Left = 5.375F;
            this.lblCases.Name = "lblCases";
            this.lblCases.Style = "color: White; text-align: center; background-color: Black; ";
            this.lblCases.Text = "Cases";
            this.lblCases.Top = 2.0625F;
            this.lblCases.Width = 0.4375F;
            // 
            // PageFooter
            // 
            this.PageFooter.CanShrink = true;
            this.PageFooter.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.srFooter});
            this.PageFooter.Height = 1.333333F;
            this.PageFooter.Name = "PageFooter";
            this.PageFooter.Format += new System.EventHandler(this.PageFooter_Format);
            // 
            // srFooter
            // 
            this.srFooter.Border.BottomColor = System.Drawing.Color.Black;
            this.srFooter.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.srFooter.Border.LeftColor = System.Drawing.Color.Black;
            this.srFooter.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.srFooter.Border.RightColor = System.Drawing.Color.Black;
            this.srFooter.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.srFooter.Border.TopColor = System.Drawing.Color.Black;
            this.srFooter.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.srFooter.CloseBorder = false;
            this.srFooter.Height = 1.3125F;
            this.srFooter.Left = 0F;
            this.srFooter.Name = "srFooter";
            this.srFooter.Report = null;
            this.srFooter.Top = 0F;
            this.srFooter.Width = 7.5F;
            // 
            // txtPalletSpaces
            // 
            this.txtPalletSpaces.Border.BottomColor = System.Drawing.Color.Black;
            this.txtPalletSpaces.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtPalletSpaces.Border.LeftColor = System.Drawing.Color.Black;
            this.txtPalletSpaces.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtPalletSpaces.Border.RightColor = System.Drawing.Color.Black;
            this.txtPalletSpaces.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtPalletSpaces.Border.TopColor = System.Drawing.Color.Black;
            this.txtPalletSpaces.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtPalletSpaces.DataField = "PalletSpaces";
            this.txtPalletSpaces.Height = 0.1875F;
            this.txtPalletSpaces.Left = 4.875F;
            this.txtPalletSpaces.Name = "txtPalletSpaces";
            this.txtPalletSpaces.Style = "color: Black; text-align: center; ";
            this.txtPalletSpaces.Text = "Spaces";
            this.txtPalletSpaces.Top = 0F;
            this.txtPalletSpaces.Width = 0.5F;
            // 
            // grpOrder
            // 
            this.grpOrder.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.txtOrderGroupID,
            this.txtRate,
            this.txtCustomerRef,
            this.txtDeliveryOrderNumber,
            this.subReferences,
            this.txtOrderServiceLevel,
            this.txtWeight,
            this.txtPallets,
            this.txtDeliveryAddress,
            this.txtCollectionAddress,
            this.txtOrderID,
            this.txtDeliveryDate,
            this.txtPalletSpaces,
            this.txtForeignRate,
            this.lineOrderFooter,
            this.txtCases});
            this.grpOrder.DataField = "OrderID";
            this.grpOrder.Height = 0.1979167F;
            this.grpOrder.KeepTogether = true;
            this.grpOrder.Name = "grpOrder";
            this.grpOrder.Format += new System.EventHandler(this.grpOrder_Format);
            // 
            // txtOrderGroupID
            // 
            this.txtOrderGroupID.Border.BottomColor = System.Drawing.Color.Black;
            this.txtOrderGroupID.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtOrderGroupID.Border.LeftColor = System.Drawing.Color.Black;
            this.txtOrderGroupID.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtOrderGroupID.Border.RightColor = System.Drawing.Color.Black;
            this.txtOrderGroupID.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtOrderGroupID.Border.TopColor = System.Drawing.Color.Black;
            this.txtOrderGroupID.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtOrderGroupID.DataField = "OrderGroupID";
            this.txtOrderGroupID.Height = 0.2F;
            this.txtOrderGroupID.Left = 0.6875F;
            this.txtOrderGroupID.Name = "txtOrderGroupID";
            this.txtOrderGroupID.Style = "color: Black; text-align: center; ";
            this.txtOrderGroupID.Text = null;
            this.txtOrderGroupID.Top = 0F;
            this.txtOrderGroupID.Visible = false;
            this.txtOrderGroupID.Width = 0.5625F;
            // 
            // txtRate
            // 
            this.txtRate.Border.BottomColor = System.Drawing.Color.Black;
            this.txtRate.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtRate.Border.LeftColor = System.Drawing.Color.Black;
            this.txtRate.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtRate.Border.RightColor = System.Drawing.Color.Black;
            this.txtRate.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtRate.Border.TopColor = System.Drawing.Color.Black;
            this.txtRate.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtRate.DataField = "Rate";
            this.txtRate.Height = 0.1875F;
            this.txtRate.Left = 6.75F;
            this.txtRate.Name = "txtRate";
            this.txtRate.OutputFormat = resources.GetString("txtRate.OutputFormat");
            this.txtRate.Style = "color: Black; text-align: right; ";
            this.txtRate.Text = null;
            this.txtRate.Top = 0F;
            this.txtRate.Visible = false;
            this.txtRate.Width = 0.75F;
            // 
            // txtCustomerRef
            // 
            this.txtCustomerRef.Border.BottomColor = System.Drawing.Color.Black;
            this.txtCustomerRef.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtCustomerRef.Border.LeftColor = System.Drawing.Color.Black;
            this.txtCustomerRef.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtCustomerRef.Border.RightColor = System.Drawing.Color.Black;
            this.txtCustomerRef.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtCustomerRef.Border.TopColor = System.Drawing.Color.Black;
            this.txtCustomerRef.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtCustomerRef.DataField = "CustomerOrderNumber";
            this.txtCustomerRef.Height = 0.1875F;
            this.txtCustomerRef.Left = 5.8125F;
            this.txtCustomerRef.Name = "txtCustomerRef";
            this.txtCustomerRef.Style = "color: Black; text-align: center; ";
            this.txtCustomerRef.Text = "Customer Ref.";
            this.txtCustomerRef.Top = 0F;
            this.txtCustomerRef.Visible = false;
            this.txtCustomerRef.Width = 0.9375F;
            // 
            // txtDeliveryOrderNumber
            // 
            this.txtDeliveryOrderNumber.Border.BottomColor = System.Drawing.Color.Black;
            this.txtDeliveryOrderNumber.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtDeliveryOrderNumber.Border.LeftColor = System.Drawing.Color.Black;
            this.txtDeliveryOrderNumber.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtDeliveryOrderNumber.Border.RightColor = System.Drawing.Color.Black;
            this.txtDeliveryOrderNumber.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtDeliveryOrderNumber.Border.TopColor = System.Drawing.Color.Black;
            this.txtDeliveryOrderNumber.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtDeliveryOrderNumber.DataField = "DeliveryOrderNumber";
            this.txtDeliveryOrderNumber.Height = 0.1875F;
            this.txtDeliveryOrderNumber.Left = 5.8125F;
            this.txtDeliveryOrderNumber.Name = "txtDeliveryOrderNumber";
            this.txtDeliveryOrderNumber.Style = "color: Black; text-align: center; ";
            this.txtDeliveryOrderNumber.Text = "Delivery Order Number";
            this.txtDeliveryOrderNumber.Top = 0F;
            this.txtDeliveryOrderNumber.Visible = false;
            this.txtDeliveryOrderNumber.Width = 0.9375F;
            // 
            // subReferences
            // 
            this.subReferences.Border.BottomColor = System.Drawing.Color.Black;
            this.subReferences.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.subReferences.Border.LeftColor = System.Drawing.Color.Black;
            this.subReferences.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.subReferences.Border.RightColor = System.Drawing.Color.Black;
            this.subReferences.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.subReferences.Border.TopColor = System.Drawing.Color.Black;
            this.subReferences.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.subReferences.CloseBorder = false;
            this.subReferences.Height = 0.1875F;
            this.subReferences.Left = 5.8125F;
            this.subReferences.Name = "subReferences";
            this.subReferences.Report = null;
            this.subReferences.Top = 0F;
            this.subReferences.Width = 0.9375F;
            // 
            // txtOrderServiceLevel
            // 
            this.txtOrderServiceLevel.Border.BottomColor = System.Drawing.Color.Black;
            this.txtOrderServiceLevel.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtOrderServiceLevel.Border.LeftColor = System.Drawing.Color.Black;
            this.txtOrderServiceLevel.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtOrderServiceLevel.Border.RightColor = System.Drawing.Color.Black;
            this.txtOrderServiceLevel.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtOrderServiceLevel.Border.TopColor = System.Drawing.Color.Black;
            this.txtOrderServiceLevel.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtOrderServiceLevel.DataField = "OrderServiceLevel";
            this.txtOrderServiceLevel.Height = 0.1875F;
            this.txtOrderServiceLevel.Left = 5.8125F;
            this.txtOrderServiceLevel.Name = "txtOrderServiceLevel";
            this.txtOrderServiceLevel.Style = "color: Black; text-align: center; ";
            this.txtOrderServiceLevel.Text = "Order Service Level";
            this.txtOrderServiceLevel.Top = 0F;
            this.txtOrderServiceLevel.Visible = false;
            this.txtOrderServiceLevel.Width = 0.9375F;
            // 
            // txtWeight
            // 
            this.txtWeight.Border.BottomColor = System.Drawing.Color.Black;
            this.txtWeight.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtWeight.Border.LeftColor = System.Drawing.Color.Black;
            this.txtWeight.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtWeight.Border.RightColor = System.Drawing.Color.Black;
            this.txtWeight.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtWeight.Border.TopColor = System.Drawing.Color.Black;
            this.txtWeight.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtWeight.DataField = "Weight";
            this.txtWeight.Height = 0.1875F;
            this.txtWeight.Left = 4.125F;
            this.txtWeight.Name = "txtWeight";
            this.txtWeight.OutputFormat = resources.GetString("txtWeight.OutputFormat");
            this.txtWeight.Style = "color: Black; text-align: center; ";
            this.txtWeight.Text = "Weight";
            this.txtWeight.Top = 0F;
            this.txtWeight.Width = 0.75F;
            // 
            // txtPallets
            // 
            this.txtPallets.Border.BottomColor = System.Drawing.Color.Black;
            this.txtPallets.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtPallets.Border.LeftColor = System.Drawing.Color.Black;
            this.txtPallets.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtPallets.Border.RightColor = System.Drawing.Color.Black;
            this.txtPallets.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtPallets.Border.TopColor = System.Drawing.Color.Black;
            this.txtPallets.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtPallets.DataField = "Cases";
            this.txtPallets.Height = 0.1875F;
            this.txtPallets.Left = 3.6875F;
            this.txtPallets.Name = "txtPallets";
            this.txtPallets.Style = "color: Black; text-align: center; ";
            this.txtPallets.Text = "Cases";
            this.txtPallets.Top = 0F;
            this.txtPallets.Width = 0.4375F;
            // 
            // txtDeliveryAddress
            // 
            this.txtDeliveryAddress.Border.BottomColor = System.Drawing.Color.Black;
            this.txtDeliveryAddress.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtDeliveryAddress.Border.LeftColor = System.Drawing.Color.Black;
            this.txtDeliveryAddress.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtDeliveryAddress.Border.RightColor = System.Drawing.Color.Black;
            this.txtDeliveryAddress.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtDeliveryAddress.Border.TopColor = System.Drawing.Color.Black;
            this.txtDeliveryAddress.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtDeliveryAddress.DataField = "DeliveryPoint";
            this.txtDeliveryAddress.Height = 0.188F;
            this.txtDeliveryAddress.Left = 2.625F;
            this.txtDeliveryAddress.Name = "txtDeliveryAddress";
            this.txtDeliveryAddress.Style = "color: Black; text-align: left; ";
            this.txtDeliveryAddress.Text = "Delivery Address";
            this.txtDeliveryAddress.Top = 0F;
            this.txtDeliveryAddress.Width = 1.063F;
            // 
            // txtCollectionAddress
            // 
            this.txtCollectionAddress.Border.BottomColor = System.Drawing.Color.Black;
            this.txtCollectionAddress.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtCollectionAddress.Border.LeftColor = System.Drawing.Color.Black;
            this.txtCollectionAddress.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtCollectionAddress.Border.RightColor = System.Drawing.Color.Black;
            this.txtCollectionAddress.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtCollectionAddress.Border.TopColor = System.Drawing.Color.Black;
            this.txtCollectionAddress.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtCollectionAddress.DataField = "CollectionPoint";
            this.txtCollectionAddress.Height = 0.1875F;
            this.txtCollectionAddress.Left = 1.25F;
            this.txtCollectionAddress.Name = "txtCollectionAddress";
            this.txtCollectionAddress.Style = "color: Black; text-align: left; ";
            this.txtCollectionAddress.Text = "Collection Address";
            this.txtCollectionAddress.Top = 0F;
            this.txtCollectionAddress.Width = 1.375F;
            // 
            // txtOrderID
            // 
            this.txtOrderID.Border.BottomColor = System.Drawing.Color.Black;
            this.txtOrderID.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtOrderID.Border.LeftColor = System.Drawing.Color.Black;
            this.txtOrderID.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtOrderID.Border.RightColor = System.Drawing.Color.Black;
            this.txtOrderID.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtOrderID.Border.TopColor = System.Drawing.Color.Black;
            this.txtOrderID.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtOrderID.DataField = "OrderID";
            this.txtOrderID.Height = 0.2F;
            this.txtOrderID.Left = 0.6875F;
            this.txtOrderID.Name = "txtOrderID";
            this.txtOrderID.Style = "color: Black; text-align: center; ";
            this.txtOrderID.Text = "Order Id";
            this.txtOrderID.Top = 0F;
            this.txtOrderID.Width = 0.5625F;
            // 
            // txtDeliveryDate
            // 
            this.txtDeliveryDate.Border.BottomColor = System.Drawing.Color.Black;
            this.txtDeliveryDate.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtDeliveryDate.Border.LeftColor = System.Drawing.Color.Black;
            this.txtDeliveryDate.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtDeliveryDate.Border.RightColor = System.Drawing.Color.Black;
            this.txtDeliveryDate.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtDeliveryDate.Border.TopColor = System.Drawing.Color.Black;
            this.txtDeliveryDate.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtDeliveryDate.DataField = "DeliveryDateTime";
            this.txtDeliveryDate.Height = 0.2F;
            this.txtDeliveryDate.Left = 0F;
            this.txtDeliveryDate.Name = "txtDeliveryDate";
            this.txtDeliveryDate.OutputFormat = resources.GetString("txtDeliveryDate.OutputFormat");
            this.txtDeliveryDate.Style = "color: Black; text-align: center; ";
            this.txtDeliveryDate.Text = "Date";
            this.txtDeliveryDate.Top = 0F;
            this.txtDeliveryDate.Width = 0.6875F;
            // 
            // txtForeignRate
            // 
            this.txtForeignRate.Border.BottomColor = System.Drawing.Color.Black;
            this.txtForeignRate.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtForeignRate.Border.LeftColor = System.Drawing.Color.Black;
            this.txtForeignRate.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtForeignRate.Border.RightColor = System.Drawing.Color.Black;
            this.txtForeignRate.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtForeignRate.Border.TopColor = System.Drawing.Color.Black;
            this.txtForeignRate.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtForeignRate.DataField = "ForeignRate";
            this.txtForeignRate.Height = 0.1875F;
            this.txtForeignRate.Left = 6.75F;
            this.txtForeignRate.Name = "txtForeignRate";
            this.txtForeignRate.Style = "text-align: right; ";
            this.txtForeignRate.Text = "Value";
            this.txtForeignRate.Top = 0F;
            this.txtForeignRate.Width = 0.75F;
            // 
            // lineOrderFooter
            // 
            this.lineOrderFooter.Border.BottomColor = System.Drawing.Color.Black;
            this.lineOrderFooter.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lineOrderFooter.Border.LeftColor = System.Drawing.Color.Black;
            this.lineOrderFooter.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lineOrderFooter.Border.RightColor = System.Drawing.Color.Black;
            this.lineOrderFooter.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lineOrderFooter.Border.TopColor = System.Drawing.Color.Black;
            this.lineOrderFooter.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lineOrderFooter.Height = 0F;
            this.lineOrderFooter.Left = 0F;
            this.lineOrderFooter.LineWeight = 1F;
            this.lineOrderFooter.Name = "lineOrderFooter";
            this.lineOrderFooter.Top = 0F;
            this.lineOrderFooter.Width = 7.5F;
            this.lineOrderFooter.X1 = 0F;
            this.lineOrderFooter.X2 = 7.5F;
            this.lineOrderFooter.Y1 = 0F;
            this.lineOrderFooter.Y2 = 0F;
            // 
            // txtCases
            // 
            this.txtCases.Border.BottomColor = System.Drawing.Color.Black;
            this.txtCases.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtCases.Border.LeftColor = System.Drawing.Color.Black;
            this.txtCases.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtCases.Border.RightColor = System.Drawing.Color.Black;
            this.txtCases.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtCases.Border.TopColor = System.Drawing.Color.Black;
            this.txtCases.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtCases.DataField = "Cases";
            this.txtCases.Height = 0.1875F;
            this.txtCases.Left = 5.375F;
            this.txtCases.Name = "txtCases";
            this.txtCases.Style = "color: Black; text-align: center; ";
            this.txtCases.Text = "Cases";
            this.txtCases.Top = 0F;
            this.txtCases.Width = 0.4375F;
            // 
            // lblVehicleRegNo
            // 
            this.lblVehicleRegNo.Border.BottomColor = System.Drawing.Color.Black;
            this.lblVehicleRegNo.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblVehicleRegNo.Border.LeftColor = System.Drawing.Color.Black;
            this.lblVehicleRegNo.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblVehicleRegNo.Border.RightColor = System.Drawing.Color.Black;
            this.lblVehicleRegNo.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblVehicleRegNo.Border.TopColor = System.Drawing.Color.Black;
            this.lblVehicleRegNo.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblVehicleRegNo.Height = 0.1875F;
            this.lblVehicleRegNo.Left = 5.4375F;
            this.lblVehicleRegNo.Name = "lblVehicleRegNo";
            this.lblVehicleRegNo.Style = "color: Black; text-align: right; vertical-align: bottom; ";
            this.lblVehicleRegNo.Text = "RegNo:";
            this.lblVehicleRegNo.Top = 0F;
            this.lblVehicleRegNo.Visible = false;
            this.lblVehicleRegNo.Width = 0.875F;
            // 
            // txtVehicleRegNo
            // 
            this.txtVehicleRegNo.Border.BottomColor = System.Drawing.Color.Black;
            this.txtVehicleRegNo.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtVehicleRegNo.Border.LeftColor = System.Drawing.Color.Black;
            this.txtVehicleRegNo.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtVehicleRegNo.Border.RightColor = System.Drawing.Color.Black;
            this.txtVehicleRegNo.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtVehicleRegNo.Border.TopColor = System.Drawing.Color.Black;
            this.txtVehicleRegNo.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtVehicleRegNo.DataField = "VehicleRegNo";
            this.txtVehicleRegNo.Height = 0.1875F;
            this.txtVehicleRegNo.Left = 6.375F;
            this.txtVehicleRegNo.Name = "txtVehicleRegNo";
            this.txtVehicleRegNo.Style = "color: Black; text-align: center; vertical-align: bottom; ";
            this.txtVehicleRegNo.Text = null;
            this.txtVehicleRegNo.Top = 0F;
            this.txtVehicleRegNo.Visible = false;
            this.txtVehicleRegNo.Width = 1.1875F;
            // 
            // grpOrderFooter
            // 
            this.grpOrderFooter.CanShrink = true;
            this.grpOrderFooter.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.txtJobFuelSurchargeAmount,
            this.txtJobFuelSurchargeRate,
            this.lblJobFuelSurcharge,
            this.txtJobExtrasTotal,
            this.lblJobExtraTotal,
            this.sbJobExtras,
            this.txtOrderIDFooter,
            this.txtOrderGroupIDFooter,
            this.sbSurcharges,
            this.subGoodsRefused});
            this.grpOrderFooter.Height = 0.581F;
            this.grpOrderFooter.KeepTogether = true;
            this.grpOrderFooter.Name = "grpOrderFooter";
            this.grpOrderFooter.Format += new System.EventHandler(this.grpOrderFooter_Format);
            this.grpOrderFooter.BeforePrint += new System.EventHandler(this.grpOrderFooter_BeforePrint);
            // 
            // txtJobFuelSurchargeAmount
            // 
            this.txtJobFuelSurchargeAmount.Border.BottomColor = System.Drawing.Color.Black;
            this.txtJobFuelSurchargeAmount.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtJobFuelSurchargeAmount.Border.LeftColor = System.Drawing.Color.Black;
            this.txtJobFuelSurchargeAmount.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtJobFuelSurchargeAmount.Border.RightColor = System.Drawing.Color.Black;
            this.txtJobFuelSurchargeAmount.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtJobFuelSurchargeAmount.Border.TopColor = System.Drawing.Color.Black;
            this.txtJobFuelSurchargeAmount.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtJobFuelSurchargeAmount.Height = 0.1875F;
            this.txtJobFuelSurchargeAmount.Left = 6.75F;
            this.txtJobFuelSurchargeAmount.Name = "txtJobFuelSurchargeAmount";
            this.txtJobFuelSurchargeAmount.OutputFormat = resources.GetString("txtJobFuelSurchargeAmount.OutputFormat");
            this.txtJobFuelSurchargeAmount.Style = "text-align: right; ";
            this.txtJobFuelSurchargeAmount.Text = null;
            this.txtJobFuelSurchargeAmount.Top = 0.3125F;
            this.txtJobFuelSurchargeAmount.Width = 0.75F;
            // 
            // txtJobFuelSurchargeRate
            // 
            this.txtJobFuelSurchargeRate.Border.BottomColor = System.Drawing.Color.Black;
            this.txtJobFuelSurchargeRate.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtJobFuelSurchargeRate.Border.LeftColor = System.Drawing.Color.Black;
            this.txtJobFuelSurchargeRate.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtJobFuelSurchargeRate.Border.RightColor = System.Drawing.Color.Black;
            this.txtJobFuelSurchargeRate.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtJobFuelSurchargeRate.Border.TopColor = System.Drawing.Color.Black;
            this.txtJobFuelSurchargeRate.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtJobFuelSurchargeRate.Height = 0.1875F;
            this.txtJobFuelSurchargeRate.Left = 6F;
            this.txtJobFuelSurchargeRate.Name = "txtJobFuelSurchargeRate";
            this.txtJobFuelSurchargeRate.OutputFormat = resources.GetString("txtJobFuelSurchargeRate.OutputFormat");
            this.txtJobFuelSurchargeRate.Style = "text-align: right; vertical-align: top; ";
            this.txtJobFuelSurchargeRate.Text = null;
            this.txtJobFuelSurchargeRate.Top = 0.3125F;
            this.txtJobFuelSurchargeRate.Width = 0.6875F;
            // 
            // lblJobFuelSurcharge
            // 
            this.lblJobFuelSurcharge.Border.BottomColor = System.Drawing.Color.Black;
            this.lblJobFuelSurcharge.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblJobFuelSurcharge.Border.LeftColor = System.Drawing.Color.Black;
            this.lblJobFuelSurcharge.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblJobFuelSurcharge.Border.RightColor = System.Drawing.Color.Black;
            this.lblJobFuelSurcharge.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblJobFuelSurcharge.Border.TopColor = System.Drawing.Color.Black;
            this.lblJobFuelSurcharge.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblJobFuelSurcharge.Height = 0.1875F;
            this.lblJobFuelSurcharge.HyperLink = null;
            this.lblJobFuelSurcharge.Left = 4.875F;
            this.lblJobFuelSurcharge.Name = "lblJobFuelSurcharge";
            this.lblJobFuelSurcharge.Style = "ddo-char-set: 1; font-weight: bold; ";
            this.lblJobFuelSurcharge.Text = "Fuel Surcharge @";
            this.lblJobFuelSurcharge.Top = 0.3125F;
            this.lblJobFuelSurcharge.Width = 1.125F;
            // 
            // txtJobExtrasTotal
            // 
            this.txtJobExtrasTotal.Border.BottomColor = System.Drawing.Color.Black;
            this.txtJobExtrasTotal.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtJobExtrasTotal.Border.LeftColor = System.Drawing.Color.Black;
            this.txtJobExtrasTotal.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtJobExtrasTotal.Border.RightColor = System.Drawing.Color.Black;
            this.txtJobExtrasTotal.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtJobExtrasTotal.Border.TopColor = System.Drawing.Color.Black;
            this.txtJobExtrasTotal.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtJobExtrasTotal.Height = 0.1875F;
            this.txtJobExtrasTotal.Left = 6.75F;
            this.txtJobExtrasTotal.Name = "txtJobExtrasTotal";
            this.txtJobExtrasTotal.OutputFormat = resources.GetString("txtJobExtrasTotal.OutputFormat");
            this.txtJobExtrasTotal.Style = "text-align: right; vertical-align: top; ";
            this.txtJobExtrasTotal.Text = null;
            this.txtJobExtrasTotal.Top = 0.125F;
            this.txtJobExtrasTotal.Width = 0.75F;
            // 
            // lblJobExtraTotal
            // 
            this.lblJobExtraTotal.Border.BottomColor = System.Drawing.Color.Black;
            this.lblJobExtraTotal.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblJobExtraTotal.Border.LeftColor = System.Drawing.Color.Black;
            this.lblJobExtraTotal.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblJobExtraTotal.Border.RightColor = System.Drawing.Color.Black;
            this.lblJobExtraTotal.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblJobExtraTotal.Border.TopColor = System.Drawing.Color.Black;
            this.lblJobExtraTotal.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblJobExtraTotal.Height = 0.1875F;
            this.lblJobExtraTotal.HyperLink = null;
            this.lblJobExtraTotal.Left = 6.1875F;
            this.lblJobExtraTotal.Name = "lblJobExtraTotal";
            this.lblJobExtraTotal.Style = "ddo-char-set: 1; font-weight: bold; ";
            this.lblJobExtraTotal.Text = "Extras :";
            this.lblJobExtraTotal.Top = 0.125F;
            this.lblJobExtraTotal.Width = 0.5F;
            // 
            // sbJobExtras
            // 
            this.sbJobExtras.Border.BottomColor = System.Drawing.Color.Black;
            this.sbJobExtras.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.sbJobExtras.Border.LeftColor = System.Drawing.Color.Black;
            this.sbJobExtras.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.sbJobExtras.Border.RightColor = System.Drawing.Color.Black;
            this.sbJobExtras.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.sbJobExtras.Border.TopColor = System.Drawing.Color.Black;
            this.sbJobExtras.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.sbJobExtras.CloseBorder = false;
            this.sbJobExtras.Height = 0.0625F;
            this.sbJobExtras.Left = 0F;
            this.sbJobExtras.Name = "sbJobExtras";
            this.sbJobExtras.Report = null;
            this.sbJobExtras.Top = 0.0625F;
            this.sbJobExtras.Width = 7.25F;
            // 
            // txtOrderIDFooter
            // 
            this.txtOrderIDFooter.Border.BottomColor = System.Drawing.Color.Black;
            this.txtOrderIDFooter.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtOrderIDFooter.Border.LeftColor = System.Drawing.Color.Black;
            this.txtOrderIDFooter.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtOrderIDFooter.Border.RightColor = System.Drawing.Color.Black;
            this.txtOrderIDFooter.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtOrderIDFooter.Border.TopColor = System.Drawing.Color.Black;
            this.txtOrderIDFooter.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtOrderIDFooter.DataField = "OrderID";
            this.txtOrderIDFooter.Height = 0.2F;
            this.txtOrderIDFooter.Left = 0F;
            this.txtOrderIDFooter.Name = "txtOrderIDFooter";
            this.txtOrderIDFooter.Style = "";
            this.txtOrderIDFooter.Text = "Order ID";
            this.txtOrderIDFooter.Top = 0.125F;
            this.txtOrderIDFooter.Visible = false;
            this.txtOrderIDFooter.Width = 1F;
            // 
            // txtOrderGroupIDFooter
            // 
            this.txtOrderGroupIDFooter.Border.BottomColor = System.Drawing.Color.Black;
            this.txtOrderGroupIDFooter.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtOrderGroupIDFooter.Border.LeftColor = System.Drawing.Color.Black;
            this.txtOrderGroupIDFooter.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtOrderGroupIDFooter.Border.RightColor = System.Drawing.Color.Black;
            this.txtOrderGroupIDFooter.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtOrderGroupIDFooter.Border.TopColor = System.Drawing.Color.Black;
            this.txtOrderGroupIDFooter.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtOrderGroupIDFooter.DataField = "OrderGroupID";
            this.txtOrderGroupIDFooter.Height = 0.2F;
            this.txtOrderGroupIDFooter.Left = 1F;
            this.txtOrderGroupIDFooter.Name = "txtOrderGroupIDFooter";
            this.txtOrderGroupIDFooter.Style = "";
            this.txtOrderGroupIDFooter.Text = "Order Group ID";
            this.txtOrderGroupIDFooter.Top = 0.125F;
            this.txtOrderGroupIDFooter.Visible = false;
            this.txtOrderGroupIDFooter.Width = 1F;
            // 
            // sbSurcharges
            // 
            this.sbSurcharges.Border.BottomColor = System.Drawing.Color.Black;
            this.sbSurcharges.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.sbSurcharges.Border.LeftColor = System.Drawing.Color.Black;
            this.sbSurcharges.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.sbSurcharges.Border.RightColor = System.Drawing.Color.Black;
            this.sbSurcharges.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.sbSurcharges.Border.TopColor = System.Drawing.Color.Black;
            this.sbSurcharges.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.sbSurcharges.CloseBorder = false;
            this.sbSurcharges.Height = 0.063F;
            this.sbSurcharges.Left = 5.25F;
            this.sbSurcharges.Name = "sbSurcharges";
            this.sbSurcharges.Report = null;
            this.sbSurcharges.ReportName = "";
            this.sbSurcharges.Top = 0F;
            this.sbSurcharges.Width = 2.25F;
            // 
            // subGoodsRefused
            // 
            this.subGoodsRefused.Border.BottomColor = System.Drawing.Color.Black;
            this.subGoodsRefused.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.subGoodsRefused.Border.LeftColor = System.Drawing.Color.Black;
            this.subGoodsRefused.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.subGoodsRefused.Border.RightColor = System.Drawing.Color.Black;
            this.subGoodsRefused.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.subGoodsRefused.Border.TopColor = System.Drawing.Color.Black;
            this.subGoodsRefused.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.subGoodsRefused.CloseBorder = false;
            this.subGoodsRefused.Height = 0.0625F;
            this.subGoodsRefused.Left = 0F;
            this.subGoodsRefused.Name = "subGoodsRefused";
            this.subGoodsRefused.Report = null;
            this.subGoodsRefused.ReportName = "subGoodsRefused";
            this.subGoodsRefused.Top = 0F;
            this.subGoodsRefused.Width = 6.708F;
            // 
            // groupHeader1
            // 
            this.groupHeader1.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.txtVehicleRegNo,
            this.lblVehicleRegNo,
            this.GoodsTypelbl,
            this.GoodsTypeTxt});
            this.groupHeader1.Height = 0.25F;
            this.groupHeader1.Name = "groupHeader1";
            // 
            // GoodsTypelbl
            // 
            this.GoodsTypelbl.Border.BottomColor = System.Drawing.Color.Black;
            this.GoodsTypelbl.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.GoodsTypelbl.Border.LeftColor = System.Drawing.Color.Black;
            this.GoodsTypelbl.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.GoodsTypelbl.Border.RightColor = System.Drawing.Color.Black;
            this.GoodsTypelbl.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.GoodsTypelbl.Border.TopColor = System.Drawing.Color.Black;
            this.GoodsTypelbl.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.GoodsTypelbl.Height = 0.1875F;
            this.GoodsTypelbl.Left = 3.25F;
            this.GoodsTypelbl.Name = "GoodsTypelbl";
            this.GoodsTypelbl.Style = "color: Black; text-align: center; vertical-align: bottom; ";
            this.GoodsTypelbl.Text = "Goods Type:";
            this.GoodsTypelbl.Top = 0F;
            this.GoodsTypelbl.Visible = false;
            this.GoodsTypelbl.Width = 0.875F;
            // 
            // GoodsTypeTxt
            // 
            this.GoodsTypeTxt.Border.BottomColor = System.Drawing.Color.Black;
            this.GoodsTypeTxt.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.GoodsTypeTxt.Border.LeftColor = System.Drawing.Color.Black;
            this.GoodsTypeTxt.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.GoodsTypeTxt.Border.RightColor = System.Drawing.Color.Black;
            this.GoodsTypeTxt.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.GoodsTypeTxt.Border.TopColor = System.Drawing.Color.Black;
            this.GoodsTypeTxt.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.GoodsTypeTxt.DataField = "GoodsType";
            this.GoodsTypeTxt.Height = 0.1875F;
            this.GoodsTypeTxt.Left = 4.1875F;
            this.GoodsTypeTxt.Name = "GoodsTypeTxt";
            this.GoodsTypeTxt.Style = "color: Black; text-align: center; vertical-align: bottom; ";
            this.GoodsTypeTxt.Text = null;
            this.GoodsTypeTxt.Top = 0F;
            this.GoodsTypeTxt.Visible = false;
            this.GoodsTypeTxt.Width = 1.1875F;
            // 
            // groupFooter1
            // 
            this.groupFooter1.Height = 0.25F;
            this.groupFooter1.Name = "groupFooter1";
            // 
            // rptOriginalGroupageInvoice
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0.5F;
            this.PageSettings.Margins.Left = 0F;
            this.PageSettings.Margins.Right = 0F;
            this.PageSettings.Margins.Top = 0.5F;
            this.PageSettings.Orientation = DataDynamics.ActiveReports.Document.PageOrientation.Portrait;
            this.PageSettings.PaperHeight = 11.69291F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.PageSettings.PaperWidth = 8.267716F;
            this.PrintWidth = 7.5625F;
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.grpOrder);
            this.Sections.Add(this.groupHeader1);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.groupFooter1);
            this.Sections.Add(this.grpOrderFooter);
            this.Sections.Add(this.PageFooter);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                        "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading3", "Normal"));
            this.ReportStart += new System.EventHandler(this.rptGroupageInvoice_ReportStart);
            ((System.ComponentModel.ISupportInitialize)(this.lblIsInvoice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvoiceNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvoiceDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustomerName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblIsPreInvoice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblOurVatNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOurVatNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblYourReference)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYourReference)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblYourVatNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYourVatNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPurchaseOrderReference)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPurchaseOrderReference)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHelpsWithFormattingDontRemove)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotalAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVAT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInvoiceTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVATAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVATPercentage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvoiceTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInvoiceFuelCharge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFuelChargeTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvoiceFuelSurchargeRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInvoiceTerms)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblExtraTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExtraTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSubTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSubTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInvoiceSubTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvoiceSubTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTradingAs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblExtras)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExtrasTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFuelSurcharge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFuelSurchargeRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFuelSurchareAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblExtrasHeading)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblOrderId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCollectionAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDeliveryAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPallets)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblWeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPalletSpaces)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRef)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPageHeaderInvoiceNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageHeaderInvoiceNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCases)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPalletSpaces)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOrderGroupID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustomerRef)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDeliveryOrderNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOrderServiceLevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPallets)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDeliveryAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCollectionAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOrderID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDeliveryDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtForeignRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCases)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVehicleRegNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVehicleRegNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtJobFuelSurchargeAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtJobFuelSurchargeRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblJobFuelSurcharge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtJobExtrasTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblJobExtraTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOrderIDFooter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOrderGroupIDFooter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GoodsTypelbl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GoodsTypeTxt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion
    }
}
