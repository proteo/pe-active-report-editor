﻿using System;
using System.Data;
using System.Drawing;
using System.Collections.Specialized;
using System.Globalization;
using System.Web;
using System.Linq;

using System.Collections.Generic;
using System.Text;
using System.Net.Mail;

using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;

using Orchestrator.Globals;

namespace Orchestrator.Reports.Invoicing.Groupage
{
    public class GroupageReportBase
        : Orchestrator.Reports.ReportBase, Orchestrator.Reports.Invoicing.IInvoiceReportWithDataSet
    {
        protected List<OrderGroupHandlingRecord> _handledOrderGroups = new List<OrderGroupHandlingRecord>();


        /// <summary>
        /// Support struct to record when order groups are handled and how much they have been handled.
        /// </summary>
        public class OrderGroupHandlingRecord
        {
            private int _orderGroupID = 0;
            private bool _extrasHandled = false;

            public OrderGroupHandlingRecord(int orderGroupID)
            {
                _orderGroupID = orderGroupID;
            }

            public int OrderGroupID
            {
                get { return _orderGroupID; }
            }

            public bool ExtrasHandled
            {
                get { return _extrasHandled; }
                set { _extrasHandled = value; }
            }
        }


        public bool OrderGroupHasBeenHandled(int orderGroupID)
        {
            return _handledOrderGroups.Exists(delegate(OrderGroupHandlingRecord orderGroupHandlingRecord)
            {
                return orderGroupHandlingRecord.OrderGroupID == orderGroupID;
            });
        }

        public bool OrderGroupHasBeenHandledWithExtras(int orderGroupID)
        {
            return _handledOrderGroups.Exists(delegate(OrderGroupHandlingRecord orderGroupHandlingRecord)
            {
                return
                    orderGroupHandlingRecord.OrderGroupID == orderGroupID &&
                    orderGroupHandlingRecord.ExtrasHandled;
            });
        }

        protected void CalculateAndShowJobExtras(GroupFooter thisFooter, int orderGroupID, int orderID,
           out decimal extrasForJobTotal, out decimal extrasForJobFuelSurchargeTotal, out decimal foreignExtrasForJobTotal, out decimal foreignExtrasForJobFuelSurchargeTotal)
        {
            extrasForJobTotal = 0;
            extrasForJobFuelSurchargeTotal = 0;
            foreignExtrasForJobTotal = 0;
            foreignExtrasForJobFuelSurchargeTotal = 0;

            IInvoiceReportWithDataSet invoiceReport = this as IInvoiceReportWithDataSet;
            SubReport sbJobExtras = thisFooter.Controls["sbJobExtras"] as SubReport;
            Label lblJobExtraTotal = thisFooter.Controls["lblJobExtraTotal"] as Label;
            TextBox txtJobExtrasTotal = thisFooter.Controls["txtJobExtrasTotal"] as TextBox;

            // Get the Extras for the Order or for the Order Group
            List<DataRow> extrasForJob;

            //If displaying Extras per Order get all that are not considered as surcharges 
            //(i.e. IsDisplayedOnAddUpdateOrder==0)
            //otherwise only get Redelivery Extras as they are ALWAYS displayed with the Order
            if (invoiceReport.PreInvoice.GenerationParameters.Options.ExtraDisplayMethod == eInvoiceDisplayMethod.PerOrder)
            {
                extrasForJob = new List<DataRow>(invoiceReport.GenerationData.Tables[2].Select(
                    string.Format("OrderID={0} AND IsDisplayedOnAddUpdateOrder=0", orderID)));
            }
            else
            {
                extrasForJob = new List<DataRow>(invoiceReport.GenerationData.Tables[2].Select(
                    string.Format("OrderID={0} AND IsDisplayedOnAddUpdateOrder=0 AND IsRedeliveryExtra=1", orderID)));
            }

            foreach (DataRow row in extrasForJob)
            {

                extrasForJobTotal += (decimal)row["ExtraAmount"];
                foreignExtrasForJobTotal += (decimal)row["ForeignAmount"];

                extrasForJobFuelSurchargeTotal += (decimal)row["FuelSurchargeAmount"];
                foreignExtrasForJobFuelSurchargeTotal += (decimal)row["FuelSurchargeForeignAmount"];
            }

            //If Extras are shown per Order and there are Extras for this Order
            //Redelivery Extras are always shown per Order
            if (extrasForJob.Count > 0)
            {
                lblJobExtraTotal.Visible = true;
                txtJobExtrasTotal.Visible = true;
                txtJobExtrasTotal.Text = foreignExtrasForJobTotal.ToString("C");

                //Better to do this in the calling code to minimise side effects
                ////Add to the report's extras total
                //invoiceReport.ExtrasTotal += extrasForJobTotal;
                //invoiceReport.ForeignExtrasTotal += foreignExtrasForJobTotal;

                ////#15566 Add to the extras total for fuel surcharge 
                //m_ExtrasForFuelSurcharge += extrasForJobFuelSurchargeTotal;
                //m_ForeignExtrasForFuelSurcharge += foreignExtrasForJobFuelSurchargeTotal;

                if (invoiceReport.PreInvoice.GenerationParameters.Options.ShowDetailsOfExtras && extrasForJob.Count > 0)
                {
                    sbJobExtras.Visible = true;
                    sbJobExtras.Report = new SubReports.Extras();
                    sbJobExtras.Report.DataSource = extrasForJob.ToArray();
                    sbJobExtras.Report.DataMember = "Table";
                }
                else
                    sbJobExtras.Visible = false;
            }
            else
            {
                sbJobExtras.Visible = false;
                lblJobExtraTotal.Visible = false;
                txtJobExtrasTotal.Visible = false;
            }
        }

        protected DataRow[] GetAndSumSurcharges(int orderGroupID, int orderID,
            out decimal surchargesTotal, out decimal surchargesFuelSurchargeTotal, out decimal foreignSurchargesTotal, out decimal foreignSurchargesFuelSurchargeTotal)
        {
            surchargesTotal = 0;
            surchargesFuelSurchargeTotal = 0;
            foreignSurchargesTotal = 0;
            foreignSurchargesFuelSurchargeTotal = 0;

            IInvoiceReportWithDataSet invoiceReport = this as IInvoiceReportWithDataSet;

            // Get the Surcharges for the Order or for the Order Group
            //List<DataRow> surcharges;
            DataRow[] surcharges;
            //if (orderGroupID > 0)
            //{
            //    // Get the OrderIds for this order group
            //    string OrderIds = string.Empty;
            //    DataRow[] groupedOrderRows;
            //    groupedOrderRows = invoiceReport.GenerationData.Tables[0].Select(string.Format("OrderGroupID = {0}", orderGroupID));

            //    foreach (DataRow groupedOrderRow in groupedOrderRows)
            //        OrderIds += groupedOrderRow["OrderId"] + ",";

            //    surcharges = invoiceReport.GenerationData.Tables[2].Select(
            //        string.Format("OrderID IN ({0}) AND IsDisplayedOnAddUpdateOrder=1", OrderIds));
            //}
            //else
            surcharges = invoiceReport.GenerationData.Tables[2].Select(
                string.Format("OrderID={0} AND IsDisplayedOnAddUpdateOrder=1", orderID));


            //Sum the surcharge amounts to pass back their totals
            foreach (DataRow row in surcharges)
            {
                surchargesTotal += (decimal)row["ExtraAmount"];
                foreignSurchargesTotal += (decimal)row["ForeignAmount"];

                surchargesFuelSurchargeTotal += (decimal)row["FuelSurchargeAmount"];
                foreignSurchargesFuelSurchargeTotal += (decimal)row["FuelSurchargeForeignAmount"];
            }

            return surcharges;
        }

        protected string BuildAddressInformation(string portion)
        {
            if (!string.IsNullOrEmpty(portion))
                return portion + Environment.NewLine;
            else
                return string.Empty;
        }

        protected string BuildAddressInformation(Entities.Address address)
        {
            StringBuilder sbAddress = new StringBuilder();

            sbAddress.Append(BuildAddressInformation(address.AddressLine1));
            sbAddress.Append(BuildAddressInformation(address.AddressLine2));
            sbAddress.Append(BuildAddressInformation(address.AddressLine3));
            sbAddress.Append(BuildAddressInformation(address.PostTown));
            sbAddress.Append(BuildAddressInformation(address.County));
            sbAddress.Append(BuildAddressInformation(address.PostCode));

            return sbAddress.ToString();
        }

        #region IInvoiceReportWithDataSet Members

        private decimal _itemTotal = 0;
        private decimal _extrasTotal = 0;
        private decimal _fuelSurcharge = 0;
        private decimal _netTotal = 0;
        private decimal _taxTotal = 0;
        private decimal _grossTotal = 0;

        private decimal _foreignItemTotal = 0;
        private decimal _foreignExtrasTotal = 0;
        private decimal _foreignFuelSurcharge = 0;
        private decimal _foreignNetTotal = 0;
        private decimal _foreignTaxTotal = 0;
        private decimal _foreignGrossTotal = 0;

        private Entities.PreInvoice _preInvoice = null;
        private DataSet _generationData = null;

        public decimal ItemTotal { get { return _itemTotal; } set { _itemTotal = value; } }
        public decimal ExtrasTotal { get { return _extrasTotal; } set { _extrasTotal = value; } }
        public decimal FuelSurcharge { get { return _fuelSurcharge; } set { _fuelSurcharge = value; } }
        public decimal NetTotal { get { return _netTotal; } set { _netTotal = value; } }
        public decimal TaxTotal { get { return _taxTotal; } set { _taxTotal = value; } }
        public decimal GrossTotal { get { return _grossTotal; } set { _grossTotal = value; } }

        public decimal ForeignItemTotal { get { return _foreignItemTotal; } set { _foreignItemTotal = value; } }
        public decimal ForeignExtrasTotal { get { return _foreignExtrasTotal; } set { _foreignExtrasTotal = value; } }
        public decimal ForeignFuelSurcharge { get { return _foreignFuelSurcharge; } set { _foreignFuelSurcharge = value; } }
        public decimal ForeignNetTotal { get { return _foreignNetTotal; } set { _foreignNetTotal = value; } }
        public decimal ForeignTaxTotal { get { return _foreignTaxTotal; } set { _foreignTaxTotal = value; } }
        public decimal ForeignGrossTotal { get { return _foreignGrossTotal; } set { _foreignGrossTotal = value; } }

        //public List<Entities.InvoiceNominalCodeSubTotal> InvoiceNominalCodeSubTotals { get; set; }
        public Orchestrator.Entities.PreInvoice PreInvoice { get { return _preInvoice; } set { _preInvoice = value; } }
        public DataSet GenerationData { get { return _generationData; } set { _generationData = value; } }

        #endregion
    }
}
