using System;
using System.Data;
using System.Drawing;
using System.Collections.Specialized;
using System.Globalization;
using System.Web;

using System.Collections.Generic;
using System.Text;

using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;

using Orchestrator.Globals;

namespace Orchestrator.Reports.Invoicing
{
    public class rptSubContractorSelfBill : Orchestrator.Reports.ReportBase, Orchestrator.Reports.Invoicing.IInvoiceReport
    {
        #region Report Variables

        private int _currentPage = -1;

        #endregion

        #region Override Header

        protected override void CreateReportHeader()
        {
            this.PageSettings.Orientation = PageOrientation.Portrait;
        }

        private void ReportBase_BeforePrint(object sender, EventArgs e)
        {
            TextBox txtPageCount = (TextBox)this.Sections["PageFooter"].Controls["ftr_txtPageCount"];
            ((Label)this.Sections["PageFooter"].Controls["ftr_lblPage"]).Text = "Page " + txtPageCount.Text;
        }

        protected override void CreatePageFooter()
        {
            Section pageFooter = Sections["PageFooter"];
            if (pageFooter == null)
            {
                // Insert the page footer
                Sections.InsertPageHF();
                pageFooter = Sections["PageFooter"];
            }
            this.Sections["PageFooter"].BeforePrint += new EventHandler(ReportBase_BeforePrint);

            // Page Count TextBox
            TextBox txtPageCount = new TextBox();
            txtPageCount.Name = "ftr_txtPageCount";
            txtPageCount.Text = "##";
            txtPageCount.Visible = false;
            txtPageCount.SummaryType = SummaryType.PageCount;
            txtPageCount.SummaryRunning = SummaryRunning.All;

            // Page Count Label
            Label lblPage = new Label();
            lblPage.Name = "ftr_lblPage";
            lblPage.Width = 5F;
            lblPage.Top = 0.1F;
            lblPage.Left = PrintWidth - lblPage.Width;
            lblPage.Alignment = TextAlignment.Right;

            // Add the controls to the footer
            if (pageFooter != null)
            {
                pageFooter.Controls.AddRange(new ARControl[] { txtPageCount, lblPage });
            }

            if (pageFooter != null && !this.showInvoicePageFooter)
            {
                // make the footer smaller for the standard footer to avoid big gaps in the report.
                pageFooter.Height = 0.2F;
                this.srFooter.Height = 0.2F;
            }

            if (this.invoicePageFooterOnLastPageOnly)
            {
                this.srFooter.Visible = false;
            }
        }

        #endregion

        public rptSubContractorSelfBill()
        {
            ReportOrientation = PageOrientation.Portrait;
            InitializeComponent();

            rptHeader rpt = new rptHeader();
            this.srHeader.Report = rpt;

            rptPageHeader rptPage = new rptPageHeader();
            this.srPageHeader.Report = rptPage;

            rptFooter rpt_ftr = new rptFooter();
            this.srFooter.Report = rpt_ftr;
        }

        private string BuildAddressInformation(string portion)
        {
            if (!string.IsNullOrEmpty(portion))
                return portion + Environment.NewLine;
            else
                return string.Empty;
        }

        private void BuildAddressInformation(Entities.Address address)
        {
            StringBuilder sbAddress = new StringBuilder();

            sbAddress.Append(BuildAddressInformation(address.AddressLine1));
            sbAddress.Append(BuildAddressInformation(address.AddressLine2));
            sbAddress.Append(BuildAddressInformation(address.AddressLine3));
            sbAddress.Append(BuildAddressInformation(address.PostTown));
            sbAddress.Append(BuildAddressInformation(address.County));
            sbAddress.Append(BuildAddressInformation(address.PostCode));

            txtAddress.Text = sbAddress.ToString();
        }

        #region Event Handlers

        private void Detail_Format(object sender, System.EventArgs eArgs)
        {
            Detail thisDetail = sender as Detail;

            TextBox txtPallets = thisDetail.Controls["txtPallets"] as TextBox;
            TextBox txtRate = thisDetail.Controls["txtRate"] as TextBox;

            if (txtPallets != null && txtPallets.Text != null)
                if (int.Parse(txtPallets.Text) == 0)
                    txtPallets.Text = string.Empty;

            try
            {
                if (txtRate != null && !String.IsNullOrEmpty(txtRate.Text))
                    (this as IInvoiceReport).ItemTotal += decimal.Parse(txtRate.Text, NumberStyles.Any, System.Threading.Thread.CurrentThread.CurrentUICulture);
            }
            catch (Exception ex)
            {
                throw;
            }

            if (txtForeignRate != null && txtForeignRate.Text != string.Empty)
            {
                decimal foreignRate = decimal.Parse(txtForeignRate.Text, NumberStyles.Any);

                (this as IInvoiceReport).ForeignItemTotal += foreignRate;
                txtForeignRate.Text = foreignRate.ToString("C");
            }

            switch (txtPalletSpaces.Text)
            {
                case "0.25":
                    txtPalletSpaces.Text = "�";
                    break;
                case "0.50":
                    txtPalletSpaces.Text = "�";
                    break;
                default:
                    decimal output;

                    if (decimal.TryParse(txtPalletSpaces.Text, out output))
                        txtPalletSpaces.Text = string.Format("{0:f0}", output);

                    if (output == 0)
                        txtPalletSpaces.Text = string.Empty;

                    break;
            }
        }

        private void Detail_BeforePrint(object sender, System.EventArgs eArgs)
        {
            Detail thisDetail = sender as Detail;
        }

        private void ReportHeader_Format(object sender, System.EventArgs eArgs)
        {
            //-------------------------------------------------------------------------------------	
            //							Client Account & Address Details Section
            //-------------------------------------------------------------------------------------	
            Facade.IOrganisation facOrganisation = new Facade.Organisation();

            // Client Name
            txtCustomerName.Text = facOrganisation.GetDisplayNameForIdentityId(this.PreInvoice.IdentityID);

            Facade.IAddress facAddress = new Facade.Point();
            Entities.Address address = facAddress.GetForAddressID(this.PreInvoice.GenerationParameters.AddressID);
            if (address != null)
            {
                // Client Address Details
                BuildAddressInformation(address);
            }

            // Configure the invoice date on the report.
            txtInvoiceDate.Text = PreInvoice.GenerationParameters.InvoiceDate.ToString("dd/MM/yy");

            //If Non Self-Bill Sub-Contractor Invoice display the Sub-Contractor Invoice Number in Invoice No field.
            if (PreInvoice.GenerationParameters.InvoiceType == eInvoiceType.SubContract)
                txtInvoiceNo.Text = PreInvoice.GenerationParameters.ClientInvoiceReference;
            else if (PreInvoice.InvoiceID > 0)
            {
                // Set the invoice number.
                txtInvoiceNo.Text = Configuration.InvoicingSelfBillPrefix + PreInvoice.InvoiceID.ToString();
            }
            // Show either the "this is a preview of the invoice" or the "this is a real invoice" text label.
            lblIsPreInvoice.Visible = PreInvoice.InvoiceID == 0;
            lblIsInvoice.Visible = PreInvoice.InvoiceID > 0;

            // Show VAT Number
            bool showHeader = false;
            bool.TryParse(System.Configuration.ConfigurationManager.AppSettings["ShowInvoiceHeader"], out showHeader);

            txtVatNo.Text = System.Configuration.ConfigurationManager.AppSettings["VatNo"];
            txtYourVatNo.Text = facOrganisation.GetVATNumberForIdentityID(this.PreInvoice.IdentityID);

            if (PreInvoice.GenerationParameters.InvoiceType == eInvoiceType.SubContract)
            {
                //Put Sub-Contractors VAT no in the "VAT No" field on the Report - imitation of Sub-Contrators Invoice.
                lblYourVatNo.Visible = txtYourVatNo.Visible = false;
                txtVatNo.Text = txtYourVatNo.Text;
                lblIsInvoice.Text = "INVOICE";
                lblIsPreInvoice.Text = "This invoice has not yet been saved.";
            }
            else if (!showHeader)
                lblVatNo.Visible = txtVatNo.Visible = !showHeader;
        }

        private void PageHeader_Format(object sender, System.EventArgs eArgs)
        {
            _currentPage = this.PageNumber;
            PageHeader ph = sender as PageHeader;

            if (this.PageNumber == 1)
            {
                this.srPageHeader.Visible = false;

                // On any page other that the first page, make give the page header more height.
                ph.Height -= 1.850f;
                foreach (ARControl control in ph.Controls)
                    control.Top -= 1.850f;
            }
            else
                this.srPageHeader.Visible = true;
        }

        private void ReportFooter_Format(object sender, System.EventArgs eArgs)
        {
            if (this.invoicePageFooterOnLastPageOnly)
            {
                this.srFooter.Visible = false;
            }
            // Display the totals;
            txtVATPercentage.Text = PreInvoice.GenerationParameters.TaxRate.ToString("F2") + "%";
            if (PreInvoice.GenerationParameters.Overrides.OverrideAmounts)
            {
                (this as IInvoiceReport).ItemTotal = PreInvoice.GenerationParameters.Overrides.NetAmount;
                txtTotalAmount.Text = PreInvoice.GenerationParameters.Overrides.NetAmount.ToString("C");
                (this as IInvoiceReport).ItemTotal = decimal.Parse(txtTotalAmount.Text, NumberStyles.Any);

                (this as IInvoiceReport).TaxTotal = PreInvoice.GenerationParameters.Overrides.TaxAmount;
                txtVATAmount.Text = PreInvoice.GenerationParameters.Overrides.TaxAmount.ToString("C");
                (this as IInvoiceReport).TaxTotal = decimal.Parse(txtVATAmount.Text, NumberStyles.Any);

                (this as IInvoiceReport).GrossTotal = PreInvoice.GenerationParameters.Overrides.TotalAmount;
                txtInvoiceTotal.Text = PreInvoice.GenerationParameters.Overrides.TotalAmount.ToString("C");
                (this as IInvoiceReport).GrossTotal = decimal.Parse(txtInvoiceTotal.Text, NumberStyles.Any);
            }
            else
            {
                (this as IInvoiceReport).ForeignItemTotal = decimal.Round((ForeignItemTotal + ForeignExtrasTotal + ForeignFuelSurcharge), 2, MidpointRounding.AwayFromZero);
                (this as IInvoiceReport).ItemTotal = decimal.Round((ItemTotal + ExtrasTotal + FuelSurcharge), 2, MidpointRounding.AwayFromZero);
                txtTotalAmount.Text = (this as IInvoiceReport).ForeignItemTotal.ToString("C");

                (this as IInvoiceReport).ForeignTaxTotal = decimal.Round(((this as IInvoiceReport).ForeignItemTotal / 100) * PreInvoice.GenerationParameters.TaxRate, 2, MidpointRounding.AwayFromZero);
                (this as IInvoiceReport).TaxTotal = decimal.Round(((this as IInvoiceReport).ItemTotal / 100) * PreInvoice.GenerationParameters.TaxRate, 2, MidpointRounding.AwayFromZero);
                txtVATAmount.Text = (this as IInvoiceReport).ForeignTaxTotal.ToString("C");

                (this as IInvoiceReport).ForeignGrossTotal = decimal.Round((this as IInvoiceReport).ForeignItemTotal + (this as IInvoiceReport).ForeignTaxTotal, 2, MidpointRounding.AwayFromZero);
                (this as IInvoiceReport).GrossTotal = decimal.Round((this as IInvoiceReport).ItemTotal + (this as IInvoiceReport).TaxTotal, 2, MidpointRounding.AwayFromZero);
                txtInvoiceTotal.Text = (this as IInvoiceReport).ForeignGrossTotal.ToString("C");
            }
        }

        #endregion

        #region ActiveReports Designer generated code
        private DataDynamics.ActiveReports.ReportHeader ReportHeader = null;
        private DataDynamics.ActiveReports.SubReport srHeader = null;
        private DataDynamics.ActiveReports.Label Label18 = null;
        private DataDynamics.ActiveReports.Label Label19 = null;
        private DataDynamics.ActiveReports.TextBox txtInvoiceNo = null;
        private DataDynamics.ActiveReports.TextBox txtInvoiceDate = null;
        private DataDynamics.ActiveReports.TextBox txtCustomerName = null;
        private DataDynamics.ActiveReports.TextBox txtAddress = null;
        private DataDynamics.ActiveReports.Label lblIsPreInvoice = null;
        private DataDynamics.ActiveReports.Label lblIsInvoice = null;
        private DataDynamics.ActiveReports.Label lblYourVatNo = null;
        private DataDynamics.ActiveReports.TextBox txtYourVatNo = null;
        private DataDynamics.ActiveReports.Label lblVatNo = null;
        private DataDynamics.ActiveReports.TextBox txtVatNo = null;
        private DataDynamics.ActiveReports.PageHeader PageHeader = null;
        private DataDynamics.ActiveReports.TextBox lblDate = null;
        private DataDynamics.ActiveReports.TextBox lblReference = null;
        private DataDynamics.ActiveReports.TextBox lblFrom = null;
        private DataDynamics.ActiveReports.TextBox lblTo = null;
        private DataDynamics.ActiveReports.TextBox lblPallets = null;
        private DataDynamics.ActiveReports.TextBox lblWeight = null;
        private DataDynamics.ActiveReports.TextBox lblRef = null;
        private DataDynamics.ActiveReports.TextBox lblValue = null;
        private DataDynamics.ActiveReports.TextBox lblPalletSpaces = null;
        private DataDynamics.ActiveReports.SubReport srPageHeader = null;
        private DataDynamics.ActiveReports.Detail Detail = null;
        private DataDynamics.ActiveReports.TextBox txtDeliveryDate = null;
        private DataDynamics.ActiveReports.TextBox txtReference = null;
        private DataDynamics.ActiveReports.TextBox txtSource = null;
        private DataDynamics.ActiveReports.TextBox txtDestination = null;
        private DataDynamics.ActiveReports.TextBox txtPallets = null;
        private DataDynamics.ActiveReports.TextBox txtWeight = null;
        private DataDynamics.ActiveReports.TextBox txtAdditionalReferences = null;
        private DataDynamics.ActiveReports.TextBox txtRate = null;
        private DataDynamics.ActiveReports.TextBox txtPalletSpaces = null;
        private DataDynamics.ActiveReports.PageFooter PageFooter = null;
        private DataDynamics.ActiveReports.SubReport srFooter = null;
        private DataDynamics.ActiveReports.SubReport sbClientCustomerReportFooter = null;
        private DataDynamics.ActiveReports.ReportFooter ReportFooter = null;
        private DataDynamics.ActiveReports.TextBox txtInvoiceTotal = null;
        private DataDynamics.ActiveReports.Label lblTotalAmount = null;
        private DataDynamics.ActiveReports.Label lblVAT = null;
        private DataDynamics.ActiveReports.Label lblInvoiceTotal = null;
        private DataDynamics.ActiveReports.Line invoiceTotalTopLine = null;
        private DataDynamics.ActiveReports.Line invoiceTotalBaseLine = null;
        private DataDynamics.ActiveReports.TextBox txtTotalAmount = null;
        private DataDynamics.ActiveReports.TextBox txtVATAmount = null;
        private DataDynamics.ActiveReports.TextBox txtVATPercentage = null;
        private DataDynamics.ActiveReports.TextBox txtForeignRate = null;
        private Label lblHelpsWithFormattingDontRemove;
        private DataDynamics.ActiveReports.Line reportFooterLine = null;
        public void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptSubContractorSelfBill));
            this.Detail = new DataDynamics.ActiveReports.Detail();
            this.txtForeignRate = new DataDynamics.ActiveReports.TextBox();
            this.txtDeliveryDate = new DataDynamics.ActiveReports.TextBox();
            this.txtReference = new DataDynamics.ActiveReports.TextBox();
            this.txtSource = new DataDynamics.ActiveReports.TextBox();
            this.txtDestination = new DataDynamics.ActiveReports.TextBox();
            this.txtPallets = new DataDynamics.ActiveReports.TextBox();
            this.txtWeight = new DataDynamics.ActiveReports.TextBox();
            this.txtAdditionalReferences = new DataDynamics.ActiveReports.TextBox();
            this.txtRate = new DataDynamics.ActiveReports.TextBox();
            this.txtPalletSpaces = new DataDynamics.ActiveReports.TextBox();
            this.ReportHeader = new DataDynamics.ActiveReports.ReportHeader();
            this.lblIsInvoice = new DataDynamics.ActiveReports.Label();
            this.lblIsPreInvoice = new DataDynamics.ActiveReports.Label();
            this.srHeader = new DataDynamics.ActiveReports.SubReport();
            this.Label18 = new DataDynamics.ActiveReports.Label();
            this.Label19 = new DataDynamics.ActiveReports.Label();
            this.txtInvoiceNo = new DataDynamics.ActiveReports.TextBox();
            this.txtInvoiceDate = new DataDynamics.ActiveReports.TextBox();
            this.txtCustomerName = new DataDynamics.ActiveReports.TextBox();
            this.txtAddress = new DataDynamics.ActiveReports.TextBox();
            this.lblYourVatNo = new DataDynamics.ActiveReports.Label();
            this.txtYourVatNo = new DataDynamics.ActiveReports.TextBox();
            this.lblVatNo = new DataDynamics.ActiveReports.Label();
            this.txtVatNo = new DataDynamics.ActiveReports.TextBox();
            this.lblHelpsWithFormattingDontRemove = new DataDynamics.ActiveReports.Label();
            this.ReportFooter = new DataDynamics.ActiveReports.ReportFooter();
            this.txtInvoiceTotal = new DataDynamics.ActiveReports.TextBox();
            this.lblTotalAmount = new DataDynamics.ActiveReports.Label();
            this.lblVAT = new DataDynamics.ActiveReports.Label();
            this.lblInvoiceTotal = new DataDynamics.ActiveReports.Label();
            this.invoiceTotalTopLine = new DataDynamics.ActiveReports.Line();
            this.invoiceTotalBaseLine = new DataDynamics.ActiveReports.Line();
            this.txtTotalAmount = new DataDynamics.ActiveReports.TextBox();
            this.txtVATAmount = new DataDynamics.ActiveReports.TextBox();
            this.txtVATPercentage = new DataDynamics.ActiveReports.TextBox();
            this.reportFooterLine = new DataDynamics.ActiveReports.Line();
            this.PageHeader = new DataDynamics.ActiveReports.PageHeader();
            this.lblDate = new DataDynamics.ActiveReports.TextBox();
            this.lblReference = new DataDynamics.ActiveReports.TextBox();
            this.lblFrom = new DataDynamics.ActiveReports.TextBox();
            this.lblTo = new DataDynamics.ActiveReports.TextBox();
            this.lblPallets = new DataDynamics.ActiveReports.TextBox();
            this.lblWeight = new DataDynamics.ActiveReports.TextBox();
            this.lblValue = new DataDynamics.ActiveReports.TextBox();
            this.lblPalletSpaces = new DataDynamics.ActiveReports.TextBox();
            this.srPageHeader = new DataDynamics.ActiveReports.SubReport();
            this.lblRef = new DataDynamics.ActiveReports.TextBox();
            this.PageFooter = new DataDynamics.ActiveReports.PageFooter();
            this.srFooter = new DataDynamics.ActiveReports.SubReport();
            this.sbClientCustomerReportFooter = new DataDynamics.ActiveReports.SubReport();
            ((System.ComponentModel.ISupportInitialize)(this.txtForeignRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDeliveryDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReference)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDestination)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPallets)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAdditionalReferences)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPalletSpaces)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblIsInvoice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblIsPreInvoice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvoiceNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvoiceDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustomerName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblYourVatNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYourVatNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVatNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVatNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHelpsWithFormattingDontRemove)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvoiceTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotalAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVAT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInvoiceTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVATAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVATPercentage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReference)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPallets)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblWeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPalletSpaces)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRef)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.txtForeignRate,
            this.txtDeliveryDate,
            this.txtReference,
            this.txtSource,
            this.txtDestination,
            this.txtPallets,
            this.txtWeight,
            this.txtAdditionalReferences,
            this.txtRate,
            this.txtPalletSpaces});
            this.Detail.Height = 0.1979167F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            this.Detail.BeforePrint += new System.EventHandler(this.Detail_BeforePrint);
            // 
            // txtForeignRate
            // 
            this.txtForeignRate.Border.BottomColor = System.Drawing.Color.Black;
            this.txtForeignRate.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtForeignRate.Border.LeftColor = System.Drawing.Color.Black;
            this.txtForeignRate.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtForeignRate.Border.RightColor = System.Drawing.Color.Black;
            this.txtForeignRate.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtForeignRate.Border.TopColor = System.Drawing.Color.Black;
            this.txtForeignRate.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtForeignRate.DataField = "ForeignRate";
            this.txtForeignRate.Height = 0.1875F;
            this.txtForeignRate.Left = 6.9375F;
            this.txtForeignRate.Name = "txtForeignRate";
            this.txtForeignRate.OutputFormat = resources.GetString("txtForeignRate.OutputFormat");
            this.txtForeignRate.Style = "color: Black; text-align: right; font-size: 8.25pt; ";
            this.txtForeignRate.Text = "Rate";
            this.txtForeignRate.Top = 0F;
            this.txtForeignRate.Width = 0.5625F;
            // 
            // txtDeliveryDate
            // 
            this.txtDeliveryDate.Border.BottomColor = System.Drawing.Color.Black;
            this.txtDeliveryDate.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtDeliveryDate.Border.LeftColor = System.Drawing.Color.Black;
            this.txtDeliveryDate.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtDeliveryDate.Border.RightColor = System.Drawing.Color.Black;
            this.txtDeliveryDate.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtDeliveryDate.Border.TopColor = System.Drawing.Color.Black;
            this.txtDeliveryDate.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtDeliveryDate.DataField = "DeliveryDateTime";
            this.txtDeliveryDate.Height = 0.1875F;
            this.txtDeliveryDate.Left = 0F;
            this.txtDeliveryDate.Name = "txtDeliveryDate";
            this.txtDeliveryDate.OutputFormat = resources.GetString("txtDeliveryDate.OutputFormat");
            this.txtDeliveryDate.Style = "color: Black; text-align: center; font-size: 8.25pt; ";
            this.txtDeliveryDate.Text = "Date";
            this.txtDeliveryDate.Top = 0F;
            this.txtDeliveryDate.Width = 0.6875F;
            // 
            // txtReference
            // 
            this.txtReference.Border.BottomColor = System.Drawing.Color.Black;
            this.txtReference.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtReference.Border.LeftColor = System.Drawing.Color.Black;
            this.txtReference.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtReference.Border.RightColor = System.Drawing.Color.Black;
            this.txtReference.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtReference.Border.TopColor = System.Drawing.Color.Black;
            this.txtReference.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtReference.DataField = "LeadReferenceValue";
            this.txtReference.Height = 0.1875F;
            this.txtReference.Left = 0.6875F;
            this.txtReference.Name = "txtReference";
            this.txtReference.OutputFormat = resources.GetString("txtReference.OutputFormat");
            this.txtReference.Style = "color: Black; text-align: left; font-size: 8.25pt; ";
            this.txtReference.Text = "Lead Ref Val";
            this.txtReference.Top = 0F;
            this.txtReference.Width = 0.8125F;
            // 
            // txtSource
            // 
            this.txtSource.Border.BottomColor = System.Drawing.Color.Black;
            this.txtSource.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtSource.Border.LeftColor = System.Drawing.Color.Black;
            this.txtSource.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtSource.Border.RightColor = System.Drawing.Color.Black;
            this.txtSource.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtSource.Border.TopColor = System.Drawing.Color.Black;
            this.txtSource.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtSource.DataField = "Source";
            this.txtSource.Height = 0.1875F;
            this.txtSource.Left = 1.5F;
            this.txtSource.Name = "txtSource";
            this.txtSource.OutputFormat = resources.GetString("txtSource.OutputFormat");
            this.txtSource.Style = "color: Black; text-align: left; font-size: 8.25pt; ";
            this.txtSource.Text = "From";
            this.txtSource.Top = 0F;
            this.txtSource.Width = 1.3125F;
            // 
            // txtDestination
            // 
            this.txtDestination.Border.BottomColor = System.Drawing.Color.Black;
            this.txtDestination.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtDestination.Border.LeftColor = System.Drawing.Color.Black;
            this.txtDestination.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtDestination.Border.RightColor = System.Drawing.Color.Black;
            this.txtDestination.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtDestination.Border.TopColor = System.Drawing.Color.Black;
            this.txtDestination.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtDestination.DataField = "Destination";
            this.txtDestination.Height = 0.1875F;
            this.txtDestination.Left = 2.8125F;
            this.txtDestination.Name = "txtDestination";
            this.txtDestination.OutputFormat = resources.GetString("txtDestination.OutputFormat");
            this.txtDestination.Style = "color: Black; text-align: left; font-size: 8.25pt; ";
            this.txtDestination.Text = "Destination";
            this.txtDestination.Top = 0F;
            this.txtDestination.Width = 1.3125F;
            // 
            // txtPallets
            // 
            this.txtPallets.Border.BottomColor = System.Drawing.Color.Black;
            this.txtPallets.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtPallets.Border.LeftColor = System.Drawing.Color.Black;
            this.txtPallets.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtPallets.Border.RightColor = System.Drawing.Color.Black;
            this.txtPallets.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtPallets.Border.TopColor = System.Drawing.Color.Black;
            this.txtPallets.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtPallets.DataField = "PalletCount";
            this.txtPallets.Height = 0.1875F;
            this.txtPallets.Left = 4.125F;
            this.txtPallets.Name = "txtPallets";
            this.txtPallets.OutputFormat = resources.GetString("txtPallets.OutputFormat");
            this.txtPallets.Style = "color: Black; text-align: right; font-size: 8.25pt; ";
            this.txtPallets.Text = "Pallets";
            this.txtPallets.Top = 0F;
            this.txtPallets.Width = 0.5F;
            // 
            // txtWeight
            // 
            this.txtWeight.Border.BottomColor = System.Drawing.Color.Black;
            this.txtWeight.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtWeight.Border.LeftColor = System.Drawing.Color.Black;
            this.txtWeight.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtWeight.Border.RightColor = System.Drawing.Color.Black;
            this.txtWeight.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtWeight.Border.TopColor = System.Drawing.Color.Black;
            this.txtWeight.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtWeight.DataField = "Weight";
            this.txtWeight.Height = 0.1875F;
            this.txtWeight.Left = 4.625F;
            this.txtWeight.Name = "txtWeight";
            this.txtWeight.OutputFormat = resources.GetString("txtWeight.OutputFormat");
            this.txtWeight.Style = "color: Black; text-align: right; font-size: 8.25pt; ";
            this.txtWeight.Text = "Weight";
            this.txtWeight.Top = 0F;
            this.txtWeight.Width = 0.75F;
            // 
            // txtAdditionalReferences
            // 
            this.txtAdditionalReferences.Border.BottomColor = System.Drawing.Color.Black;
            this.txtAdditionalReferences.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtAdditionalReferences.Border.LeftColor = System.Drawing.Color.Black;
            this.txtAdditionalReferences.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtAdditionalReferences.Border.RightColor = System.Drawing.Color.Black;
            this.txtAdditionalReferences.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtAdditionalReferences.Border.TopColor = System.Drawing.Color.Black;
            this.txtAdditionalReferences.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtAdditionalReferences.DataField = "AdditionalReferencesDisplay";
            this.txtAdditionalReferences.Height = 0.1875F;
            this.txtAdditionalReferences.Left = 5.875F;
            this.txtAdditionalReferences.Name = "txtAdditionalReferences";
            this.txtAdditionalReferences.OutputFormat = resources.GetString("txtAdditionalReferences.OutputFormat");
            this.txtAdditionalReferences.Style = "color: Black; ddo-char-set: 0; text-align: right; font-size: 8.25pt; ";
            this.txtAdditionalReferences.Text = "Additional Refs";
            this.txtAdditionalReferences.Top = 0F;
            this.txtAdditionalReferences.Width = 1.0625F;
            // 
            // txtRate
            // 
            this.txtRate.Border.BottomColor = System.Drawing.Color.Black;
            this.txtRate.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtRate.Border.LeftColor = System.Drawing.Color.Black;
            this.txtRate.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtRate.Border.RightColor = System.Drawing.Color.Black;
            this.txtRate.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtRate.Border.TopColor = System.Drawing.Color.Black;
            this.txtRate.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtRate.DataField = "Rate";
            this.txtRate.Height = 0.1875F;
            this.txtRate.Left = 7F;
            this.txtRate.Name = "txtRate";
            this.txtRate.OutputFormat = resources.GetString("txtRate.OutputFormat");
            this.txtRate.Style = "color: Black; text-align: right; font-size: 8.25pt; ";
            this.txtRate.Text = null;
            this.txtRate.Top = 0F;
            this.txtRate.Visible = false;
            this.txtRate.Width = 0.5625F;
            // 
            // txtPalletSpaces
            // 
            this.txtPalletSpaces.Border.BottomColor = System.Drawing.Color.Black;
            this.txtPalletSpaces.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtPalletSpaces.Border.LeftColor = System.Drawing.Color.Black;
            this.txtPalletSpaces.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtPalletSpaces.Border.RightColor = System.Drawing.Color.Black;
            this.txtPalletSpaces.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtPalletSpaces.Border.TopColor = System.Drawing.Color.Black;
            this.txtPalletSpaces.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtPalletSpaces.DataField = "PalletSpaces";
            this.txtPalletSpaces.Height = 0.1875F;
            this.txtPalletSpaces.Left = 5.375F;
            this.txtPalletSpaces.Name = "txtPalletSpaces";
            this.txtPalletSpaces.OutputFormat = resources.GetString("txtPalletSpaces.OutputFormat");
            this.txtPalletSpaces.Style = "color: Black; text-align: right; font-size: 8.25pt; ";
            this.txtPalletSpaces.Text = "Spaces";
            this.txtPalletSpaces.Top = 0F;
            this.txtPalletSpaces.Width = 0.5F;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.lblIsInvoice,
            this.lblIsPreInvoice,
            this.srHeader,
            this.Label18,
            this.Label19,
            this.txtInvoiceNo,
            this.txtInvoiceDate,
            this.txtCustomerName,
            this.txtAddress,
            this.lblYourVatNo,
            this.txtYourVatNo,
            this.lblVatNo,
            this.txtVatNo,
            this.lblHelpsWithFormattingDontRemove});
            this.ReportHeader.Height = 3.5F;
            this.ReportHeader.Name = "ReportHeader";
            this.ReportHeader.Format += new System.EventHandler(this.ReportHeader_Format);
            // 
            // lblIsInvoice
            // 
            this.lblIsInvoice.Border.BottomColor = System.Drawing.Color.Black;
            this.lblIsInvoice.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblIsInvoice.Border.LeftColor = System.Drawing.Color.Black;
            this.lblIsInvoice.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblIsInvoice.Border.RightColor = System.Drawing.Color.Black;
            this.lblIsInvoice.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblIsInvoice.Border.TopColor = System.Drawing.Color.Black;
            this.lblIsInvoice.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblIsInvoice.Height = 0.25F;
            this.lblIsInvoice.HyperLink = null;
            this.lblIsInvoice.Left = 0.0625F;
            this.lblIsInvoice.Name = "lblIsInvoice";
            this.lblIsInvoice.Style = "ddo-char-set: 1; text-align: center; font-weight: bold; font-size: 14pt; ";
            this.lblIsInvoice.Text = "THIS IS YOUR SELF-BILL";
            this.lblIsInvoice.Top = 3.1875F;
            this.lblIsInvoice.Width = 7.375F;
            // 
            // lblIsPreInvoice
            // 
            this.lblIsPreInvoice.Border.BottomColor = System.Drawing.Color.Black;
            this.lblIsPreInvoice.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblIsPreInvoice.Border.LeftColor = System.Drawing.Color.Black;
            this.lblIsPreInvoice.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblIsPreInvoice.Border.RightColor = System.Drawing.Color.Black;
            this.lblIsPreInvoice.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblIsPreInvoice.Border.TopColor = System.Drawing.Color.Black;
            this.lblIsPreInvoice.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblIsPreInvoice.Height = 0.1875F;
            this.lblIsPreInvoice.HyperLink = null;
            this.lblIsPreInvoice.Left = 0.0625F;
            this.lblIsPreInvoice.Name = "lblIsPreInvoice";
            this.lblIsPreInvoice.Style = "color: Red; text-align: center; ";
            this.lblIsPreInvoice.Text = "This sub-contractor self-bill has not yet been saved, add self-bill to allocate s" +
                "elf-bill No.";
            this.lblIsPreInvoice.Top = 3.1875F;
            this.lblIsPreInvoice.Width = 7.375F;
            // 
            // srHeader
            // 
            this.srHeader.Border.BottomColor = System.Drawing.Color.Black;
            this.srHeader.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.srHeader.Border.LeftColor = System.Drawing.Color.Black;
            this.srHeader.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.srHeader.Border.RightColor = System.Drawing.Color.Black;
            this.srHeader.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.srHeader.Border.TopColor = System.Drawing.Color.Black;
            this.srHeader.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.srHeader.CloseBorder = false;
            this.srHeader.Height = 1.875F;
            this.srHeader.Left = 0F;
            this.srHeader.Name = "srHeader";
            this.srHeader.Report = null;
            this.srHeader.ReportName = "rptHeader";
            this.srHeader.Top = 0F;
            this.srHeader.Width = 7.5F;
            // 
            // Label18
            // 
            this.Label18.Border.BottomColor = System.Drawing.Color.Black;
            this.Label18.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label18.Border.LeftColor = System.Drawing.Color.Black;
            this.Label18.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label18.Border.RightColor = System.Drawing.Color.Black;
            this.Label18.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label18.Border.TopColor = System.Drawing.Color.Black;
            this.Label18.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label18.Height = 0.1875F;
            this.Label18.HyperLink = null;
            this.Label18.Left = 4.9375F;
            this.Label18.Name = "Label18";
            this.Label18.Style = "ddo-char-set: 1; font-weight: bold; ";
            this.Label18.Text = "Self-bill No:";
            this.Label18.Top = 2.3125F;
            this.Label18.Width = 0.9375F;
            // 
            // Label19
            // 
            this.Label19.Border.BottomColor = System.Drawing.Color.Black;
            this.Label19.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label19.Border.LeftColor = System.Drawing.Color.Black;
            this.Label19.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label19.Border.RightColor = System.Drawing.Color.Black;
            this.Label19.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label19.Border.TopColor = System.Drawing.Color.Black;
            this.Label19.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label19.Height = 0.1875F;
            this.Label19.HyperLink = null;
            this.Label19.Left = 4.9375F;
            this.Label19.Name = "Label19";
            this.Label19.Style = "ddo-char-set: 1; font-weight: bold; ";
            this.Label19.Text = "Self-bill Date:";
            this.Label19.Top = 2.5F;
            this.Label19.Width = 0.9375F;
            // 
            // txtInvoiceNo
            // 
            this.txtInvoiceNo.Border.BottomColor = System.Drawing.Color.Black;
            this.txtInvoiceNo.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtInvoiceNo.Border.LeftColor = System.Drawing.Color.Black;
            this.txtInvoiceNo.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtInvoiceNo.Border.RightColor = System.Drawing.Color.Black;
            this.txtInvoiceNo.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtInvoiceNo.Border.TopColor = System.Drawing.Color.Black;
            this.txtInvoiceNo.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtInvoiceNo.Height = 0.1875F;
            this.txtInvoiceNo.Left = 5.9375F;
            this.txtInvoiceNo.MultiLine = false;
            this.txtInvoiceNo.Name = "txtInvoiceNo";
            this.txtInvoiceNo.Style = "color: Black; ";
            this.txtInvoiceNo.Text = "To Be Issued";
            this.txtInvoiceNo.Top = 2.3125F;
            this.txtInvoiceNo.Width = 1.5F;
            // 
            // txtInvoiceDate
            // 
            this.txtInvoiceDate.Border.BottomColor = System.Drawing.Color.Black;
            this.txtInvoiceDate.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtInvoiceDate.Border.LeftColor = System.Drawing.Color.Black;
            this.txtInvoiceDate.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtInvoiceDate.Border.RightColor = System.Drawing.Color.Black;
            this.txtInvoiceDate.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtInvoiceDate.Border.TopColor = System.Drawing.Color.Black;
            this.txtInvoiceDate.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtInvoiceDate.Height = 0.1875F;
            this.txtInvoiceDate.Left = 5.9375F;
            this.txtInvoiceDate.MultiLine = false;
            this.txtInvoiceDate.Name = "txtInvoiceDate";
            this.txtInvoiceDate.Style = "";
            this.txtInvoiceDate.Text = null;
            this.txtInvoiceDate.Top = 2.5F;
            this.txtInvoiceDate.Width = 1.5F;
            // 
            // txtCustomerName
            // 
            this.txtCustomerName.Border.BottomColor = System.Drawing.Color.Black;
            this.txtCustomerName.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtCustomerName.Border.LeftColor = System.Drawing.Color.Black;
            this.txtCustomerName.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtCustomerName.Border.RightColor = System.Drawing.Color.Black;
            this.txtCustomerName.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtCustomerName.Border.TopColor = System.Drawing.Color.Black;
            this.txtCustomerName.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtCustomerName.DataField = "OrganisationName";
            this.txtCustomerName.Height = 0.1875F;
            this.txtCustomerName.Left = 0.125F;
            this.txtCustomerName.Name = "txtCustomerName";
            this.txtCustomerName.Style = "ddo-char-set: 1; font-weight: bold; ";
            this.txtCustomerName.Text = null;
            this.txtCustomerName.Top = 1.9375F;
            this.txtCustomerName.Width = 3.4375F;
            // 
            // txtAddress
            // 
            this.txtAddress.Border.BottomColor = System.Drawing.Color.Black;
            this.txtAddress.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtAddress.Border.LeftColor = System.Drawing.Color.Black;
            this.txtAddress.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtAddress.Border.RightColor = System.Drawing.Color.Black;
            this.txtAddress.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtAddress.Border.TopColor = System.Drawing.Color.Black;
            this.txtAddress.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtAddress.Height = 0.75F;
            this.txtAddress.Left = 0.125F;
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Style = "ddo-char-set: 1; font-weight: bold; ";
            this.txtAddress.Text = null;
            this.txtAddress.Top = 2.125F;
            this.txtAddress.Width = 2.1875F;
            // 
            // lblYourVatNo
            // 
            this.lblYourVatNo.Border.BottomColor = System.Drawing.Color.Black;
            this.lblYourVatNo.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblYourVatNo.Border.LeftColor = System.Drawing.Color.Black;
            this.lblYourVatNo.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblYourVatNo.Border.RightColor = System.Drawing.Color.Black;
            this.lblYourVatNo.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblYourVatNo.Border.TopColor = System.Drawing.Color.Black;
            this.lblYourVatNo.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblYourVatNo.Height = 0.1875F;
            this.lblYourVatNo.HyperLink = null;
            this.lblYourVatNo.Left = 4.9375F;
            this.lblYourVatNo.Name = "lblYourVatNo";
            this.lblYourVatNo.Style = "ddo-char-set: 1; font-weight: bold; ";
            this.lblYourVatNo.Text = "Your VAT No:";
            this.lblYourVatNo.Top = 2.875F;
            this.lblYourVatNo.Width = 0.9375F;
            // 
            // txtYourVatNo
            // 
            this.txtYourVatNo.Border.BottomColor = System.Drawing.Color.Black;
            this.txtYourVatNo.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtYourVatNo.Border.LeftColor = System.Drawing.Color.Black;
            this.txtYourVatNo.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtYourVatNo.Border.RightColor = System.Drawing.Color.Black;
            this.txtYourVatNo.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtYourVatNo.Border.TopColor = System.Drawing.Color.Black;
            this.txtYourVatNo.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtYourVatNo.Height = 0.1875F;
            this.txtYourVatNo.Left = 5.9375F;
            this.txtYourVatNo.MultiLine = false;
            this.txtYourVatNo.Name = "txtYourVatNo";
            this.txtYourVatNo.Style = "vertical-align: top; ";
            this.txtYourVatNo.Text = null;
            this.txtYourVatNo.Top = 2.875F;
            this.txtYourVatNo.Width = 1.5F;
            // 
            // lblVatNo
            // 
            this.lblVatNo.Border.BottomColor = System.Drawing.Color.Black;
            this.lblVatNo.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblVatNo.Border.LeftColor = System.Drawing.Color.Black;
            this.lblVatNo.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblVatNo.Border.RightColor = System.Drawing.Color.Black;
            this.lblVatNo.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblVatNo.Border.TopColor = System.Drawing.Color.Black;
            this.lblVatNo.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblVatNo.Height = 0.1875F;
            this.lblVatNo.HyperLink = null;
            this.lblVatNo.Left = 4.9375F;
            this.lblVatNo.Name = "lblVatNo";
            this.lblVatNo.Style = "ddo-char-set: 1; font-weight: bold; ";
            this.lblVatNo.Text = "Our VAT No:";
            this.lblVatNo.Top = 2.6875F;
            this.lblVatNo.Width = 0.9375F;
            // 
            // txtVatNo
            // 
            this.txtVatNo.Border.BottomColor = System.Drawing.Color.Black;
            this.txtVatNo.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtVatNo.Border.LeftColor = System.Drawing.Color.Black;
            this.txtVatNo.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtVatNo.Border.RightColor = System.Drawing.Color.Black;
            this.txtVatNo.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtVatNo.Border.TopColor = System.Drawing.Color.Black;
            this.txtVatNo.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtVatNo.Height = 0.1875F;
            this.txtVatNo.Left = 5.9375F;
            this.txtVatNo.MultiLine = false;
            this.txtVatNo.Name = "txtVatNo";
            this.txtVatNo.Style = "vertical-align: top; ";
            this.txtVatNo.Text = null;
            this.txtVatNo.Top = 2.6875F;
            this.txtVatNo.Width = 1.5F;
            // 
            // lblHelpsWithFormattingDontRemove
            // 
            this.lblHelpsWithFormattingDontRemove.Border.BottomColor = System.Drawing.Color.Black;
            this.lblHelpsWithFormattingDontRemove.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblHelpsWithFormattingDontRemove.Border.LeftColor = System.Drawing.Color.Black;
            this.lblHelpsWithFormattingDontRemove.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblHelpsWithFormattingDontRemove.Border.RightColor = System.Drawing.Color.Black;
            this.lblHelpsWithFormattingDontRemove.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblHelpsWithFormattingDontRemove.Border.TopColor = System.Drawing.Color.Black;
            this.lblHelpsWithFormattingDontRemove.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblHelpsWithFormattingDontRemove.Height = 0.1979167F;
            this.lblHelpsWithFormattingDontRemove.HyperLink = null;
            this.lblHelpsWithFormattingDontRemove.Left = 2.875F;
            this.lblHelpsWithFormattingDontRemove.Name = "lblHelpsWithFormattingDontRemove";
            this.lblHelpsWithFormattingDontRemove.Style = "";
            this.lblHelpsWithFormattingDontRemove.Text = "";
            this.lblHelpsWithFormattingDontRemove.Top = 2.6875F;
            this.lblHelpsWithFormattingDontRemove.Width = 1F;
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.txtInvoiceTotal,
            this.lblTotalAmount,
            this.lblVAT,
            this.lblInvoiceTotal,
            this.invoiceTotalTopLine,
            this.invoiceTotalBaseLine,
            this.txtTotalAmount,
            this.txtVATAmount,
            this.txtVATPercentage,
            this.sbClientCustomerReportFooter,
            this.reportFooterLine});
            this.ReportFooter.Height = 0.8125F;
            this.ReportFooter.KeepTogether = true;
            this.ReportFooter.Name = "ReportFooter";
            this.ReportFooter.PrintAtBottom = true;
            this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
            // 
            // txtInvoiceTotal
            // 
            this.txtInvoiceTotal.Border.BottomColor = System.Drawing.Color.Black;
            this.txtInvoiceTotal.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtInvoiceTotal.Border.LeftColor = System.Drawing.Color.Black;
            this.txtInvoiceTotal.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtInvoiceTotal.Border.RightColor = System.Drawing.Color.Black;
            this.txtInvoiceTotal.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtInvoiceTotal.Border.TopColor = System.Drawing.Color.Black;
            this.txtInvoiceTotal.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtInvoiceTotal.Height = 0.2F;
            this.txtInvoiceTotal.Left = 6.737847F;
            this.txtInvoiceTotal.Name = "txtInvoiceTotal";
            this.txtInvoiceTotal.OutputFormat = resources.GetString("txtInvoiceTotal.OutputFormat");
            this.txtInvoiceTotal.Style = "text-align: right; ";
            this.txtInvoiceTotal.Text = null;
            this.txtInvoiceTotal.Top = 0.5625F;
            this.txtInvoiceTotal.Width = 0.7621534F;
            // 
            // lblTotalAmount
            // 
            this.lblTotalAmount.Border.BottomColor = System.Drawing.Color.Black;
            this.lblTotalAmount.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblTotalAmount.Border.LeftColor = System.Drawing.Color.Black;
            this.lblTotalAmount.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblTotalAmount.Border.RightColor = System.Drawing.Color.Black;
            this.lblTotalAmount.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblTotalAmount.Border.TopColor = System.Drawing.Color.Black;
            this.lblTotalAmount.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblTotalAmount.Height = 0.1875F;
            this.lblTotalAmount.HyperLink = null;
            this.lblTotalAmount.Left = 4F;
            this.lblTotalAmount.Name = "lblTotalAmount";
            this.lblTotalAmount.Style = "ddo-char-set: 1; font-weight: bold; ";
            this.lblTotalAmount.Text = "TOTAL AMOUNT";
            this.lblTotalAmount.Top = 0.0625F;
            this.lblTotalAmount.Width = 1.5625F;
            // 
            // lblVAT
            // 
            this.lblVAT.Border.BottomColor = System.Drawing.Color.Black;
            this.lblVAT.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblVAT.Border.LeftColor = System.Drawing.Color.Black;
            this.lblVAT.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblVAT.Border.RightColor = System.Drawing.Color.Black;
            this.lblVAT.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblVAT.Border.TopColor = System.Drawing.Color.Black;
            this.lblVAT.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblVAT.Height = 0.2F;
            this.lblVAT.HyperLink = null;
            this.lblVAT.Left = 4F;
            this.lblVAT.Name = "lblVAT";
            this.lblVAT.Style = "ddo-char-set: 1; font-weight: bold; ";
            this.lblVAT.Text = "VAT @";
            this.lblVAT.Top = 0.25F;
            this.lblVAT.Width = 0.5625F;
            // 
            // lblInvoiceTotal
            // 
            this.lblInvoiceTotal.Border.BottomColor = System.Drawing.Color.Black;
            this.lblInvoiceTotal.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblInvoiceTotal.Border.LeftColor = System.Drawing.Color.Black;
            this.lblInvoiceTotal.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblInvoiceTotal.Border.RightColor = System.Drawing.Color.Black;
            this.lblInvoiceTotal.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblInvoiceTotal.Border.TopColor = System.Drawing.Color.Black;
            this.lblInvoiceTotal.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblInvoiceTotal.Height = 0.2F;
            this.lblInvoiceTotal.HyperLink = null;
            this.lblInvoiceTotal.Left = 4F;
            this.lblInvoiceTotal.Name = "lblInvoiceTotal";
            this.lblInvoiceTotal.Style = "ddo-char-set: 1; font-weight: bold; ";
            this.lblInvoiceTotal.Text = "SELF-BILL TOTAL";
            this.lblInvoiceTotal.Top = 0.5625F;
            this.lblInvoiceTotal.Width = 1.25F;
            // 
            // invoiceTotalTopLine
            // 
            this.invoiceTotalTopLine.Border.BottomColor = System.Drawing.Color.Black;
            this.invoiceTotalTopLine.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.invoiceTotalTopLine.Border.LeftColor = System.Drawing.Color.Black;
            this.invoiceTotalTopLine.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.invoiceTotalTopLine.Border.RightColor = System.Drawing.Color.Black;
            this.invoiceTotalTopLine.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.invoiceTotalTopLine.Border.TopColor = System.Drawing.Color.Black;
            this.invoiceTotalTopLine.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.invoiceTotalTopLine.Height = 0F;
            this.invoiceTotalTopLine.Left = 3.9375F;
            this.invoiceTotalTopLine.LineWeight = 1F;
            this.invoiceTotalTopLine.Name = "invoiceTotalTopLine";
            this.invoiceTotalTopLine.Top = 0.5625F;
            this.invoiceTotalTopLine.Width = 3.625F;
            this.invoiceTotalTopLine.X1 = 3.9375F;
            this.invoiceTotalTopLine.X2 = 7.5625F;
            this.invoiceTotalTopLine.Y1 = 0.5625F;
            this.invoiceTotalTopLine.Y2 = 0.5625F;
            // 
            // invoiceTotalBaseLine
            // 
            this.invoiceTotalBaseLine.Border.BottomColor = System.Drawing.Color.Black;
            this.invoiceTotalBaseLine.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.invoiceTotalBaseLine.Border.LeftColor = System.Drawing.Color.Black;
            this.invoiceTotalBaseLine.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.invoiceTotalBaseLine.Border.RightColor = System.Drawing.Color.Black;
            this.invoiceTotalBaseLine.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.invoiceTotalBaseLine.Border.TopColor = System.Drawing.Color.Black;
            this.invoiceTotalBaseLine.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.invoiceTotalBaseLine.Height = 0F;
            this.invoiceTotalBaseLine.Left = 3.9375F;
            this.invoiceTotalBaseLine.LineWeight = 1F;
            this.invoiceTotalBaseLine.Name = "invoiceTotalBaseLine";
            this.invoiceTotalBaseLine.Top = 0.75F;
            this.invoiceTotalBaseLine.Width = 3.625F;
            this.invoiceTotalBaseLine.X1 = 3.9375F;
            this.invoiceTotalBaseLine.X2 = 7.5625F;
            this.invoiceTotalBaseLine.Y1 = 0.75F;
            this.invoiceTotalBaseLine.Y2 = 0.75F;
            // 
            // txtTotalAmount
            // 
            this.txtTotalAmount.Border.BottomColor = System.Drawing.Color.Black;
            this.txtTotalAmount.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtTotalAmount.Border.LeftColor = System.Drawing.Color.Black;
            this.txtTotalAmount.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtTotalAmount.Border.RightColor = System.Drawing.Color.Black;
            this.txtTotalAmount.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtTotalAmount.Border.TopColor = System.Drawing.Color.Black;
            this.txtTotalAmount.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtTotalAmount.Height = 0.1875F;
            this.txtTotalAmount.Left = 6.75F;
            this.txtTotalAmount.Name = "txtTotalAmount";
            this.txtTotalAmount.OutputFormat = resources.GetString("txtTotalAmount.OutputFormat");
            this.txtTotalAmount.Style = "text-align: right; ";
            this.txtTotalAmount.Text = null;
            this.txtTotalAmount.Top = 0.0625F;
            this.txtTotalAmount.Width = 0.75F;
            // 
            // txtVATAmount
            // 
            this.txtVATAmount.Border.BottomColor = System.Drawing.Color.Black;
            this.txtVATAmount.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtVATAmount.Border.LeftColor = System.Drawing.Color.Black;
            this.txtVATAmount.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtVATAmount.Border.RightColor = System.Drawing.Color.Black;
            this.txtVATAmount.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtVATAmount.Border.TopColor = System.Drawing.Color.Black;
            this.txtVATAmount.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtVATAmount.Height = 0.2F;
            this.txtVATAmount.Left = 6.75F;
            this.txtVATAmount.Name = "txtVATAmount";
            this.txtVATAmount.OutputFormat = resources.GetString("txtVATAmount.OutputFormat");
            this.txtVATAmount.Style = "text-align: right; ";
            this.txtVATAmount.Text = null;
            this.txtVATAmount.Top = 0.25F;
            this.txtVATAmount.Width = 0.75F;
            // 
            // txtVATPercentage
            // 
            this.txtVATPercentage.Border.BottomColor = System.Drawing.Color.Black;
            this.txtVATPercentage.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtVATPercentage.Border.LeftColor = System.Drawing.Color.Black;
            this.txtVATPercentage.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtVATPercentage.Border.RightColor = System.Drawing.Color.Black;
            this.txtVATPercentage.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtVATPercentage.Border.TopColor = System.Drawing.Color.Black;
            this.txtVATPercentage.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtVATPercentage.Height = 0.2F;
            this.txtVATPercentage.Left = 5.5625F;
            this.txtVATPercentage.Name = "txtVATPercentage";
            this.txtVATPercentage.OutputFormat = resources.GetString("txtVATPercentage.OutputFormat");
            this.txtVATPercentage.Style = "ddo-char-set: 1; font-weight: bold; ";
            this.txtVATPercentage.Text = null;
            this.txtVATPercentage.Top = 0.25F;
            this.txtVATPercentage.Width = 0.9375F;
            // 
            // sbClientCustomerReportFooter
            // 
            this.sbClientCustomerReportFooter.Border.BottomColor = System.Drawing.Color.Black;
            this.sbClientCustomerReportFooter.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.sbClientCustomerReportFooter.Border.LeftColor = System.Drawing.Color.Black;
            this.sbClientCustomerReportFooter.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.sbClientCustomerReportFooter.Border.RightColor = System.Drawing.Color.Black;
            this.sbClientCustomerReportFooter.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.sbClientCustomerReportFooter.Border.TopColor = System.Drawing.Color.Black;
            this.sbClientCustomerReportFooter.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.sbClientCustomerReportFooter.CloseBorder = false;
            this.sbClientCustomerReportFooter.Height = 1.3125F;
            this.sbClientCustomerReportFooter.Left = 0F;
            this.sbClientCustomerReportFooter.Name = "sbClientCustomerReportFooter";
            this.sbClientCustomerReportFooter.Report = null;
            this.sbClientCustomerReportFooter.Top = 1.1F;
            this.sbClientCustomerReportFooter.Visible = false;
            this.sbClientCustomerReportFooter.Width = 7.5625F;
            // 
            // reportFooterLine
            // 
            this.reportFooterLine.Border.BottomColor = System.Drawing.Color.Black;
            this.reportFooterLine.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.reportFooterLine.Border.LeftColor = System.Drawing.Color.Black;
            this.reportFooterLine.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.reportFooterLine.Border.RightColor = System.Drawing.Color.Black;
            this.reportFooterLine.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.reportFooterLine.Border.TopColor = System.Drawing.Color.Black;
            this.reportFooterLine.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.reportFooterLine.Height = 0F;
            this.reportFooterLine.Left = 0F;
            this.reportFooterLine.LineWeight = 1F;
            this.reportFooterLine.Name = "reportFooterLine";
            this.reportFooterLine.Top = 0.0625F;
            this.reportFooterLine.Width = 7.5F;
            this.reportFooterLine.X1 = 0F;
            this.reportFooterLine.X2 = 7.5F;
            this.reportFooterLine.Y1 = 0.0625F;
            this.reportFooterLine.Y2 = 0.0625F;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.lblDate,
            this.lblReference,
            this.lblFrom,
            this.lblTo,
            this.lblPallets,
            this.lblWeight,
            this.lblValue,
            this.lblPalletSpaces,
            this.srPageHeader,
            this.lblRef});
            this.PageHeader.Height = 2.09375F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
            // 
            // lblDate
            // 
            this.lblDate.Border.BottomColor = System.Drawing.Color.Black;
            this.lblDate.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblDate.Border.LeftColor = System.Drawing.Color.Black;
            this.lblDate.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblDate.Border.RightColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.lblDate.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblDate.Border.TopColor = System.Drawing.Color.Black;
            this.lblDate.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblDate.Height = 0.2F;
            this.lblDate.Left = 0F;
            this.lblDate.Name = "lblDate";
            this.lblDate.Style = "color: White; text-align: center; background-color: Black; ";
            this.lblDate.Text = "Date";
            this.lblDate.Top = 1.875F;
            this.lblDate.Width = 0.6875F;
            // 
            // lblReference
            // 
            this.lblReference.Border.BottomColor = System.Drawing.Color.Black;
            this.lblReference.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblReference.Border.LeftColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.lblReference.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblReference.Border.RightColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.lblReference.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblReference.Border.TopColor = System.Drawing.Color.Black;
            this.lblReference.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblReference.Height = 0.2F;
            this.lblReference.Left = 0.6875F;
            this.lblReference.Name = "lblReference";
            this.lblReference.Style = "color: White; text-align: center; background-color: Black; ";
            this.lblReference.Text = "Reference";
            this.lblReference.Top = 1.875F;
            this.lblReference.Width = 0.8125F;
            // 
            // lblFrom
            // 
            this.lblFrom.Border.BottomColor = System.Drawing.Color.Black;
            this.lblFrom.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblFrom.Border.LeftColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.lblFrom.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblFrom.Border.RightColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.lblFrom.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblFrom.Border.TopColor = System.Drawing.Color.Black;
            this.lblFrom.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblFrom.Height = 0.2F;
            this.lblFrom.Left = 1.5F;
            this.lblFrom.Name = "lblFrom";
            this.lblFrom.Style = "color: White; text-align: center; background-color: Black; ";
            this.lblFrom.Text = "From";
            this.lblFrom.Top = 1.875F;
            this.lblFrom.Width = 1.3125F;
            // 
            // lblTo
            // 
            this.lblTo.Border.BottomColor = System.Drawing.Color.Black;
            this.lblTo.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblTo.Border.LeftColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.lblTo.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblTo.Border.RightColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.lblTo.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblTo.Border.TopColor = System.Drawing.Color.Black;
            this.lblTo.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblTo.Height = 0.2F;
            this.lblTo.Left = 2.8125F;
            this.lblTo.Name = "lblTo";
            this.lblTo.Style = "color: White; text-align: center; background-color: Black; ";
            this.lblTo.Text = "To";
            this.lblTo.Top = 1.875F;
            this.lblTo.Width = 1.3125F;
            // 
            // lblPallets
            // 
            this.lblPallets.Border.BottomColor = System.Drawing.Color.Black;
            this.lblPallets.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblPallets.Border.LeftColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.lblPallets.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblPallets.Border.RightColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.lblPallets.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblPallets.Border.TopColor = System.Drawing.Color.Black;
            this.lblPallets.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblPallets.Height = 0.2F;
            this.lblPallets.Left = 4.125F;
            this.lblPallets.Name = "lblPallets";
            this.lblPallets.Style = "color: White; text-align: center; background-color: Black; ";
            this.lblPallets.Text = "Pallets";
            this.lblPallets.Top = 1.875F;
            this.lblPallets.Width = 0.5F;
            // 
            // lblWeight
            // 
            this.lblWeight.Border.BottomColor = System.Drawing.Color.Black;
            this.lblWeight.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblWeight.Border.LeftColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.lblWeight.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblWeight.Border.RightColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.lblWeight.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblWeight.Border.TopColor = System.Drawing.Color.Black;
            this.lblWeight.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblWeight.Height = 0.2F;
            this.lblWeight.Left = 4.625F;
            this.lblWeight.Name = "lblWeight";
            this.lblWeight.Style = "color: White; text-align: center; background-color: Black; ";
            this.lblWeight.Text = "Weight(kg)";
            this.lblWeight.Top = 1.875F;
            this.lblWeight.Width = 0.75F;
            // 
            // lblValue
            // 
            this.lblValue.Border.BottomColor = System.Drawing.Color.Black;
            this.lblValue.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblValue.Border.LeftColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.lblValue.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblValue.Border.RightColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.lblValue.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblValue.Border.TopColor = System.Drawing.Color.Black;
            this.lblValue.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblValue.Height = 0.2F;
            this.lblValue.Left = 6.9375F;
            this.lblValue.Name = "lblValue";
            this.lblValue.Style = "color: White; text-align: center; background-color: Black; ";
            this.lblValue.Text = "Rate";
            this.lblValue.Top = 1.875F;
            this.lblValue.Width = 0.5625F;
            // 
            // lblPalletSpaces
            // 
            this.lblPalletSpaces.Border.BottomColor = System.Drawing.Color.Black;
            this.lblPalletSpaces.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblPalletSpaces.Border.LeftColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.lblPalletSpaces.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblPalletSpaces.Border.RightColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.lblPalletSpaces.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblPalletSpaces.Border.TopColor = System.Drawing.Color.Black;
            this.lblPalletSpaces.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblPalletSpaces.CanGrow = false;
            this.lblPalletSpaces.Height = 0.2F;
            this.lblPalletSpaces.Left = 5.375F;
            this.lblPalletSpaces.Name = "lblPalletSpaces";
            this.lblPalletSpaces.Style = "color: White; text-align: left; background-color: Black; ";
            this.lblPalletSpaces.Text = "Spaces";
            this.lblPalletSpaces.Top = 1.875F;
            this.lblPalletSpaces.Width = 0.5625F;
            // 
            // srPageHeader
            // 
            this.srPageHeader.Border.BottomColor = System.Drawing.Color.Black;
            this.srPageHeader.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.srPageHeader.Border.LeftColor = System.Drawing.Color.Black;
            this.srPageHeader.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.srPageHeader.Border.RightColor = System.Drawing.Color.Black;
            this.srPageHeader.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.srPageHeader.Border.TopColor = System.Drawing.Color.Black;
            this.srPageHeader.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.srPageHeader.CloseBorder = false;
            this.srPageHeader.Height = 1.875F;
            this.srPageHeader.Left = 0F;
            this.srPageHeader.Name = "srPageHeader";
            this.srPageHeader.Report = null;
            this.srPageHeader.ReportName = "";
            this.srPageHeader.Top = 0F;
            this.srPageHeader.Width = 7.5F;
            // 
            // lblRef
            // 
            this.lblRef.Border.BottomColor = System.Drawing.Color.Black;
            this.lblRef.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblRef.Border.LeftColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.lblRef.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblRef.Border.RightColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.lblRef.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblRef.Border.TopColor = System.Drawing.Color.Black;
            this.lblRef.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblRef.Height = 0.2F;
            this.lblRef.Left = 5.875F;
            this.lblRef.Name = "lblRef";
            this.lblRef.Style = "color: White; text-align: center; background-color: Black; ";
            this.lblRef.Text = "Additional Refs.";
            this.lblRef.Top = 1.875F;
            this.lblRef.Width = 1.0625F;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.srFooter});
            this.PageFooter.Height = 1.3125F;
            this.PageFooter.Name = "PageFooter";
            // 
            // srFooter
            // 
            this.srFooter.Border.BottomColor = System.Drawing.Color.Black;
            this.srFooter.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.srFooter.Border.LeftColor = System.Drawing.Color.Black;
            this.srFooter.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.srFooter.Border.RightColor = System.Drawing.Color.Black;
            this.srFooter.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.srFooter.Border.TopColor = System.Drawing.Color.Black;
            this.srFooter.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.srFooter.CloseBorder = false;
            this.srFooter.Height = 1.3125F;
            this.srFooter.Left = 0F;
            this.srFooter.Name = "srFooter";
            this.srFooter.Report = null;
            this.srFooter.Top = 0F;
            this.srFooter.Width = 7.5F;
            // 
            // rptSubContractorSelfBill
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11.69F;
            this.PageSettings.PaperWidth = 8.27F;
            this.PrintWidth = 7.510417F;
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                        "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading3", "Normal"));
            this.ReportStart += new System.EventHandler(this.rptSubContractorSelfBill_ReportStart);
            ((System.ComponentModel.ISupportInitialize)(this.txtForeignRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDeliveryDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReference)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDestination)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPallets)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAdditionalReferences)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPalletSpaces)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblIsInvoice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblIsPreInvoice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvoiceNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvoiceDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustomerName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblYourVatNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYourVatNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVatNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVatNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHelpsWithFormattingDontRemove)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvoiceTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotalAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVAT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInvoiceTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVATAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVATPercentage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReference)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPallets)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblWeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPalletSpaces)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRef)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        #region IInvoiceReport Members

        private decimal _itemTotal = 0;
        private decimal _extrasTotal = 0;
        private decimal _fuelSurcharge = 0;
        private decimal _netTotal = 0;
        private decimal _taxTotal = 0;
        private decimal _grossTotal = 0;

        private decimal _foreignItemTotal = 0;
        private decimal _foreignExtrasTotal = 0;
        private decimal _foreignFuelSurcharge = 0;
        private decimal _foreignNetTotal = 0;
        private decimal _foreignTaxTotal = 0;
        private decimal _foreignGrossTotal = 0;

        private Entities.PreInvoice _preInvoice = null;

        public decimal ItemTotal { get { return _itemTotal; } set { _itemTotal = value; } }
        public decimal ExtrasTotal { get { return _extrasTotal; } set { _extrasTotal = value; } }
        public decimal FuelSurcharge { get { return _fuelSurcharge; } set { _fuelSurcharge = value; } }
        public decimal NetTotal { get { return _netTotal; } set { _netTotal = value; } }
        public decimal TaxTotal { get { return _taxTotal; } set { _taxTotal = value; } }
        public decimal GrossTotal { get { return _grossTotal; } set { _grossTotal = value; } }

        public decimal ForeignItemTotal { get { return _foreignItemTotal; } set { _foreignItemTotal = value; } }
        public decimal ForeignExtrasTotal { get { return _foreignExtrasTotal; } set { _foreignExtrasTotal = value; } }
        public decimal ForeignFuelSurcharge { get { return _foreignFuelSurcharge; } set { _foreignFuelSurcharge = value; } }
        public decimal ForeignNetTotal { get { return _foreignNetTotal; } set { _foreignNetTotal = value; } }
        public decimal ForeignTaxTotal { get { return _foreignTaxTotal; } set { _foreignTaxTotal = value; } }
        public decimal ForeignGrossTotal { get { return _foreignGrossTotal; } set { _foreignGrossTotal = value; } }

        //public List<Entities.InvoiceNominalCodeSubTotal> InvoiceNominalCodeSubTotals { get; set; }
        public Orchestrator.Entities.PreInvoice PreInvoice { get { return _preInvoice; } set { _preInvoice = value; } }

        #endregion

        private void rptSubContractorSelfBill_ReportStart(object sender, EventArgs e)
        {

        }
    }
}
