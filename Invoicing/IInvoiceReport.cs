using System;
using System.Collections.Generic;
using System.Text;

using System.Data;

namespace Orchestrator.Reports.Invoicing
{
    internal interface IInvoiceReport
    {
        decimal ItemTotal { get; set; }
        decimal ExtrasTotal { get; set; }
        decimal FuelSurcharge { get; set; }
        decimal TaxTotal { get; set; }
        decimal GrossTotal { get; set; }

        decimal ForeignItemTotal { get; set; }
        decimal ForeignExtrasTotal { get; set; }
        decimal ForeignFuelSurcharge { get; set; }
        decimal ForeignTaxTotal { get; set; }
        decimal ForeignGrossTotal { get; set; }

        //List<Entities.InvoiceNominalCodeSubTotal> InvoiceNominalCodeSubTotals { get; set; }

        Entities.PreInvoice PreInvoice { get; set; }
    }

    internal interface IInvoiceReportWithDataSet : IInvoiceReport
    {
        DataSet GenerationData { get; set; }
    }
}
