using System;
using System.Data;
using System.Drawing;
using System.Collections.Specialized;
using System.Globalization;
using System.Web;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;

using Orchestrator.Globals;
using System.Threading;
namespace Orchestrator.Reports.Invoicing
{
	public class rptCreditNoteOneLiner : Orchestrator.Reports.ReportBase
	{
		#region Report Variables
		private decimal VATrate = 0;
		private decimal VATAmount = 0;
		private decimal netAmount = 0;
		private decimal grossAmount = 0;
        private Label lblYourVatNo;
        private TextBox txtYourVatNo;
        private RichTextBox txtCustomNotes;
        private NameValueCollection reportParams = (NameValueCollection)HttpContext.Current.Session[Orchestrator.Globals.Constants.ReportParamsSessionVariable];
        private SubReport srFooter;
        CultureInfo culture = new CultureInfo("en-GB");
        #endregion

        #region Override Header
        protected override void CreateReportHeader()
		{
		}

        private void ReportBase_BeforePrint(object sender, EventArgs e)
        {
            TextBox txtPageCount = (TextBox)this.Sections["PageFooter"].Controls["ftr_txtPageCount"];
            ((Label)this.Sections["PageFooter"].Controls["ftr_lblPage"]).Text = "Page " + txtPageCount.Text;
        }

        protected override void CreatePageFooter()
        {
            // Call the base class' header creation method so we have a created header in place
            Section pageFooter = Sections["PageFooter"];
            if (pageFooter == null)
            {
                // Insert the page footer
                Sections.InsertPageHF();
                pageFooter = Sections["PageFooter"];
            }
            this.Sections["PageFooter"].BeforePrint += new EventHandler(ReportBase_BeforePrint);

            // Page Count TextBox
            TextBox txtPageCount = new TextBox();
            txtPageCount.Name = "ftr_txtPageCount";
            txtPageCount.Text = "##";
            txtPageCount.Visible = false;
            txtPageCount.SummaryType = SummaryType.PageCount;
            txtPageCount.SummaryRunning = SummaryRunning.All;

            // Page Count Label
            Label lblPage = new Label();
            lblPage.Name = "ftr_lblPage";
            lblPage.Width = 5F;
            lblPage.Top = 0.1F;
            lblPage.Left = PrintWidth - lblPage.Width;
            lblPage.Alignment = TextAlignment.Right;

            // Add the controls to the footer
            if (pageFooter != null)
            {
                pageFooter.Controls.AddRange(new ARControl[] { txtPageCount, lblPage });
            }

            if (PageFooter != null && !this.showInvoicePageFooter)
            {
                // make the footer smaller for the standard footer to avoid big gaps in the report.
                PageFooter.Height = 0.2F;
                this.srFooter.Height = 0.2F;
            }
        }
		#endregion

		#region Page/Load/Init/Error
		public rptCreditNoteOneLiner() 
		{
			InitializeComponent();

			this.PageSettings.Orientation = PageOrientation.Portrait;
            this.PageSettings.Margins.Left = 0.4F;
            this.PageSettings.Margins.Right = 0.4F;
            this.PageSettings.Margins.Top = 0.1F;
            this.PageSettings.Margins.Bottom = 0.1F;

            rptHeader rpt = new rptHeader();
            this.srHeader.Report = rpt;

            rptFooter rpt_ftr = new rptFooter();
            this.srFooter.Report = rpt_ftr;

            lblTradingAs.Text = System.Web.Configuration.WebConfigurationManager.AppSettings["TradingAs"];

            // this can now be turned off on a per print basis.
            if (!string.IsNullOrEmpty(reportParams.Get("UseHeadedPaper")))
            {
                if (bool.Parse(reportParams.Get("UseHeadedPaper")) == true)
                {
                    this.srFooter.Visible = false;
                    this.srHeader.Visible = false;
                    //this.srPageHeader.Visible = false;
                }
            }

        }

        // this method is called from the reportRunner before the report is run ie before the report_start event.
        public override void SetReportCulture()
        {
            Facade.IOrganisation facOrg = new Facade.Organisation();
            Entities.Organisation org = facOrg.GetForIdentityId(int.Parse(reportParams.Get("ClientId")));
            culture = new CultureInfo(org.LCID);
        }
		#endregion

		#region Events and Methods
		private void ReportHeader_Format(object sender, EventArgs e)
		{
			//-------------------------------------------------------------------------------------	
			//							Client Account & Address Details Section
			//-------------------------------------------------------------------------------------	
		
				Facade.IInvoice facInv = new Facade.Invoice();
			
				DataSet ds = facInv.GetInvoiceDetailsForClientId(int.Parse(reportParams.Get("ClientId")));
			try
			{
				if (ds != null)
				{
                    int identityID = int.Parse(reportParams.Get("ClientId"));
                    Facade.IOrganisation facOrganisation = new Facade.Organisation();
                    txtCustomerName.Text = facOrganisation.GetDisplayNameForIdentityId(identityID);

                    DataSet dsDefaults = facOrganisation.GetDefaultsForIdentityId(identityID);

                    if (dsDefaults.Tables[0].Rows.Count > 0)
                        this.lblNote.Text = dsDefaults.Tables[0].Rows[0]["PaymentTerms"].ToString();

                    string vatNo = facOrganisation.GetVATNumberForIdentityID(int.Parse(reportParams.Get("ClientId")));
                    // set the client Vat number
                    if (String.IsNullOrEmpty(vatNo))
                    {
                        this.txtYourVatNo.Visible = false;
                        this.lblYourVatNo.Visible = false;
                    }
                    else if (vatNo.ToLower().Trim() == "not set")
                    {
                        this.txtYourVatNo.Visible = false;
                        this.lblYourVatNo.Visible = false;
                    }
                    else
                    {
                        this.txtYourVatNo.Visible = true;
                        this.lblYourVatNo.Visible = true;
                        this.txtYourVatNo.Text = vatNo;
                    }

					// Client Address Details
					string mAddress = string.Empty;

                    if (Convert.ToString(ds.Tables[0].Rows[0]["AddressLine1"]) != "")
                        mAddress = Convert.ToString(ds.Tables[0].Rows[0]["AddressLine1"]) + "," + Environment.NewLine;

                    if (Convert.ToString(ds.Tables[0].Rows[0]["AddressLine2"]) != "")
                        mAddress += Convert.ToString(ds.Tables[0].Rows[0]["AddressLine2"]) + "," + Environment.NewLine;

                    if (Convert.ToString(ds.Tables[0].Rows[0]["AddressLine3"]) != "")
                        mAddress += Convert.ToString(ds.Tables[0].Rows[0]["AddressLine3"]) + "," + Environment.NewLine;

                    if (Convert.ToString(ds.Tables[0].Rows[0]["PostTown"]) != "")
                        mAddress += Convert.ToString(ds.Tables[0].Rows[0]["PostTown"]) + "," + Environment.NewLine;

                    if (Convert.ToString(ds.Tables[0].Rows[0]["County"]) != "")
                        mAddress += Convert.ToString(ds.Tables[0].Rows[0]["County"]) + "," + Environment.NewLine;

                    mAddress += Convert.ToString(ds.Tables[0].Rows[0]["PostCode"]);  

					txtAddress.Text = mAddress;

//					// Client Address Details
//					txtCustomerName.Text = reportParams.Get("Client").ToString();
//					txtAddress.Text = Convert.ToString(ds.Tables[0].Rows[0]["AddressLine1"]);;  
//					txtAddress1.Text = Convert.ToString(ds.Tables[0].Rows[0]["AddressLine3"]);;  
//					txtPostTown.Text = Convert.ToString(ds.Tables[0].Rows[0]["PostTown"]);;  
//					txtPostCode.Text = Convert.ToString(ds.Tables[0].Rows[0]["PostCode"]);;  
//		
					// Client Account Details
					txtAccountNo.Text = Convert.ToString(ds.Tables[0].Rows[0]["AccountCode"]);
				}
			}
			catch
			{

			}

			lblCreditNoteNotice.Visible = true; 

			//txtInvoiceDate.Text = DateTime.Now.ToShortDateString();
			if (reportParams.Get("CreditNoteDate") != null)
				txtCreditNoteDate.Text = reportParams.Get("CreditNoteDate").ToString();
			
			if (reportParams.Get("CreditNoteId") != "0")  
			{
                //TODO will probably need a Credit Note prefix
                //txtCreditNoteNo.Text = Configuration.InvoicingInvoicePrefix + reportParams.Get("CreditNoteId"); 
                txtCreditNoteNo.Text = reportParams.Get("CreditNoteId"); 
				txtCreditNoteNo.ForeColor = Color.Black; 
				lblCreditNoteNotice.Visible = false;
			}

            // Reason 
            if (reportParams.Get("Reason").ToString() != string.Empty)
                txtReason.Text = reportParams.Get("Reason").ToString();

            //// VAT, NET AMOUNT, TOTAL AMOUNT

            //netAmount = Decimal.Parse(reportParams.Get("NetAmount"));

            SetReportCulture();

            Decimal.TryParse(reportParams.Get("NetAmount"), NumberStyles.Currency, Thread.CurrentThread.CurrentUICulture, out netAmount);

            this.txtAmountDetail.Text = netAmount.ToString("C", culture);
            this.txtUnitTotal.Text = netAmount.ToString("C", culture);

            // Show VAT Number
            bool showHeader = false;
            bool.TryParse(System.Configuration.ConfigurationManager.AppSettings["ShowInvoiceHeader"], out showHeader);

            lblOurVatNo.Visible = !showHeader;
            txtOurVatNo.Visible = !showHeader;
            txtOurVatNo.Text = System.Configuration.ConfigurationManager.AppSettings["VatNo"];
        }

		private void Detail_Format(object sender, EventArgs e)
		{
		}
		
		private void ReportFooter_Format(object sender, EventArgs e)
		{
			// Reason 
			if (reportParams.Get("Reason").ToString() != string.Empty)
				txtReason.Text = reportParams.Get("Reason").ToString();
			
			// VAT, NET AMOUNT, TOTAL AMOUNT
            VATrate = Decimal.Parse(reportParams.Get("VATrate"), NumberStyles.Currency, Thread.CurrentThread.CurrentUICulture) / 100;
            netAmount = Decimal.Parse(reportParams.Get("NetAmount"), NumberStyles.Currency, Thread.CurrentThread.CurrentUICulture);
            VATAmount = netAmount * VATrate;
            grossAmount = netAmount + VATAmount;

            this.txtAmountDetail.Text = netAmount.ToString("C", culture);
            this.txtTotalAmount.Text = netAmount.ToString("C", culture);

            this.txtVATPercentage.Text = VATrate.ToString("0.00%");
            this.txtVATAmount.Text = VATAmount.ToString("C", culture);
            this.txtCreditNoteTotal.Text = grossAmount.ToString("C", culture);

            //TODO Are payment Terms required for Credit Notes?
            DataAccess.IOrganisationDefault DAL = new DataAccess.Organisation();
            DataSet dsDefaults = DAL.GetOrganisationDefaultsForIdentityId(int.Parse(reportParams.Get("ClientId")));
            if (dsDefaults.Tables[0].Rows.Count > 0 && ((string)dsDefaults.Tables[0].Rows[0]["PaymentTerms"]).Length > 0)
                this.lblNote.Text = dsDefaults.Tables[0].Rows[0]["PaymentTerms"].ToString();
            else
                lblNote.Visible = false;

            if (Globals.Configuration.InvoiceMessageForTypes.IndexOf("rptCreditNoteOneLiner") > -1
                && Globals.Configuration.ShowInvoiceMessage)
            {
                this.txtCustomNotes.Text = String.Empty;
                this.txtCustomNotes.Html = Globals.Configuration.CustomInvoiceMessage;
            }
            else
            {
                this.txtCustomNotes.Html = String.Empty;
                this.txtCustomNotes.Text = String.Empty;
            }
        }
		#endregion

		#region ActiveReports Designer generated code
		private DataDynamics.ActiveReports.ReportHeader ReportHeader = null;
		private DataDynamics.ActiveReports.Label Label18 = null;
		private DataDynamics.ActiveReports.Label Label19 = null;
		private DataDynamics.ActiveReports.Label Label20 = null;
		private DataDynamics.ActiveReports.TextBox txtCreditNoteNo = null;
		private DataDynamics.ActiveReports.TextBox txtCreditNoteDate = null;
		private DataDynamics.ActiveReports.TextBox txtAccountNo = null;
		private DataDynamics.ActiveReports.Label lblCreditNoteNotice = null;
		private DataDynamics.ActiveReports.TextBox txtCustomerName = null;
		private DataDynamics.ActiveReports.TextBox txtAddress = null;
		private DataDynamics.ActiveReports.Label lblReason = null;
		private DataDynamics.ActiveReports.TextBox txtReason = null;
		private DataDynamics.ActiveReports.TextBox txtAmountDetail = null;
		private DataDynamics.ActiveReports.Label Label = null;
		private DataDynamics.ActiveReports.Label Label1 = null;
		private DataDynamics.ActiveReports.TextBox txtUnitTotal = null;
		private DataDynamics.ActiveReports.Label lblHelpsWithFormattingDontRemove = null;
		private DataDynamics.ActiveReports.SubReport srHeader = null;
		private DataDynamics.ActiveReports.Label lblOurVatNo = null;
		private DataDynamics.ActiveReports.TextBox txtOurVatNo = null;
		private DataDynamics.ActiveReports.PageHeader PageHeader = null;
		private DataDynamics.ActiveReports.Detail Detail = null;
        private DataDynamics.ActiveReports.PageFooter PageFooter = null;
		private DataDynamics.ActiveReports.ReportFooter ReportFooter = null;
		private DataDynamics.ActiveReports.Label lblNote = null;
		private DataDynamics.ActiveReports.Label lblTotalAmount = null;
		private DataDynamics.ActiveReports.Label lblVAT = null;
		private DataDynamics.ActiveReports.Label lblCreditNoteTotal = null;
		private DataDynamics.ActiveReports.TextBox txtTotalAmount = null;
		private DataDynamics.ActiveReports.TextBox txtVATAmount = null;
		private DataDynamics.ActiveReports.TextBox txtVATPercentage = null;
		private DataDynamics.ActiveReports.TextBox txtCreditNoteTotal = null;
		private DataDynamics.ActiveReports.Line Line10 = null;
		private DataDynamics.ActiveReports.Line Line9 = null;
		private DataDynamics.ActiveReports.Label lblTradingAs = null;
		public void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptCreditNoteOneLiner));
            this.Detail = new DataDynamics.ActiveReports.Detail();
            this.ReportHeader = new DataDynamics.ActiveReports.ReportHeader();
            this.Label18 = new DataDynamics.ActiveReports.Label();
            this.Label19 = new DataDynamics.ActiveReports.Label();
            this.Label20 = new DataDynamics.ActiveReports.Label();
            this.txtCreditNoteNo = new DataDynamics.ActiveReports.TextBox();
            this.txtCreditNoteDate = new DataDynamics.ActiveReports.TextBox();
            this.txtAccountNo = new DataDynamics.ActiveReports.TextBox();
            this.lblCreditNoteNotice = new DataDynamics.ActiveReports.Label();
            this.txtCustomerName = new DataDynamics.ActiveReports.TextBox();
            this.txtAddress = new DataDynamics.ActiveReports.TextBox();
            this.lblReason = new DataDynamics.ActiveReports.Label();
            this.txtReason = new DataDynamics.ActiveReports.TextBox();
            this.txtAmountDetail = new DataDynamics.ActiveReports.TextBox();
            this.Label = new DataDynamics.ActiveReports.Label();
            this.Label1 = new DataDynamics.ActiveReports.Label();
            this.txtUnitTotal = new DataDynamics.ActiveReports.TextBox();
            this.lblHelpsWithFormattingDontRemove = new DataDynamics.ActiveReports.Label();
            this.srHeader = new DataDynamics.ActiveReports.SubReport();
            this.lblOurVatNo = new DataDynamics.ActiveReports.Label();
            this.txtOurVatNo = new DataDynamics.ActiveReports.TextBox();
            this.lblYourVatNo = new DataDynamics.ActiveReports.Label();
            this.txtYourVatNo = new DataDynamics.ActiveReports.TextBox();
            this.ReportFooter = new DataDynamics.ActiveReports.ReportFooter();
            this.lblNote = new DataDynamics.ActiveReports.Label();
            this.lblTotalAmount = new DataDynamics.ActiveReports.Label();
            this.lblVAT = new DataDynamics.ActiveReports.Label();
            this.lblCreditNoteTotal = new DataDynamics.ActiveReports.Label();
            this.txtTotalAmount = new DataDynamics.ActiveReports.TextBox();
            this.txtVATAmount = new DataDynamics.ActiveReports.TextBox();
            this.txtVATPercentage = new DataDynamics.ActiveReports.TextBox();
            this.txtCreditNoteTotal = new DataDynamics.ActiveReports.TextBox();
            this.Line10 = new DataDynamics.ActiveReports.Line();
            this.Line9 = new DataDynamics.ActiveReports.Line();
            this.lblTradingAs = new DataDynamics.ActiveReports.Label();
            this.txtCustomNotes = new DataDynamics.ActiveReports.RichTextBox();
            this.PageHeader = new DataDynamics.ActiveReports.PageHeader();
            this.PageFooter = new DataDynamics.ActiveReports.PageFooter();
            this.srFooter = new DataDynamics.ActiveReports.SubReport();
            ((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCreditNoteNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCreditNoteDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCreditNoteNotice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustomerName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReason)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmountDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUnitTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHelpsWithFormattingDontRemove)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblOurVatNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOurVatNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblYourVatNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYourVatNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNote)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotalAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVAT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCreditNoteTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVATAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVATPercentage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCreditNoteTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTradingAs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Height = 0F;
            this.Detail.Name = "Detail";
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.Label18,
            this.Label19,
            this.Label20,
            this.txtCreditNoteNo,
            this.txtCreditNoteDate,
            this.txtAccountNo,
            this.lblCreditNoteNotice,
            this.txtCustomerName,
            this.txtAddress,
            this.lblReason,
            this.txtReason,
            this.txtAmountDetail,
            this.Label,
            this.Label1,
            this.txtUnitTotal,
            this.lblHelpsWithFormattingDontRemove,
            this.srHeader,
            this.lblOurVatNo,
            this.txtOurVatNo,
            this.lblYourVatNo,
            this.txtYourVatNo});
            this.ReportHeader.Height = 3.6875F;
            this.ReportHeader.Name = "ReportHeader";
            this.ReportHeader.Format += new System.EventHandler(this.ReportHeader_Format);
            // 
            // Label18
            // 
            this.Label18.Border.BottomColor = System.Drawing.Color.Black;
            this.Label18.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label18.Border.LeftColor = System.Drawing.Color.Black;
            this.Label18.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label18.Border.RightColor = System.Drawing.Color.Black;
            this.Label18.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label18.Border.TopColor = System.Drawing.Color.Black;
            this.Label18.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label18.Height = 0.2F;
            this.Label18.HyperLink = null;
            this.Label18.Left = 5.125F;
            this.Label18.Name = "Label18";
            this.Label18.Style = "ddo-char-set: 1; text-align: left; font-weight: bold; ";
            this.Label18.Text = "Credit Note No:";
            this.Label18.Top = 1.9375F;
            this.Label18.Width = 1.1875F;
            // 
            // Label19
            // 
            this.Label19.Border.BottomColor = System.Drawing.Color.Black;
            this.Label19.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label19.Border.LeftColor = System.Drawing.Color.Black;
            this.Label19.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label19.Border.RightColor = System.Drawing.Color.Black;
            this.Label19.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label19.Border.TopColor = System.Drawing.Color.Black;
            this.Label19.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label19.Height = 0.2F;
            this.Label19.HyperLink = null;
            this.Label19.Left = 5.125F;
            this.Label19.Name = "Label19";
            this.Label19.Style = "ddo-char-set: 1; text-align: left; font-weight: bold; ";
            this.Label19.Text = "Credit Note Date:";
            this.Label19.Top = 2.125F;
            this.Label19.Width = 1.1875F;
            // 
            // Label20
            // 
            this.Label20.Border.BottomColor = System.Drawing.Color.Black;
            this.Label20.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label20.Border.LeftColor = System.Drawing.Color.Black;
            this.Label20.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label20.Border.RightColor = System.Drawing.Color.Black;
            this.Label20.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label20.Border.TopColor = System.Drawing.Color.Black;
            this.Label20.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label20.Height = 0.1875F;
            this.Label20.HyperLink = null;
            this.Label20.Left = 5.125F;
            this.Label20.Name = "Label20";
            this.Label20.Style = "ddo-char-set: 1; text-align: left; font-weight: bold; ";
            this.Label20.Text = "Account No:";
            this.Label20.Top = 2.3125F;
            this.Label20.Width = 1.1875F;
            // 
            // txtCreditNoteNo
            // 
            this.txtCreditNoteNo.Border.BottomColor = System.Drawing.Color.Black;
            this.txtCreditNoteNo.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtCreditNoteNo.Border.LeftColor = System.Drawing.Color.Black;
            this.txtCreditNoteNo.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtCreditNoteNo.Border.RightColor = System.Drawing.Color.Black;
            this.txtCreditNoteNo.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtCreditNoteNo.Border.TopColor = System.Drawing.Color.Black;
            this.txtCreditNoteNo.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtCreditNoteNo.Height = 0.1875F;
            this.txtCreditNoteNo.Left = 6.3125F;
            this.txtCreditNoteNo.Name = "txtCreditNoteNo";
            this.txtCreditNoteNo.Style = "color: Red; ";
            this.txtCreditNoteNo.Text = "To Be Issued";
            this.txtCreditNoteNo.Top = 1.9375F;
            this.txtCreditNoteNo.Width = 1.125F;
            // 
            // txtCreditNoteDate
            // 
            this.txtCreditNoteDate.Border.BottomColor = System.Drawing.Color.Black;
            this.txtCreditNoteDate.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtCreditNoteDate.Border.LeftColor = System.Drawing.Color.Black;
            this.txtCreditNoteDate.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtCreditNoteDate.Border.RightColor = System.Drawing.Color.Black;
            this.txtCreditNoteDate.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtCreditNoteDate.Border.TopColor = System.Drawing.Color.Black;
            this.txtCreditNoteDate.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtCreditNoteDate.Height = 0.1875F;
            this.txtCreditNoteDate.Left = 6.3125F;
            this.txtCreditNoteDate.Name = "txtCreditNoteDate";
            this.txtCreditNoteDate.Style = "";
            this.txtCreditNoteDate.Text = null;
            this.txtCreditNoteDate.Top = 2.125F;
            this.txtCreditNoteDate.Width = 1.125F;
            // 
            // txtAccountNo
            // 
            this.txtAccountNo.Border.BottomColor = System.Drawing.Color.Black;
            this.txtAccountNo.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtAccountNo.Border.LeftColor = System.Drawing.Color.Black;
            this.txtAccountNo.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtAccountNo.Border.RightColor = System.Drawing.Color.Black;
            this.txtAccountNo.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtAccountNo.Border.TopColor = System.Drawing.Color.Black;
            this.txtAccountNo.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtAccountNo.Height = 0.1875F;
            this.txtAccountNo.Left = 6.3125F;
            this.txtAccountNo.Name = "txtAccountNo";
            this.txtAccountNo.Style = "vertical-align: top; ";
            this.txtAccountNo.Text = null;
            this.txtAccountNo.Top = 2.3125F;
            this.txtAccountNo.Width = 1.125F;
            // 
            // lblCreditNoteNotice
            // 
            this.lblCreditNoteNotice.Border.BottomColor = System.Drawing.Color.Black;
            this.lblCreditNoteNotice.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblCreditNoteNotice.Border.LeftColor = System.Drawing.Color.Black;
            this.lblCreditNoteNotice.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblCreditNoteNotice.Border.RightColor = System.Drawing.Color.Black;
            this.lblCreditNoteNotice.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblCreditNoteNotice.Border.TopColor = System.Drawing.Color.Black;
            this.lblCreditNoteNotice.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblCreditNoteNotice.Height = 0.375F;
            this.lblCreditNoteNotice.HyperLink = null;
            this.lblCreditNoteNotice.Left = 2.1875F;
            this.lblCreditNoteNotice.Name = "lblCreditNoteNotice";
            this.lblCreditNoteNotice.Style = "color: Red; text-align: center; ";
            this.lblCreditNoteNotice.Text = "This Credit Note has not yet been saved, add the Credit Note to allocate a Credit" +
                " Note No.";
            this.lblCreditNoteNotice.Top = 2.8125F;
            this.lblCreditNoteNotice.Visible = false;
            this.lblCreditNoteNotice.Width = 2.875F;
            // 
            // txtCustomerName
            // 
            this.txtCustomerName.Border.BottomColor = System.Drawing.Color.Black;
            this.txtCustomerName.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtCustomerName.Border.LeftColor = System.Drawing.Color.Black;
            this.txtCustomerName.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtCustomerName.Border.RightColor = System.Drawing.Color.Black;
            this.txtCustomerName.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtCustomerName.Border.TopColor = System.Drawing.Color.Black;
            this.txtCustomerName.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtCustomerName.DataField = "OrganisationName";
            this.txtCustomerName.Height = 0.2F;
            this.txtCustomerName.Left = 0.25F;
            this.txtCustomerName.Name = "txtCustomerName";
            this.txtCustomerName.Style = "ddo-char-set: 1; font-weight: bold; ";
            this.txtCustomerName.Text = null;
            this.txtCustomerName.Top = 1.9375F;
            this.txtCustomerName.Width = 3.53125F;
            // 
            // txtAddress
            // 
            this.txtAddress.Border.BottomColor = System.Drawing.Color.Black;
            this.txtAddress.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtAddress.Border.LeftColor = System.Drawing.Color.Black;
            this.txtAddress.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtAddress.Border.RightColor = System.Drawing.Color.Black;
            this.txtAddress.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtAddress.Border.TopColor = System.Drawing.Color.Black;
            this.txtAddress.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtAddress.Height = 0.2F;
            this.txtAddress.Left = 0.25F;
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Style = "ddo-char-set: 1; font-weight: bold; ";
            this.txtAddress.Text = null;
            this.txtAddress.Top = 2.125F;
            this.txtAddress.Width = 2.1875F;
            // 
            // lblReason
            // 
            this.lblReason.Border.BottomColor = System.Drawing.Color.Black;
            this.lblReason.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblReason.Border.LeftColor = System.Drawing.Color.Black;
            this.lblReason.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblReason.Border.RightColor = System.Drawing.Color.Black;
            this.lblReason.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblReason.Border.TopColor = System.Drawing.Color.Black;
            this.lblReason.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblReason.Height = 0.2F;
            this.lblReason.HyperLink = null;
            this.lblReason.Left = 0.0625F;
            this.lblReason.Name = "lblReason";
            this.lblReason.Style = "ddo-char-set: 1; font-weight: bold; ";
            this.lblReason.Text = "Description";
            this.lblReason.Top = 3.1875F;
            this.lblReason.Width = 1.25F;
            // 
            // txtReason
            // 
            this.txtReason.Border.BottomColor = System.Drawing.Color.Black;
            this.txtReason.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtReason.Border.LeftColor = System.Drawing.Color.Black;
            this.txtReason.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtReason.Border.RightColor = System.Drawing.Color.Black;
            this.txtReason.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtReason.Border.TopColor = System.Drawing.Color.Black;
            this.txtReason.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtReason.Height = 0.1875F;
            this.txtReason.Left = 0.0625F;
            this.txtReason.Name = "txtReason";
            this.txtReason.OutputFormat = resources.GetString("txtReason.OutputFormat");
            this.txtReason.Style = "text-align: left; ";
            this.txtReason.Text = null;
            this.txtReason.Top = 3.375F;
            this.txtReason.Width = 4.9375F;
            // 
            // txtAmountDetail
            // 
            this.txtAmountDetail.Border.BottomColor = System.Drawing.Color.Black;
            this.txtAmountDetail.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtAmountDetail.Border.LeftColor = System.Drawing.Color.Black;
            this.txtAmountDetail.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtAmountDetail.Border.RightColor = System.Drawing.Color.Black;
            this.txtAmountDetail.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtAmountDetail.Border.TopColor = System.Drawing.Color.Black;
            this.txtAmountDetail.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtAmountDetail.Height = 0.2F;
            this.txtAmountDetail.Left = 6.5F;
            this.txtAmountDetail.Name = "txtAmountDetail";
            this.txtAmountDetail.OutputFormat = resources.GetString("txtAmountDetail.OutputFormat");
            this.txtAmountDetail.Style = "text-align: right; ";
            this.txtAmountDetail.Text = null;
            this.txtAmountDetail.Top = 3.375F;
            this.txtAmountDetail.Width = 0.9375F;
            // 
            // Label
            // 
            this.Label.Border.BottomColor = System.Drawing.Color.Black;
            this.Label.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label.Border.LeftColor = System.Drawing.Color.Black;
            this.Label.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label.Border.RightColor = System.Drawing.Color.Black;
            this.Label.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label.Border.TopColor = System.Drawing.Color.Black;
            this.Label.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label.Height = 0.2F;
            this.Label.HyperLink = null;
            this.Label.Left = 6.5F;
            this.Label.Name = "Label";
            this.Label.Style = "ddo-char-set: 1; text-align: right; font-weight: bold; ";
            this.Label.Text = "Total";
            this.Label.Top = 3.1875F;
            this.Label.Width = 0.9583333F;
            // 
            // Label1
            // 
            this.Label1.Border.BottomColor = System.Drawing.Color.Black;
            this.Label1.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label1.Border.LeftColor = System.Drawing.Color.Black;
            this.Label1.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label1.Border.RightColor = System.Drawing.Color.Black;
            this.Label1.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label1.Border.TopColor = System.Drawing.Color.Black;
            this.Label1.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label1.Height = 0.2F;
            this.Label1.HyperLink = null;
            this.Label1.Left = 5.3125F;
            this.Label1.Name = "Label1";
            this.Label1.Style = "ddo-char-set: 1; text-align: right; font-weight: bold; ";
            this.Label1.Text = "Unit Price";
            this.Label1.Top = 3.1875F;
            this.Label1.Width = 1F;
            // 
            // txtUnitTotal
            // 
            this.txtUnitTotal.Border.BottomColor = System.Drawing.Color.Black;
            this.txtUnitTotal.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtUnitTotal.Border.LeftColor = System.Drawing.Color.Black;
            this.txtUnitTotal.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtUnitTotal.Border.RightColor = System.Drawing.Color.Black;
            this.txtUnitTotal.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtUnitTotal.Border.TopColor = System.Drawing.Color.Black;
            this.txtUnitTotal.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtUnitTotal.Height = 0.2F;
            this.txtUnitTotal.Left = 5.3125F;
            this.txtUnitTotal.Name = "txtUnitTotal";
            this.txtUnitTotal.OutputFormat = resources.GetString("txtUnitTotal.OutputFormat");
            this.txtUnitTotal.Style = "text-align: right; ";
            this.txtUnitTotal.Text = null;
            this.txtUnitTotal.Top = 3.375F;
            this.txtUnitTotal.Width = 1F;
            // 
            // lblHelpsWithFormattingDontRemove
            // 
            this.lblHelpsWithFormattingDontRemove.Border.BottomColor = System.Drawing.Color.Black;
            this.lblHelpsWithFormattingDontRemove.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblHelpsWithFormattingDontRemove.Border.LeftColor = System.Drawing.Color.Black;
            this.lblHelpsWithFormattingDontRemove.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblHelpsWithFormattingDontRemove.Border.RightColor = System.Drawing.Color.Black;
            this.lblHelpsWithFormattingDontRemove.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblHelpsWithFormattingDontRemove.Border.TopColor = System.Drawing.Color.Black;
            this.lblHelpsWithFormattingDontRemove.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblHelpsWithFormattingDontRemove.Height = 0.2F;
            this.lblHelpsWithFormattingDontRemove.HyperLink = null;
            this.lblHelpsWithFormattingDontRemove.Left = 2.625F;
            this.lblHelpsWithFormattingDontRemove.Name = "lblHelpsWithFormattingDontRemove";
            this.lblHelpsWithFormattingDontRemove.Style = "";
            this.lblHelpsWithFormattingDontRemove.Text = "";
            this.lblHelpsWithFormattingDontRemove.Top = 2.5F;
            this.lblHelpsWithFormattingDontRemove.Width = 1F;
            // 
            // srHeader
            // 
            this.srHeader.Border.BottomColor = System.Drawing.Color.Black;
            this.srHeader.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.srHeader.Border.LeftColor = System.Drawing.Color.Black;
            this.srHeader.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.srHeader.Border.RightColor = System.Drawing.Color.Black;
            this.srHeader.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.srHeader.Border.TopColor = System.Drawing.Color.Black;
            this.srHeader.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.srHeader.CloseBorder = false;
            this.srHeader.Height = 1.875F;
            this.srHeader.Left = 0F;
            this.srHeader.Name = "srHeader";
            this.srHeader.Report = null;
            this.srHeader.ReportName = "rptHeader";
            this.srHeader.Top = 0F;
            this.srHeader.Width = 7.5625F;
            // 
            // lblOurVatNo
            // 
            this.lblOurVatNo.Border.BottomColor = System.Drawing.Color.Black;
            this.lblOurVatNo.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblOurVatNo.Border.LeftColor = System.Drawing.Color.Black;
            this.lblOurVatNo.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblOurVatNo.Border.RightColor = System.Drawing.Color.Black;
            this.lblOurVatNo.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblOurVatNo.Border.TopColor = System.Drawing.Color.Black;
            this.lblOurVatNo.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblOurVatNo.Height = 0.2F;
            this.lblOurVatNo.HyperLink = null;
            this.lblOurVatNo.Left = 5.125F;
            this.lblOurVatNo.Name = "lblOurVatNo";
            this.lblOurVatNo.Style = "ddo-char-set: 1; text-align: left; font-weight: bold; ";
            this.lblOurVatNo.Text = "Our VAT No:";
            this.lblOurVatNo.Top = 2.5F;
            this.lblOurVatNo.Width = 1.1875F;
            // 
            // txtOurVatNo
            // 
            this.txtOurVatNo.Border.BottomColor = System.Drawing.Color.Black;
            this.txtOurVatNo.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtOurVatNo.Border.LeftColor = System.Drawing.Color.Black;
            this.txtOurVatNo.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtOurVatNo.Border.RightColor = System.Drawing.Color.Black;
            this.txtOurVatNo.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtOurVatNo.Border.TopColor = System.Drawing.Color.Black;
            this.txtOurVatNo.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtOurVatNo.Height = 0.1875F;
            this.txtOurVatNo.Left = 6.3125F;
            this.txtOurVatNo.Name = "txtOurVatNo";
            this.txtOurVatNo.Style = "vertical-align: top; ";
            this.txtOurVatNo.Text = null;
            this.txtOurVatNo.Top = 2.5F;
            this.txtOurVatNo.Width = 1.125F;
            // 
            // lblYourVatNo
            // 
            this.lblYourVatNo.Border.BottomColor = System.Drawing.Color.Black;
            this.lblYourVatNo.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblYourVatNo.Border.LeftColor = System.Drawing.Color.Black;
            this.lblYourVatNo.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblYourVatNo.Border.RightColor = System.Drawing.Color.Black;
            this.lblYourVatNo.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblYourVatNo.Border.TopColor = System.Drawing.Color.Black;
            this.lblYourVatNo.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblYourVatNo.Height = 0.2F;
            this.lblYourVatNo.HyperLink = null;
            this.lblYourVatNo.Left = 5.125F;
            this.lblYourVatNo.Name = "lblYourVatNo";
            this.lblYourVatNo.Style = "ddo-char-set: 1; text-align: left; font-weight: bold; ";
            this.lblYourVatNo.Text = "Your VAT No:";
            this.lblYourVatNo.Top = 2.6875F;
            this.lblYourVatNo.Width = 1.1875F;
            // 
            // txtYourVatNo
            // 
            this.txtYourVatNo.Border.BottomColor = System.Drawing.Color.Black;
            this.txtYourVatNo.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtYourVatNo.Border.LeftColor = System.Drawing.Color.Black;
            this.txtYourVatNo.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtYourVatNo.Border.RightColor = System.Drawing.Color.Black;
            this.txtYourVatNo.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtYourVatNo.Border.TopColor = System.Drawing.Color.Black;
            this.txtYourVatNo.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtYourVatNo.Height = 0.1875F;
            this.txtYourVatNo.Left = 6.3125F;
            this.txtYourVatNo.Name = "txtYourVatNo";
            this.txtYourVatNo.Style = "vertical-align: top; ";
            this.txtYourVatNo.Text = null;
            this.txtYourVatNo.Top = 2.6875F;
            this.txtYourVatNo.Width = 1.125F;
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.lblNote,
            this.lblTotalAmount,
            this.lblVAT,
            this.lblCreditNoteTotal,
            this.txtTotalAmount,
            this.txtVATAmount,
            this.txtVATPercentage,
            this.txtCreditNoteTotal,
            this.Line10,
            this.Line9,
            this.lblTradingAs,
            this.txtCustomNotes,
            this.srFooter});
            this.ReportFooter.Height = 2.4375F;
            this.ReportFooter.Name = "ReportFooter";
            this.ReportFooter.PrintAtBottom = true;
            this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
            this.ReportFooter.BeforePrint += new System.EventHandler(this.ReportFooter_BeforePrint);
            // 
            // lblNote
            // 
            this.lblNote.Border.BottomColor = System.Drawing.Color.Black;
            this.lblNote.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblNote.Border.LeftColor = System.Drawing.Color.Black;
            this.lblNote.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblNote.Border.RightColor = System.Drawing.Color.Black;
            this.lblNote.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblNote.Border.TopColor = System.Drawing.Color.Black;
            this.lblNote.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblNote.Height = 0.2F;
            this.lblNote.HyperLink = null;
            this.lblNote.Left = 3.0625F;
            this.lblNote.Name = "lblNote";
            this.lblNote.Style = "color: DimGray; ddo-char-set: 0; text-align: center; font-weight: bold; font-styl" +
                "e: italic; font-size: 8.25pt; ";
            this.lblNote.Text = "Strictly 30 Days Nett.";
            this.lblNote.Top = 0.625F;
            this.lblNote.Width = 1.375F;
            // 
            // lblTotalAmount
            // 
            this.lblTotalAmount.Border.BottomColor = System.Drawing.Color.Black;
            this.lblTotalAmount.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblTotalAmount.Border.LeftColor = System.Drawing.Color.Black;
            this.lblTotalAmount.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblTotalAmount.Border.RightColor = System.Drawing.Color.Black;
            this.lblTotalAmount.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblTotalAmount.Border.TopColor = System.Drawing.Color.Black;
            this.lblTotalAmount.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblTotalAmount.Height = 0.2F;
            this.lblTotalAmount.HyperLink = null;
            this.lblTotalAmount.Left = 5.1875F;
            this.lblTotalAmount.Name = "lblTotalAmount";
            this.lblTotalAmount.Style = "ddo-char-set: 1; font-weight: bold; ";
            this.lblTotalAmount.Text = "TOTAL AMOUNT";
            this.lblTotalAmount.Top = 0F;
            this.lblTotalAmount.Width = 1.1875F;
            // 
            // lblVAT
            // 
            this.lblVAT.Border.BottomColor = System.Drawing.Color.Black;
            this.lblVAT.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblVAT.Border.LeftColor = System.Drawing.Color.Black;
            this.lblVAT.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblVAT.Border.RightColor = System.Drawing.Color.Black;
            this.lblVAT.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblVAT.Border.TopColor = System.Drawing.Color.Black;
            this.lblVAT.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblVAT.Height = 0.2F;
            this.lblVAT.HyperLink = null;
            this.lblVAT.Left = 5.1875F;
            this.lblVAT.Name = "lblVAT";
            this.lblVAT.Style = "ddo-char-set: 1; font-weight: bold; ";
            this.lblVAT.Text = "VAT @";
            this.lblVAT.Top = 0.1875F;
            this.lblVAT.Width = 0.625F;
            // 
            // lblCreditNoteTotal
            // 
            this.lblCreditNoteTotal.Border.BottomColor = System.Drawing.Color.Black;
            this.lblCreditNoteTotal.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblCreditNoteTotal.Border.LeftColor = System.Drawing.Color.Black;
            this.lblCreditNoteTotal.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblCreditNoteTotal.Border.RightColor = System.Drawing.Color.Black;
            this.lblCreditNoteTotal.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblCreditNoteTotal.Border.TopColor = System.Drawing.Color.Black;
            this.lblCreditNoteTotal.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblCreditNoteTotal.Height = 0.2F;
            this.lblCreditNoteTotal.HyperLink = null;
            this.lblCreditNoteTotal.Left = 4.875F;
            this.lblCreditNoteTotal.Name = "lblCreditNoteTotal";
            this.lblCreditNoteTotal.Style = "ddo-char-set: 1; font-weight: bold; ";
            this.lblCreditNoteTotal.Text = "CREDIT NOTE TOTAL";
            this.lblCreditNoteTotal.Top = 0.5F;
            this.lblCreditNoteTotal.Width = 1.5F;
            // 
            // txtTotalAmount
            // 
            this.txtTotalAmount.Border.BottomColor = System.Drawing.Color.Black;
            this.txtTotalAmount.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtTotalAmount.Border.LeftColor = System.Drawing.Color.Black;
            this.txtTotalAmount.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtTotalAmount.Border.RightColor = System.Drawing.Color.Black;
            this.txtTotalAmount.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtTotalAmount.Border.TopColor = System.Drawing.Color.Black;
            this.txtTotalAmount.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtTotalAmount.Height = 0.2F;
            this.txtTotalAmount.Left = 6.4375F;
            this.txtTotalAmount.Name = "txtTotalAmount";
            this.txtTotalAmount.OutputFormat = resources.GetString("txtTotalAmount.OutputFormat");
            this.txtTotalAmount.Style = "text-align: right; ";
            this.txtTotalAmount.Text = null;
            this.txtTotalAmount.Top = 0F;
            this.txtTotalAmount.Width = 1F;
            // 
            // txtVATAmount
            // 
            this.txtVATAmount.Border.BottomColor = System.Drawing.Color.Black;
            this.txtVATAmount.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtVATAmount.Border.LeftColor = System.Drawing.Color.Black;
            this.txtVATAmount.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtVATAmount.Border.RightColor = System.Drawing.Color.Black;
            this.txtVATAmount.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtVATAmount.Border.TopColor = System.Drawing.Color.Black;
            this.txtVATAmount.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtVATAmount.Height = 0.2F;
            this.txtVATAmount.Left = 6.4375F;
            this.txtVATAmount.Name = "txtVATAmount";
            this.txtVATAmount.OutputFormat = resources.GetString("txtVATAmount.OutputFormat");
            this.txtVATAmount.Style = "text-align: right; ";
            this.txtVATAmount.Text = null;
            this.txtVATAmount.Top = 0.1875F;
            this.txtVATAmount.Width = 1F;
            // 
            // txtVATPercentage
            // 
            this.txtVATPercentage.Border.BottomColor = System.Drawing.Color.Black;
            this.txtVATPercentage.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtVATPercentage.Border.LeftColor = System.Drawing.Color.Black;
            this.txtVATPercentage.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtVATPercentage.Border.RightColor = System.Drawing.Color.Black;
            this.txtVATPercentage.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtVATPercentage.Border.TopColor = System.Drawing.Color.Black;
            this.txtVATPercentage.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtVATPercentage.Height = 0.2F;
            this.txtVATPercentage.Left = 5.8125F;
            this.txtVATPercentage.Name = "txtVATPercentage";
            this.txtVATPercentage.OutputFormat = resources.GetString("txtVATPercentage.OutputFormat");
            this.txtVATPercentage.Style = "ddo-char-set: 1; font-weight: bold; ";
            this.txtVATPercentage.Text = null;
            this.txtVATPercentage.Top = 0.1875F;
            this.txtVATPercentage.Width = 0.5625F;
            // 
            // txtCreditNoteTotal
            // 
            this.txtCreditNoteTotal.Border.BottomColor = System.Drawing.Color.Black;
            this.txtCreditNoteTotal.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtCreditNoteTotal.Border.LeftColor = System.Drawing.Color.Black;
            this.txtCreditNoteTotal.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtCreditNoteTotal.Border.RightColor = System.Drawing.Color.Black;
            this.txtCreditNoteTotal.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtCreditNoteTotal.Border.TopColor = System.Drawing.Color.Black;
            this.txtCreditNoteTotal.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtCreditNoteTotal.Height = 0.2F;
            this.txtCreditNoteTotal.Left = 6.4375F;
            this.txtCreditNoteTotal.Name = "txtCreditNoteTotal";
            this.txtCreditNoteTotal.OutputFormat = resources.GetString("txtCreditNoteTotal.OutputFormat");
            this.txtCreditNoteTotal.Style = "text-align: right; ";
            this.txtCreditNoteTotal.Text = null;
            this.txtCreditNoteTotal.Top = 0.5F;
            this.txtCreditNoteTotal.Width = 1F;
            // 
            // Line10
            // 
            this.Line10.Border.BottomColor = System.Drawing.Color.Black;
            this.Line10.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Line10.Border.LeftColor = System.Drawing.Color.Black;
            this.Line10.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Line10.Border.RightColor = System.Drawing.Color.Black;
            this.Line10.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Line10.Border.TopColor = System.Drawing.Color.Black;
            this.Line10.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Line10.Height = 0F;
            this.Line10.Left = 5.125F;
            this.Line10.LineWeight = 1F;
            this.Line10.Name = "Line10";
            this.Line10.Top = 0.5F;
            this.Line10.Width = 2.3125F;
            this.Line10.X1 = 5.125F;
            this.Line10.X2 = 7.4375F;
            this.Line10.Y1 = 0.5F;
            this.Line10.Y2 = 0.5F;
            // 
            // Line9
            // 
            this.Line9.Border.BottomColor = System.Drawing.Color.Black;
            this.Line9.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Line9.Border.LeftColor = System.Drawing.Color.Black;
            this.Line9.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Line9.Border.RightColor = System.Drawing.Color.Black;
            this.Line9.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Line9.Border.TopColor = System.Drawing.Color.Black;
            this.Line9.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Line9.Height = 0F;
            this.Line9.Left = 0F;
            this.Line9.LineWeight = 1F;
            this.Line9.Name = "Line9";
            this.Line9.Top = 0F;
            this.Line9.Width = 7.5F;
            this.Line9.X1 = 0F;
            this.Line9.X2 = 7.5F;
            this.Line9.Y1 = 0F;
            this.Line9.Y2 = 0F;
            // 
            // lblTradingAs
            // 
            this.lblTradingAs.Border.BottomColor = System.Drawing.Color.Black;
            this.lblTradingAs.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblTradingAs.Border.LeftColor = System.Drawing.Color.Black;
            this.lblTradingAs.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblTradingAs.Border.RightColor = System.Drawing.Color.Black;
            this.lblTradingAs.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblTradingAs.Border.TopColor = System.Drawing.Color.Black;
            this.lblTradingAs.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblTradingAs.Height = 0.2F;
            this.lblTradingAs.HyperLink = null;
            this.lblTradingAs.Left = 0.0625F;
            this.lblTradingAs.Name = "lblTradingAs";
            this.lblTradingAs.Style = "color: DimGray; ddo-char-set: 0; text-align: center; font-weight: bold; font-styl" +
                "e: italic; font-size: 8.25pt; ";
            this.lblTradingAs.Text = "Strictly 30 Days Nett.";
            this.lblTradingAs.Top = 0.8125F;
            this.lblTradingAs.Width = 7.375F;
            // 
            // txtCustomNotes
            // 
            this.txtCustomNotes.AutoReplaceFields = true;
            this.txtCustomNotes.BackColor = System.Drawing.Color.Transparent;
            this.txtCustomNotes.Border.BottomColor = System.Drawing.Color.Black;
            this.txtCustomNotes.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtCustomNotes.Border.LeftColor = System.Drawing.Color.Black;
            this.txtCustomNotes.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtCustomNotes.Border.RightColor = System.Drawing.Color.Black;
            this.txtCustomNotes.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtCustomNotes.Border.TopColor = System.Drawing.Color.Black;
            this.txtCustomNotes.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtCustomNotes.CanShrink = true;
            this.txtCustomNotes.Font = new System.Drawing.Font("Arial", 10F);
            this.txtCustomNotes.ForeColor = System.Drawing.Color.Black;
            this.txtCustomNotes.Height = 0.1875F;
            this.txtCustomNotes.Left = 0.0625F;
            this.txtCustomNotes.Name = "txtCustomNotes";
            this.txtCustomNotes.RTF = resources.GetString("txtCustomNotes.RTF");
            this.txtCustomNotes.Top = 0F;
            this.txtCustomNotes.Width = 4.75F;
            // 
            // PageHeader
            // 
            this.PageHeader.Height = 0F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.CanShrink = true;
            this.PageFooter.Height = 1.291667F;
            this.PageFooter.Name = "PageFooter";
            // 
            // srFooter
            // 
            this.srFooter.Border.BottomColor = System.Drawing.Color.Black;
            this.srFooter.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.srFooter.Border.LeftColor = System.Drawing.Color.Black;
            this.srFooter.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.srFooter.Border.RightColor = System.Drawing.Color.Black;
            this.srFooter.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.srFooter.Border.TopColor = System.Drawing.Color.Black;
            this.srFooter.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.srFooter.CanShrink = false;
            this.srFooter.CloseBorder = false;
            this.srFooter.Height = 1.3125F;
            this.srFooter.Left = 0F;
            this.srFooter.Name = "srFooter";
            this.srFooter.Report = null;
            this.srFooter.Top = 1.0625F;
            this.srFooter.Width = 7.5F;
            // 
            // rptCreditNoteOneLiner
            // 
            this.MasterReport = false;
            this.PageSettings.Orientation = DataDynamics.ActiveReports.Document.PageOrientation.Landscape;
            this.PageSettings.PaperHeight = 11.69F;
            this.PageSettings.PaperWidth = 8.27F;
            this.PrintWidth = 7.510417F;
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                        "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading3", "Normal"));
            this.UserData = "CreditReport";
            ((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCreditNoteNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCreditNoteDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCreditNoteNotice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustomerName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReason)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmountDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUnitTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHelpsWithFormattingDontRemove)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblOurVatNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOurVatNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblYourVatNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYourVatNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNote)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotalAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVAT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCreditNoteTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVATAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVATPercentage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCreditNoteTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTradingAs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		 }

		#endregion

        private void ReportFooter_BeforePrint(object sender, EventArgs e)
        {
            this.txtCustomNotes.Top = this.lblTotalAmount.Top;
        }
	}
//	this.ReportHeader.Format +=new EventHandler(ReportHeader_Format);
//	this.Detail.Format +=new EventHandler(Detail_Format);
//	this.ReportFooter.Format +=new EventHandler(ReportFooter_Format); 
}
