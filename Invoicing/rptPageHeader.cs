using System;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;
using System.Configuration;

namespace Orchestrator.Reports.Invoicing
{
    public class rptPageHeader : DataDynamics.ActiveReports.ActiveReport3
	{
		public rptPageHeader()
		{
			InitializeComponent();

            bool showInvoicePageHeader = false;
            bool.TryParse(ConfigurationManager.AppSettings["ShowInvoicePageHeader"], out showInvoicePageHeader);

            if (!showInvoicePageHeader)
            {
                HideAllControls();
                return;
            }
            else
            {
                switch (ConfigurationManager.AppSettings["HeaderReportName"].ToLower())
                {
                    case "firminsimageheader": //Alan Firmin
                        sbPageHeader.Report = new SubReports.FirminsImageHeader();
                        break;
                    case "nichollsimageheader": //Nicholls
                        sbPageHeader.Report = new SubReports.NichollsImageHeader();
                        break;
                    case "ffimageheader": //FF
                        sbPageHeader.Report = new SubReports.FFImageHeader();
                        break;
                    case "fandwimageheader": //Fagan and Whalley
                        sbPageHeader.Report = new SubReports.FandWImageHeader();
                        break;
                    case "woodallimageheader": // Woodall
                        sbPageHeader.Report = new SubReports.WoodallImageHeader();
                        break;
                    case "kerseyimageheader": // Kersey
                        sbPageHeader.Report = new SubReports.KerseyImageHeader();
                        break;
                    case "williamsimageheader": // Kersey
                        sbPageHeader.Report = new SubReports.WilliamsImageHeader();
                        break;
                    case "hubpartnersimageheader": // Hub Partners (fomerly Wisbech Roadways)
                        sbPageHeader.Report = new SubReports.KnowlesImageHeader();
                        break;
                    case "chilternimageheader": // Chiltern
                        sbPageHeader.Report = new SubReports.ChilternImageHeader();
                        break;
                    default:
                        sbPageHeader.Visible = false;
                        break;
                }
            }
		}

        private void HideAllControls()
        {
            foreach (DataDynamics.ActiveReports.ARControl cntl in this.Detail.Controls)
            {
                cntl.Visible = false;
            }
        }

		#region ActiveReports Designer generated code
		private DataDynamics.ActiveReports.PageHeader PageHeader = null;
		private DataDynamics.ActiveReports.Detail Detail = null;
		private DataDynamics.ActiveReports.SubReport sbPageHeader = null;
		private DataDynamics.ActiveReports.PageFooter PageFooter = null;
		public void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptPageHeader));
            this.Detail = new DataDynamics.ActiveReports.Detail();
            this.sbPageHeader = new DataDynamics.ActiveReports.SubReport();
            this.PageHeader = new DataDynamics.ActiveReports.PageHeader();
            this.PageFooter = new DataDynamics.ActiveReports.PageFooter();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.CanShrink = true;
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.sbPageHeader});
            this.Detail.Height = 0F;
            this.Detail.Name = "Detail";
            // 
            // sbPageHeader
            // 
            this.sbPageHeader.Border.BottomColor = System.Drawing.Color.Black;
            this.sbPageHeader.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.sbPageHeader.Border.LeftColor = System.Drawing.Color.Black;
            this.sbPageHeader.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.sbPageHeader.Border.RightColor = System.Drawing.Color.Black;
            this.sbPageHeader.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.sbPageHeader.Border.TopColor = System.Drawing.Color.Black;
            this.sbPageHeader.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.sbPageHeader.CloseBorder = false;
            this.sbPageHeader.Height = 1.9375F;
            this.sbPageHeader.Left = 0F;
            this.sbPageHeader.Name = "sbPageHeader";
            this.sbPageHeader.Report = null;
            this.sbPageHeader.Top = 0F;
            this.sbPageHeader.Width = 7.5F;
            // 
            // PageHeader
            // 
            this.PageHeader.Height = 1.708333F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // rptPageHeader
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11.69F;
            this.PageSettings.PaperWidth = 8.27F;
            this.PrintWidth = 7.510417F;
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                        "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

		#endregion
	}
}
