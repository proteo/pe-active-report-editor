using System;
using System.Data;
using System.Drawing;
using System.Collections.Specialized;
using System.Globalization;
using System.Web;

using System.Collections.Generic;
using System.Text;

using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;

using Orchestrator.Globals;

namespace Orchestrator.Reports.Invoicing
{
    /// <summary>
    /// Summary description for rptNewSubContractorSelfBill.
    /// </summary>
    public partial class rptNewSubContractorSelfBill : Orchestrator.Reports.ReportBase, Orchestrator.Reports.Invoicing.IInvoiceReport
    {
        #region Report Variables

        private int _currentPage = -1;

        #endregion

        #region Override Header

        protected override void CreateReportHeader()
        {
            this.PageSettings.Orientation = PageOrientation.Portrait;
        }

        private void ReportBase_BeforePrint(object sender, EventArgs e)
        {
            TextBox txtPageCount = (TextBox)this.Sections["PageFooter"].Controls["ftr_txtPageCount"];
            ((Label)this.Sections["PageFooter"].Controls["ftr_lblPage"]).Text = "Page " + txtPageCount.Text;
        }

        protected override void CreatePageFooter()
        {
            Section pageFooter = Sections["PageFooter"];
            if (pageFooter == null)
            {
                // Insert the page footer
                Sections.InsertPageHF();
                pageFooter = Sections["PageFooter"];
            }
            this.Sections["PageFooter"].BeforePrint += new EventHandler(ReportBase_BeforePrint);

            // Page Count TextBox
            TextBox txtPageCount = new TextBox();
            txtPageCount.Name = "ftr_txtPageCount";
            txtPageCount.Text = "##";
            txtPageCount.Visible = false;
            txtPageCount.SummaryType = SummaryType.PageCount;
            txtPageCount.SummaryRunning = SummaryRunning.All;

            // Page Count Label
            Label lblPage = new Label();
            lblPage.Name = "ftr_lblPage";
            lblPage.Width = 5F;
            lblPage.Top = 0.1F;
            lblPage.Left = PrintWidth - lblPage.Width;
            lblPage.Alignment = TextAlignment.Right;

            // Add the controls to the footer
            if (pageFooter != null)
            {
                pageFooter.Controls.AddRange(new ARControl[] { txtPageCount, lblPage });
            }

            if (pageFooter != null && !this.showInvoicePageFooter)
            {
                // make the footer smaller for the standard footer to avoid big gaps in the report.
                pageFooter.Height = 0.2F;
                this.PageFooter.Height = 0.2F;
            }
            if (this.invoicePageFooterOnLastPageOnly)
                this.srFooter.Visible = false;
        }

        #endregion

        public rptNewSubContractorSelfBill()
        {
            ReportOrientation = PageOrientation.Portrait;
            InitializeComponent();

            rptHeader rpt = new rptHeader();
            this.srHeader.Report = rpt;

            rptPageHeader rptPage = new rptPageHeader();
            this.srPageHeader.Report = rptPage;

            rptFooter rpt_ftr = new rptFooter();
            this.srFooter.Report = rpt_ftr;
        }

        private string BuildAddressInformation(string portion)
        {
            if (!string.IsNullOrEmpty(portion))
                return portion + Environment.NewLine;
            else
                return string.Empty;
        }

        private void BuildAddressInformation(Entities.Address address)
        {
            StringBuilder sbAddress = new StringBuilder();

            sbAddress.Append(BuildAddressInformation(address.AddressLine1));
            sbAddress.Append(BuildAddressInformation(address.AddressLine2));
            sbAddress.Append(BuildAddressInformation(address.AddressLine3));
            sbAddress.Append(BuildAddressInformation(address.PostTown));
            sbAddress.Append(BuildAddressInformation(address.County));
            sbAddress.Append(BuildAddressInformation(address.PostCode));

            txtAddress.Text = sbAddress.ToString();
        }

        #region Event Handlers

        private void Detail_Format(object sender, System.EventArgs eArgs)
        {
            Detail thisDetail = sender as Detail;
            TextBox txtPallets = thisDetail.Controls["txtPallets"] as TextBox;
            TextBox txtRate = thisDetail.Controls["txtRate"] as TextBox;

            if (txtPallets != null && txtPallets.Text != null)
                if (int.Parse(txtPallets.Text) == 0)
                    txtPallets.Text = string.Empty;

            decimal currentItemValue = 0m;

            try
            {
                if (txtRate != null && !String.IsNullOrEmpty(txtRate.Text))
                    currentItemValue += decimal.Parse(txtRate.Text, NumberStyles.Any, System.Threading.Thread.CurrentThread.CurrentUICulture);
            }
            catch (Exception ex)
            {
                throw;
            }

            txtRate.Text = currentItemValue.ToString("C", CultureInfo.InvariantCulture.NumberFormat);
            (this as IInvoiceReport).ItemTotal += currentItemValue;

            if (txtForeignRate != null && txtForeignRate.Text != string.Empty)
            {
                decimal foreignRate = decimal.Parse(txtForeignRate.Text, NumberStyles.Any);

                (this as IInvoiceReport).ForeignItemTotal += foreignRate;
                txtForeignRate.Text = foreignRate.ToString("C");
            }

            //switch (txtPalletSpaces.Text)
            //{
            //    case "0.25":
            //        txtPalletSpaces.Text = "�";
            //        break;
            //    case "0.50":
            //        txtPalletSpaces.Text = "�";
            //        break;
            //    default:
            //        decimal output;

            //        if (decimal.TryParse(txtPalletSpaces.Text, out output))
            //            txtPalletSpaces.Text = string.Format("{0:f0}", output);

            //        if (output == 0)
            //            txtPalletSpaces.Text = string.Empty;

            //        break;
            //}
        }

        private void Detail_BeforePrint(object sender, System.EventArgs eArgs)
        {
            Detail thisDetail = sender as Detail;
        }

        private void ReportHeader_Format(object sender, System.EventArgs eArgs)
        {
            //-------------------------------------------------------------------------------------	
            //							Client Account & Address Details Section
            //-------------------------------------------------------------------------------------	
            Facade.IOrganisation facOrganisation = new Facade.Organisation();

            // Client Name
            //txtCustomerName.Text = facOrganisation.GetDisplayNameForIdentityId(this.PreInvoice.IdentityID);

            //Facade.IAddress facAddress = new Facade.Point();
            //Entities.Address address = facAddress.GetForAddressID(this.PreInvoice.GenerationParameters.AddressID);
            //if (address != null)
            //{
            //    // Client Address Details
            //    BuildAddressInformation(address);
            //}

            // Configure the invoice date on the report.
            //txtInvoiceDate.Text = PreInvoice.GenerationParameters.InvoiceDate.ToString("dd/MM/yy");

            //If Non Self-Bill Sub-Contractor Invoice display the Sub-Contractor Invoice Number in Invoice No field.
            //if (PreInvoice.GenerationParameters.InvoiceType == eInvoiceType.SubContract)
            //{
            //    txtInvoiceNo.Text = PreInvoice.GenerationParameters.ClientInvoiceReference;
            //    this.txtPageHeaderSelfBillNo.Text = PreInvoice.GenerationParameters.ClientInvoiceReference;
            //}
            //else if (PreInvoice.InvoiceID > 0)
            //{
            //    // Set the invoice number.
            //    txtInvoiceNo.Text = Configuration.InvoicingSelfBillPrefix + PreInvoice.InvoiceID.ToString();
            //    this.txtPageHeaderSelfBillNo.Text = Configuration.InvoicingSelfBillPrefix + PreInvoice.InvoiceID.ToString();
            //}
            //// Show either the "this is a preview of the invoice" or the "this is a real invoice" text label.
            //lblIsPreInvoice.Visible = PreInvoice.InvoiceID == 0;
            //lblIsInvoice.Visible = PreInvoice.InvoiceID > 0;

            // Show VAT Number
            bool showHeader = false;
            bool.TryParse(System.Configuration.ConfigurationManager.AppSettings["ShowInvoiceHeader"], out showHeader);

            txtVatNo.Text = System.Configuration.ConfigurationManager.AppSettings["VatNo"];
            //txtYourVatNo.Text = facOrganisation.GetVATNumberForIdentityID(this.PreInvoice.IdentityID);

            //if (PreInvoice.GenerationParameters.InvoiceType == eInvoiceType.SubContract)
            //{
            //    //Put Sub-Contractors VAT no in the "VAT No" field on the Report - imitation of Sub-Contrators Invoice.
            //    lblYourVatNo.Visible = txtYourVatNo.Visible = false;
            //    txtVatNo.Text = txtYourVatNo.Text;
            //    lblIsInvoice.Text = "INVOICE";
            //    lblIsPreInvoice.Text = "This invoice has not yet been saved.";
            //}
            //else if (!showHeader)
            //    lblVatNo.Visible = txtVatNo.Visible = !showHeader;
        }

        private void PageHeader_Format(object sender, System.EventArgs eArgs)
        {
            _currentPage = this.PageNumber;
            PageHeader ph = sender as PageHeader;

            if (this.PageNumber == 1)
            {
                this.srPageHeader.Visible = false;

                // On any page other that the first page, make give the page header more height.
                ph.Height -= 1.850f;
                foreach (ARControl control in ph.Controls)
                    control.Top -= 1.850f;
            }
            else
                this.srPageHeader.Visible = true;

            if (this.PageNumber == 1)
            {
                this.lblPageHeaderSelfBillNo.Visible = false;
                this.txtPageHeaderSelfBillNo.Visible = false;
            }
            else
            {
                this.lblPageHeaderSelfBillNo.Top += 1.850f;
                this.lblPageHeaderSelfBillNo.Visible = true;
                this.txtPageHeaderSelfBillNo.Top += 1.850f;
                this.txtPageHeaderSelfBillNo.Visible = true;
            }
        }

        private void PageFooter_Format(object sender, EventArgs e)
        {

        }

        private void ReportFooter_Format(object sender, System.EventArgs eArgs)
        {
            if (this.invoicePageFooterOnLastPageOnly)
                this.srFooter.Visible = false;
            // Display the totals;
            //txtVATPercentage.Text = PreInvoice.GenerationParameters.TaxRate.ToString("F2") + "%";
            //if (PreInvoice.GenerationParameters.Overrides.OverrideAmounts)
            //{
            //    (this as IInvoiceReport).ItemTotal = PreInvoice.GenerationParameters.Overrides.NetAmount;
            //    txtTotalAmount.Text = PreInvoice.GenerationParameters.Overrides.NetAmount.ToString("C");
            //    (this as IInvoiceReport).ItemTotal = decimal.Parse(txtTotalAmount.Text, NumberStyles.Any);

            //    (this as IInvoiceReport).TaxTotal = PreInvoice.GenerationParameters.Overrides.TaxAmount;
            //    txtVATAmount.Text = PreInvoice.GenerationParameters.Overrides.TaxAmount.ToString("C");
            //    (this as IInvoiceReport).TaxTotal = decimal.Parse(txtVATAmount.Text, NumberStyles.Any);

            //    (this as IInvoiceReport).GrossTotal = PreInvoice.GenerationParameters.Overrides.TotalAmount;
            //    txtInvoiceTotal.Text = PreInvoice.GenerationParameters.Overrides.TotalAmount.ToString("C");
            //    (this as IInvoiceReport).GrossTotal = decimal.Parse(txtInvoiceTotal.Text, NumberStyles.Any);
            //}
            //else
            //{
            //    (this as IInvoiceReport).ForeignItemTotal = decimal.Round((ForeignItemTotal + ForeignExtrasTotal + ForeignFuelSurcharge), 2, MidpointRounding.AwayFromZero);
            //    (this as IInvoiceReport).ItemTotal = decimal.Round((ItemTotal + ExtrasTotal + FuelSurcharge), 2, MidpointRounding.AwayFromZero);
            //    txtTotalAmount.Text = (this as IInvoiceReport).ForeignItemTotal.ToString("C");

            //    (this as IInvoiceReport).ForeignTaxTotal = decimal.Round(((this as IInvoiceReport).ForeignItemTotal / 100) * PreInvoice.GenerationParameters.TaxRate, 2, MidpointRounding.AwayFromZero);
            //    (this as IInvoiceReport).TaxTotal = decimal.Round(((this as IInvoiceReport).ItemTotal / 100) * PreInvoice.GenerationParameters.TaxRate, 2, MidpointRounding.AwayFromZero);
            //    txtVATAmount.Text = (this as IInvoiceReport).ForeignTaxTotal.ToString("C");

            //    (this as IInvoiceReport).ForeignGrossTotal = decimal.Round((this as IInvoiceReport).ForeignItemTotal + (this as IInvoiceReport).ForeignTaxTotal, 2, MidpointRounding.AwayFromZero);
            //    (this as IInvoiceReport).GrossTotal = decimal.Round((this as IInvoiceReport).ItemTotal + (this as IInvoiceReport).TaxTotal, 2, MidpointRounding.AwayFromZero);
            //    txtInvoiceTotal.Text = (this as IInvoiceReport).ForeignGrossTotal.ToString("C");
            //}
        }

        private void ReportFooter_BeforePrint(object sender, System.EventArgs eArgs)
        {
            // Adjust the positioning of items in the report footer based on the heights and visibility of those items.
            ReportFooter reportFooter = sender as ReportFooter;

            float requiredY = 0F;
            float gap = 0.188F;

            gap = 0.100F;
            reportFooterLine.Y1 = reportFooterLine.Y2 = requiredY;

            lblTotalAmount.Top = txtTotalAmount.Top = requiredY;
            requiredY += txtTotalAmount.Height;

            lblVAT.Top = txtVATPercentage.Top = txtVATAmount.Top = requiredY;
            requiredY += txtVATAmount.Height + gap;

            invoiceTotalTopLine.Y1 = invoiceTotalTopLine.Y2 = lblInvoiceTotal.Top = txtInvoiceTotal.Top = requiredY;
            requiredY += txtInvoiceTotal.Height;
            invoiceTotalBaseLine.Y1 = invoiceTotalBaseLine.Y2 = requiredY;
            requiredY += gap;
        }

        #endregion

        #region IInvoiceReport Members

        private decimal _itemTotal = 0;
        private decimal _extrasTotal = 0;
        private decimal _fuelSurcharge = 0;
        private decimal _netTotal = 0;
        private decimal _taxTotal = 0;
        private decimal _grossTotal = 0;

        private decimal _foreignItemTotal = 0;
        private decimal _foreignExtrasTotal = 0;
        private decimal _foreignFuelSurcharge = 0;
        private decimal _foreignNetTotal = 0;
        private decimal _foreignTaxTotal = 0;
        private decimal _foreignGrossTotal = 0;

        private Entities.PreInvoice _preInvoice = null;

        public decimal ItemTotal { get { return _itemTotal; } set { _itemTotal = value; } }
        public decimal ExtrasTotal { get { return _extrasTotal; } set { _extrasTotal = value; } }
        public decimal FuelSurcharge { get { return _fuelSurcharge; } set { _fuelSurcharge = value; } }
        public decimal NetTotal { get { return _netTotal; } set { _netTotal = value; } }
        public decimal TaxTotal { get { return _taxTotal; } set { _taxTotal = value; } }
        public decimal GrossTotal { get { return _grossTotal; } set { _grossTotal = value; } }

        public decimal ForeignItemTotal { get { return _foreignItemTotal; } set { _foreignItemTotal = value; } }
        public decimal ForeignExtrasTotal { get { return _foreignExtrasTotal; } set { _foreignExtrasTotal = value; } }
        public decimal ForeignFuelSurcharge { get { return _foreignFuelSurcharge; } set { _foreignFuelSurcharge = value; } }
        public decimal ForeignNetTotal { get { return _foreignNetTotal; } set { _foreignNetTotal = value; } }
        public decimal ForeignTaxTotal { get { return _foreignTaxTotal; } set { _foreignTaxTotal = value; } }
        public decimal ForeignGrossTotal { get { return _foreignGrossTotal; } set { _foreignGrossTotal = value; } }

        //public List<Entities.InvoiceNominalCodeSubTotal> InvoiceNominalCodeSubTotals { get; set; }
        public Orchestrator.Entities.PreInvoice PreInvoice { get { return _preInvoice; } set { _preInvoice = value; } }

        #endregion

        private void rptNewSubContractorSelfBill_ReportStart(object sender, EventArgs e)
        {

        }
    }
}
