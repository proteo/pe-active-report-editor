namespace Orchestrator.Reports.Invoicing
{
    /// <summary>
    /// Summary description for rptNewSubContractorSelfBill.
    /// </summary>
    partial class rptNewSubContractorSelfBill
    {
        private DataDynamics.ActiveReports.PageHeader PageHeader;
        private DataDynamics.ActiveReports.Detail Detail;
        private DataDynamics.ActiveReports.PageFooter PageFooter;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(rptNewSubContractorSelfBill));
            this.PageHeader = new DataDynamics.ActiveReports.PageHeader();
            this.srPageHeader = new DataDynamics.ActiveReports.SubReport();
            this.lblDate = new DataDynamics.ActiveReports.TextBox();
            this.lblReference = new DataDynamics.ActiveReports.TextBox();
            this.lblFrom = new DataDynamics.ActiveReports.TextBox();
            this.lblTo = new DataDynamics.ActiveReports.TextBox();
            this.lblValue = new DataDynamics.ActiveReports.TextBox();
            this.lblRef = new DataDynamics.ActiveReports.TextBox();
            this.lblPallets = new DataDynamics.ActiveReports.TextBox();
            this.lblPageHeaderSelfBillNo = new DataDynamics.ActiveReports.Label();
            this.txtPageHeaderSelfBillNo = new DataDynamics.ActiveReports.TextBox();
            this.Detail = new DataDynamics.ActiveReports.Detail();
            this.txtDeliveryDate = new DataDynamics.ActiveReports.TextBox();
            this.txtForeignRate = new DataDynamics.ActiveReports.TextBox();
            this.txtReference = new DataDynamics.ActiveReports.TextBox();
            this.txtSource = new DataDynamics.ActiveReports.TextBox();
            this.txtDestination = new DataDynamics.ActiveReports.TextBox();
            this.txtAdditionalReferences = new DataDynamics.ActiveReports.TextBox();
            this.txtRate = new DataDynamics.ActiveReports.TextBox();
            this.txtPallets = new DataDynamics.ActiveReports.TextBox();
            this.PageFooter = new DataDynamics.ActiveReports.PageFooter();
            this.srFooter = new DataDynamics.ActiveReports.SubReport();
            this.ReportHeader = new DataDynamics.ActiveReports.ReportHeader();
            this.srHeader = new DataDynamics.ActiveReports.SubReport();
            this.lblIsInvoice = new DataDynamics.ActiveReports.Label();
            this.lblIsPreInvoice = new DataDynamics.ActiveReports.Label();
            this.Label18 = new DataDynamics.ActiveReports.Label();
            this.Label19 = new DataDynamics.ActiveReports.Label();
            this.txtInvoiceNo = new DataDynamics.ActiveReports.TextBox();
            this.txtInvoiceDate = new DataDynamics.ActiveReports.TextBox();
            this.txtCustomerName = new DataDynamics.ActiveReports.TextBox();
            this.txtAddress = new DataDynamics.ActiveReports.TextBox();
            this.lblYourVatNo = new DataDynamics.ActiveReports.Label();
            this.txtYourVatNo = new DataDynamics.ActiveReports.TextBox();
            this.lblVatNo = new DataDynamics.ActiveReports.Label();
            this.txtVatNo = new DataDynamics.ActiveReports.TextBox();
            this.lblHelpsWithFormattingDontRemove = new DataDynamics.ActiveReports.Label();
            this.ReportFooter = new DataDynamics.ActiveReports.ReportFooter();
            this.txtInvoiceTotal = new DataDynamics.ActiveReports.TextBox();
            this.lblTotalAmount = new DataDynamics.ActiveReports.Label();
            this.lblVAT = new DataDynamics.ActiveReports.Label();
            this.lblInvoiceTotal = new DataDynamics.ActiveReports.Label();
            this.invoiceTotalTopLine = new DataDynamics.ActiveReports.Line();
            this.invoiceTotalBaseLine = new DataDynamics.ActiveReports.Line();
            this.txtTotalAmount = new DataDynamics.ActiveReports.TextBox();
            this.txtVATAmount = new DataDynamics.ActiveReports.TextBox();
            this.txtVATPercentage = new DataDynamics.ActiveReports.TextBox();
            this.reportFooterLine = new DataDynamics.ActiveReports.Line();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReference)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRef)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPallets)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPageHeaderSelfBillNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageHeaderSelfBillNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDeliveryDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtForeignRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReference)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDestination)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAdditionalReferences)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPallets)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblIsInvoice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblIsPreInvoice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvoiceNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvoiceDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustomerName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblYourVatNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYourVatNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVatNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVatNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHelpsWithFormattingDontRemove)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvoiceTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotalAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVAT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInvoiceTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVATAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVATPercentage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.srPageHeader,
            this.lblDate,
            this.lblReference,
            this.lblFrom,
            this.lblTo,
            this.lblValue,
            this.lblRef,
            this.lblPallets,
            this.lblPageHeaderSelfBillNo,
            this.txtPageHeaderSelfBillNo});
            this.PageHeader.Height = 2.270833F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
            // 
            // srPageHeader
            // 
            this.srPageHeader.Border.BottomColor = System.Drawing.Color.Black;
            this.srPageHeader.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.srPageHeader.Border.LeftColor = System.Drawing.Color.Black;
            this.srPageHeader.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.srPageHeader.Border.RightColor = System.Drawing.Color.Black;
            this.srPageHeader.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.srPageHeader.Border.TopColor = System.Drawing.Color.Black;
            this.srPageHeader.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.srPageHeader.CloseBorder = false;
            this.srPageHeader.Height = 1.875F;
            this.srPageHeader.Left = 0F;
            this.srPageHeader.Name = "srPageHeader";
            this.srPageHeader.Report = null;
            this.srPageHeader.ReportName = "";
            this.srPageHeader.Top = 0.1875F;
            this.srPageHeader.Width = 7.5F;
            // 
            // lblDate
            // 
            this.lblDate.Border.BottomColor = System.Drawing.Color.Black;
            this.lblDate.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblDate.Border.LeftColor = System.Drawing.Color.Black;
            this.lblDate.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblDate.Border.RightColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.lblDate.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblDate.Border.TopColor = System.Drawing.Color.Black;
            this.lblDate.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblDate.Height = 0.2F;
            this.lblDate.Left = 0F;
            this.lblDate.Name = "lblDate";
            this.lblDate.Style = "color: White; text-align: center; background-color: Black; ";
            this.lblDate.Text = "Date";
            this.lblDate.Top = 2.0625F;
            this.lblDate.Width = 0.6875F;
            // 
            // lblReference
            // 
            this.lblReference.Border.BottomColor = System.Drawing.Color.Black;
            this.lblReference.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblReference.Border.LeftColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.lblReference.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblReference.Border.RightColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.lblReference.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblReference.Border.TopColor = System.Drawing.Color.Black;
            this.lblReference.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblReference.Height = 0.2F;
            this.lblReference.Left = 0.6875F;
            this.lblReference.Name = "lblReference";
            this.lblReference.Style = "color: White; text-align: center; background-color: Black; ";
            this.lblReference.Text = "Reference";
            this.lblReference.Top = 2.0625F;
            this.lblReference.Width = 0.8125F;
            // 
            // lblFrom
            // 
            this.lblFrom.Border.BottomColor = System.Drawing.Color.Black;
            this.lblFrom.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblFrom.Border.LeftColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.lblFrom.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblFrom.Border.RightColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.lblFrom.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblFrom.Border.TopColor = System.Drawing.Color.Black;
            this.lblFrom.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblFrom.Height = 0.2F;
            this.lblFrom.Left = 1.5F;
            this.lblFrom.Name = "lblFrom";
            this.lblFrom.Style = "color: White; text-align: center; background-color: Black; ";
            this.lblFrom.Text = "From";
            this.lblFrom.Top = 2.0625F;
            this.lblFrom.Width = 1.3125F;
            // 
            // lblTo
            // 
            this.lblTo.Border.BottomColor = System.Drawing.Color.Black;
            this.lblTo.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblTo.Border.LeftColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.lblTo.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblTo.Border.RightColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.lblTo.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblTo.Border.TopColor = System.Drawing.Color.Black;
            this.lblTo.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblTo.Height = 0.1875F;
            this.lblTo.Left = 2.8125F;
            this.lblTo.Name = "lblTo";
            this.lblTo.Style = "color: White; text-align: center; background-color: Black; ";
            this.lblTo.Text = "To";
            this.lblTo.Top = 2.0625F;
            this.lblTo.Width = 2.5625F;
            // 
            // lblValue
            // 
            this.lblValue.Border.BottomColor = System.Drawing.Color.Black;
            this.lblValue.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblValue.Border.LeftColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.lblValue.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblValue.Border.RightColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.lblValue.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblValue.Border.TopColor = System.Drawing.Color.Black;
            this.lblValue.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblValue.Height = 0.2F;
            this.lblValue.Left = 6.9375F;
            this.lblValue.Name = "lblValue";
            this.lblValue.Style = "color: White; text-align: center; background-color: Black; ";
            this.lblValue.Text = "Rate";
            this.lblValue.Top = 2.0625F;
            this.lblValue.Width = 0.5625F;
            // 
            // lblRef
            // 
            this.lblRef.Border.BottomColor = System.Drawing.Color.Black;
            this.lblRef.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblRef.Border.LeftColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.lblRef.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblRef.Border.RightColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.lblRef.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblRef.Border.TopColor = System.Drawing.Color.Black;
            this.lblRef.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblRef.Height = 0.2F;
            this.lblRef.Left = 5.875F;
            this.lblRef.Name = "lblRef";
            this.lblRef.Style = "color: White; text-align: center; background-color: Black; ";
            this.lblRef.Text = "Additional Refs.";
            this.lblRef.Top = 2.0625F;
            this.lblRef.Width = 1.0625F;
            // 
            // lblPallets
            // 
            this.lblPallets.Border.BottomColor = System.Drawing.Color.Black;
            this.lblPallets.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblPallets.Border.LeftColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.lblPallets.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblPallets.Border.RightColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.lblPallets.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblPallets.Border.TopColor = System.Drawing.Color.Black;
            this.lblPallets.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblPallets.Height = 0.2F;
            this.lblPallets.Left = 5.375F;
            this.lblPallets.Name = "lblPallets";
            this.lblPallets.Style = "color: White; text-align: center; background-color: Black; ";
            this.lblPallets.Text = "Pallets";
            this.lblPallets.Top = 2.0625F;
            this.lblPallets.Width = 0.5F;
            // 
            // lblPageHeaderSelfBillNo
            // 
            this.lblPageHeaderSelfBillNo.Border.BottomColor = System.Drawing.Color.Black;
            this.lblPageHeaderSelfBillNo.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblPageHeaderSelfBillNo.Border.LeftColor = System.Drawing.Color.Black;
            this.lblPageHeaderSelfBillNo.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblPageHeaderSelfBillNo.Border.RightColor = System.Drawing.Color.Black;
            this.lblPageHeaderSelfBillNo.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblPageHeaderSelfBillNo.Border.TopColor = System.Drawing.Color.Black;
            this.lblPageHeaderSelfBillNo.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblPageHeaderSelfBillNo.Height = 0.1875F;
            this.lblPageHeaderSelfBillNo.HyperLink = null;
            this.lblPageHeaderSelfBillNo.Left = 5.75F;
            this.lblPageHeaderSelfBillNo.Name = "lblPageHeaderSelfBillNo";
            this.lblPageHeaderSelfBillNo.Style = "ddo-char-set: 1; font-weight: bold; ";
            this.lblPageHeaderSelfBillNo.Text = "Self-bill No:";
            this.lblPageHeaderSelfBillNo.Top = 0F;
            this.lblPageHeaderSelfBillNo.Width = 0.8125F;
            // 
            // txtPageHeaderSelfBillNo
            // 
            this.txtPageHeaderSelfBillNo.Border.BottomColor = System.Drawing.Color.Black;
            this.txtPageHeaderSelfBillNo.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtPageHeaderSelfBillNo.Border.LeftColor = System.Drawing.Color.Black;
            this.txtPageHeaderSelfBillNo.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtPageHeaderSelfBillNo.Border.RightColor = System.Drawing.Color.Black;
            this.txtPageHeaderSelfBillNo.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtPageHeaderSelfBillNo.Border.TopColor = System.Drawing.Color.Black;
            this.txtPageHeaderSelfBillNo.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtPageHeaderSelfBillNo.Height = 0.1875F;
            this.txtPageHeaderSelfBillNo.Left = 6.5625F;
            this.txtPageHeaderSelfBillNo.MultiLine = false;
            this.txtPageHeaderSelfBillNo.Name = "txtPageHeaderSelfBillNo";
            this.txtPageHeaderSelfBillNo.Style = "color: Black; text-align: right; ";
            this.txtPageHeaderSelfBillNo.Text = "To Be Issued";
            this.txtPageHeaderSelfBillNo.Top = 0F;
            this.txtPageHeaderSelfBillNo.Width = 0.9375F;
            // 
            // Detail
            // 
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.txtDeliveryDate,
            this.txtForeignRate,
            this.txtReference,
            this.txtSource,
            this.txtDestination,
            this.txtAdditionalReferences,
            this.txtRate,
            this.txtPallets});
            this.Detail.Height = 0.1979167F;
            this.Detail.Name = "Detail";
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            this.Detail.BeforePrint += new System.EventHandler(this.Detail_BeforePrint);
            // 
            // txtDeliveryDate
            // 
            this.txtDeliveryDate.Border.BottomColor = System.Drawing.Color.Black;
            this.txtDeliveryDate.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtDeliveryDate.Border.LeftColor = System.Drawing.Color.Black;
            this.txtDeliveryDate.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtDeliveryDate.Border.RightColor = System.Drawing.Color.Black;
            this.txtDeliveryDate.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtDeliveryDate.Border.TopColor = System.Drawing.Color.Black;
            this.txtDeliveryDate.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtDeliveryDate.DataField = "DeliveryDateTime";
            this.txtDeliveryDate.Height = 0.1875F;
            this.txtDeliveryDate.Left = 0F;
            this.txtDeliveryDate.Name = "txtDeliveryDate";
            this.txtDeliveryDate.OutputFormat = resources.GetString("txtDeliveryDate.OutputFormat");
            this.txtDeliveryDate.Style = "color: Black; text-align: center; font-size: 8.25pt; ";
            this.txtDeliveryDate.Text = "Date";
            this.txtDeliveryDate.Top = 0F;
            this.txtDeliveryDate.Width = 0.6875F;
            // 
            // txtForeignRate
            // 
            this.txtForeignRate.Border.BottomColor = System.Drawing.Color.Black;
            this.txtForeignRate.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtForeignRate.Border.LeftColor = System.Drawing.Color.Black;
            this.txtForeignRate.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtForeignRate.Border.RightColor = System.Drawing.Color.Black;
            this.txtForeignRate.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtForeignRate.Border.TopColor = System.Drawing.Color.Black;
            this.txtForeignRate.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtForeignRate.DataField = "ForeignRate";
            this.txtForeignRate.Height = 0.1875F;
            this.txtForeignRate.Left = 6.9375F;
            this.txtForeignRate.Name = "txtForeignRate";
            this.txtForeignRate.OutputFormat = resources.GetString("txtForeignRate.OutputFormat");
            this.txtForeignRate.Style = "color: Black; text-align: right; font-size: 8.25pt; ";
            this.txtForeignRate.Text = "Rate";
            this.txtForeignRate.Top = 0F;
            this.txtForeignRate.Width = 0.5625F;
            // 
            // txtReference
            // 
            this.txtReference.Border.BottomColor = System.Drawing.Color.Black;
            this.txtReference.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtReference.Border.LeftColor = System.Drawing.Color.Black;
            this.txtReference.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtReference.Border.RightColor = System.Drawing.Color.Black;
            this.txtReference.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtReference.Border.TopColor = System.Drawing.Color.Black;
            this.txtReference.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtReference.DataField = "LeadReferenceValue";
            this.txtReference.Height = 0.1875F;
            this.txtReference.Left = 0.6875F;
            this.txtReference.Name = "txtReference";
            this.txtReference.OutputFormat = resources.GetString("txtReference.OutputFormat");
            this.txtReference.Style = "color: Black; text-align: left; font-size: 8.25pt; ";
            this.txtReference.Text = "Lead Ref Val";
            this.txtReference.Top = 0F;
            this.txtReference.Width = 0.8125F;
            // 
            // txtSource
            // 
            this.txtSource.Border.BottomColor = System.Drawing.Color.Black;
            this.txtSource.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtSource.Border.LeftColor = System.Drawing.Color.Black;
            this.txtSource.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtSource.Border.RightColor = System.Drawing.Color.Black;
            this.txtSource.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtSource.Border.TopColor = System.Drawing.Color.Black;
            this.txtSource.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtSource.DataField = "Source";
            this.txtSource.Height = 0.1875F;
            this.txtSource.Left = 1.5F;
            this.txtSource.Name = "txtSource";
            this.txtSource.OutputFormat = resources.GetString("txtSource.OutputFormat");
            this.txtSource.Style = "color: Black; text-align: left; font-size: 8.25pt; ";
            this.txtSource.Text = "From";
            this.txtSource.Top = 0F;
            this.txtSource.Width = 1.3125F;
            // 
            // txtDestination
            // 
            this.txtDestination.Border.BottomColor = System.Drawing.Color.Black;
            this.txtDestination.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtDestination.Border.LeftColor = System.Drawing.Color.Black;
            this.txtDestination.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtDestination.Border.RightColor = System.Drawing.Color.Black;
            this.txtDestination.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtDestination.Border.TopColor = System.Drawing.Color.Black;
            this.txtDestination.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtDestination.DataField = "Destination";
            this.txtDestination.Height = 0.1875F;
            this.txtDestination.Left = 2.8125F;
            this.txtDestination.Name = "txtDestination";
            this.txtDestination.OutputFormat = resources.GetString("txtDestination.OutputFormat");
            this.txtDestination.Style = "color: Black; text-align: left; font-size: 8.25pt; ";
            this.txtDestination.Text = "Destination";
            this.txtDestination.Top = 0F;
            this.txtDestination.Width = 2.5625F;
            // 
            // txtAdditionalReferences
            // 
            this.txtAdditionalReferences.Border.BottomColor = System.Drawing.Color.Black;
            this.txtAdditionalReferences.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtAdditionalReferences.Border.LeftColor = System.Drawing.Color.Black;
            this.txtAdditionalReferences.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtAdditionalReferences.Border.RightColor = System.Drawing.Color.Black;
            this.txtAdditionalReferences.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtAdditionalReferences.Border.TopColor = System.Drawing.Color.Black;
            this.txtAdditionalReferences.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtAdditionalReferences.DataField = "AdditionalReferencesDisplay";
            this.txtAdditionalReferences.Height = 0.1875F;
            this.txtAdditionalReferences.Left = 5.875F;
            this.txtAdditionalReferences.Name = "txtAdditionalReferences";
            this.txtAdditionalReferences.OutputFormat = resources.GetString("txtAdditionalReferences.OutputFormat");
            this.txtAdditionalReferences.Style = "color: Black; ddo-char-set: 0; text-align: right; font-size: 8.25pt; ";
            this.txtAdditionalReferences.Text = "Additional Refs";
            this.txtAdditionalReferences.Top = 0F;
            this.txtAdditionalReferences.Width = 1.0625F;
            // 
            // txtRate
            // 
            this.txtRate.Border.BottomColor = System.Drawing.Color.Black;
            this.txtRate.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtRate.Border.LeftColor = System.Drawing.Color.Black;
            this.txtRate.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtRate.Border.RightColor = System.Drawing.Color.Black;
            this.txtRate.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtRate.Border.TopColor = System.Drawing.Color.Black;
            this.txtRate.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtRate.DataField = "Rate";
            this.txtRate.Height = 0.1875F;
            this.txtRate.Left = 6.9375F;
            this.txtRate.Name = "txtRate";
            this.txtRate.OutputFormat = resources.GetString("txtRate.OutputFormat");
            this.txtRate.Style = "color: Black; text-align: right; font-size: 8.25pt; ";
            this.txtRate.Text = null;
            this.txtRate.Top = 0F;
            this.txtRate.Visible = false;
            this.txtRate.Width = 0.5625F;
            // 
            // txtPallets
            // 
            this.txtPallets.Border.BottomColor = System.Drawing.Color.Black;
            this.txtPallets.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtPallets.Border.LeftColor = System.Drawing.Color.Black;
            this.txtPallets.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtPallets.Border.RightColor = System.Drawing.Color.Black;
            this.txtPallets.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtPallets.Border.TopColor = System.Drawing.Color.Black;
            this.txtPallets.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtPallets.DataField = "PalletCount";
            this.txtPallets.Height = 0.1875F;
            this.txtPallets.Left = 5.375F;
            this.txtPallets.Name = "txtPallets";
            this.txtPallets.OutputFormat = resources.GetString("txtPallets.OutputFormat");
            this.txtPallets.Style = "color: Black; text-align: right; font-size: 8.25pt; ";
            this.txtPallets.Text = "Pallets";
            this.txtPallets.Top = 0F;
            this.txtPallets.Width = 0.5F;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.srFooter});
            this.PageFooter.Height = 1.333333F;
            this.PageFooter.Name = "PageFooter";
            this.PageFooter.Format += new System.EventHandler(this.PageFooter_Format);
            // 
            // srFooter
            // 
            this.srFooter.Border.BottomColor = System.Drawing.Color.Black;
            this.srFooter.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.srFooter.Border.LeftColor = System.Drawing.Color.Black;
            this.srFooter.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.srFooter.Border.RightColor = System.Drawing.Color.Black;
            this.srFooter.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.srFooter.Border.TopColor = System.Drawing.Color.Black;
            this.srFooter.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.srFooter.CloseBorder = false;
            this.srFooter.Height = 1.3125F;
            this.srFooter.Left = 0F;
            this.srFooter.Name = "srFooter";
            this.srFooter.Report = null;
            this.srFooter.Top = 0F;
            this.srFooter.Width = 7.5F;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.srHeader,
            this.lblIsInvoice,
            this.lblIsPreInvoice,
            this.Label18,
            this.Label19,
            this.txtInvoiceNo,
            this.txtInvoiceDate,
            this.txtCustomerName,
            this.txtAddress,
            this.lblYourVatNo,
            this.txtYourVatNo,
            this.lblVatNo,
            this.txtVatNo,
            this.lblHelpsWithFormattingDontRemove});
            this.ReportHeader.Height = 3.5F;
            this.ReportHeader.Name = "ReportHeader";
            this.ReportHeader.Format += new System.EventHandler(this.ReportHeader_Format);
            // 
            // srHeader
            // 
            this.srHeader.Border.BottomColor = System.Drawing.Color.Black;
            this.srHeader.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.srHeader.Border.LeftColor = System.Drawing.Color.Black;
            this.srHeader.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.srHeader.Border.RightColor = System.Drawing.Color.Black;
            this.srHeader.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.srHeader.Border.TopColor = System.Drawing.Color.Black;
            this.srHeader.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.srHeader.CloseBorder = false;
            this.srHeader.Height = 1.875F;
            this.srHeader.Left = 0F;
            this.srHeader.Name = "srHeader";
            this.srHeader.Report = null;
            this.srHeader.ReportName = "rptHeader";
            this.srHeader.Top = 0F;
            this.srHeader.Width = 7.5F;
            // 
            // lblIsInvoice
            // 
            this.lblIsInvoice.Border.BottomColor = System.Drawing.Color.Black;
            this.lblIsInvoice.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblIsInvoice.Border.LeftColor = System.Drawing.Color.Black;
            this.lblIsInvoice.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblIsInvoice.Border.RightColor = System.Drawing.Color.Black;
            this.lblIsInvoice.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblIsInvoice.Border.TopColor = System.Drawing.Color.Black;
            this.lblIsInvoice.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblIsInvoice.Height = 0.25F;
            this.lblIsInvoice.HyperLink = null;
            this.lblIsInvoice.Left = 0.0625F;
            this.lblIsInvoice.Name = "lblIsInvoice";
            this.lblIsInvoice.Style = "ddo-char-set: 1; text-align: center; font-weight: bold; font-size: 14pt; ";
            this.lblIsInvoice.Text = "THIS IS YOUR SELF-BILL";
            this.lblIsInvoice.Top = 3.1875F;
            this.lblIsInvoice.Width = 7.375F;
            // 
            // lblIsPreInvoice
            // 
            this.lblIsPreInvoice.Border.BottomColor = System.Drawing.Color.Black;
            this.lblIsPreInvoice.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblIsPreInvoice.Border.LeftColor = System.Drawing.Color.Black;
            this.lblIsPreInvoice.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblIsPreInvoice.Border.RightColor = System.Drawing.Color.Black;
            this.lblIsPreInvoice.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblIsPreInvoice.Border.TopColor = System.Drawing.Color.Black;
            this.lblIsPreInvoice.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblIsPreInvoice.Height = 0.1875F;
            this.lblIsPreInvoice.HyperLink = null;
            this.lblIsPreInvoice.Left = 0.0625F;
            this.lblIsPreInvoice.Name = "lblIsPreInvoice";
            this.lblIsPreInvoice.Style = "color: Red; text-align: center; ";
            this.lblIsPreInvoice.Text = "This sub-contractor self-bill has not yet been saved, add self-bill to allocate s" +
                "elf-bill No.";
            this.lblIsPreInvoice.Top = 3.1875F;
            this.lblIsPreInvoice.Width = 7.375F;
            // 
            // Label18
            // 
            this.Label18.Border.BottomColor = System.Drawing.Color.Black;
            this.Label18.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label18.Border.LeftColor = System.Drawing.Color.Black;
            this.Label18.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label18.Border.RightColor = System.Drawing.Color.Black;
            this.Label18.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label18.Border.TopColor = System.Drawing.Color.Black;
            this.Label18.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label18.Height = 0.1875F;
            this.Label18.HyperLink = null;
            this.Label18.Left = 4.9375F;
            this.Label18.Name = "Label18";
            this.Label18.Style = "ddo-char-set: 1; font-weight: bold; ";
            this.Label18.Text = "Self-bill No:";
            this.Label18.Top = 2.3125F;
            this.Label18.Width = 0.9375F;
            // 
            // Label19
            // 
            this.Label19.Border.BottomColor = System.Drawing.Color.Black;
            this.Label19.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label19.Border.LeftColor = System.Drawing.Color.Black;
            this.Label19.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label19.Border.RightColor = System.Drawing.Color.Black;
            this.Label19.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label19.Border.TopColor = System.Drawing.Color.Black;
            this.Label19.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label19.Height = 0.1875F;
            this.Label19.HyperLink = null;
            this.Label19.Left = 4.9375F;
            this.Label19.Name = "Label19";
            this.Label19.Style = "ddo-char-set: 1; font-weight: bold; ";
            this.Label19.Text = "Self-bill Date:";
            this.Label19.Top = 2.5F;
            this.Label19.Width = 0.9375F;
            // 
            // txtInvoiceNo
            // 
            this.txtInvoiceNo.Border.BottomColor = System.Drawing.Color.Black;
            this.txtInvoiceNo.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtInvoiceNo.Border.LeftColor = System.Drawing.Color.Black;
            this.txtInvoiceNo.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtInvoiceNo.Border.RightColor = System.Drawing.Color.Black;
            this.txtInvoiceNo.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtInvoiceNo.Border.TopColor = System.Drawing.Color.Black;
            this.txtInvoiceNo.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtInvoiceNo.Height = 0.1875F;
            this.txtInvoiceNo.Left = 5.9375F;
            this.txtInvoiceNo.MultiLine = false;
            this.txtInvoiceNo.Name = "txtInvoiceNo";
            this.txtInvoiceNo.Style = "color: Black; ";
            this.txtInvoiceNo.Text = "To Be Issued";
            this.txtInvoiceNo.Top = 2.3125F;
            this.txtInvoiceNo.Width = 1.5F;
            // 
            // txtInvoiceDate
            // 
            this.txtInvoiceDate.Border.BottomColor = System.Drawing.Color.Black;
            this.txtInvoiceDate.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtInvoiceDate.Border.LeftColor = System.Drawing.Color.Black;
            this.txtInvoiceDate.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtInvoiceDate.Border.RightColor = System.Drawing.Color.Black;
            this.txtInvoiceDate.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtInvoiceDate.Border.TopColor = System.Drawing.Color.Black;
            this.txtInvoiceDate.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtInvoiceDate.Height = 0.1875F;
            this.txtInvoiceDate.Left = 5.9375F;
            this.txtInvoiceDate.MultiLine = false;
            this.txtInvoiceDate.Name = "txtInvoiceDate";
            this.txtInvoiceDate.Style = "";
            this.txtInvoiceDate.Text = null;
            this.txtInvoiceDate.Top = 2.5F;
            this.txtInvoiceDate.Width = 1.5F;
            // 
            // txtCustomerName
            // 
            this.txtCustomerName.Border.BottomColor = System.Drawing.Color.Black;
            this.txtCustomerName.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtCustomerName.Border.LeftColor = System.Drawing.Color.Black;
            this.txtCustomerName.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtCustomerName.Border.RightColor = System.Drawing.Color.Black;
            this.txtCustomerName.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtCustomerName.Border.TopColor = System.Drawing.Color.Black;
            this.txtCustomerName.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtCustomerName.DataField = "OrganisationName";
            this.txtCustomerName.Height = 0.1875F;
            this.txtCustomerName.Left = 0.125F;
            this.txtCustomerName.Name = "txtCustomerName";
            this.txtCustomerName.Style = "ddo-char-set: 1; font-weight: bold; ";
            this.txtCustomerName.Text = null;
            this.txtCustomerName.Top = 1.9375F;
            this.txtCustomerName.Width = 3.4375F;
            // 
            // txtAddress
            // 
            this.txtAddress.Border.BottomColor = System.Drawing.Color.Black;
            this.txtAddress.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtAddress.Border.LeftColor = System.Drawing.Color.Black;
            this.txtAddress.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtAddress.Border.RightColor = System.Drawing.Color.Black;
            this.txtAddress.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtAddress.Border.TopColor = System.Drawing.Color.Black;
            this.txtAddress.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtAddress.Height = 0.75F;
            this.txtAddress.Left = 0.125F;
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Style = "ddo-char-set: 1; font-weight: bold; ";
            this.txtAddress.Text = null;
            this.txtAddress.Top = 2.125F;
            this.txtAddress.Width = 2.1875F;
            // 
            // lblYourVatNo
            // 
            this.lblYourVatNo.Border.BottomColor = System.Drawing.Color.Black;
            this.lblYourVatNo.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblYourVatNo.Border.LeftColor = System.Drawing.Color.Black;
            this.lblYourVatNo.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblYourVatNo.Border.RightColor = System.Drawing.Color.Black;
            this.lblYourVatNo.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblYourVatNo.Border.TopColor = System.Drawing.Color.Black;
            this.lblYourVatNo.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblYourVatNo.Height = 0.1875F;
            this.lblYourVatNo.HyperLink = null;
            this.lblYourVatNo.Left = 4.9375F;
            this.lblYourVatNo.Name = "lblYourVatNo";
            this.lblYourVatNo.Style = "ddo-char-set: 1; font-weight: bold; ";
            this.lblYourVatNo.Text = "Your VAT No:";
            this.lblYourVatNo.Top = 2.875F;
            this.lblYourVatNo.Width = 0.9375F;
            // 
            // txtYourVatNo
            // 
            this.txtYourVatNo.Border.BottomColor = System.Drawing.Color.Black;
            this.txtYourVatNo.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtYourVatNo.Border.LeftColor = System.Drawing.Color.Black;
            this.txtYourVatNo.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtYourVatNo.Border.RightColor = System.Drawing.Color.Black;
            this.txtYourVatNo.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtYourVatNo.Border.TopColor = System.Drawing.Color.Black;
            this.txtYourVatNo.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtYourVatNo.Height = 0.1875F;
            this.txtYourVatNo.Left = 5.9375F;
            this.txtYourVatNo.MultiLine = false;
            this.txtYourVatNo.Name = "txtYourVatNo";
            this.txtYourVatNo.Style = "vertical-align: top; ";
            this.txtYourVatNo.Text = null;
            this.txtYourVatNo.Top = 2.875F;
            this.txtYourVatNo.Width = 1.5F;
            // 
            // lblVatNo
            // 
            this.lblVatNo.Border.BottomColor = System.Drawing.Color.Black;
            this.lblVatNo.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblVatNo.Border.LeftColor = System.Drawing.Color.Black;
            this.lblVatNo.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblVatNo.Border.RightColor = System.Drawing.Color.Black;
            this.lblVatNo.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblVatNo.Border.TopColor = System.Drawing.Color.Black;
            this.lblVatNo.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblVatNo.Height = 0.1875F;
            this.lblVatNo.HyperLink = null;
            this.lblVatNo.Left = 4.9375F;
            this.lblVatNo.Name = "lblVatNo";
            this.lblVatNo.Style = "ddo-char-set: 1; font-weight: bold; ";
            this.lblVatNo.Text = "Our VAT No:";
            this.lblVatNo.Top = 2.6875F;
            this.lblVatNo.Width = 0.9375F;
            // 
            // txtVatNo
            // 
            this.txtVatNo.Border.BottomColor = System.Drawing.Color.Black;
            this.txtVatNo.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtVatNo.Border.LeftColor = System.Drawing.Color.Black;
            this.txtVatNo.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtVatNo.Border.RightColor = System.Drawing.Color.Black;
            this.txtVatNo.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtVatNo.Border.TopColor = System.Drawing.Color.Black;
            this.txtVatNo.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtVatNo.Height = 0.1875F;
            this.txtVatNo.Left = 5.9375F;
            this.txtVatNo.MultiLine = false;
            this.txtVatNo.Name = "txtVatNo";
            this.txtVatNo.Style = "vertical-align: top; ";
            this.txtVatNo.Text = null;
            this.txtVatNo.Top = 2.6875F;
            this.txtVatNo.Width = 1.5F;
            // 
            // lblHelpsWithFormattingDontRemove
            // 
            this.lblHelpsWithFormattingDontRemove.Border.BottomColor = System.Drawing.Color.Black;
            this.lblHelpsWithFormattingDontRemove.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblHelpsWithFormattingDontRemove.Border.LeftColor = System.Drawing.Color.Black;
            this.lblHelpsWithFormattingDontRemove.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblHelpsWithFormattingDontRemove.Border.RightColor = System.Drawing.Color.Black;
            this.lblHelpsWithFormattingDontRemove.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblHelpsWithFormattingDontRemove.Border.TopColor = System.Drawing.Color.Black;
            this.lblHelpsWithFormattingDontRemove.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblHelpsWithFormattingDontRemove.Height = 0.1979167F;
            this.lblHelpsWithFormattingDontRemove.HyperLink = null;
            this.lblHelpsWithFormattingDontRemove.Left = 2.875F;
            this.lblHelpsWithFormattingDontRemove.Name = "lblHelpsWithFormattingDontRemove";
            this.lblHelpsWithFormattingDontRemove.Style = "";
            this.lblHelpsWithFormattingDontRemove.Text = "";
            this.lblHelpsWithFormattingDontRemove.Top = 2.6875F;
            this.lblHelpsWithFormattingDontRemove.Width = 1F;
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.txtInvoiceTotal,
            this.lblTotalAmount,
            this.lblVAT,
            this.lblInvoiceTotal,
            this.invoiceTotalTopLine,
            this.invoiceTotalBaseLine,
            this.txtTotalAmount,
            this.txtVATAmount,
            this.txtVATPercentage,
            this.reportFooterLine});
            this.ReportFooter.Height = 0.8333333F;
            this.ReportFooter.Name = "ReportFooter";
            this.ReportFooter.PrintAtBottom = true;
            this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
            this.ReportFooter.BeforePrint += new System.EventHandler(this.ReportFooter_BeforePrint);
            // 
            // txtInvoiceTotal
            // 
            this.txtInvoiceTotal.Border.BottomColor = System.Drawing.Color.Black;
            this.txtInvoiceTotal.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtInvoiceTotal.Border.LeftColor = System.Drawing.Color.Black;
            this.txtInvoiceTotal.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtInvoiceTotal.Border.RightColor = System.Drawing.Color.Black;
            this.txtInvoiceTotal.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtInvoiceTotal.Border.TopColor = System.Drawing.Color.Black;
            this.txtInvoiceTotal.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtInvoiceTotal.Height = 0.2F;
            this.txtInvoiceTotal.Left = 6.737847F;
            this.txtInvoiceTotal.Name = "txtInvoiceTotal";
            this.txtInvoiceTotal.OutputFormat = resources.GetString("txtInvoiceTotal.OutputFormat");
            this.txtInvoiceTotal.Style = "text-align: right; ";
            this.txtInvoiceTotal.Text = null;
            this.txtInvoiceTotal.Top = 0.5F;
            this.txtInvoiceTotal.Width = 0.7621534F;
            // 
            // lblTotalAmount
            // 
            this.lblTotalAmount.Border.BottomColor = System.Drawing.Color.Black;
            this.lblTotalAmount.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblTotalAmount.Border.LeftColor = System.Drawing.Color.Black;
            this.lblTotalAmount.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblTotalAmount.Border.RightColor = System.Drawing.Color.Black;
            this.lblTotalAmount.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblTotalAmount.Border.TopColor = System.Drawing.Color.Black;
            this.lblTotalAmount.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblTotalAmount.Height = 0.1875F;
            this.lblTotalAmount.HyperLink = null;
            this.lblTotalAmount.Left = 4F;
            this.lblTotalAmount.Name = "lblTotalAmount";
            this.lblTotalAmount.Style = "ddo-char-set: 1; font-weight: bold; ";
            this.lblTotalAmount.Text = "TOTAL AMOUNT";
            this.lblTotalAmount.Top = 0F;
            this.lblTotalAmount.Width = 1.5625F;
            // 
            // lblVAT
            // 
            this.lblVAT.Border.BottomColor = System.Drawing.Color.Black;
            this.lblVAT.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblVAT.Border.LeftColor = System.Drawing.Color.Black;
            this.lblVAT.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblVAT.Border.RightColor = System.Drawing.Color.Black;
            this.lblVAT.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblVAT.Border.TopColor = System.Drawing.Color.Black;
            this.lblVAT.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblVAT.Height = 0.2F;
            this.lblVAT.HyperLink = null;
            this.lblVAT.Left = 4F;
            this.lblVAT.Name = "lblVAT";
            this.lblVAT.Style = "ddo-char-set: 1; font-weight: bold; ";
            this.lblVAT.Text = "VAT @";
            this.lblVAT.Top = 0.1875F;
            this.lblVAT.Width = 0.5625F;
            // 
            // lblInvoiceTotal
            // 
            this.lblInvoiceTotal.Border.BottomColor = System.Drawing.Color.Black;
            this.lblInvoiceTotal.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblInvoiceTotal.Border.LeftColor = System.Drawing.Color.Black;
            this.lblInvoiceTotal.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblInvoiceTotal.Border.RightColor = System.Drawing.Color.Black;
            this.lblInvoiceTotal.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblInvoiceTotal.Border.TopColor = System.Drawing.Color.Black;
            this.lblInvoiceTotal.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblInvoiceTotal.Height = 0.2F;
            this.lblInvoiceTotal.HyperLink = null;
            this.lblInvoiceTotal.Left = 4F;
            this.lblInvoiceTotal.Name = "lblInvoiceTotal";
            this.lblInvoiceTotal.Style = "ddo-char-set: 1; font-weight: bold; ";
            this.lblInvoiceTotal.Text = "SELF-BILL TOTAL";
            this.lblInvoiceTotal.Top = 0.5F;
            this.lblInvoiceTotal.Width = 1.25F;
            // 
            // invoiceTotalTopLine
            // 
            this.invoiceTotalTopLine.Border.BottomColor = System.Drawing.Color.Black;
            this.invoiceTotalTopLine.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.invoiceTotalTopLine.Border.LeftColor = System.Drawing.Color.Black;
            this.invoiceTotalTopLine.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.invoiceTotalTopLine.Border.RightColor = System.Drawing.Color.Black;
            this.invoiceTotalTopLine.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.invoiceTotalTopLine.Border.TopColor = System.Drawing.Color.Black;
            this.invoiceTotalTopLine.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.invoiceTotalTopLine.Height = 0F;
            this.invoiceTotalTopLine.Left = 3.9375F;
            this.invoiceTotalTopLine.LineWeight = 1F;
            this.invoiceTotalTopLine.Name = "invoiceTotalTopLine";
            this.invoiceTotalTopLine.Top = 0.5F;
            this.invoiceTotalTopLine.Width = 3.625F;
            this.invoiceTotalTopLine.X1 = 3.9375F;
            this.invoiceTotalTopLine.X2 = 7.5625F;
            this.invoiceTotalTopLine.Y1 = 0.5F;
            this.invoiceTotalTopLine.Y2 = 0.5F;
            // 
            // invoiceTotalBaseLine
            // 
            this.invoiceTotalBaseLine.Border.BottomColor = System.Drawing.Color.Black;
            this.invoiceTotalBaseLine.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.invoiceTotalBaseLine.Border.LeftColor = System.Drawing.Color.Black;
            this.invoiceTotalBaseLine.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.invoiceTotalBaseLine.Border.RightColor = System.Drawing.Color.Black;
            this.invoiceTotalBaseLine.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.invoiceTotalBaseLine.Border.TopColor = System.Drawing.Color.Black;
            this.invoiceTotalBaseLine.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.invoiceTotalBaseLine.Height = 0F;
            this.invoiceTotalBaseLine.Left = 3.9375F;
            this.invoiceTotalBaseLine.LineWeight = 1F;
            this.invoiceTotalBaseLine.Name = "invoiceTotalBaseLine";
            this.invoiceTotalBaseLine.Top = 0.6875F;
            this.invoiceTotalBaseLine.Width = 3.625F;
            this.invoiceTotalBaseLine.X1 = 3.9375F;
            this.invoiceTotalBaseLine.X2 = 7.5625F;
            this.invoiceTotalBaseLine.Y1 = 0.6875F;
            this.invoiceTotalBaseLine.Y2 = 0.6875F;
            // 
            // txtTotalAmount
            // 
            this.txtTotalAmount.Border.BottomColor = System.Drawing.Color.Black;
            this.txtTotalAmount.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtTotalAmount.Border.LeftColor = System.Drawing.Color.Black;
            this.txtTotalAmount.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtTotalAmount.Border.RightColor = System.Drawing.Color.Black;
            this.txtTotalAmount.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtTotalAmount.Border.TopColor = System.Drawing.Color.Black;
            this.txtTotalAmount.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtTotalAmount.Height = 0.1875F;
            this.txtTotalAmount.Left = 6.75F;
            this.txtTotalAmount.Name = "txtTotalAmount";
            this.txtTotalAmount.OutputFormat = resources.GetString("txtTotalAmount.OutputFormat");
            this.txtTotalAmount.Style = "text-align: right; ";
            this.txtTotalAmount.Text = null;
            this.txtTotalAmount.Top = 0F;
            this.txtTotalAmount.Width = 0.75F;
            // 
            // txtVATAmount
            // 
            this.txtVATAmount.Border.BottomColor = System.Drawing.Color.Black;
            this.txtVATAmount.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtVATAmount.Border.LeftColor = System.Drawing.Color.Black;
            this.txtVATAmount.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtVATAmount.Border.RightColor = System.Drawing.Color.Black;
            this.txtVATAmount.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtVATAmount.Border.TopColor = System.Drawing.Color.Black;
            this.txtVATAmount.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtVATAmount.Height = 0.2F;
            this.txtVATAmount.Left = 6.75F;
            this.txtVATAmount.Name = "txtVATAmount";
            this.txtVATAmount.OutputFormat = resources.GetString("txtVATAmount.OutputFormat");
            this.txtVATAmount.Style = "text-align: right; ";
            this.txtVATAmount.Text = null;
            this.txtVATAmount.Top = 0.1875F;
            this.txtVATAmount.Width = 0.75F;
            // 
            // txtVATPercentage
            // 
            this.txtVATPercentage.Border.BottomColor = System.Drawing.Color.Black;
            this.txtVATPercentage.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtVATPercentage.Border.LeftColor = System.Drawing.Color.Black;
            this.txtVATPercentage.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtVATPercentage.Border.RightColor = System.Drawing.Color.Black;
            this.txtVATPercentage.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtVATPercentage.Border.TopColor = System.Drawing.Color.Black;
            this.txtVATPercentage.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtVATPercentage.Height = 0.2F;
            this.txtVATPercentage.Left = 5.5625F;
            this.txtVATPercentage.Name = "txtVATPercentage";
            this.txtVATPercentage.OutputFormat = resources.GetString("txtVATPercentage.OutputFormat");
            this.txtVATPercentage.Style = "ddo-char-set: 1; font-weight: bold; ";
            this.txtVATPercentage.Text = null;
            this.txtVATPercentage.Top = 0.1875F;
            this.txtVATPercentage.Width = 0.9375F;
            // 
            // reportFooterLine
            // 
            this.reportFooterLine.Border.BottomColor = System.Drawing.Color.Black;
            this.reportFooterLine.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.reportFooterLine.Border.LeftColor = System.Drawing.Color.Black;
            this.reportFooterLine.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.reportFooterLine.Border.RightColor = System.Drawing.Color.Black;
            this.reportFooterLine.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.reportFooterLine.Border.TopColor = System.Drawing.Color.Black;
            this.reportFooterLine.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.reportFooterLine.Height = 0F;
            this.reportFooterLine.Left = 0F;
            this.reportFooterLine.LineWeight = 1F;
            this.reportFooterLine.Name = "reportFooterLine";
            this.reportFooterLine.Top = 0F;
            this.reportFooterLine.Width = 7.5F;
            this.reportFooterLine.X1 = 0F;
            this.reportFooterLine.X2 = 7.5F;
            this.reportFooterLine.Y1 = 0F;
            this.reportFooterLine.Y2 = 0F;
            // 
            // rptNewSubContractorSelfBill
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11.69F;
            this.PageSettings.PaperWidth = 8.27F;
            this.PrintWidth = 7.5F;
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" +
                        "l; font-size: 10pt; color: Black; ", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; ", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" +
                        "lic; ", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ", "Heading3", "Normal"));
            this.ReportStart += new System.EventHandler(this.rptNewSubContractorSelfBill_ReportStart);
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReference)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRef)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPallets)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPageHeaderSelfBillNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPageHeaderSelfBillNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDeliveryDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtForeignRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReference)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDestination)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAdditionalReferences)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPallets)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblIsInvoice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblIsPreInvoice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvoiceNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvoiceDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustomerName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblYourVatNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYourVatNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVatNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVatNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHelpsWithFormattingDontRemove)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvoiceTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotalAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVAT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInvoiceTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVATAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVATPercentage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private DataDynamics.ActiveReports.ReportHeader ReportHeader;
        private DataDynamics.ActiveReports.SubReport srHeader;
        private DataDynamics.ActiveReports.Label lblIsInvoice;
        private DataDynamics.ActiveReports.Label lblIsPreInvoice;
        private DataDynamics.ActiveReports.Label Label18;
        private DataDynamics.ActiveReports.Label Label19;
        private DataDynamics.ActiveReports.TextBox txtInvoiceNo;
        private DataDynamics.ActiveReports.TextBox txtInvoiceDate;
        private DataDynamics.ActiveReports.TextBox txtCustomerName;
        private DataDynamics.ActiveReports.TextBox txtAddress;
        private DataDynamics.ActiveReports.Label lblYourVatNo;
        private DataDynamics.ActiveReports.TextBox txtYourVatNo;
        private DataDynamics.ActiveReports.Label lblVatNo;
        private DataDynamics.ActiveReports.TextBox txtVatNo;
        private DataDynamics.ActiveReports.Label lblHelpsWithFormattingDontRemove;
        private DataDynamics.ActiveReports.ReportFooter ReportFooter;
        private DataDynamics.ActiveReports.SubReport srPageHeader;
        private DataDynamics.ActiveReports.TextBox lblDate;
        private DataDynamics.ActiveReports.TextBox lblReference;
        private DataDynamics.ActiveReports.TextBox lblFrom;
        private DataDynamics.ActiveReports.TextBox lblTo;
        private DataDynamics.ActiveReports.TextBox lblValue;
        private DataDynamics.ActiveReports.TextBox lblRef;
        private DataDynamics.ActiveReports.TextBox txtDeliveryDate;
        private DataDynamics.ActiveReports.TextBox txtForeignRate;
        private DataDynamics.ActiveReports.TextBox txtReference;
        private DataDynamics.ActiveReports.TextBox txtSource;
        private DataDynamics.ActiveReports.TextBox txtDestination;
        private DataDynamics.ActiveReports.TextBox txtAdditionalReferences;
        private DataDynamics.ActiveReports.TextBox txtRate;
        private DataDynamics.ActiveReports.TextBox txtInvoiceTotal;
        private DataDynamics.ActiveReports.Label lblTotalAmount;
        private DataDynamics.ActiveReports.Label lblVAT;
        private DataDynamics.ActiveReports.Label lblInvoiceTotal;
        private DataDynamics.ActiveReports.Line invoiceTotalTopLine;
        private DataDynamics.ActiveReports.Line invoiceTotalBaseLine;
        private DataDynamics.ActiveReports.TextBox txtTotalAmount;
        private DataDynamics.ActiveReports.TextBox txtVATAmount;
        private DataDynamics.ActiveReports.TextBox txtVATPercentage;
        private DataDynamics.ActiveReports.Line reportFooterLine;
        private DataDynamics.ActiveReports.SubReport srFooter;
        private DataDynamics.ActiveReports.TextBox lblPallets;
        private DataDynamics.ActiveReports.TextBox txtPallets;
        private DataDynamics.ActiveReports.Label lblPageHeaderSelfBillNo;
        private DataDynamics.ActiveReports.TextBox txtPageHeaderSelfBillNo;

    }
}
