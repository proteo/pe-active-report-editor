namespace Orchestrator.Reports.Invoicing.SubReports
{
    /// <summary>
    /// Summary description for FandWFooter.
    /// </summary>
    partial class FandWFooter
    {
        private DataDynamics.ActiveReports.PageHeader pageHeader;
        private DataDynamics.ActiveReports.Detail detail;
        private DataDynamics.ActiveReports.PageFooter pageFooter;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FandWFooter));
            this.pageHeader = new DataDynamics.ActiveReports.PageHeader();
            this.detail = new DataDynamics.ActiveReports.Detail();
            this.pageFooter = new DataDynamics.ActiveReports.PageFooter();
            this.reportHeader1 = new DataDynamics.ActiveReports.ReportHeader();
            this.reportFooter1 = new DataDynamics.ActiveReports.ReportFooter();
            this.picture1 = new DataDynamics.ActiveReports.Picture();
            this.picture2 = new DataDynamics.ActiveReports.Picture();
            this.picture3 = new DataDynamics.ActiveReports.Picture();
            this.picture4 = new DataDynamics.ActiveReports.Picture();
            this.picture5 = new DataDynamics.ActiveReports.Picture();
            this.label1 = new DataDynamics.ActiveReports.Label();
            this.label2 = new DataDynamics.ActiveReports.Label();
            this.label3 = new DataDynamics.ActiveReports.Label();
            ((System.ComponentModel.ISupportInitialize)(this.picture1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Height = 0F;
            this.pageHeader.Name = "pageHeader";
            // 
            // detail
            // 
            this.detail.ColumnSpacing = 0F;
            this.detail.Height = 0F;
            this.detail.Name = "detail";
            // 
            // pageFooter
            // 
            this.pageFooter.Height = 0F;
            this.pageFooter.Name = "pageFooter";
            // 
            // reportHeader1
            // 
            this.reportHeader1.Height = 0F;
            this.reportHeader1.Name = "reportHeader1";
            // 
            // reportFooter1
            // 
            this.reportFooter1.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.picture1,
            this.picture2,
            this.picture3,
            this.picture4,
            this.picture5,
            this.label1,
            this.label2,
            this.label3});
            this.reportFooter1.Height = 1.072917F;
            this.reportFooter1.Name = "reportFooter1";
            // 
            // picture1
            // 
            this.picture1.Border.BottomColor = System.Drawing.Color.Black;
            this.picture1.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.picture1.Border.LeftColor = System.Drawing.Color.Black;
            this.picture1.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.picture1.Border.RightColor = System.Drawing.Color.Black;
            this.picture1.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.picture1.Border.TopColor = System.Drawing.Color.Black;
            this.picture1.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.picture1.Height = 0.625F;
            this.picture1.Image = ((System.Drawing.Image)(resources.GetObject("picture1.Image")));
            this.picture1.ImageData = ((System.IO.Stream)(resources.GetObject("picture1.ImageData")));
            this.picture1.Left = 1.0625F;
            this.picture1.LineWeight = 0F;
            this.picture1.Name = "picture1";
            this.picture1.Top = 0F;
            this.picture1.Width = 0.625F;
            // 
            // picture2
            // 
            this.picture2.Border.BottomColor = System.Drawing.Color.Black;
            this.picture2.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.picture2.Border.LeftColor = System.Drawing.Color.Black;
            this.picture2.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.picture2.Border.RightColor = System.Drawing.Color.Black;
            this.picture2.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.picture2.Border.TopColor = System.Drawing.Color.Black;
            this.picture2.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.picture2.Height = 0.5F;
            this.picture2.Image = ((System.Drawing.Image)(resources.GetObject("picture2.Image")));
            this.picture2.ImageData = ((System.IO.Stream)(resources.GetObject("picture2.ImageData")));
            this.picture2.Left = 1.8125F;
            this.picture2.LineWeight = 0F;
            this.picture2.Name = "picture2";
            this.picture2.Top = 0.0625F;
            this.picture2.Width = 1.6875F;
            // 
            // picture3
            // 
            this.picture3.Border.BottomColor = System.Drawing.Color.Black;
            this.picture3.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.picture3.Border.LeftColor = System.Drawing.Color.Black;
            this.picture3.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.picture3.Border.RightColor = System.Drawing.Color.Black;
            this.picture3.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.picture3.Border.TopColor = System.Drawing.Color.Black;
            this.picture3.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.picture3.Height = 0.5F;
            this.picture3.Image = ((System.Drawing.Image)(resources.GetObject("picture3.Image")));
            this.picture3.ImageData = ((System.IO.Stream)(resources.GetObject("picture3.ImageData")));
            this.picture3.Left = 3.625F;
            this.picture3.LineWeight = 0F;
            this.picture3.Name = "picture3";
            this.picture3.SizeMode = DataDynamics.ActiveReports.SizeModes.Stretch;
            this.picture3.Top = 0.0625F;
            this.picture3.Width = 1F;
            // 
            // picture4
            // 
            this.picture4.Border.BottomColor = System.Drawing.Color.Black;
            this.picture4.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.picture4.Border.LeftColor = System.Drawing.Color.Black;
            this.picture4.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.picture4.Border.RightColor = System.Drawing.Color.Black;
            this.picture4.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.picture4.Border.TopColor = System.Drawing.Color.Black;
            this.picture4.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.picture4.Height = 0.625F;
            this.picture4.Image = ((System.Drawing.Image)(resources.GetObject("picture4.Image")));
            this.picture4.ImageData = ((System.IO.Stream)(resources.GetObject("picture4.ImageData")));
            this.picture4.Left = 4.75F;
            this.picture4.LineWeight = 0F;
            this.picture4.Name = "picture4";
            this.picture4.Top = 0F;
            this.picture4.Width = 0.5625F;
            // 
            // picture5
            // 
            this.picture5.Border.BottomColor = System.Drawing.Color.Black;
            this.picture5.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.picture5.Border.LeftColor = System.Drawing.Color.Black;
            this.picture5.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.picture5.Border.RightColor = System.Drawing.Color.Black;
            this.picture5.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.picture5.Border.TopColor = System.Drawing.Color.Black;
            this.picture5.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.picture5.Height = 0.5625F;
            this.picture5.Image = ((System.Drawing.Image)(resources.GetObject("picture5.Image")));
            this.picture5.ImageData = ((System.IO.Stream)(resources.GetObject("picture5.ImageData")));
            this.picture5.Left = 5.4375F;
            this.picture5.LineWeight = 0F;
            this.picture5.Name = "picture5";
            this.picture5.Top = 0.0625F;
            this.picture5.Width = 0.875F;
            // 
            // label1
            // 
            this.label1.Border.BottomColor = System.Drawing.Color.Black;
            this.label1.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.label1.Border.LeftColor = System.Drawing.Color.Black;
            this.label1.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.label1.Border.RightColor = System.Drawing.Color.Black;
            this.label1.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.label1.Border.TopColor = System.Drawing.Color.Black;
            this.label1.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.label1.Height = 0.1875F;
            this.label1.HyperLink = null;
            this.label1.Left = 0.9375F;
            this.label1.Name = "label1";
            this.label1.Style = "ddo-char-set: 1; text-align: left; font-size: 8pt; font-family: Arial; vertical-a" +
                "lign: middle; ";
            this.label1.Text = "VAT REG. NO. 175 2372 60";
            this.label1.Top = 0.625F;
            this.label1.Width = 1.4375F;
            // 
            // label2
            // 
            this.label2.Border.BottomColor = System.Drawing.Color.Black;
            this.label2.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.label2.Border.LeftColor = System.Drawing.Color.Black;
            this.label2.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.label2.Border.RightColor = System.Drawing.Color.Black;
            this.label2.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.label2.Border.TopColor = System.Drawing.Color.Black;
            this.label2.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.label2.Height = 0.1875F;
            this.label2.HyperLink = null;
            this.label2.Left = 3.625F;
            this.label2.Name = "label2";
            this.label2.Style = "ddo-char-set: 0; text-align: right; font-size: 8.25pt; font-family: Arial; vertic" +
                "al-align: middle; ";
            this.label2.Text = "TERMS 30 DAYS NETT FROM DATE OF INVOICE";
            this.label2.Top = 0.625F;
            this.label2.Width = 2.8125F;
            // 
            // label3
            // 
            this.label3.Border.BottomColor = System.Drawing.Color.Black;
            this.label3.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.label3.Border.LeftColor = System.Drawing.Color.Black;
            this.label3.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.label3.Border.RightColor = System.Drawing.Color.Black;
            this.label3.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.label3.Border.TopColor = System.Drawing.Color.Black;
            this.label3.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.label3.Height = 0.25F;
            this.label3.HyperLink = null;
            this.label3.Left = 0.8125F;
            this.label3.Name = "label3";
            this.label3.Style = "ddo-char-set: 0; text-align: center; font-size: 6.75pt; font-family: Arial; verti" +
                "cal-align: top; ";
            this.label3.Text = "All goods are carried and stored in accordance with the Road Haulage Association " +
                "conditions of carriage 1998, copies on application.";
            this.label3.Top = 0.8125F;
            this.label3.Width = 5.75F;
            // 
            // FandWFooter
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.5F;
            this.Sections.Add(this.reportHeader1);
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.pageFooter);
            this.Sections.Add(this.reportFooter1);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" +
                        "l; font-size: 10pt; color: Black; ", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; ", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" +
                        "lic; ", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.picture1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private DataDynamics.ActiveReports.ReportHeader reportHeader1;
        private DataDynamics.ActiveReports.ReportFooter reportFooter1;
        private DataDynamics.ActiveReports.Picture picture1;
        private DataDynamics.ActiveReports.Picture picture2;
        private DataDynamics.ActiveReports.Picture picture3;
        private DataDynamics.ActiveReports.Picture picture4;
        private DataDynamics.ActiveReports.Picture picture5;
        private DataDynamics.ActiveReports.Label label1;
        private DataDynamics.ActiveReports.Label label2;
        private DataDynamics.ActiveReports.Label label3;
    }
}
