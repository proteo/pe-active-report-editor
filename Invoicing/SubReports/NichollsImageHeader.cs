using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;
using System.Configuration;
using System.Collections.Specialized;
using System.Data;
using System.Web;

namespace Orchestrator.Reports.Invoicing.SubReports
{

    //-------------------------------------------------------------------------------------

    /// <summary>
    /// Summary description for NichollsImageHeader.
    /// </summary>
    public partial class NichollsImageHeader : DataDynamics.ActiveReports.ActiveReport3
    {

        //-------------------------------------------------------------------------------------

        public NichollsImageHeader()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();

            this.lblCompanyName.Text = ConfigurationManager.AppSettings["CompanyName"];
            this.lblAddressLine1.Text = ConfigurationManager.AppSettings["Address1.Line1"];
            this.lblAddressLine2.Text = ConfigurationManager.AppSettings["Address1.Line2"];
            this.lblAddressLine3.Text = ConfigurationManager.AppSettings["Address1.Line3"];

            this.lblRegNoTitle.Text = "Reg No: " + ConfigurationManager.AppSettings["RegistrationNumber"];
        }

        //-------------------------------------------------------------------------------------

    }

    //-------------------------------------------------------------------------------------
}
