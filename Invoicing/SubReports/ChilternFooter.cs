using System;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;

namespace Orchestrator.Reports.Invoicing.SubReports
{
	public class ChilternFooter : DataDynamics.ActiveReports.ActiveReport3
	{
		public ChilternFooter()
		{
			InitializeComponent();
		}


		#region ActiveReports Designer generated code
		private DataDynamics.ActiveReports.ReportHeader ReportHeader = null;
		private DataDynamics.ActiveReports.PageHeader PageHeader = null;
		private DataDynamics.ActiveReports.Detail Detail = null;
		private DataDynamics.ActiveReports.PageFooter PageFooter = null;
        private DataDynamics.ActiveReports.ReportFooter ReportFooter = null;
		private Label lblChequePayable;
        private Label lblBankDetail;
        private Label Label;
        private Label label2;
		private DataDynamics.ActiveReports.Label Label1 = null;
		public void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ChilternFooter));
            this.Detail = new DataDynamics.ActiveReports.Detail();
            this.ReportHeader = new DataDynamics.ActiveReports.ReportHeader();
            this.ReportFooter = new DataDynamics.ActiveReports.ReportFooter();
            this.Label = new DataDynamics.ActiveReports.Label();
            this.Label1 = new DataDynamics.ActiveReports.Label();
            this.lblChequePayable = new DataDynamics.ActiveReports.Label();
            this.lblBankDetail = new DataDynamics.ActiveReports.Label();
            this.PageHeader = new DataDynamics.ActiveReports.PageHeader();
            this.PageFooter = new DataDynamics.ActiveReports.PageFooter();
            this.label2 = new DataDynamics.ActiveReports.Label();
            ((System.ComponentModel.ISupportInitialize)(this.Label)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblChequePayable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBankDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Height = 0F;
            this.Detail.Name = "Detail";
            // 
            // ReportHeader
            // 
            this.ReportHeader.Height = 0F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.Label,
            this.Label1,
            this.lblChequePayable,
            this.lblBankDetail,
            this.label2});
            this.ReportFooter.Height = 1.3125F;
            this.ReportFooter.Name = "ReportFooter";
            this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
            this.ReportFooter.BeforePrint += new System.EventHandler(this.ReportFooter_BeforePrint);
            // 
            // Label
            // 
            this.Label.Border.BottomColor = System.Drawing.Color.Black;
            this.Label.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label.Border.LeftColor = System.Drawing.Color.Black;
            this.Label.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label.Border.RightColor = System.Drawing.Color.Black;
            this.Label.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label.Border.TopColor = System.Drawing.Color.Black;
            this.Label.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label.Height = 0.125F;
            this.Label.HyperLink = null;
            this.Label.Left = 1F;
            this.Label.Name = "Label";
            this.Label.Style = "ddo-char-set: 1; text-align: center; font-size: 6.5pt; ";
            this.Label.Text = "services of the Food Storage and Distribution Federation (incorpating CSDF), copi" +
                "es of which are available on request.";
            this.Label.Top = 1F;
            this.Label.Width = 5.5625F;
            // 
            // Label1
            // 
            this.Label1.Border.BottomColor = System.Drawing.Color.Black;
            this.Label1.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label1.Border.LeftColor = System.Drawing.Color.Black;
            this.Label1.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label1.Border.RightColor = System.Drawing.Color.Black;
            this.Label1.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label1.Border.TopColor = System.Drawing.Color.Black;
            this.Label1.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label1.Height = 0.125F;
            this.Label1.HyperLink = null;
            this.Label1.Left = 1F;
            this.Label1.Name = "Label1";
            this.Label1.Style = "ddo-char-set: 1; text-align: center; font-size: 6.5pt; ";
            this.Label1.Text = "All business of this company is transacted under the recommended condition for st" +
                "orage & distribution";
            this.Label1.Top = 0.875F;
            this.Label1.Width = 5.5625F;
            // 
            // lblChequePayable
            // 
            this.lblChequePayable.Border.BottomColor = System.Drawing.Color.Black;
            this.lblChequePayable.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblChequePayable.Border.LeftColor = System.Drawing.Color.Black;
            this.lblChequePayable.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblChequePayable.Border.RightColor = System.Drawing.Color.Black;
            this.lblChequePayable.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblChequePayable.Border.TopColor = System.Drawing.Color.Black;
            this.lblChequePayable.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblChequePayable.Height = 0.1875F;
            this.lblChequePayable.HyperLink = null;
            this.lblChequePayable.Left = 1.75F;
            this.lblChequePayable.Name = "lblChequePayable";
            this.lblChequePayable.Style = "ddo-char-set: 1; text-align: center; font-size: 10pt; ";
            this.lblChequePayable.Text = "Cheques payable to Chiltern Distribution Ltd";
            this.lblChequePayable.Top = 0.125F;
            this.lblChequePayable.Width = 4F;
            // 
            // lblBankDetail
            // 
            this.lblBankDetail.Border.BottomColor = System.Drawing.Color.Black;
            this.lblBankDetail.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblBankDetail.Border.LeftColor = System.Drawing.Color.Black;
            this.lblBankDetail.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblBankDetail.Border.RightColor = System.Drawing.Color.Black;
            this.lblBankDetail.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblBankDetail.Border.TopColor = System.Drawing.Color.Black;
            this.lblBankDetail.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblBankDetail.Height = 0.1875F;
            this.lblBankDetail.HyperLink = null;
            this.lblBankDetail.Left = 1.75F;
            this.lblBankDetail.Name = "lblBankDetail";
            this.lblBankDetail.Style = "ddo-char-set: 1; text-align: center; font-size: 10pt; ";
            this.lblBankDetail.Text = "BACS:  Sort Code 20-37-63  Account No 03801195";
            this.lblBankDetail.Top = 0.3125F;
            this.lblBankDetail.Width = 4F;
            // 
            // PageHeader
            // 
            this.PageHeader.Height = 0F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // label2
            // 
            this.label2.Border.BottomColor = System.Drawing.Color.Black;
            this.label2.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.label2.Border.LeftColor = System.Drawing.Color.Black;
            this.label2.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.label2.Border.RightColor = System.Drawing.Color.Black;
            this.label2.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.label2.Border.TopColor = System.Drawing.Color.Black;
            this.label2.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.label2.Height = 0.1875F;
            this.label2.HyperLink = null;
            this.label2.Left = 1.75F;
            this.label2.Name = "label2";
            this.label2.Style = "ddo-char-set: 1; text-align: center; font-size: 10pt; ";
            this.label2.Text = "Registered in England and Wales. Registration No. 05771015";
            this.label2.Top = 0.5625F;
            this.label2.Width = 4F;
            // 
            // ChilternFooter
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11.69F;
            this.PageSettings.PaperWidth = 8.27F;
            this.PrintWidth = 7.520833F;
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                        "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.Label)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblChequePayable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBankDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}

		#endregion

		private void ReportFooter_BeforePrint(object sender, EventArgs e)
		{            
			//If this footer is for the Credit report or Subby Self Bill
			//do NOT display the payment terms and bank details
            if (this.ParentReport.ParentReport != null)
            {
                System.Type reportType = this.ParentReport.ParentReport.GetType();
                if (reportType == typeof(rptCreditNoteOneLiner)
                    || reportType == typeof(rptSubContractorSelfBill)
                    || reportType == typeof(rptNewSubContractorSelfBill))
                {
                    lblChequePayable.Visible = false;
                    lblBankDetail.Visible = false;
                }
            }
		}

        private void ReportFooter_Format(object sender, EventArgs e)
        {

        }
	}
}
