using System;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;

namespace Orchestrator.Reports.Invoicing.SubReports
{
    public class References : DataDynamics.ActiveReports.ActiveReport3
	{
		public References()
		{
			InitializeComponent();
		}

		private void References_ReportStart(object sender, System.EventArgs eArgs)
		{
			
		}

		#region ActiveReports Designer generated code
		private DataDynamics.ActiveReports.Detail Detail = null;
		private DataDynamics.ActiveReports.TextBox txtReference = null;
		public void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(References));
            this.Detail = new DataDynamics.ActiveReports.Detail();
            this.txtReference = new DataDynamics.ActiveReports.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.txtReference)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.txtReference});
            this.Detail.Height = 0.2F;
            this.Detail.Name = "Detail";
            // 
            // txtReference
            // 
            this.txtReference.Border.BottomColor = System.Drawing.Color.Black;
            this.txtReference.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtReference.Border.LeftColor = System.Drawing.Color.Black;
            this.txtReference.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtReference.Border.RightColor = System.Drawing.Color.Black;
            this.txtReference.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtReference.Border.TopColor = System.Drawing.Color.Black;
            this.txtReference.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtReference.DataField = "Reference";
            this.txtReference.Height = 0.1875F;
            this.txtReference.Left = 0F;
            this.txtReference.Name = "txtReference";
            this.txtReference.Style = "";
            this.txtReference.Text = "Reference";
            this.txtReference.Top = 0F;
            this.txtReference.Width = 0.9375F;
            // 
            // References
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 1.125F;
            this.Sections.Add(this.Detail);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                        "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading3", "Normal"));
            this.ReportStart += new System.EventHandler(this.References_ReportStart);
            ((System.ComponentModel.ISupportInitialize)(this.txtReference)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		 }

		#endregion
	}
}
