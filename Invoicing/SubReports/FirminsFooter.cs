using System;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;

namespace Orchestrator.Reports.Invoicing.SubReports
{
	public class FirminsFooter : DataDynamics.ActiveReports.ActiveReport3
	{
		public FirminsFooter()
		{
			InitializeComponent();
		}


		#region ActiveReports Designer generated code
		private DataDynamics.ActiveReports.ReportHeader ReportHeader = null;
		private DataDynamics.ActiveReports.PageHeader PageHeader = null;
		private DataDynamics.ActiveReports.Detail Detail = null;
		private DataDynamics.ActiveReports.PageFooter PageFooter = null;
        private DataDynamics.ActiveReports.ReportFooter ReportFooter = null;
		private DataDynamics.ActiveReports.Label lblAddressee = null;
		private DataDynamics.ActiveReports.Label Label = null;
		private Label lblChequePayable;
		private Label lblBankDetail;
        private Label lblOutputTax;
        private Label lblIBan;
        private Label lblSwift;
        private Picture PictureISO14001;
        private Picture PictureISO18001;
        private Picture PictureISO9001;
		private DataDynamics.ActiveReports.Label Label1 = null;
		public void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FirminsFooter));
            this.Detail = new DataDynamics.ActiveReports.Detail();
            this.ReportHeader = new DataDynamics.ActiveReports.ReportHeader();
            this.ReportFooter = new DataDynamics.ActiveReports.ReportFooter();
            this.lblAddressee = new DataDynamics.ActiveReports.Label();
            this.Label = new DataDynamics.ActiveReports.Label();
            this.Label1 = new DataDynamics.ActiveReports.Label();
            this.lblChequePayable = new DataDynamics.ActiveReports.Label();
            this.lblBankDetail = new DataDynamics.ActiveReports.Label();
            this.lblOutputTax = new DataDynamics.ActiveReports.Label();
            this.lblIBan = new DataDynamics.ActiveReports.Label();
            this.lblSwift = new DataDynamics.ActiveReports.Label();
            this.PictureISO14001 = new DataDynamics.ActiveReports.Picture();
            this.PictureISO18001 = new DataDynamics.ActiveReports.Picture();
            this.PageHeader = new DataDynamics.ActiveReports.PageHeader();
            this.PageFooter = new DataDynamics.ActiveReports.PageFooter();
            this.PictureISO9001 = new DataDynamics.ActiveReports.Picture();
            ((System.ComponentModel.ISupportInitialize)(this.lblAddressee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblChequePayable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBankDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblOutputTax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblIBan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSwift)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureISO14001)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureISO18001)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureISO9001)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Height = 0F;
            this.Detail.Name = "Detail";
            // 
            // ReportHeader
            // 
            this.ReportHeader.Height = 0F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.lblAddressee,
            this.Label,
            this.Label1,
            this.lblChequePayable,
            this.lblBankDetail,
            this.lblOutputTax,
            this.lblIBan,
            this.lblSwift,
            this.PictureISO9001,
            this.PictureISO14001,
            this.PictureISO18001});
            this.ReportFooter.Height = 1.3125F;
            this.ReportFooter.Name = "ReportFooter";
            this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
            this.ReportFooter.BeforePrint += new System.EventHandler(this.ReportFooter_BeforePrint);
            // 
            // lblAddressee
            // 
            this.lblAddressee.Border.BottomColor = System.Drawing.Color.Black;
            this.lblAddressee.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblAddressee.Border.LeftColor = System.Drawing.Color.Black;
            this.lblAddressee.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblAddressee.Border.RightColor = System.Drawing.Color.Black;
            this.lblAddressee.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblAddressee.Border.TopColor = System.Drawing.Color.Black;
            this.lblAddressee.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblAddressee.Height = 0.1875F;
            this.lblAddressee.HyperLink = null;
            this.lblAddressee.Left = 0F;
            this.lblAddressee.Name = "lblAddressee";
            this.lblAddressee.Style = "ddo-char-set: 1; text-align: center; font-weight: bold; font-size: 12pt; ";
            this.lblAddressee.Text = "ADDRESSEE COPY";
            this.lblAddressee.Top = 1.0125F;
            this.lblAddressee.Width = 1.75F;
            // 
            // Label
            // 
            this.Label.Border.BottomColor = System.Drawing.Color.Black;
            this.Label.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label.Border.LeftColor = System.Drawing.Color.Black;
            this.Label.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label.Border.RightColor = System.Drawing.Color.Black;
            this.Label.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label.Border.TopColor = System.Drawing.Color.Black;
            this.Label.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label.Height = 0.125F;
            this.Label.HyperLink = null;
            this.Label.Left = 1.8125F;
            this.Label.Name = "Label";
            this.Label.Style = "ddo-char-set: 1; text-align: center; font-size: 6.5pt; ";
            this.Label.Text = "All goods stored in accordance with the United Kingdom Warehousing Association Co" +
                "nditions of Contract, copies available upon request.";
            this.Label.Top = 1F;
            this.Label.Width = 5.5625F;
            // 
            // Label1
            // 
            this.Label1.Border.BottomColor = System.Drawing.Color.Black;
            this.Label1.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label1.Border.LeftColor = System.Drawing.Color.Black;
            this.Label1.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label1.Border.RightColor = System.Drawing.Color.Black;
            this.Label1.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label1.Border.TopColor = System.Drawing.Color.Black;
            this.Label1.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label1.Height = 0.125F;
            this.Label1.HyperLink = null;
            this.Label1.Left = 2.125F;
            this.Label1.Name = "Label1";
            this.Label1.Style = "ddo-char-set: 1; text-align: center; font-size: 6.5pt; ";
            this.Label1.Text = "All goods carried in accordance with the Road Haulage Association Conditions of C" +
                "arriage, copies available upon request.";
            this.Label1.Top = 1.125F;
            this.Label1.Width = 4.9375F;
            // 
            // lblChequePayable
            // 
            this.lblChequePayable.Border.BottomColor = System.Drawing.Color.Black;
            this.lblChequePayable.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblChequePayable.Border.LeftColor = System.Drawing.Color.Black;
            this.lblChequePayable.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblChequePayable.Border.RightColor = System.Drawing.Color.Black;
            this.lblChequePayable.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblChequePayable.Border.TopColor = System.Drawing.Color.Black;
            this.lblChequePayable.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblChequePayable.Height = 0.1875F;
            this.lblChequePayable.HyperLink = null;
            this.lblChequePayable.Left = 3.4375F;
            this.lblChequePayable.Name = "lblChequePayable";
            this.lblChequePayable.Style = "ddo-char-set: 1; text-align: center; font-size: 10pt; ";
            this.lblChequePayable.Text = "Cheques payable to Alan Firmin Ltd";
            this.lblChequePayable.Top = 0.1875F;
            this.lblChequePayable.Width = 4F;
            // 
            // lblBankDetail
            // 
            this.lblBankDetail.Border.BottomColor = System.Drawing.Color.Black;
            this.lblBankDetail.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblBankDetail.Border.LeftColor = System.Drawing.Color.Black;
            this.lblBankDetail.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblBankDetail.Border.RightColor = System.Drawing.Color.Black;
            this.lblBankDetail.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblBankDetail.Border.TopColor = System.Drawing.Color.Black;
            this.lblBankDetail.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblBankDetail.Height = 0.1875F;
            this.lblBankDetail.HyperLink = null;
            this.lblBankDetail.Left = 3.4375F;
            this.lblBankDetail.Name = "lblBankDetail";
            this.lblBankDetail.Style = "ddo-char-set: 1; text-align: center; font-size: 10pt; ";
            this.lblBankDetail.Text = "BACS:  Sort Code 60-04-02  Account No 42251354";
            this.lblBankDetail.Top = 0.375F;
            this.lblBankDetail.Width = 4F;
            // 
            // lblOutputTax
            // 
            this.lblOutputTax.Border.BottomColor = System.Drawing.Color.Black;
            this.lblOutputTax.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblOutputTax.Border.LeftColor = System.Drawing.Color.Black;
            this.lblOutputTax.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblOutputTax.Border.RightColor = System.Drawing.Color.Black;
            this.lblOutputTax.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblOutputTax.Border.TopColor = System.Drawing.Color.Black;
            this.lblOutputTax.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblOutputTax.Height = 0.1875F;
            this.lblOutputTax.HyperLink = null;
            this.lblOutputTax.Left = 3.4375F;
            this.lblOutputTax.Name = "lblOutputTax";
            this.lblOutputTax.Style = "text-align: center; ";
            this.lblOutputTax.Text = "THE TAX SHOWN IS YOUR OUTPUT TAX DUE TO HMRC";
            this.lblOutputTax.Top = 0F;
            this.lblOutputTax.Visible = false;
            this.lblOutputTax.Width = 4F;
            // 
            // lblIBan
            // 
            this.lblIBan.Border.BottomColor = System.Drawing.Color.Black;
            this.lblIBan.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblIBan.Border.LeftColor = System.Drawing.Color.Black;
            this.lblIBan.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblIBan.Border.RightColor = System.Drawing.Color.Black;
            this.lblIBan.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblIBan.Border.TopColor = System.Drawing.Color.Black;
            this.lblIBan.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblIBan.Height = 0.1875F;
            this.lblIBan.HyperLink = null;
            this.lblIBan.Left = 3.4375F;
            this.lblIBan.Name = "lblIBan";
            this.lblIBan.Style = "ddo-char-set: 1; text-align: center; font-size: 10pt; ";
            this.lblIBan.Text = "i-BAN: GB33 NWB K 6004 0242 2513 54";
            this.lblIBan.Top = 0.5625F;
            this.lblIBan.Width = 4F;
            // 
            // lblSwift
            // 
            this.lblSwift.Border.BottomColor = System.Drawing.Color.Black;
            this.lblSwift.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblSwift.Border.LeftColor = System.Drawing.Color.Black;
            this.lblSwift.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblSwift.Border.RightColor = System.Drawing.Color.Black;
            this.lblSwift.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblSwift.Border.TopColor = System.Drawing.Color.Black;
            this.lblSwift.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblSwift.Height = 0.1875F;
            this.lblSwift.HyperLink = null;
            this.lblSwift.Left = 3.4375F;
            this.lblSwift.Name = "lblSwift";
            this.lblSwift.Style = "ddo-char-set: 1; text-align: center; font-size: 10pt; ";
            this.lblSwift.Text = "Swift/BIC code: NWBK GB 2L";
            this.lblSwift.Top = 0.75F;
            this.lblSwift.Width = 4F;
            // 
            // PictureISO14001
            // 
            this.PictureISO14001.Border.BottomColor = System.Drawing.Color.Black;
            this.PictureISO14001.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.PictureISO14001.Border.LeftColor = System.Drawing.Color.Black;
            this.PictureISO14001.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.PictureISO14001.Border.RightColor = System.Drawing.Color.Black;
            this.PictureISO14001.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.PictureISO14001.Border.TopColor = System.Drawing.Color.Black;
            this.PictureISO14001.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.PictureISO14001.Height = 0.875F;
            this.PictureISO14001.Image = ((System.Drawing.Image)(resources.GetObject("PictureISO14001.Image")));
            this.PictureISO14001.ImageData = ((System.IO.Stream)(resources.GetObject("PictureISO14001.ImageData")));
            this.PictureISO14001.Left = 1.1875F;
            this.PictureISO14001.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.PictureISO14001.LineWeight = 0F;
            this.PictureISO14001.Name = "PictureISO14001";
            this.PictureISO14001.Top = 0.0625F;
            this.PictureISO14001.Width = 1.0625F;
            // 
            // PictureISO18001
            // 
            this.PictureISO18001.Border.BottomColor = System.Drawing.Color.Black;
            this.PictureISO18001.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.PictureISO18001.Border.LeftColor = System.Drawing.Color.Black;
            this.PictureISO18001.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.PictureISO18001.Border.RightColor = System.Drawing.Color.Black;
            this.PictureISO18001.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.PictureISO18001.Border.TopColor = System.Drawing.Color.Black;
            this.PictureISO18001.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.PictureISO18001.Height = 0.875F;
            this.PictureISO18001.Image = ((System.Drawing.Image)(resources.GetObject("PictureISO18001.Image")));
            this.PictureISO18001.ImageData = ((System.IO.Stream)(resources.GetObject("PictureISO18001.ImageData")));
            this.PictureISO18001.Left = 2.375F;
            this.PictureISO18001.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.PictureISO18001.LineWeight = 0F;
            this.PictureISO18001.Name = "PictureISO18001";
            this.PictureISO18001.Top = 0.0625F;
            this.PictureISO18001.Width = 1.0625F;
            // 
            // PageHeader
            // 
            this.PageHeader.Height = 0F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // PictureISO9001
            // 
            this.PictureISO9001.Border.BottomColor = System.Drawing.Color.Black;
            this.PictureISO9001.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.PictureISO9001.Border.LeftColor = System.Drawing.Color.Black;
            this.PictureISO9001.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.PictureISO9001.Border.RightColor = System.Drawing.Color.Black;
            this.PictureISO9001.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.PictureISO9001.Border.TopColor = System.Drawing.Color.Black;
            this.PictureISO9001.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.PictureISO9001.Height = 0.875F;
            this.PictureISO9001.Image = ((System.Drawing.Image)(resources.GetObject("PictureISO9001.Image")));
            this.PictureISO9001.ImageData = ((System.IO.Stream)(resources.GetObject("PictureISO9001.ImageData")));
            this.PictureISO9001.Left = 0.0625F;
            this.PictureISO9001.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.PictureISO9001.LineWeight = 0F;
            this.PictureISO9001.Name = "PictureISO9001";
            this.PictureISO9001.Top = 0.0625F;
            this.PictureISO9001.Width = 1.0625F;
            // 
            // FirminsFooter
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11.69F;
            this.PageSettings.PaperWidth = 8.27F;
            this.PrintWidth = 7.520833F;
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                        "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.lblAddressee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblChequePayable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBankDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblOutputTax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblIBan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSwift)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureISO14001)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureISO18001)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureISO9001)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}

		#endregion

		private void ReportFooter_BeforePrint(object sender, EventArgs e)
		{
			//If this footer is for the Credit report or Subby Self Bill
			//do NOT display the payment terms and bank details
            if (this.ParentReport.ParentReport != null)
            {
                System.Type reportType = this.ParentReport.ParentReport.GetType();
                //if (reportType == typeof(rptCreditNoteOneLiner)
                //    || reportType == typeof(rptSubContractorSelfBill)
                //    || reportType == typeof(rptNewSubContractorSelfBill))
                //{
                //    lblChequePayable.Visible = false;
                //    lblBankDetail.Visible = false;
                //}

                if (reportType == typeof(rptSubContractorSelfBill) || reportType == typeof(rptNewSubContractorSelfBill))
                    lblOutputTax.Visible = true;
            }
		}

        private void ReportFooter_Format(object sender, EventArgs e)
        {

        }
	}
}
