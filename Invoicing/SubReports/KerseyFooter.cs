using System;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;

namespace Orchestrator.Reports.Invoicing.SubReports
{
    public class KerseyFooter : DataDynamics.ActiveReports.ActiveReport3
	{
        public KerseyFooter()
		{
			InitializeComponent();
		}


		#region ActiveReports Designer generated code
		private DataDynamics.ActiveReports.ReportHeader ReportHeader = null;
		private DataDynamics.ActiveReports.PageHeader PageHeader = null;
		private DataDynamics.ActiveReports.Detail Detail = null;
        private DataDynamics.ActiveReports.PageFooter PageFooter = null;
        private TextBox Conditions;
        private TextBox textBox1;
        private TextBox textBox2;
        private Picture picture1;
        private Picture picture2;
        private Picture picture3;
        private Picture picture5;
        private DataDynamics.ActiveReports.ReportFooter ReportFooter = null;
		public void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(KerseyFooter));
            this.Detail = new DataDynamics.ActiveReports.Detail();
            this.ReportHeader = new DataDynamics.ActiveReports.ReportHeader();
            this.ReportFooter = new DataDynamics.ActiveReports.ReportFooter();
            this.Conditions = new DataDynamics.ActiveReports.TextBox();
            this.textBox1 = new DataDynamics.ActiveReports.TextBox();
            this.textBox2 = new DataDynamics.ActiveReports.TextBox();
            this.picture1 = new DataDynamics.ActiveReports.Picture();
            this.picture2 = new DataDynamics.ActiveReports.Picture();
            this.picture3 = new DataDynamics.ActiveReports.Picture();
            this.picture5 = new DataDynamics.ActiveReports.Picture();
            this.PageHeader = new DataDynamics.ActiveReports.PageHeader();
            this.PageFooter = new DataDynamics.ActiveReports.PageFooter();
            ((System.ComponentModel.ISupportInitialize)(this.Conditions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Height = 0F;
            this.Detail.Name = "Detail";
            // 
            // ReportHeader
            // 
            this.ReportHeader.Height = 0F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.Conditions,
            this.textBox1,
            this.textBox2,
            this.picture1,
            this.picture2,
            this.picture3,
            this.picture5});
            this.ReportFooter.Height = 1.313F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // Conditions
            // 
            this.Conditions.Border.BottomColor = System.Drawing.Color.Black;
            this.Conditions.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Conditions.Border.LeftColor = System.Drawing.Color.Black;
            this.Conditions.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Conditions.Border.RightColor = System.Drawing.Color.Black;
            this.Conditions.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Conditions.Border.TopColor = System.Drawing.Color.Black;
            this.Conditions.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Conditions.Height = 0.125F;
            this.Conditions.Left = 0F;
            this.Conditions.Name = "Conditions";
            this.Conditions.Style = "ddo-char-set: 1; text-align: center; font-size: 8pt; ";
            this.Conditions.Text = "All business is carried out under the company\'s trading conditions (BIFA 2005). A" +
                "ll UK transport is carried out under standard RHA 2009 conditions.";
            this.Conditions.Top = 1F;
            this.Conditions.Width = 7.5F;
            // 
            // textBox1
            // 
            this.textBox1.Border.BottomColor = System.Drawing.Color.Black;
            this.textBox1.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.textBox1.Border.LeftColor = System.Drawing.Color.Black;
            this.textBox1.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.textBox1.Border.RightColor = System.Drawing.Color.Black;
            this.textBox1.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.textBox1.Border.TopColor = System.Drawing.Color.Black;
            this.textBox1.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.textBox1.Height = 0.125F;
            this.textBox1.Left = 0F;
            this.textBox1.Name = "textBox1";
            this.textBox1.Style = "ddo-char-set: 1; text-align: center; font-size: 8pt; ";
            this.textBox1.Text = "Registered in England No.  04995662    VAT Number: 831207758";
            this.textBox1.Top = 0.875F;
            this.textBox1.Width = 7.5F;
            // 
            // textBox2
            // 
            this.textBox2.Border.BottomColor = System.Drawing.Color.Black;
            this.textBox2.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.textBox2.Border.LeftColor = System.Drawing.Color.Black;
            this.textBox2.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.textBox2.Border.RightColor = System.Drawing.Color.Black;
            this.textBox2.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.textBox2.Border.TopColor = System.Drawing.Color.Black;
            this.textBox2.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.textBox2.Height = 0.125F;
            this.textBox2.Left = 0F;
            this.textBox2.Name = "textBox2";
            this.textBox2.Style = "ddo-char-set: 1; text-align: center; font-size: 8pt; ";
            this.textBox2.Text = "All warehousing is carried out under standard RHA conditions of storage - copies " +
                "of which are available on request";
            this.textBox2.Top = 1.125F;
            this.textBox2.Width = 7.5F;
            // 
            // picture1
            // 
            this.picture1.Border.BottomColor = System.Drawing.Color.Black;
            this.picture1.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.picture1.Border.LeftColor = System.Drawing.Color.Black;
            this.picture1.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.picture1.Border.RightColor = System.Drawing.Color.Black;
            this.picture1.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.picture1.Border.TopColor = System.Drawing.Color.Black;
            this.picture1.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.picture1.Height = 0.8F;
            this.picture1.Image = ((System.Drawing.Image)(resources.GetObject("picture1.Image")));
            this.picture1.ImageData = ((System.IO.Stream)(resources.GetObject("picture1.ImageData")));
            this.picture1.Left = 0.5F;
            this.picture1.LineWeight = 0F;
            this.picture1.Name = "picture1";
            this.picture1.SizeMode = DataDynamics.ActiveReports.SizeModes.Stretch;
            this.picture1.Top = 0F;
            this.picture1.Width = 0.8F;
            // 
            // picture2
            // 
            this.picture2.Border.BottomColor = System.Drawing.Color.Black;
            this.picture2.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.picture2.Border.LeftColor = System.Drawing.Color.Black;
            this.picture2.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.picture2.Border.RightColor = System.Drawing.Color.Black;
            this.picture2.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.picture2.Border.TopColor = System.Drawing.Color.Black;
            this.picture2.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.picture2.Height = 0.4375F;
            this.picture2.Image = ((System.Drawing.Image)(resources.GetObject("picture2.Image")));
            this.picture2.ImageData = ((System.IO.Stream)(resources.GetObject("picture2.ImageData")));
            this.picture2.Left = 2.125F;
            this.picture2.LineWeight = 0F;
            this.picture2.Name = "picture2";
            this.picture2.SizeMode = DataDynamics.ActiveReports.SizeModes.Stretch;
            this.picture2.Top = 0.1875F;
            this.picture2.Width = 1.0625F;
            // 
            // picture3
            // 
            this.picture3.Border.BottomColor = System.Drawing.Color.Black;
            this.picture3.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.picture3.Border.LeftColor = System.Drawing.Color.Black;
            this.picture3.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.picture3.Border.RightColor = System.Drawing.Color.Black;
            this.picture3.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.picture3.Border.TopColor = System.Drawing.Color.Black;
            this.picture3.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.picture3.Height = 0.8125F;
            this.picture3.Image = ((System.Drawing.Image)(resources.GetObject("picture3.Image")));
            this.picture3.ImageData = ((System.IO.Stream)(resources.GetObject("picture3.ImageData")));
            this.picture3.Left = 4.25F;
            this.picture3.LineWeight = 0F;
            this.picture3.Name = "picture3";
            this.picture3.SizeMode = DataDynamics.ActiveReports.SizeModes.Stretch;
            this.picture3.Top = 0F;
            this.picture3.Width = 0.9375F;
            // 
            // picture5
            // 
            this.picture5.Border.BottomColor = System.Drawing.Color.Black;
            this.picture5.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.picture5.Border.LeftColor = System.Drawing.Color.Black;
            this.picture5.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.picture5.Border.RightColor = System.Drawing.Color.Black;
            this.picture5.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.picture5.Border.TopColor = System.Drawing.Color.Black;
            this.picture5.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.picture5.Height = 0.875F;
            this.picture5.Image = ((System.Drawing.Image)(resources.GetObject("picture5.Image")));
            this.picture5.ImageData = ((System.IO.Stream)(resources.GetObject("picture5.ImageData")));
            this.picture5.Left = 6.0625F;
            this.picture5.LineWeight = 0F;
            this.picture5.Name = "picture5";
            this.picture5.SizeMode = DataDynamics.ActiveReports.SizeModes.Stretch;
            this.picture5.Top = 0F;
            this.picture5.Width = 1F;
            // 
            // PageHeader
            // 
            this.PageHeader.Height = 0F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // KerseyFooter
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11.69F;
            this.PageSettings.PaperWidth = 8.27F;
            this.PrintWidth = 7.520833F;
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                        "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.Conditions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picture5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

		#endregion
    }
}
