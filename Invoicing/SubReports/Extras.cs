using System;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;

namespace Orchestrator.Reports.Invoicing.SubReports
{
    public class Extras : DataDynamics.ActiveReports.ActiveReport3
	{
		public Extras()
		{
			InitializeComponent();
		}

		private void Detail_BeforePrint(object sender, System.EventArgs eArgs)
		{
			// Adjust the heights of the controls so they match.

            float maxHeight = 0;

            if (txtDate.Height > maxHeight)
                maxHeight = txtDate.Height;
            if (txtReference.Height > maxHeight)
                maxHeight = txtReference.Height;
            if (txtExtraType.Height > maxHeight)
                maxHeight = txtExtraType.Height;
            if (txtDescription.Height > maxHeight)
                maxHeight = txtDescription.Height;
            if (txtClientContact.Height > maxHeight)
                maxHeight = txtClientContact.Height;
            if (txtExtraAmount.Height > maxHeight)
                maxHeight = txtExtraAmount.Height;

            txtDate.Height = txtReference.Height = txtExtraType.Height = txtDescription.Height = txtClientContact.Height = txtExtraAmount.Height = maxHeight;
            txtExtraAmount.Text = txtExtraAmount.Text.ToString(System.Threading.Thread.CurrentThread.CurrentCulture);
        }

		#region ActiveReports Designer generated code
		private DataDynamics.ActiveReports.ReportHeader ReportHeader = null;
		private DataDynamics.ActiveReports.Label lblDate = null;
		private DataDynamics.ActiveReports.Label lblReference = null;
		private DataDynamics.ActiveReports.Label lblExtraType = null;
		private DataDynamics.ActiveReports.Label Label = null;
		private DataDynamics.ActiveReports.Label Label1 = null;
		private DataDynamics.ActiveReports.Label lblExtraAmount = null;
		private DataDynamics.ActiveReports.Detail Detail = null;
		private DataDynamics.ActiveReports.TextBox txtDate = null;
		private DataDynamics.ActiveReports.TextBox txtReference = null;
		private DataDynamics.ActiveReports.TextBox txtExtraType = null;
		private DataDynamics.ActiveReports.TextBox txtDescription = null;
		private DataDynamics.ActiveReports.TextBox txtClientContact = null;
		private DataDynamics.ActiveReports.TextBox txtExtraAmount = null;
		private DataDynamics.ActiveReports.ReportFooter ReportFooter = null;
		public void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Extras));
            this.Detail = new DataDynamics.ActiveReports.Detail();
            this.txtDate = new DataDynamics.ActiveReports.TextBox();
            this.txtReference = new DataDynamics.ActiveReports.TextBox();
            this.txtExtraType = new DataDynamics.ActiveReports.TextBox();
            this.txtDescription = new DataDynamics.ActiveReports.TextBox();
            this.txtClientContact = new DataDynamics.ActiveReports.TextBox();
            this.txtExtraAmount = new DataDynamics.ActiveReports.TextBox();
            this.ReportHeader = new DataDynamics.ActiveReports.ReportHeader();
            this.lblDate = new DataDynamics.ActiveReports.Label();
            this.lblReference = new DataDynamics.ActiveReports.Label();
            this.lblExtraType = new DataDynamics.ActiveReports.Label();
            this.Label = new DataDynamics.ActiveReports.Label();
            this.Label1 = new DataDynamics.ActiveReports.Label();
            this.lblExtraAmount = new DataDynamics.ActiveReports.Label();
            this.ReportFooter = new DataDynamics.ActiveReports.ReportFooter();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReference)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExtraType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtClientContact)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExtraAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReference)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblExtraType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblExtraAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.txtDate,
            this.txtReference,
            this.txtExtraType,
            this.txtDescription,
            this.txtClientContact,
            this.txtExtraAmount});
            this.Detail.Height = 0.1875F;
            this.Detail.Name = "Detail";
            this.Detail.BeforePrint += new System.EventHandler(this.Detail_BeforePrint);
            // 
            // txtDate
            // 
            this.txtDate.Border.BottomColor = System.Drawing.Color.Black;
            this.txtDate.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtDate.Border.LeftColor = System.Drawing.Color.Black;
            this.txtDate.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtDate.Border.RightColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.txtDate.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtDate.Border.TopColor = System.Drawing.Color.Black;
            this.txtDate.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtDate.DataField = "DeliveryDateTime";
            this.txtDate.Height = 0.188F;
            this.txtDate.Left = 0F;
            this.txtDate.Name = "txtDate";
            this.txtDate.OutputFormat = resources.GetString("txtDate.OutputFormat");
            this.txtDate.Style = "";
            this.txtDate.Text = null;
            this.txtDate.Top = 0F;
            this.txtDate.Width = 0.5F;
            // 
            // txtReference
            // 
            this.txtReference.Border.BottomColor = System.Drawing.Color.Black;
            this.txtReference.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtReference.Border.LeftColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.txtReference.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtReference.Border.RightColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.txtReference.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtReference.Border.TopColor = System.Drawing.Color.Black;
            this.txtReference.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtReference.DataField = "Reference";
            this.txtReference.Height = 0.1875F;
            this.txtReference.Left = 0.5F;
            this.txtReference.Name = "txtReference";
            this.txtReference.Style = "";
            this.txtReference.Text = null;
            this.txtReference.Top = 0F;
            this.txtReference.Width = 1F;
            // 
            // txtExtraType
            // 
            this.txtExtraType.Border.BottomColor = System.Drawing.Color.Black;
            this.txtExtraType.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtExtraType.Border.LeftColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.txtExtraType.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtExtraType.Border.RightColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.txtExtraType.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtExtraType.Border.TopColor = System.Drawing.Color.Black;
            this.txtExtraType.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtExtraType.DataField = "ExtraType";
            this.txtExtraType.Height = 0.1875F;
            this.txtExtraType.Left = 1.5F;
            this.txtExtraType.Name = "txtExtraType";
            this.txtExtraType.Style = "";
            this.txtExtraType.Text = null;
            this.txtExtraType.Top = 0F;
            this.txtExtraType.Width = 0.9375F;
            // 
            // txtDescription
            // 
            this.txtDescription.Border.BottomColor = System.Drawing.Color.Black;
            this.txtDescription.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtDescription.Border.LeftColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.txtDescription.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtDescription.Border.RightColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.txtDescription.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtDescription.Border.TopColor = System.Drawing.Color.Black;
            this.txtDescription.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtDescription.DataField = "CustomDescription";
            this.txtDescription.Height = 0.1875F;
            this.txtDescription.Left = 2.4375F;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Style = "";
            this.txtDescription.Text = null;
            this.txtDescription.Top = 0F;
            this.txtDescription.Width = 2.5F;
            // 
            // txtClientContact
            // 
            this.txtClientContact.Border.BottomColor = System.Drawing.Color.Black;
            this.txtClientContact.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtClientContact.Border.LeftColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.txtClientContact.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtClientContact.Border.RightColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.txtClientContact.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtClientContact.Border.TopColor = System.Drawing.Color.Black;
            this.txtClientContact.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtClientContact.DataField = "ClientContact";
            this.txtClientContact.Height = 0.1875F;
            this.txtClientContact.Left = 4.9375F;
            this.txtClientContact.Name = "txtClientContact";
            this.txtClientContact.Style = "";
            this.txtClientContact.Text = null;
            this.txtClientContact.Top = 0F;
            this.txtClientContact.Width = 1F;
            // 
            // txtExtraAmount
            // 
            this.txtExtraAmount.Border.BottomColor = System.Drawing.Color.Black;
            this.txtExtraAmount.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtExtraAmount.Border.LeftColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.txtExtraAmount.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtExtraAmount.Border.RightColor = System.Drawing.Color.Black;
            this.txtExtraAmount.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtExtraAmount.Border.TopColor = System.Drawing.Color.Black;
            this.txtExtraAmount.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtExtraAmount.DataField = "ForeignAmount";
            this.txtExtraAmount.Height = 0.1875F;
            this.txtExtraAmount.Left = 5.9375F;
            this.txtExtraAmount.Name = "txtExtraAmount";
            this.txtExtraAmount.OutputFormat = resources.GetString("txtExtraAmount.OutputFormat");
            this.txtExtraAmount.Style = "text-align: right; ";
            this.txtExtraAmount.Text = null;
            this.txtExtraAmount.Top = 0F;
            this.txtExtraAmount.Width = 0.75F;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.lblDate,
            this.lblReference,
            this.lblExtraType,
            this.Label,
            this.Label1,
            this.lblExtraAmount});
            this.ReportHeader.Height = 0.188F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // lblDate
            // 
            this.lblDate.Border.BottomColor = System.Drawing.Color.Black;
            this.lblDate.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblDate.Border.LeftColor = System.Drawing.Color.Black;
            this.lblDate.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblDate.Border.RightColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.lblDate.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblDate.Border.TopColor = System.Drawing.Color.Black;
            this.lblDate.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblDate.Height = 0.188F;
            this.lblDate.HyperLink = null;
            this.lblDate.Left = 0F;
            this.lblDate.Name = "lblDate";
            this.lblDate.Style = "font-weight: bold; ";
            this.lblDate.Text = "Date";
            this.lblDate.Top = 0F;
            this.lblDate.Width = 0.5F;
            // 
            // lblReference
            // 
            this.lblReference.Border.BottomColor = System.Drawing.Color.Black;
            this.lblReference.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblReference.Border.LeftColor = System.Drawing.Color.Black;
            this.lblReference.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblReference.Border.RightColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.lblReference.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblReference.Border.TopColor = System.Drawing.Color.Black;
            this.lblReference.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblReference.Height = 0.1875F;
            this.lblReference.HyperLink = null;
            this.lblReference.Left = 0.5F;
            this.lblReference.Name = "lblReference";
            this.lblReference.Style = "font-weight: bold; background-color: White; ";
            this.lblReference.Text = "Reference";
            this.lblReference.Top = 0F;
            this.lblReference.Width = 1F;
            // 
            // lblExtraType
            // 
            this.lblExtraType.Border.BottomColor = System.Drawing.Color.Black;
            this.lblExtraType.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblExtraType.Border.LeftColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.lblExtraType.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblExtraType.Border.RightColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.lblExtraType.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblExtraType.Border.TopColor = System.Drawing.Color.Black;
            this.lblExtraType.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblExtraType.Height = 0.1875F;
            this.lblExtraType.HyperLink = null;
            this.lblExtraType.Left = 1.5F;
            this.lblExtraType.Name = "lblExtraType";
            this.lblExtraType.Style = "font-weight: bold; background-color: White; ";
            this.lblExtraType.Text = "Extra Type";
            this.lblExtraType.Top = 0F;
            this.lblExtraType.Width = 0.9375F;
            // 
            // Label
            // 
            this.Label.Border.BottomColor = System.Drawing.Color.Black;
            this.Label.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.Label.Border.LeftColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.Label.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.Label.Border.RightColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.Label.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.Label.Border.TopColor = System.Drawing.Color.Black;
            this.Label.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.Label.Height = 0.1875F;
            this.Label.HyperLink = null;
            this.Label.Left = 2.4375F;
            this.Label.Name = "Label";
            this.Label.Style = "font-weight: bold; background-color: White; ";
            this.Label.Text = "Description";
            this.Label.Top = 0F;
            this.Label.Width = 2.5F;
            // 
            // Label1
            // 
            this.Label1.Border.BottomColor = System.Drawing.Color.Black;
            this.Label1.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.Label1.Border.LeftColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.Label1.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.Label1.Border.RightColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.Label1.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.Label1.Border.TopColor = System.Drawing.Color.Black;
            this.Label1.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.Label1.Height = 0.1875F;
            this.Label1.HyperLink = null;
            this.Label1.Left = 4.9375F;
            this.Label1.Name = "Label1";
            this.Label1.Style = "font-weight: bold; background-color: White; ";
            this.Label1.Text = "Accepted By";
            this.Label1.Top = 0F;
            this.Label1.Width = 1F;
            // 
            // lblExtraAmount
            // 
            this.lblExtraAmount.Border.BottomColor = System.Drawing.Color.Black;
            this.lblExtraAmount.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblExtraAmount.Border.LeftColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.lblExtraAmount.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblExtraAmount.Border.RightColor = System.Drawing.Color.Black;
            this.lblExtraAmount.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblExtraAmount.Border.TopColor = System.Drawing.Color.Black;
            this.lblExtraAmount.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblExtraAmount.Height = 0.1875F;
            this.lblExtraAmount.HyperLink = null;
            this.lblExtraAmount.Left = 5.9375F;
            this.lblExtraAmount.Name = "lblExtraAmount";
            this.lblExtraAmount.Style = "text-align: right; font-weight: bold; background-color: White; ";
            this.lblExtraAmount.Text = "Extra Amount";
            this.lblExtraAmount.Top = 0F;
            this.lblExtraAmount.Width = 0.75F;
            // 
            // ReportFooter
            // 
            this.ReportFooter.CanGrow = false;
            this.ReportFooter.Height = 0F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // Extras
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 6.708333F;
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                        "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReference)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExtraType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtClientContact)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExtraAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReference)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblExtraType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblExtraAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		 }

		#endregion
	}
}
