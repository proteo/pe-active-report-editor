namespace Orchestrator.Reports.Invoicing.SubReports
{
    /// <summary>
    /// Summary description for FFImageHeader.
    /// </summary>
    partial class FFImageHeader
    {
        private DataDynamics.ActiveReports.PageHeader PageHeader;
        private DataDynamics.ActiveReports.Detail Detail;
        private DataDynamics.ActiveReports.PageFooter PageFooter;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FFImageHeader));
            this.PageHeader = new DataDynamics.ActiveReports.PageHeader();
            this.Detail = new DataDynamics.ActiveReports.Detail();
            this.PageFooter = new DataDynamics.ActiveReports.PageFooter();
            this.ReportHeader = new DataDynamics.ActiveReports.ReportHeader();
            this.shape1 = new DataDynamics.ActiveReports.Shape();
            this.lblTerms1 = new DataDynamics.ActiveReports.Label();
            this.lblTerms2 = new DataDynamics.ActiveReports.Label();
            this.lblTerms3 = new DataDynamics.ActiveReports.Label();
            this.lbl = new DataDynamics.ActiveReports.Label();
            this.picFFLogo = new DataDynamics.ActiveReports.Picture();
            this.lblCompanyName = new DataDynamics.ActiveReports.Label();
            this.lblAddressLine1 = new DataDynamics.ActiveReports.Label();
            this.lblAddressLine2 = new DataDynamics.ActiveReports.Label();
            this.lblTel = new DataDynamics.ActiveReports.Label();
            this.lblFax = new DataDynamics.ActiveReports.Label();
            this.txtTel = new DataDynamics.ActiveReports.TextBox();
            this.txtFax = new DataDynamics.ActiveReports.TextBox();
            this.ReportFooter = new DataDynamics.ActiveReports.ReportFooter();
            ((System.ComponentModel.ISupportInitialize)(this.lblTerms1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTerms2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTerms3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFFLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCompanyName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAddressLine1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAddressLine2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // PageHeader
            // 
            this.PageHeader.Height = 0F;
            this.PageHeader.Name = "PageHeader";
            // 
            // Detail
            // 
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Height = 0F;
            this.Detail.Name = "Detail";
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.shape1,
            this.lblTerms1,
            this.lblTerms2,
            this.lblTerms3,
            this.lbl,
            this.picFFLogo,
            this.lblCompanyName,
            this.lblAddressLine1,
            this.lblAddressLine2,
            this.lblTel,
            this.lblFax,
            this.txtTel,
            this.txtFax});
            this.ReportHeader.Height = 1.885417F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // shape1
            // 
            this.shape1.Border.BottomColor = System.Drawing.Color.Black;
            this.shape1.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.shape1.Border.LeftColor = System.Drawing.Color.Black;
            this.shape1.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.shape1.Border.RightColor = System.Drawing.Color.Black;
            this.shape1.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.shape1.Border.TopColor = System.Drawing.Color.Black;
            this.shape1.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.shape1.Height = 1.0625F;
            this.shape1.Left = 0F;
            this.shape1.Name = "shape1";
            this.shape1.RoundingRadius = 9.999999F;
            this.shape1.Top = 0.75F;
            this.shape1.Width = 4.3125F;
            // 
            // lblTerms1
            // 
            this.lblTerms1.Border.BottomColor = System.Drawing.Color.Black;
            this.lblTerms1.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblTerms1.Border.LeftColor = System.Drawing.Color.Black;
            this.lblTerms1.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblTerms1.Border.RightColor = System.Drawing.Color.Black;
            this.lblTerms1.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblTerms1.Border.TopColor = System.Drawing.Color.Black;
            this.lblTerms1.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblTerms1.Height = 0.125F;
            this.lblTerms1.HyperLink = null;
            this.lblTerms1.Left = 0.0625F;
            this.lblTerms1.Name = "lblTerms1";
            this.lblTerms1.Style = "text-decoration: underline; ddo-char-set: 0; text-align: center; font-weight: bol" +
                "d; font-size: 7pt; ";
            this.lblTerms1.Text = "Important Note regarding Good in Transit Claims";
            this.lblTerms1.Top = 0.75F;
            this.lblTerms1.Width = 4.3125F;
            // 
            // lblTerms2
            // 
            this.lblTerms2.Border.BottomColor = System.Drawing.Color.Black;
            this.lblTerms2.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblTerms2.Border.LeftColor = System.Drawing.Color.Black;
            this.lblTerms2.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblTerms2.Border.RightColor = System.Drawing.Color.Black;
            this.lblTerms2.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblTerms2.Border.TopColor = System.Drawing.Color.Black;
            this.lblTerms2.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblTerms2.Height = 0.125F;
            this.lblTerms2.HyperLink = null;
            this.lblTerms2.Left = 0.0625F;
            this.lblTerms2.Name = "lblTerms2";
            this.lblTerms2.Style = "ddo-char-set: 0; text-decoration: underline; text-align: center; font-weight: bol" +
                "d; font-size: 7pt; ";
            this.lblTerms2.Text = "Please ensure the correct department are aware of this.";
            this.lblTerms2.Top = 0.875F;
            this.lblTerms2.Width = 4.3125F;
            // 
            // lblTerms3
            // 
            this.lblTerms3.Border.BottomColor = System.Drawing.Color.Black;
            this.lblTerms3.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblTerms3.Border.LeftColor = System.Drawing.Color.Black;
            this.lblTerms3.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblTerms3.Border.RightColor = System.Drawing.Color.Black;
            this.lblTerms3.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblTerms3.Border.TopColor = System.Drawing.Color.Black;
            this.lblTerms3.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblTerms3.Height = 0.5625F;
            this.lblTerms3.HyperLink = null;
            this.lblTerms3.Left = 0.0625F;
            this.lblTerms3.Name = "lblTerms3";
            this.lblTerms3.Style = "ddo-char-set: 0; text-decoration: none; text-align: left; font-size: 7pt; ";
            this.lblTerms3.Text = resources.GetString("lblTerms3.Text");
            this.lblTerms3.Top = 1F;
            this.lblTerms3.Width = 4.3125F;
            // 
            // lbl
            // 
            this.lbl.Border.BottomColor = System.Drawing.Color.Black;
            this.lbl.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lbl.Border.LeftColor = System.Drawing.Color.Black;
            this.lbl.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lbl.Border.RightColor = System.Drawing.Color.Black;
            this.lbl.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lbl.Border.TopColor = System.Drawing.Color.Black;
            this.lbl.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lbl.Height = 0.25F;
            this.lbl.HyperLink = null;
            this.lbl.Left = 0F;
            this.lbl.Name = "lbl";
            this.lbl.Style = "text-decoration: none; ddo-char-set: 0; text-align: center; font-weight: bold; fo" +
                "nt-size: 7pt; ";
            this.lbl.Text = "We strongly advise that you include this wording on any delivery documentation yo" +
                "u supply, to ensure your customers are also aware of these terms.";
            this.lbl.Top = 1.5625F;
            this.lbl.Width = 4.3125F;
            // 
            // picFFLogo
            // 
            this.picFFLogo.Border.BottomColor = System.Drawing.Color.Black;
            this.picFFLogo.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.picFFLogo.Border.LeftColor = System.Drawing.Color.Black;
            this.picFFLogo.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.picFFLogo.Border.RightColor = System.Drawing.Color.Black;
            this.picFFLogo.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.picFFLogo.Border.TopColor = System.Drawing.Color.Black;
            this.picFFLogo.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.picFFLogo.Height = 1.0625F;
            this.picFFLogo.Image = ((System.Drawing.Image)(resources.GetObject("picFFLogo.Image")));
            this.picFFLogo.ImageData = ((System.IO.Stream)(resources.GetObject("picFFLogo.ImageData")));
            this.picFFLogo.Left = 5.125F;
            this.picFFLogo.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.picFFLogo.LineWeight = 0F;
            this.picFFLogo.Name = "picFFLogo";
            this.picFFLogo.SizeMode = DataDynamics.ActiveReports.SizeModes.Stretch;
            this.picFFLogo.Top = 0.4375F;
            this.picFFLogo.Width = 1.625F;
            // 
            // lblCompanyName
            // 
            this.lblCompanyName.Border.BottomColor = System.Drawing.Color.Black;
            this.lblCompanyName.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblCompanyName.Border.LeftColor = System.Drawing.Color.Black;
            this.lblCompanyName.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblCompanyName.Border.RightColor = System.Drawing.Color.Black;
            this.lblCompanyName.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblCompanyName.Border.TopColor = System.Drawing.Color.Black;
            this.lblCompanyName.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblCompanyName.Height = 0.25F;
            this.lblCompanyName.HyperLink = null;
            this.lblCompanyName.Left = 0.75F;
            this.lblCompanyName.Name = "lblCompanyName";
            this.lblCompanyName.Style = "ddo-char-set: 0; text-align: center; font-weight: normal; font-size: 12pt; font-f" +
                "amily: Copperplate Gothic Bold; ";
            this.lblCompanyName.Text = "FREIGHTFORCE DISTRIBUTION";
            this.lblCompanyName.Top = 0F;
            this.lblCompanyName.Width = 3F;
            // 
            // lblAddressLine1
            // 
            this.lblAddressLine1.Border.BottomColor = System.Drawing.Color.Black;
            this.lblAddressLine1.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblAddressLine1.Border.LeftColor = System.Drawing.Color.Black;
            this.lblAddressLine1.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblAddressLine1.Border.RightColor = System.Drawing.Color.Black;
            this.lblAddressLine1.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblAddressLine1.Border.TopColor = System.Drawing.Color.Black;
            this.lblAddressLine1.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblAddressLine1.Height = 0.1875F;
            this.lblAddressLine1.HyperLink = null;
            this.lblAddressLine1.Left = 0.9375F;
            this.lblAddressLine1.Name = "lblAddressLine1";
            this.lblAddressLine1.Style = "ddo-char-set: 0; text-align: center; font-size: 9pt; font-family: Arial; ";
            this.lblAddressLine1.Text = "Unit 1A Guardian Road Industrial Estate";
            this.lblAddressLine1.Top = 0.1875F;
            this.lblAddressLine1.Width = 2.5625F;
            // 
            // lblAddressLine2
            // 
            this.lblAddressLine2.Border.BottomColor = System.Drawing.Color.Black;
            this.lblAddressLine2.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblAddressLine2.Border.LeftColor = System.Drawing.Color.Black;
            this.lblAddressLine2.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblAddressLine2.Border.RightColor = System.Drawing.Color.Black;
            this.lblAddressLine2.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblAddressLine2.Border.TopColor = System.Drawing.Color.Black;
            this.lblAddressLine2.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblAddressLine2.Height = 0.1875F;
            this.lblAddressLine2.HyperLink = null;
            this.lblAddressLine2.Left = 1.4375F;
            this.lblAddressLine2.Name = "lblAddressLine2";
            this.lblAddressLine2.Style = "ddo-char-set: 0; text-align: center; font-size: 9pt; font-family: Arial; ";
            this.lblAddressLine2.Text = "Norwich, Norfolk, NR5 8PF";
            this.lblAddressLine2.Top = 0.3125F;
            this.lblAddressLine2.Width = 1.75F;
            // 
            // lblTel
            // 
            this.lblTel.Border.BottomColor = System.Drawing.Color.Black;
            this.lblTel.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblTel.Border.LeftColor = System.Drawing.Color.Black;
            this.lblTel.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblTel.Border.RightColor = System.Drawing.Color.Black;
            this.lblTel.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblTel.Border.TopColor = System.Drawing.Color.Black;
            this.lblTel.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblTel.Height = 0.1875F;
            this.lblTel.HyperLink = null;
            this.lblTel.Left = 0.9375F;
            this.lblTel.Name = "lblTel";
            this.lblTel.Style = "ddo-char-set: 0; font-size: 9pt; font-family: Arial; ";
            this.lblTel.Text = "Tel : ";
            this.lblTel.Top = 0.4375F;
            this.lblTel.Width = 0.25F;
            // 
            // lblFax
            // 
            this.lblFax.Border.BottomColor = System.Drawing.Color.Black;
            this.lblFax.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblFax.Border.LeftColor = System.Drawing.Color.Black;
            this.lblFax.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblFax.Border.RightColor = System.Drawing.Color.Black;
            this.lblFax.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblFax.Border.TopColor = System.Drawing.Color.Black;
            this.lblFax.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblFax.Height = 0.1875F;
            this.lblFax.HyperLink = null;
            this.lblFax.Left = 2.25F;
            this.lblFax.Name = "lblFax";
            this.lblFax.Style = "ddo-char-set: 0; font-size: 9pt; font-family: Arial; ";
            this.lblFax.Text = "Fax :";
            this.lblFax.Top = 0.4375F;
            this.lblFax.Width = 0.3125F;
            // 
            // txtTel
            // 
            this.txtTel.Border.BottomColor = System.Drawing.Color.Black;
            this.txtTel.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtTel.Border.LeftColor = System.Drawing.Color.Black;
            this.txtTel.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtTel.Border.RightColor = System.Drawing.Color.Black;
            this.txtTel.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtTel.Border.TopColor = System.Drawing.Color.Black;
            this.txtTel.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtTel.Height = 0.1875F;
            this.txtTel.Left = 1.125F;
            this.txtTel.Name = "txtTel";
            this.txtTel.Style = "ddo-char-set: 0; text-align: center; font-size: 9pt; font-family: Arial; ";
            this.txtTel.Text = "01603 630011";
            this.txtTel.Top = 0.4375F;
            this.txtTel.Width = 1.0625F;
            // 
            // txtFax
            // 
            this.txtFax.Border.BottomColor = System.Drawing.Color.Black;
            this.txtFax.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtFax.Border.LeftColor = System.Drawing.Color.Black;
            this.txtFax.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtFax.Border.RightColor = System.Drawing.Color.Black;
            this.txtFax.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtFax.Border.TopColor = System.Drawing.Color.Black;
            this.txtFax.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtFax.Height = 0.1875F;
            this.txtFax.Left = 2.5625F;
            this.txtFax.Name = "txtFax";
            this.txtFax.Style = "ddo-char-set: 0; text-align: center; font-size: 9pt; font-family: Arial; ";
            this.txtFax.Text = "01603 630012";
            this.txtFax.Top = 0.4375F;
            this.txtFax.Width = 1.0625F;
            // 
            // ReportFooter
            // 
            this.ReportFooter.Height = 0F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // FFImageHeader
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.479167F;
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" +
                        "l; font-size: 10pt; color: Black; ", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; ", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" +
                        "lic; ", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.lblTerms1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTerms2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTerms3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFFLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCompanyName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAddressLine1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAddressLine2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private DataDynamics.ActiveReports.ReportHeader ReportHeader;
        private DataDynamics.ActiveReports.ReportFooter ReportFooter;
        private DataDynamics.ActiveReports.Picture picFFLogo;
        private DataDynamics.ActiveReports.Label lblCompanyName;
        private DataDynamics.ActiveReports.Label lblAddressLine1;
        private DataDynamics.ActiveReports.Label lblAddressLine2;
        private DataDynamics.ActiveReports.Label lblTel;
        private DataDynamics.ActiveReports.Label lblFax;
        private DataDynamics.ActiveReports.TextBox txtTel;
        private DataDynamics.ActiveReports.TextBox txtFax;
        private DataDynamics.ActiveReports.Shape shape1;
        private DataDynamics.ActiveReports.Label lblTerms1;
        private DataDynamics.ActiveReports.Label lblTerms2;
        private DataDynamics.ActiveReports.Label lblTerms3;
        private DataDynamics.ActiveReports.Label lbl;
    }
}
