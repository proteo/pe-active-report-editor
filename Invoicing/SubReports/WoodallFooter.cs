using System;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;
using System.Data;
using System.Drawing;
using System.Collections.Specialized;
using System.Globalization;
using System.Web;
using System.Linq;
using Orchestrator.Reports.Invoicing.Groupage;


namespace Orchestrator.Reports.Invoicing.SubReports
{
    public class WoodallFooter : DataDynamics.ActiveReports.ActiveReport3
	{
        public WoodallFooter()
		{            
            InitializeComponent();           
		}

        private void ReportFooter_BeforePrint(object sender, EventArgs e)
        {
          
            Facade.IOrganisation facOrganisation = new Facade.Organisation();

            DataSet dsDefaults = null;

            if(this.ParentReport.ParentReport.GetType() == typeof(rptInvoiceOneLiner))
                dsDefaults = facOrganisation.GetDefaultsForIdentityId(Int32.Parse(((rptInvoiceOneLiner)this.ParentReport.ParentReport).reportParams.Get("ClientId")));
            else if (this.ParentReport.ParentReport.GetType() == typeof(rptInvoiceExtra))
                dsDefaults = facOrganisation.GetDefaultsForIdentityId(Int32.Parse(((rptInvoiceExtra)this.ParentReport.ParentReport).m_reportParams.Get("IdentityId")));
            else
                dsDefaults = facOrganisation.GetDefaultsForIdentityId(((IInvoiceReportWithDataSet)this.ParentReport).PreInvoice.IdentityID);

            string invoiceTerms = dsDefaults.Tables[0].Rows[0]["PaymentTerms"].ToString();

            if (invoiceTerms == "")
                this.Payment5.Text = "Payment terms strictly 14 days from invoice date";
            else
                this.Payment5.Text = invoiceTerms;
        }


        #region ActiveReports Designer generated code
        private DataDynamics.ActiveReports.ReportHeader ReportHeader = null;
		private DataDynamics.ActiveReports.PageHeader PageHeader = null;
		private DataDynamics.ActiveReports.Detail Detail = null;
        private DataDynamics.ActiveReports.PageFooter PageFooter = null;
        private Label Address1;
        private Label Address2;
        private Label Address3;
        private Label Address4;
        private Label TelNo;
        private Label CoVehicleName;
        private Label WebSite;
        private Label Payment2;
        private Label Payment3;
        private Label Payment4;
        private Label Payment5;
        private Label Address5;
        private Label Address6;
        private TextBox Conditions;
        private DataDynamics.ActiveReports.ReportFooter ReportFooter = null;
		public void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WoodallFooter));
            this.Detail = new DataDynamics.ActiveReports.Detail();
            this.ReportHeader = new DataDynamics.ActiveReports.ReportHeader();
            this.ReportFooter = new DataDynamics.ActiveReports.ReportFooter();
            this.Address1 = new DataDynamics.ActiveReports.Label();
            this.Address2 = new DataDynamics.ActiveReports.Label();
            this.Address3 = new DataDynamics.ActiveReports.Label();
            this.Address4 = new DataDynamics.ActiveReports.Label();
            this.TelNo = new DataDynamics.ActiveReports.Label();
            this.CoVehicleName = new DataDynamics.ActiveReports.Label();
            this.WebSite = new DataDynamics.ActiveReports.Label();
            this.Payment2 = new DataDynamics.ActiveReports.Label();
            this.Payment3 = new DataDynamics.ActiveReports.Label();
            this.Payment4 = new DataDynamics.ActiveReports.Label();
            this.Payment5 = new DataDynamics.ActiveReports.Label();
            this.Address5 = new DataDynamics.ActiveReports.Label();
            this.Address6 = new DataDynamics.ActiveReports.Label();
            this.Conditions = new DataDynamics.ActiveReports.TextBox();
            this.PageHeader = new DataDynamics.ActiveReports.PageHeader();
            this.PageFooter = new DataDynamics.ActiveReports.PageFooter();
            ((System.ComponentModel.ISupportInitialize)(this.Address1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Address2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Address3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Address4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TelNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CoVehicleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WebSite)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Payment2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Payment3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Payment4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Payment5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Address5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Address6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Conditions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Height = 0F;
            this.Detail.Name = "Detail";
            // 
            // ReportHeader
            // 
            this.ReportHeader.Height = 0F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.Address1,
            this.Address2,
            this.Address3,
            this.Address4,
            this.TelNo,
            this.CoVehicleName,
            this.WebSite,
            this.Payment2,
            this.Payment3,
            this.Payment4,
            this.Payment5,
            this.Address5,
            this.Address6,
            this.Conditions});
            this.ReportFooter.Height = 1.3125F;
            this.ReportFooter.Name = "ReportFooter";
            this.ReportFooter.BeforePrint += new System.EventHandler(this.ReportFooter_BeforePrint);
            // 
            // Address1
            // 
            this.Address1.Border.BottomColor = System.Drawing.Color.Black;
            this.Address1.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Address1.Border.LeftColor = System.Drawing.Color.Black;
            this.Address1.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Address1.Border.RightColor = System.Drawing.Color.Black;
            this.Address1.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Address1.Border.TopColor = System.Drawing.Color.Black;
            this.Address1.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Address1.Height = 0.125F;
            this.Address1.HyperLink = null;
            this.Address1.Left = 0.0625F;
            this.Address1.Name = "Address1";
            this.Address1.Style = "font-size: 7pt; ";
            this.Address1.Text = "Woods (Haulage) Ltd";
            this.Address1.Top = 0F;
            this.Address1.Width = 2F;
            // 
            // Address2
            // 
            this.Address2.Border.BottomColor = System.Drawing.Color.Black;
            this.Address2.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Address2.Border.LeftColor = System.Drawing.Color.Black;
            this.Address2.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Address2.Border.RightColor = System.Drawing.Color.Black;
            this.Address2.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Address2.Border.TopColor = System.Drawing.Color.Black;
            this.Address2.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Address2.Height = 0.125F;
            this.Address2.HyperLink = null;
            this.Address2.Left = 0.0625F;
            this.Address2.Name = "Address2";
            this.Address2.Style = "font-size: 7pt; ";
            this.Address2.Text = "t/a Woodall Group";
            this.Address2.Top = 0.125F;
            this.Address2.Width = 2F;
            // 
            // Address3
            // 
            this.Address3.Border.BottomColor = System.Drawing.Color.Black;
            this.Address3.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Address3.Border.LeftColor = System.Drawing.Color.Black;
            this.Address3.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Address3.Border.RightColor = System.Drawing.Color.Black;
            this.Address3.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Address3.Border.TopColor = System.Drawing.Color.Black;
            this.Address3.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Address3.Height = 0.125F;
            this.Address3.HyperLink = null;
            this.Address3.Left = 0.0625F;
            this.Address3.Name = "Address3";
            this.Address3.Style = "font-size: 7pt; ";
            this.Address3.Text = "Unit 2, Trillenium,";
            this.Address3.Top = 0.25F;
            this.Address3.Width = 2F;
            // 
            // Address4
            // 
            this.Address4.Border.BottomColor = System.Drawing.Color.Black;
            this.Address4.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Address4.Border.LeftColor = System.Drawing.Color.Black;
            this.Address4.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Address4.Border.RightColor = System.Drawing.Color.Black;
            this.Address4.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Address4.Border.TopColor = System.Drawing.Color.Black;
            this.Address4.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Address4.Height = 0.125F;
            this.Address4.HyperLink = null;
            this.Address4.Left = 0.0625F;
            this.Address4.Name = "Address4";
            this.Address4.Style = "font-size: 7pt; ";
            this.Address4.Text = "Gorsey Lane,";
            this.Address4.Top = 0.375F;
            this.Address4.Width = 2F;
            // 
            // TelNo
            // 
            this.TelNo.Border.BottomColor = System.Drawing.Color.Black;
            this.TelNo.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.TelNo.Border.LeftColor = System.Drawing.Color.Black;
            this.TelNo.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.TelNo.Border.RightColor = System.Drawing.Color.Black;
            this.TelNo.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.TelNo.Border.TopColor = System.Drawing.Color.Black;
            this.TelNo.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.TelNo.Height = 0.125F;
            this.TelNo.HyperLink = null;
            this.TelNo.Left = 0.0625F;
            this.TelNo.Name = "TelNo";
            this.TelNo.Style = "font-size: 7pt; ";
            this.TelNo.Text = "Tel: 01675 466020";
            this.TelNo.Top = 0.75F;
            this.TelNo.Width = 2F;
            // 
            // CoVehicleName
            // 
            this.CoVehicleName.Border.BottomColor = System.Drawing.Color.Black;
            this.CoVehicleName.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.CoVehicleName.Border.LeftColor = System.Drawing.Color.Black;
            this.CoVehicleName.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.CoVehicleName.Border.RightColor = System.Drawing.Color.Black;
            this.CoVehicleName.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.CoVehicleName.Border.TopColor = System.Drawing.Color.Black;
            this.CoVehicleName.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.CoVehicleName.Height = 0.125F;
            this.CoVehicleName.HyperLink = null;
            this.CoVehicleName.Left = 0.0625F;
            this.CoVehicleName.Name = "CoVehicleName";
            this.CoVehicleName.Style = "font-size: 7pt; ";
            this.CoVehicleName.Text = "Company Reg Number: 00377859";
            this.CoVehicleName.Top = 0.875F;
            this.CoVehicleName.Width = 2F;
            // 
            // WebSite
            // 
            this.WebSite.Border.BottomColor = System.Drawing.Color.Black;
            this.WebSite.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.WebSite.Border.LeftColor = System.Drawing.Color.Black;
            this.WebSite.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.WebSite.Border.RightColor = System.Drawing.Color.Black;
            this.WebSite.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.WebSite.Border.TopColor = System.Drawing.Color.Black;
            this.WebSite.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.WebSite.Height = 0.125F;
            this.WebSite.HyperLink = null;
            this.WebSite.Left = 0.0625F;
            this.WebSite.Name = "WebSite";
            this.WebSite.Style = "font-size: 7pt; ";
            this.WebSite.Text = "www.woodall-group.com";
            this.WebSite.Top = 1F;
            this.WebSite.Width = 2F;
            // 
            // Payment2
            // 
            this.Payment2.Border.BottomColor = System.Drawing.Color.Black;
            this.Payment2.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Payment2.Border.LeftColor = System.Drawing.Color.Black;
            this.Payment2.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Payment2.Border.RightColor = System.Drawing.Color.Black;
            this.Payment2.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Payment2.Border.TopColor = System.Drawing.Color.Black;
            this.Payment2.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Payment2.Height = 0.125F;
            this.Payment2.HyperLink = null;
            this.Payment2.Left = 4.8125F;
            this.Payment2.Name = "Payment2";
            this.Payment2.Style = "text-align: center; font-size: 7pt; ";
            this.Payment2.Text = "Please make payments by BACS to:";
            this.Payment2.Top = 0F;
            this.Payment2.Width = 2.3125F;
            // 
            // Payment3
            // 
            this.Payment3.Border.BottomColor = System.Drawing.Color.Black;
            this.Payment3.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Payment3.Border.LeftColor = System.Drawing.Color.Black;
            this.Payment3.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Payment3.Border.RightColor = System.Drawing.Color.Black;
            this.Payment3.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Payment3.Border.TopColor = System.Drawing.Color.Black;
            this.Payment3.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Payment3.Height = 0.125F;
            this.Payment3.HyperLink = null;
            this.Payment3.Left = 4.8125F;
            this.Payment3.Name = "Payment3";
            this.Payment3.Style = "text-align: center; font-size: 7pt; ";
            this.Payment3.Text = "\"Woods (Haulage) Ltd\"";
            this.Payment3.Top = 0.125F;
            this.Payment3.Width = 2.3125F;
            // 
            // Payment4
            // 
            this.Payment4.Border.BottomColor = System.Drawing.Color.Black;
            this.Payment4.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Payment4.Border.LeftColor = System.Drawing.Color.Black;
            this.Payment4.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Payment4.Border.RightColor = System.Drawing.Color.Black;
            this.Payment4.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Payment4.Border.TopColor = System.Drawing.Color.Black;
            this.Payment4.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Payment4.Height = 0.125F;
            this.Payment4.HyperLink = null;
            this.Payment4.Left = 4.8125F;
            this.Payment4.Name = "Payment4";
            this.Payment4.Style = "text-align: center; font-size: 7pt; ";
            this.Payment4.Text = "Account No: 11094523  Sort Code: 16-22-11";
            this.Payment4.Top = 0.25F;
            this.Payment4.Width = 2.3125F;
            // 
            // Payment5
            // 
            this.Payment5.Border.BottomColor = System.Drawing.Color.Black;
            this.Payment5.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Payment5.Border.LeftColor = System.Drawing.Color.Black;
            this.Payment5.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Payment5.Border.RightColor = System.Drawing.Color.Black;
            this.Payment5.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Payment5.Border.TopColor = System.Drawing.Color.Black;
            this.Payment5.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Payment5.Height = 0.125F;
            this.Payment5.HyperLink = null;
            this.Payment5.Left = 4.8125F;
            this.Payment5.Name = "Payment5";
            this.Payment5.Style = "text-align: center; font-weight: bold; font-size: 7pt; ";
            this.Payment5.Text = "Payment Terms Strictly 999 from invoice date.";
            this.Payment5.Top = 0.375F;
            this.Payment5.Width = 2.3125F;
            // 
            // Address5
            // 
            this.Address5.Border.BottomColor = System.Drawing.Color.Black;
            this.Address5.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Address5.Border.LeftColor = System.Drawing.Color.Black;
            this.Address5.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Address5.Border.RightColor = System.Drawing.Color.Black;
            this.Address5.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Address5.Border.TopColor = System.Drawing.Color.Black;
            this.Address5.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Address5.Height = 0.125F;
            this.Address5.HyperLink = null;
            this.Address5.Left = 0.0625F;
            this.Address5.Name = "Address5";
            this.Address5.Style = "font-size: 7pt; ";
            this.Address5.Text = "Coleshill,";
            this.Address5.Top = 0.5F;
            this.Address5.Width = 2F;
            // 
            // Address6
            // 
            this.Address6.Border.BottomColor = System.Drawing.Color.Black;
            this.Address6.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Address6.Border.LeftColor = System.Drawing.Color.Black;
            this.Address6.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Address6.Border.RightColor = System.Drawing.Color.Black;
            this.Address6.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Address6.Border.TopColor = System.Drawing.Color.Black;
            this.Address6.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Address6.Height = 0.125F;
            this.Address6.HyperLink = null;
            this.Address6.Left = 0.0625F;
            this.Address6.Name = "Address6";
            this.Address6.Style = "font-size: 7pt; ";
            this.Address6.Text = "B46 1JU";
            this.Address6.Top = 0.625F;
            this.Address6.Width = 2F;
            // 
            // Conditions
            // 
            this.Conditions.Border.BottomColor = System.Drawing.Color.Black;
            this.Conditions.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Conditions.Border.LeftColor = System.Drawing.Color.Black;
            this.Conditions.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Conditions.Border.RightColor = System.Drawing.Color.Black;
            this.Conditions.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Conditions.Border.TopColor = System.Drawing.Color.Black;
            this.Conditions.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Conditions.Height = 0.125F;
            this.Conditions.Left = 2.1875F;
            this.Conditions.Name = "Conditions";
            this.Conditions.Style = "font-size: 7pt; ";
            this.Conditions.Text = "All goods carried subject to RHA Terms and Conditions (2009). Copies are availabl" +
                "e on request.";
            this.Conditions.Top = 1.125F;
            this.Conditions.Width = 4.1875F;
            // 
            // PageHeader
            // 
            this.PageHeader.Height = 0F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // WoodallFooter
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11.69F;
            this.PageSettings.PaperWidth = 8.27F;
            this.PrintWidth = 7.520833F;
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                        "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.Address1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Address2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Address3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Address4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TelNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CoVehicleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WebSite)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Payment2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Payment3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Payment4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Payment5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Address5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Address6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Conditions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

		#endregion
    }
}
