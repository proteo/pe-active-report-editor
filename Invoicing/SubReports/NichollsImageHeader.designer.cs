using System.Drawing;
namespace Orchestrator.Reports.Invoicing.SubReports
{
    /// <summary>
    /// Summary description for NichollsImageHeader.
    /// </summary>
    partial class NichollsImageHeader
    {
        private DataDynamics.ActiveReports.PageHeader PageHeader;
        private DataDynamics.ActiveReports.Detail Detail;
        private DataDynamics.ActiveReports.PageFooter PageFooter;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NichollsImageHeader));
            this.PageHeader = new DataDynamics.ActiveReports.PageHeader();
            this.Detail = new DataDynamics.ActiveReports.Detail();
            this.PageFooter = new DataDynamics.ActiveReports.PageFooter();
            this.ReportHeader = new DataDynamics.ActiveReports.ReportHeader();
            this.lblCompanyName = new DataDynamics.ActiveReports.Label();
            this.lblAddressLine1 = new DataDynamics.ActiveReports.Label();
            this.lblAddressLine2 = new DataDynamics.ActiveReports.Label();
            this.lblAddressLine3 = new DataDynamics.ActiveReports.Label();
            this.lblRHA = new DataDynamics.ActiveReports.Label();
            this.lblRegNoTitle = new DataDynamics.ActiveReports.Label();
            this.picNichollsLogo = new DataDynamics.ActiveReports.Picture();
            this.label1 = new DataDynamics.ActiveReports.Label();
            this.ReportFooter = new DataDynamics.ActiveReports.ReportFooter();
            ((System.ComponentModel.ISupportInitialize)(this.lblCompanyName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAddressLine1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAddressLine2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAddressLine3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRHA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRegNoTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picNichollsLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // PageHeader
            // 
            this.PageHeader.Height = 0F;
            this.PageHeader.Name = "PageHeader";
            // 
            // Detail
            // 
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Height = 0F;
            this.Detail.Name = "Detail";
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // ReportHeader
            // 
            this.ReportHeader.CanShrink = true;
            this.ReportHeader.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.lblCompanyName,
            this.lblAddressLine1,
            this.lblAddressLine2,
            this.lblAddressLine3,
            this.lblRHA,
            this.lblRegNoTitle,
            this.picNichollsLogo,
            this.label1});
            this.ReportHeader.Height = 1.885417F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // lblCompanyName
            // 
            this.lblCompanyName.Border.BottomColor = System.Drawing.Color.Black;
            this.lblCompanyName.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblCompanyName.Border.LeftColor = System.Drawing.Color.Black;
            this.lblCompanyName.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblCompanyName.Border.RightColor = System.Drawing.Color.Black;
            this.lblCompanyName.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblCompanyName.Border.TopColor = System.Drawing.Color.Black;
            this.lblCompanyName.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblCompanyName.Height = 0.4375F;
            this.lblCompanyName.HyperLink = null;
            this.lblCompanyName.Left = 0.5625F;
            this.lblCompanyName.Name = "lblCompanyName";
            this.lblCompanyName.Style = "ddo-char-set: 1; text-align: center; font-size: 25pt; ";
            this.lblCompanyName.Text = "E H Nicholls";
            this.lblCompanyName.Top = 0.125F;
            this.lblCompanyName.Width = 4.4375F;
            // 
            // lblAddressLine1
            // 
            this.lblAddressLine1.Border.BottomColor = System.Drawing.Color.Black;
            this.lblAddressLine1.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblAddressLine1.Border.LeftColor = System.Drawing.Color.Black;
            this.lblAddressLine1.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblAddressLine1.Border.RightColor = System.Drawing.Color.Black;
            this.lblAddressLine1.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblAddressLine1.Border.TopColor = System.Drawing.Color.Black;
            this.lblAddressLine1.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblAddressLine1.Height = 0.1875F;
            this.lblAddressLine1.HyperLink = null;
            this.lblAddressLine1.Left = 0.5625F;
            this.lblAddressLine1.Name = "lblAddressLine1";
            this.lblAddressLine1.Style = "ddo-char-set: 0; text-align: center; font-size: 9.75pt; font-family: Microsoft Sa" +
                "ns Serif; ";
            this.lblAddressLine1.Text = "Sittingbourne Logistics Park, Swale Way, Sittingbourne, Kent, ME10 2FF";
            this.lblAddressLine1.Top = 0.5625F;
            this.lblAddressLine1.Width = 4.4375F;
            // 
            // lblAddressLine2
            // 
            this.lblAddressLine2.Border.BottomColor = System.Drawing.Color.Black;
            this.lblAddressLine2.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblAddressLine2.Border.LeftColor = System.Drawing.Color.Black;
            this.lblAddressLine2.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblAddressLine2.Border.RightColor = System.Drawing.Color.Black;
            this.lblAddressLine2.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblAddressLine2.Border.TopColor = System.Drawing.Color.Black;
            this.lblAddressLine2.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblAddressLine2.Height = 0.1875F;
            this.lblAddressLine2.HyperLink = null;
            this.lblAddressLine2.Left = 1F;
            this.lblAddressLine2.Name = "lblAddressLine2";
            this.lblAddressLine2.Style = "ddo-char-set: 0; text-align: center; font-size: 9.75pt; font-family: Microsoft Sa" +
                "ns Serif; ";
            this.lblAddressLine2.Text = "Fakenham, Norfolk, NR21 8NL";
            this.lblAddressLine2.Top = 0.75F;
            this.lblAddressLine2.Width = 3.5625F;
            // 
            // lblAddressLine3
            // 
            this.lblAddressLine3.Border.BottomColor = System.Drawing.Color.Black;
            this.lblAddressLine3.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblAddressLine3.Border.LeftColor = System.Drawing.Color.Black;
            this.lblAddressLine3.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblAddressLine3.Border.RightColor = System.Drawing.Color.Black;
            this.lblAddressLine3.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblAddressLine3.Border.TopColor = System.Drawing.Color.Black;
            this.lblAddressLine3.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblAddressLine3.Height = 0.1875F;
            this.lblAddressLine3.HyperLink = null;
            this.lblAddressLine3.Left = 1F;
            this.lblAddressLine3.Name = "lblAddressLine3";
            this.lblAddressLine3.Style = "ddo-char-set: 0; text-align: center; font-size: 9.75pt; font-family: Microsoft Sa" +
                "ns Serif; ";
            this.lblAddressLine3.Text = "Tel: 01328 863 111 - Fax: 01328 851 026";
            this.lblAddressLine3.Top = 0.9375F;
            this.lblAddressLine3.Width = 3.5625F;
            // 
            // lblRHA
            // 
            this.lblRHA.Border.BottomColor = System.Drawing.Color.Black;
            this.lblRHA.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblRHA.Border.LeftColor = System.Drawing.Color.Black;
            this.lblRHA.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblRHA.Border.RightColor = System.Drawing.Color.Black;
            this.lblRHA.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblRHA.Border.TopColor = System.Drawing.Color.Black;
            this.lblRHA.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblRHA.Height = 0.1875F;
            this.lblRHA.HyperLink = null;
            this.lblRHA.Left = 0.59375F;
            this.lblRHA.Name = "lblRHA";
            this.lblRHA.Style = "text-align: center; font-size: 8.25pt; ";
            this.lblRHA.Text = "All goods carried subject to RHA Conditions of Carriage 1998";
            this.lblRHA.Top = 1.5625F;
            this.lblRHA.Width = 4.375F;
            // 
            // lblRegNoTitle
            // 
            this.lblRegNoTitle.Border.BottomColor = System.Drawing.Color.Black;
            this.lblRegNoTitle.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblRegNoTitle.Border.LeftColor = System.Drawing.Color.Black;
            this.lblRegNoTitle.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblRegNoTitle.Border.RightColor = System.Drawing.Color.Black;
            this.lblRegNoTitle.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblRegNoTitle.Border.TopColor = System.Drawing.Color.Black;
            this.lblRegNoTitle.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblRegNoTitle.Height = 0.1875F;
            this.lblRegNoTitle.HyperLink = null;
            this.lblRegNoTitle.Left = 1F;
            this.lblRegNoTitle.Name = "lblRegNoTitle";
            this.lblRegNoTitle.Style = "ddo-char-set: 0; text-align: center; font-size: 9.75pt; font-family: Microsoft Sa" +
                "ns Serif; ";
            this.lblRegNoTitle.Text = "Reg No:";
            this.lblRegNoTitle.Top = 1.125F;
            this.lblRegNoTitle.Width = 3.5625F;
            // 
            // picNichollsLogo
            // 
            this.picNichollsLogo.Border.BottomColor = System.Drawing.Color.Black;
            this.picNichollsLogo.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.picNichollsLogo.Border.LeftColor = System.Drawing.Color.Black;
            this.picNichollsLogo.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.picNichollsLogo.Border.RightColor = System.Drawing.Color.Black;
            this.picNichollsLogo.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.picNichollsLogo.Border.TopColor = System.Drawing.Color.Black;
            this.picNichollsLogo.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.picNichollsLogo.Height = 1.524F;
            this.picNichollsLogo.Image = ((System.Drawing.Image)(resources.GetObject("picNichollsLogo.Image")));
            this.picNichollsLogo.ImageData = ((System.IO.Stream)(resources.GetObject("picNichollsLogo.ImageData")));
            this.picNichollsLogo.Left = 5.625F;
            this.picNichollsLogo.LineWeight = 0F;
            this.picNichollsLogo.Name = "picNichollsLogo";
            this.picNichollsLogo.Top = 0.1875F;
            this.picNichollsLogo.Width = 1.524F;
            // 
            // label1
            // 
            this.label1.Border.BottomColor = System.Drawing.Color.Black;
            this.label1.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.label1.Border.LeftColor = System.Drawing.Color.Black;
            this.label1.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.label1.Border.RightColor = System.Drawing.Color.Black;
            this.label1.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.label1.Border.TopColor = System.Drawing.Color.Black;
            this.label1.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.label1.Height = 0.1875F;
            this.label1.HyperLink = null;
            this.label1.Left = 0.59375F;
            this.label1.Name = "label1";
            this.label1.Style = "text-align: center; font-size: 8.25pt; ";
            this.label1.Text = "or CMR Conditions of Carriage";
            this.label1.Top = 1.6875F;
            this.label1.Width = 4.375F;
            // 
            // ReportFooter
            // 
            this.ReportFooter.Height = 0F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // NichollsImageHeader
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11.69F;
            this.PageSettings.PaperWidth = 8.27F;
            this.PrintWidth = 7.479167F;
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" +
                        "l; font-size: 10pt; color: Black; ", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; ", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" +
                        "lic; ", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.lblCompanyName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAddressLine1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAddressLine2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAddressLine3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRHA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRegNoTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picNichollsLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private DataDynamics.ActiveReports.ReportHeader ReportHeader;
        private DataDynamics.ActiveReports.ReportFooter ReportFooter;
        private DataDynamics.ActiveReports.Label lblCompanyName;
        private DataDynamics.ActiveReports.Label lblAddressLine1;
        private DataDynamics.ActiveReports.Label lblAddressLine2;
        private DataDynamics.ActiveReports.Label lblAddressLine3;
        private DataDynamics.ActiveReports.Label lblRHA;
        private DataDynamics.ActiveReports.Label lblRegNoTitle;
        private DataDynamics.ActiveReports.Picture picNichollsLogo;
        private DataDynamics.ActiveReports.Label label1;
    }
}
