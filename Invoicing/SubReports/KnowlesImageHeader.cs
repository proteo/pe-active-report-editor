using System;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;

namespace Orchestrator.Reports.Invoicing.SubReports
{
    public class KnowlesImageHeader : DataDynamics.ActiveReports.ActiveReport3
	{
		public KnowlesImageHeader()
		{
			InitializeComponent();
            lblCompanyName.Text = System.Configuration.ConfigurationManager.AppSettings["CompanyName"];
            lblAddressLine1.Text = System.Configuration.ConfigurationManager.AppSettings["Address1.Line1"];
            lblAddressLine2.Text = System.Configuration.ConfigurationManager.AppSettings["Address1.Line2"];
            lblAddressLine3.Text = System.Configuration.ConfigurationManager.AppSettings["Address1.Line3"];
            lblRegNo.Text = System.Configuration.ConfigurationManager.AppSettings["RegistrationNumber"];
            lblVATNumber.Text = System.Configuration.ConfigurationManager.AppSettings["VatNo"];
		}

		#region ActiveReports Designer generated code
		private DataDynamics.ActiveReports.ReportHeader ReportHeader = null;
        private DataDynamics.ActiveReports.Picture picHubPartnersLogo = null;
		private DataDynamics.ActiveReports.Label lblCompanyName = null;
		private DataDynamics.ActiveReports.Label lblAddressLine1 = null;
        private DataDynamics.ActiveReports.Label lblAddressLine2 = null;
		private DataDynamics.ActiveReports.TextBox lblAddressLine3 = null;
		private DataDynamics.ActiveReports.PageHeader PageHeader = null;
		private DataDynamics.ActiveReports.Detail Detail = null;
		private DataDynamics.ActiveReports.PageFooter PageFooter = null;
        private Label lblRHA;
        private Label lblRegNoTitle;
        private Label lblRegNo;
        private Label lblVATNumberTitle;
        private Label lblVATNumber;
		private DataDynamics.ActiveReports.ReportFooter ReportFooter = null;
		public void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(KnowlesImageHeader));
            this.Detail = new DataDynamics.ActiveReports.Detail();
            this.ReportHeader = new DataDynamics.ActiveReports.ReportHeader();
            this.picHubPartnersLogo = new DataDynamics.ActiveReports.Picture();
            this.lblCompanyName = new DataDynamics.ActiveReports.Label();
            this.lblAddressLine1 = new DataDynamics.ActiveReports.Label();
            this.lblAddressLine2 = new DataDynamics.ActiveReports.Label();
            this.lblAddressLine3 = new DataDynamics.ActiveReports.TextBox();
            this.lblRHA = new DataDynamics.ActiveReports.Label();
            this.lblRegNoTitle = new DataDynamics.ActiveReports.Label();
            this.lblRegNo = new DataDynamics.ActiveReports.Label();
            this.lblVATNumberTitle = new DataDynamics.ActiveReports.Label();
            this.lblVATNumber = new DataDynamics.ActiveReports.Label();
            this.ReportFooter = new DataDynamics.ActiveReports.ReportFooter();
            this.PageHeader = new DataDynamics.ActiveReports.PageHeader();
            this.PageFooter = new DataDynamics.ActiveReports.PageFooter();
            ((System.ComponentModel.ISupportInitialize)(this.picHubPartnersLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCompanyName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAddressLine1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAddressLine2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAddressLine3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRHA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRegNoTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRegNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVATNumberTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVATNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Height = 0F;
            this.Detail.Name = "Detail";
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.picHubPartnersLogo,
            this.lblCompanyName,
            this.lblAddressLine1,
            this.lblAddressLine2,
            this.lblAddressLine3,
            this.lblRHA,
            this.lblRegNoTitle,
            this.lblRegNo,
            this.lblVATNumberTitle,
            this.lblVATNumber});
            this.ReportHeader.Height = 1.864583F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // picHubPartnersLogo
            // 
            this.picHubPartnersLogo.Border.BottomColor = System.Drawing.Color.Black;
            this.picHubPartnersLogo.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.picHubPartnersLogo.Border.LeftColor = System.Drawing.Color.Black;
            this.picHubPartnersLogo.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.picHubPartnersLogo.Border.RightColor = System.Drawing.Color.Black;
            this.picHubPartnersLogo.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.picHubPartnersLogo.Border.TopColor = System.Drawing.Color.Black;
            this.picHubPartnersLogo.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.picHubPartnersLogo.Height = 0.6875F;
            this.picHubPartnersLogo.Image = ((System.Drawing.Image)(resources.GetObject("picHubPartnersLogo.Image")));
            this.picHubPartnersLogo.ImageData = ((System.IO.Stream)(resources.GetObject("picHubPartnersLogo.ImageData")));
            this.picHubPartnersLogo.Left = 3.625F;
            this.picHubPartnersLogo.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.picHubPartnersLogo.LineWeight = 0F;
            this.picHubPartnersLogo.Name = "picHubPartnersLogo";
            this.picHubPartnersLogo.PictureAlignment = DataDynamics.ActiveReports.PictureAlignment.TopRight;
            this.picHubPartnersLogo.SizeMode = DataDynamics.ActiveReports.SizeModes.Zoom;
            this.picHubPartnersLogo.Top = 0.0625F;
            this.picHubPartnersLogo.Width = 3.8125F;
            // 
            // lblCompanyName
            // 
            this.lblCompanyName.Border.BottomColor = System.Drawing.Color.Black;
            this.lblCompanyName.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblCompanyName.Border.LeftColor = System.Drawing.Color.Black;
            this.lblCompanyName.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblCompanyName.Border.RightColor = System.Drawing.Color.Black;
            this.lblCompanyName.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblCompanyName.Border.TopColor = System.Drawing.Color.Black;
            this.lblCompanyName.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblCompanyName.Height = 0.1875F;
            this.lblCompanyName.HyperLink = null;
            this.lblCompanyName.Left = 5.25F;
            this.lblCompanyName.Name = "lblCompanyName";
            this.lblCompanyName.Style = "color: #231F20; ddo-char-set: 0; text-align: left; font-weight: normal; font-size" +
                ": 8.25pt; font-family: Verdana; ";
            this.lblCompanyName.Text = "Leverintgton Road Industrial Estate";
            this.lblCompanyName.Top = 0.75F;
            this.lblCompanyName.Width = 2.1875F;
            // 
            // lblAddressLine1
            // 
            this.lblAddressLine1.Border.BottomColor = System.Drawing.Color.Black;
            this.lblAddressLine1.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblAddressLine1.Border.LeftColor = System.Drawing.Color.Black;
            this.lblAddressLine1.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblAddressLine1.Border.RightColor = System.Drawing.Color.Black;
            this.lblAddressLine1.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblAddressLine1.Border.TopColor = System.Drawing.Color.Black;
            this.lblAddressLine1.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblAddressLine1.Height = 0.1875F;
            this.lblAddressLine1.HyperLink = null;
            this.lblAddressLine1.Left = 5.25F;
            this.lblAddressLine1.Name = "lblAddressLine1";
            this.lblAddressLine1.Style = "color: #231F20; ddo-char-set: 0; text-align: left; font-size: 8.25pt; font-family" +
                ": Verdana; ";
            this.lblAddressLine1.Text = "Wisbech - Cambridgeshire PE13 1PL";
            this.lblAddressLine1.Top = 0.9375F;
            this.lblAddressLine1.Width = 2.1875F;
            // 
            // lblAddressLine2
            // 
            this.lblAddressLine2.Border.BottomColor = System.Drawing.Color.Black;
            this.lblAddressLine2.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblAddressLine2.Border.LeftColor = System.Drawing.Color.Black;
            this.lblAddressLine2.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblAddressLine2.Border.RightColor = System.Drawing.Color.Black;
            this.lblAddressLine2.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblAddressLine2.Border.TopColor = System.Drawing.Color.Black;
            this.lblAddressLine2.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblAddressLine2.Height = 0.1875F;
            this.lblAddressLine2.HyperLink = null;
            this.lblAddressLine2.Left = 5.25F;
            this.lblAddressLine2.Name = "lblAddressLine2";
            this.lblAddressLine2.Style = "color: #231F20; ddo-char-set: 0; text-align: left; font-size: 8.25pt; font-family" +
                ": Verdana; ";
            this.lblAddressLine2.Text = "t: 01945 465900 - f: 01945950";
            this.lblAddressLine2.Top = 1.125F;
            this.lblAddressLine2.Width = 2.1875F;
            // 
            // lblAddressLine3
            // 
            this.lblAddressLine3.Border.BottomColor = System.Drawing.Color.Black;
            this.lblAddressLine3.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblAddressLine3.Border.LeftColor = System.Drawing.Color.Black;
            this.lblAddressLine3.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblAddressLine3.Border.RightColor = System.Drawing.Color.Black;
            this.lblAddressLine3.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblAddressLine3.Border.TopColor = System.Drawing.Color.Black;
            this.lblAddressLine3.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblAddressLine3.Height = 0.1875F;
            this.lblAddressLine3.Left = 5.25F;
            this.lblAddressLine3.Name = "lblAddressLine3";
            this.lblAddressLine3.Style = "color: #231F20; ddo-char-set: 0; text-align: left; font-weight: bold; font-size: " +
                "8.25pt; font-family: Verdana; ";
            this.lblAddressLine3.Text = "www.hubpartners.co.uk";
            this.lblAddressLine3.Top = 1.3125F;
            this.lblAddressLine3.Width = 2.1875F;
            // 
            // lblRHA
            // 
            this.lblRHA.Border.BottomColor = System.Drawing.Color.Black;
            this.lblRHA.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblRHA.Border.LeftColor = System.Drawing.Color.Black;
            this.lblRHA.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblRHA.Border.RightColor = System.Drawing.Color.Black;
            this.lblRHA.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblRHA.Border.TopColor = System.Drawing.Color.Black;
            this.lblRHA.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblRHA.Height = 0.1875F;
            this.lblRHA.HyperLink = null;
            this.lblRHA.Left = 0.375F;
            this.lblRHA.Name = "lblRHA";
            this.lblRHA.Style = "text-align: center; font-size: 8.25pt; ";
            this.lblRHA.Text = "All goods carried subject to RHA Conditions of Carriage 2009";
            this.lblRHA.Top = 1.5F;
            this.lblRHA.Width = 7.072917F;
            // 
            // lblRegNoTitle
            // 
            this.lblRegNoTitle.Border.BottomColor = System.Drawing.Color.Black;
            this.lblRegNoTitle.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblRegNoTitle.Border.LeftColor = System.Drawing.Color.Black;
            this.lblRegNoTitle.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblRegNoTitle.Border.RightColor = System.Drawing.Color.Black;
            this.lblRegNoTitle.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblRegNoTitle.Border.TopColor = System.Drawing.Color.Black;
            this.lblRegNoTitle.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblRegNoTitle.Height = 0.1875F;
            this.lblRegNoTitle.HyperLink = null;
            this.lblRegNoTitle.Left = 0.5625F;
            this.lblRegNoTitle.Name = "lblRegNoTitle";
            this.lblRegNoTitle.Style = "font-size: 8pt; ";
            this.lblRegNoTitle.Text = "Reg No:";
            this.lblRegNoTitle.Top = 1.5F;
            this.lblRegNoTitle.Width = 0.5F;
            // 
            // lblRegNo
            // 
            this.lblRegNo.Border.BottomColor = System.Drawing.Color.Black;
            this.lblRegNo.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblRegNo.Border.LeftColor = System.Drawing.Color.Black;
            this.lblRegNo.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblRegNo.Border.RightColor = System.Drawing.Color.Black;
            this.lblRegNo.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblRegNo.Border.TopColor = System.Drawing.Color.Black;
            this.lblRegNo.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblRegNo.Height = 0.1875F;
            this.lblRegNo.HyperLink = null;
            this.lblRegNo.Left = 1F;
            this.lblRegNo.Name = "lblRegNo";
            this.lblRegNo.Style = "font-size: 8pt; ";
            this.lblRegNo.Text = "1041518";
            this.lblRegNo.Top = 1.5F;
            this.lblRegNo.Width = 1.0625F;
            // 
            // lblVATNumberTitle
            // 
            this.lblVATNumberTitle.Border.BottomColor = System.Drawing.Color.Black;
            this.lblVATNumberTitle.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblVATNumberTitle.Border.LeftColor = System.Drawing.Color.Black;
            this.lblVATNumberTitle.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblVATNumberTitle.Border.RightColor = System.Drawing.Color.Black;
            this.lblVATNumberTitle.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblVATNumberTitle.Border.TopColor = System.Drawing.Color.Black;
            this.lblVATNumberTitle.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblVATNumberTitle.Height = 0.1875F;
            this.lblVATNumberTitle.HyperLink = null;
            this.lblVATNumberTitle.Left = 5.8125F;
            this.lblVATNumberTitle.Name = "lblVATNumberTitle";
            this.lblVATNumberTitle.Style = "font-size: 8pt; ";
            this.lblVATNumberTitle.Text = "VAT No:";
            this.lblVATNumberTitle.Top = 1.5F;
            this.lblVATNumberTitle.Width = 0.5F;
            // 
            // lblVATNumber
            // 
            this.lblVATNumber.Border.BottomColor = System.Drawing.Color.Black;
            this.lblVATNumber.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblVATNumber.Border.LeftColor = System.Drawing.Color.Black;
            this.lblVATNumber.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblVATNumber.Border.RightColor = System.Drawing.Color.Black;
            this.lblVATNumber.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblVATNumber.Border.TopColor = System.Drawing.Color.Black;
            this.lblVATNumber.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblVATNumber.Height = 0.1875F;
            this.lblVATNumber.HyperLink = null;
            this.lblVATNumber.Left = 6.25F;
            this.lblVATNumber.Name = "lblVATNumber";
            this.lblVATNumber.Style = "font-size: 8pt; ";
            this.lblVATNumber.Text = "676 8148 86";
            this.lblVATNumber.Top = 1.5F;
            this.lblVATNumber.Width = 1.1875F;
            // 
            // ReportFooter
            // 
            this.ReportFooter.Height = 0F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // PageHeader
            // 
            this.PageHeader.Height = 0F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // KnowlesImageHeader
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11.69F;
            this.PageSettings.PaperWidth = 8.27F;
            this.PrintWidth = 7.510417F;
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                        "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.picHubPartnersLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCompanyName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAddressLine1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAddressLine2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAddressLine3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRHA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRegNoTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRegNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVATNumberTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVATNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

		#endregion
	}
}
