using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;

namespace Orchestrator.Reports.Invoicing.SubReports
{
    /// <summary>
    /// Summary description for FFImageHeader.
    /// </summary>
    public partial class FFImageHeader : DataDynamics.ActiveReports.ActiveReport3
    {

        public FFImageHeader()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
        }
    }
}
