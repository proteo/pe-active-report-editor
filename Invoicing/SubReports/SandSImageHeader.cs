using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;
using System.Configuration;
using System.Collections.Specialized;
using System.Data;
using System.Web;

namespace Orchestrator.Reports.Invoicing.SubReports
{
    public class SandSImageHeader : DataDynamics.ActiveReports.ActiveReport3
	{
        public SandSImageHeader()
		{
			InitializeComponent();

            this.addressLine1.Text = ConfigurationManager.AppSettings["CompanyName"];
            this.addressLine2.Text = ConfigurationManager.AppSettings["Address1.Line1"];
            this.addressLine3.Text = ConfigurationManager.AppSettings["Address1.Line2"];
            this.addressLine4.Text = ConfigurationManager.AppSettings["Address1.Line3"];
            this.addressLine5.Text = ConfigurationManager.AppSettings["Address1.Line4"];

            this.addressLine6.Text = ConfigurationManager.AppSettings["RegistrationNumber"];
		}

		#region ActiveReports Designer generated code
        private DataDynamics.ActiveReports.ReportHeader ReportHeader = null;
		private DataDynamics.ActiveReports.PageHeader PageHeader = null;
		private DataDynamics.ActiveReports.Detail Detail = null;
        private DataDynamics.ActiveReports.PageFooter PageFooter = null;
        private Picture companyLog;
        private TextBox addressLine1;
        private TextBox addressLine2;
        private TextBox addressLine3;
        private TextBox addressLine4;
        private TextBox addressLine5;
        private TextBox addressLine6;
		private DataDynamics.ActiveReports.ReportFooter ReportFooter = null;
		public void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SandSImageHeader));
            this.Detail = new DataDynamics.ActiveReports.Detail();
            this.ReportHeader = new DataDynamics.ActiveReports.ReportHeader();
            this.companyLog = new DataDynamics.ActiveReports.Picture();
            this.addressLine1 = new DataDynamics.ActiveReports.TextBox();
            this.addressLine2 = new DataDynamics.ActiveReports.TextBox();
            this.addressLine3 = new DataDynamics.ActiveReports.TextBox();
            this.addressLine4 = new DataDynamics.ActiveReports.TextBox();
            this.addressLine5 = new DataDynamics.ActiveReports.TextBox();
            this.addressLine6 = new DataDynamics.ActiveReports.TextBox();
            this.ReportFooter = new DataDynamics.ActiveReports.ReportFooter();
            this.PageHeader = new DataDynamics.ActiveReports.PageHeader();
            this.PageFooter = new DataDynamics.ActiveReports.PageFooter();
            ((System.ComponentModel.ISupportInitialize)(this.companyLog)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.addressLine1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.addressLine2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.addressLine3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.addressLine4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.addressLine5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.addressLine6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Height = 0F;
            this.Detail.Name = "Detail";
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.companyLog,
            this.addressLine1,
            this.addressLine2,
            this.addressLine3,
            this.addressLine4,
            this.addressLine5,
            this.addressLine6});
            this.ReportHeader.Height = 1.5F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // companyLog
            // 
            this.companyLog.Border.BottomColor = System.Drawing.Color.Black;
            this.companyLog.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.companyLog.Border.LeftColor = System.Drawing.Color.Black;
            this.companyLog.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.companyLog.Border.RightColor = System.Drawing.Color.Black;
            this.companyLog.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.companyLog.Border.TopColor = System.Drawing.Color.Black;
            this.companyLog.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.companyLog.Height = 1F;
            this.companyLog.Image = ((System.Drawing.Image)(resources.GetObject("companyLog.Image")));
            this.companyLog.ImageData = ((System.IO.Stream)(resources.GetObject("companyLog.ImageData")));
            this.companyLog.Left = 2.75F;
            this.companyLog.LineWeight = 0F;
            this.companyLog.Name = "companyLog";
            this.companyLog.Top = 0.0625F;
            this.companyLog.Width = 4.6875F;
            // 
            // addressLine1
            // 
            this.addressLine1.Border.BottomColor = System.Drawing.Color.Black;
            this.addressLine1.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.addressLine1.Border.LeftColor = System.Drawing.Color.Black;
            this.addressLine1.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.addressLine1.Border.RightColor = System.Drawing.Color.Black;
            this.addressLine1.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.addressLine1.Border.TopColor = System.Drawing.Color.Black;
            this.addressLine1.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.addressLine1.Height = 0.1875F;
            this.addressLine1.Left = 0.0625F;
            this.addressLine1.Name = "addressLine1";
            this.addressLine1.Style = "";
            this.addressLine1.Text = "Scott and Scott";
            this.addressLine1.Top = 0.125F;
            this.addressLine1.Width = 2.625F;
            // 
            // addressLine2
            // 
            this.addressLine2.Border.BottomColor = System.Drawing.Color.Black;
            this.addressLine2.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.addressLine2.Border.LeftColor = System.Drawing.Color.Black;
            this.addressLine2.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.addressLine2.Border.RightColor = System.Drawing.Color.Black;
            this.addressLine2.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.addressLine2.Border.TopColor = System.Drawing.Color.Black;
            this.addressLine2.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.addressLine2.Height = 0.1875F;
            this.addressLine2.Left = 0.0625F;
            this.addressLine2.Name = "addressLine2";
            this.addressLine2.Style = "";
            this.addressLine2.Text = "Owls Farm, Owls Lane, Buntingford";
            this.addressLine2.Top = 0.3125F;
            this.addressLine2.Width = 2.625F;
            // 
            // addressLine3
            // 
            this.addressLine3.Border.BottomColor = System.Drawing.Color.Black;
            this.addressLine3.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.addressLine3.Border.LeftColor = System.Drawing.Color.Black;
            this.addressLine3.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.addressLine3.Border.RightColor = System.Drawing.Color.Black;
            this.addressLine3.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.addressLine3.Border.TopColor = System.Drawing.Color.Black;
            this.addressLine3.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.addressLine3.Height = 0.1875F;
            this.addressLine3.Left = 0.0625F;
            this.addressLine3.Name = "addressLine3";
            this.addressLine3.Style = "";
            this.addressLine3.Text = "SG9 9PL";
            this.addressLine3.Top = 0.5F;
            this.addressLine3.Width = 2.625F;
            // 
            // addressLine4
            // 
            this.addressLine4.Border.BottomColor = System.Drawing.Color.Black;
            this.addressLine4.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.addressLine4.Border.LeftColor = System.Drawing.Color.Black;
            this.addressLine4.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.addressLine4.Border.RightColor = System.Drawing.Color.Black;
            this.addressLine4.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.addressLine4.Border.TopColor = System.Drawing.Color.Black;
            this.addressLine4.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.addressLine4.Height = 0.1875F;
            this.addressLine4.Left = 0.0625F;
            this.addressLine4.Name = "addressLine4";
            this.addressLine4.Style = "";
            this.addressLine4.Top = 0.6875F;
            this.addressLine4.Width = 2.625F;
            // 
            // addressLine5
            // 
            this.addressLine5.Border.BottomColor = System.Drawing.Color.Black;
            this.addressLine5.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.addressLine5.Border.LeftColor = System.Drawing.Color.Black;
            this.addressLine5.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.addressLine5.Border.RightColor = System.Drawing.Color.Black;
            this.addressLine5.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.addressLine5.Border.TopColor = System.Drawing.Color.Black;
            this.addressLine5.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.addressLine5.Height = 0.1875F;
            this.addressLine5.Left = 0.0625F;
            this.addressLine5.Name = "addressLine5";
            this.addressLine5.Style = "";
            this.addressLine5.Text = "Tel: 01763 290 341";
            this.addressLine5.Top = 0.875F;
            this.addressLine5.Width = 2.625F;
            // 
            // addressLine6
            // 
            this.addressLine6.Border.BottomColor = System.Drawing.Color.Black;
            this.addressLine6.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.addressLine6.Border.LeftColor = System.Drawing.Color.Black;
            this.addressLine6.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.addressLine6.Border.RightColor = System.Drawing.Color.Black;
            this.addressLine6.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.addressLine6.Border.TopColor = System.Drawing.Color.Black;
            this.addressLine6.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.addressLine6.Height = 0.1875F;
            this.addressLine6.Left = 0.0625F;
            this.addressLine6.Name = "addressLine6";
            this.addressLine6.Style = "";
            this.addressLine6.Text = "VAT Reg No: TBC";
            this.addressLine6.Top = 1.0625F;
            this.addressLine6.Width = 2.625F;
            // 
            // ReportFooter
            // 
            this.ReportFooter.Height = 0F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // PageHeader
            // 
            this.PageHeader.Height = 0F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // SandSImageHeader
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11.69F;
            this.PageSettings.PaperWidth = 8.27F;
            this.PrintWidth = 7.510417F;
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                        "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.companyLog)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.addressLine1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.addressLine2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.addressLine3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.addressLine4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.addressLine5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.addressLine6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

		#endregion
	}
}
