using System;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;

namespace Orchestrator.Reports.Invoicing.SubReports
{
    public class FirminsImageHeader : DataDynamics.ActiveReports.ActiveReport3
	{
		public FirminsImageHeader()
		{
			InitializeComponent();
		}

		#region ActiveReports Designer generated code
		private DataDynamics.ActiveReports.ReportHeader ReportHeader = null;
        private DataDynamics.ActiveReports.Picture picAlanFirminLogo = null;
		private DataDynamics.ActiveReports.Picture picFirminRecruit = null;
		private DataDynamics.ActiveReports.Label lblAlanFirminLtd = null;
		private DataDynamics.ActiveReports.Label Label = null;
		private DataDynamics.ActiveReports.Label Label1 = null;
		private DataDynamics.ActiveReports.Label Label2 = null;
		private DataDynamics.ActiveReports.Label Label3 = null;
		private DataDynamics.ActiveReports.TextBox TextBox = null;
		private DataDynamics.ActiveReports.TextBox TextBox1 = null;
		private DataDynamics.ActiveReports.PageHeader PageHeader = null;
		private DataDynamics.ActiveReports.Detail Detail = null;
		private DataDynamics.ActiveReports.PageFooter PageFooter = null;
        private Picture picFirminExpress;
		private DataDynamics.ActiveReports.ReportFooter ReportFooter = null;
		public void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FirminsImageHeader));
            this.Detail = new DataDynamics.ActiveReports.Detail();
            this.ReportHeader = new DataDynamics.ActiveReports.ReportHeader();
            this.picAlanFirminLogo = new DataDynamics.ActiveReports.Picture();
            this.picFirminRecruit = new DataDynamics.ActiveReports.Picture();
            this.lblAlanFirminLtd = new DataDynamics.ActiveReports.Label();
            this.Label = new DataDynamics.ActiveReports.Label();
            this.Label1 = new DataDynamics.ActiveReports.Label();
            this.Label2 = new DataDynamics.ActiveReports.Label();
            this.Label3 = new DataDynamics.ActiveReports.Label();
            this.TextBox = new DataDynamics.ActiveReports.TextBox();
            this.TextBox1 = new DataDynamics.ActiveReports.TextBox();
            this.picFirminExpress = new DataDynamics.ActiveReports.Picture();
            this.ReportFooter = new DataDynamics.ActiveReports.ReportFooter();
            this.PageHeader = new DataDynamics.ActiveReports.PageHeader();
            this.PageFooter = new DataDynamics.ActiveReports.PageFooter();
            ((System.ComponentModel.ISupportInitialize)(this.picAlanFirminLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFirminRecruit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAlanFirminLtd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFirminExpress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Height = 0F;
            this.Detail.Name = "Detail";
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.picAlanFirminLogo,
            this.picFirminRecruit,
            this.lblAlanFirminLtd,
            this.Label,
            this.Label1,
            this.Label2,
            this.Label3,
            this.TextBox,
            this.TextBox1,
            this.picFirminExpress});
            this.ReportHeader.Height = 1.71875F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // picAlanFirminLogo
            // 
            this.picAlanFirminLogo.Border.BottomColor = System.Drawing.Color.Black;
            this.picAlanFirminLogo.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.picAlanFirminLogo.Border.LeftColor = System.Drawing.Color.Black;
            this.picAlanFirminLogo.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.picAlanFirminLogo.Border.RightColor = System.Drawing.Color.Black;
            this.picAlanFirminLogo.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.picAlanFirminLogo.Border.TopColor = System.Drawing.Color.Black;
            this.picAlanFirminLogo.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.picAlanFirminLogo.Height = 1.0625F;
            this.picAlanFirminLogo.Image = ((System.Drawing.Image)(resources.GetObject("picAlanFirminLogo.Image")));
            this.picAlanFirminLogo.ImageData = ((System.IO.Stream)(resources.GetObject("picAlanFirminLogo.ImageData")));
            this.picAlanFirminLogo.Left = 0F;
            this.picAlanFirminLogo.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.picAlanFirminLogo.LineWeight = 0F;
            this.picAlanFirminLogo.Name = "picAlanFirminLogo";
            this.picAlanFirminLogo.SizeMode = DataDynamics.ActiveReports.SizeModes.Zoom;
            this.picAlanFirminLogo.Top = 0F;
            this.picAlanFirminLogo.Width = 4.6875F;
            // 
            // picFirminRecruit
            // 
            this.picFirminRecruit.Border.BottomColor = System.Drawing.Color.Black;
            this.picFirminRecruit.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.picFirminRecruit.Border.LeftColor = System.Drawing.Color.Black;
            this.picFirminRecruit.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.picFirminRecruit.Border.RightColor = System.Drawing.Color.Black;
            this.picFirminRecruit.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.picFirminRecruit.Border.TopColor = System.Drawing.Color.Black;
            this.picFirminRecruit.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.picFirminRecruit.Height = 0.563F;
            this.picFirminRecruit.Image = ((System.Drawing.Image)(resources.GetObject("picFirminRecruit.Image")));
            this.picFirminRecruit.ImageData = ((System.IO.Stream)(resources.GetObject("picFirminRecruit.ImageData")));
            this.picFirminRecruit.Left = 5.125F;
            this.picFirminRecruit.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.picFirminRecruit.LineWeight = 0F;
            this.picFirminRecruit.Name = "picFirminRecruit";
            this.picFirminRecruit.PictureAlignment = DataDynamics.ActiveReports.PictureAlignment.TopRight;
            this.picFirminRecruit.SizeMode = DataDynamics.ActiveReports.SizeModes.Zoom;
            this.picFirminRecruit.Top = 1.125F;
            this.picFirminRecruit.Width = 2.313F;
            // 
            // lblAlanFirminLtd
            // 
            this.lblAlanFirminLtd.Border.BottomColor = System.Drawing.Color.Black;
            this.lblAlanFirminLtd.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblAlanFirminLtd.Border.LeftColor = System.Drawing.Color.Black;
            this.lblAlanFirminLtd.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblAlanFirminLtd.Border.RightColor = System.Drawing.Color.Black;
            this.lblAlanFirminLtd.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblAlanFirminLtd.Border.TopColor = System.Drawing.Color.Black;
            this.lblAlanFirminLtd.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblAlanFirminLtd.Height = 0.15F;
            this.lblAlanFirminLtd.HyperLink = null;
            this.lblAlanFirminLtd.Left = 1.4375F;
            this.lblAlanFirminLtd.Name = "lblAlanFirminLtd";
            this.lblAlanFirminLtd.Style = "ddo-char-set: 1; text-align: center; font-weight: normal; font-size: 9pt; font-fa" +
                "mily: Copperplate Gothic Bold; ";
            this.lblAlanFirminLtd.Text = "A L A N   F I R M I N   L T D";
            this.lblAlanFirminLtd.Top = 1.0625F;
            this.lblAlanFirminLtd.Width = 1.938F;
            // 
            // Label
            // 
            this.Label.Border.BottomColor = System.Drawing.Color.Black;
            this.Label.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label.Border.LeftColor = System.Drawing.Color.Black;
            this.Label.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label.Border.RightColor = System.Drawing.Color.Black;
            this.Label.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label.Border.TopColor = System.Drawing.Color.Black;
            this.Label.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label.Height = 0.135F;
            this.Label.HyperLink = null;
            this.Label.Left = 1.4375F;
            this.Label.Name = "Label";
            this.Label.Style = "ddo-char-set: 1; text-align: center; font-size: 7.5pt; font-family: Arial; ";
            this.Label.Text = "Kemsley Fields Business Park";
            this.Label.Top = 1.1875F;
            this.Label.Width = 1.937F;
            // 
            // Label1
            // 
            this.Label1.Border.BottomColor = System.Drawing.Color.Black;
            this.Label1.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label1.Border.LeftColor = System.Drawing.Color.Black;
            this.Label1.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label1.Border.RightColor = System.Drawing.Color.Black;
            this.Label1.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label1.Border.TopColor = System.Drawing.Color.Black;
            this.Label1.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label1.Height = 0.1450001F;
            this.Label1.HyperLink = null;
            this.Label1.Left = 1.6875F;
            this.Label1.Name = "Label1";
            this.Label1.Style = "ddo-char-set: 1; text-align: center; font-size: 7.5pt; font-family: Arial; ";
            this.Label1.Text = "Sittingbourne, ME10 2FE";
            this.Label1.Top = 1.3125F;
            this.Label1.Width = 1.313F;
            // 
            // Label2
            // 
            this.Label2.Border.BottomColor = System.Drawing.Color.Black;
            this.Label2.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label2.Border.LeftColor = System.Drawing.Color.Black;
            this.Label2.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label2.Border.RightColor = System.Drawing.Color.Black;
            this.Label2.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label2.Border.TopColor = System.Drawing.Color.Black;
            this.Label2.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label2.Height = 0.125F;
            this.Label2.HyperLink = null;
            this.Label2.Left = 1.375F;
            this.Label2.Name = "Label2";
            this.Label2.Style = "ddo-char-set: 1; font-size: 7.5pt; font-family: Arial; ";
            this.Label2.Text = "Tel : ";
            this.Label2.Top = 1.4375F;
            this.Label2.Width = 0.25F;
            // 
            // Label3
            // 
            this.Label3.Border.BottomColor = System.Drawing.Color.Black;
            this.Label3.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label3.Border.LeftColor = System.Drawing.Color.Black;
            this.Label3.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label3.Border.RightColor = System.Drawing.Color.Black;
            this.Label3.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label3.Border.TopColor = System.Drawing.Color.Black;
            this.Label3.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label3.Height = 0.125F;
            this.Label3.HyperLink = null;
            this.Label3.Left = 2.375F;
            this.Label3.Name = "Label3";
            this.Label3.Style = "ddo-char-set: 1; font-size: 7.5pt; font-family: Arial; ";
            this.Label3.Text = "Fax :";
            this.Label3.Top = 1.4375F;
            this.Label3.Width = 0.3125F;
            // 
            // TextBox
            // 
            this.TextBox.Border.BottomColor = System.Drawing.Color.Black;
            this.TextBox.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.TextBox.Border.LeftColor = System.Drawing.Color.Black;
            this.TextBox.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.TextBox.Border.RightColor = System.Drawing.Color.Black;
            this.TextBox.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.TextBox.Border.TopColor = System.Drawing.Color.Black;
            this.TextBox.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.TextBox.Height = 0.125F;
            this.TextBox.Left = 1.625F;
            this.TextBox.Name = "TextBox";
            this.TextBox.Style = "ddo-char-set: 1; text-align: center; font-size: 7.5pt; font-family: Arial; ";
            this.TextBox.Text = "0845 130 5050";
            this.TextBox.Top = 1.4375F;
            this.TextBox.Width = 0.75F;
            // 
            // TextBox1
            // 
            this.TextBox1.Border.BottomColor = System.Drawing.Color.Black;
            this.TextBox1.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.TextBox1.Border.LeftColor = System.Drawing.Color.Black;
            this.TextBox1.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.TextBox1.Border.RightColor = System.Drawing.Color.Black;
            this.TextBox1.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.TextBox1.Border.TopColor = System.Drawing.Color.Black;
            this.TextBox1.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.TextBox1.Height = 0.125F;
            this.TextBox1.Left = 2.6875F;
            this.TextBox1.Name = "TextBox1";
            this.TextBox1.Style = "ddo-char-set: 1; text-align: center; font-size: 7.5pt; font-family: Arial; ";
            this.TextBox1.Text = "0845 130 0470";
            this.TextBox1.Top = 1.4375F;
            this.TextBox1.Width = 0.75F;
            // 
            // picFirminExpress
            // 
            this.picFirminExpress.Border.BottomColor = System.Drawing.Color.Black;
            this.picFirminExpress.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.picFirminExpress.Border.LeftColor = System.Drawing.Color.Black;
            this.picFirminExpress.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.picFirminExpress.Border.RightColor = System.Drawing.Color.Black;
            this.picFirminExpress.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.picFirminExpress.Border.TopColor = System.Drawing.Color.Black;
            this.picFirminExpress.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.picFirminExpress.Height = 1.0625F;
            this.picFirminExpress.Image = ((System.Drawing.Image)(resources.GetObject("picFirminExpress.Image")));
            this.picFirminExpress.ImageData = ((System.IO.Stream)(resources.GetObject("picFirminExpress.ImageData")));
            this.picFirminExpress.Left = 5.0625F;
            this.picFirminExpress.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.picFirminExpress.LineWeight = 0F;
            this.picFirminExpress.Name = "picFirminExpress";
            this.picFirminExpress.PictureAlignment = DataDynamics.ActiveReports.PictureAlignment.TopRight;
            this.picFirminExpress.SizeMode = DataDynamics.ActiveReports.SizeModes.Zoom;
            this.picFirminExpress.Top = 0.0625F;
            this.picFirminExpress.Width = 2.3125F;
            // 
            // ReportFooter
            // 
            this.ReportFooter.Height = 0F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // PageHeader
            // 
            this.PageHeader.Height = 0F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // FirminsImageHeader
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11.69F;
            this.PageSettings.PaperWidth = 8.27F;
            this.PrintWidth = 7.510417F;
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                        "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.picAlanFirminLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFirminRecruit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAlanFirminLtd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picFirminExpress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

		#endregion
	}
}
