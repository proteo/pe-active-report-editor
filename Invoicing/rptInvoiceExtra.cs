using System;
using System.Data;
using System.Drawing;
using System.Collections.Specialized;

using Orchestrator.Globals;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;
using System.Web;
using System.Globalization;
using System.Threading;

namespace Orchestrator.Reports.Invoicing
{
	public class rptInvoiceExtra : Orchestrator.Reports.ReportBase
	{
		#region Report Variables

        private int _currentPage = -1;
		public NameValueCollection	m_reportParams;
        private TextBox txtYourVatNo;
        private Label lblYourVatNo;
        private RichTextBox txtCustomNotes;
		private bool				m_includeExtraDetail;

		#endregion

		#region Override Header
        protected override void CreateReportHeader()
        {
        }

        private void ReportBase_BeforePrint(object sender, EventArgs e)
        {
            TextBox txtPageCount = (TextBox)this.Sections["PageFooter"].Controls["ftr_txtPageCount"];
            ((Label)this.Sections["PageFooter"].Controls["ftr_lblPage"]).Text = "Page " + txtPageCount.Text;
        }

        protected override void CreatePageFooter()
        {
            Section PageFooter = Sections["PageFooter"];

            if (PageFooter == null)
            {
                // Insert the page footer,
                Sections.InsertPageHF();
                PageFooter = Sections["PageFooter"];
            }

            this.Sections["PageFooter"].BeforePrint += new EventHandler(ReportBase_BeforePrint);

            // Page Count TextBox
            TextBox txtPageCount = new TextBox();
            txtPageCount.Name = "ftr_txtPageCount";
            txtPageCount.Text = "##";
            txtPageCount.Visible = false;
            txtPageCount.SummaryType = SummaryType.PageCount;
            txtPageCount.SummaryRunning = SummaryRunning.All;

            // Page Count Label
            Label lblPage = new Label();
            lblPage.Name = "ftr_lblPage";
            lblPage.Width = 5F;
            lblPage.Top = 0.1F;
            lblPage.Left = PrintWidth - lblPage.Width;
            lblPage.Alignment = TextAlignment.Right;

            //Add the controls to the footer
            if (PageFooter != null)
                PageFooter.Controls.AddRange(new ARControl[] { txtPageCount, lblPage });

            if (PageFooter != null && !this.showInvoicePageFooter)
            {
                // make the footer smaller for the standard footer to avoid big gaps in the report.
                PageFooter.Height = 0.2F;
                this.srFooter.Height = 0.2F;
            }
            
            if (this.invoicePageFooterOnLastPageOnly == true)
            {
                    this.srFooter.Visible = false;
                
            }
        }

		#endregion

		#region Constructor
		public rptInvoiceExtra()
		{
            m_reportParams = (NameValueCollection)HttpContext.Current.Session[Orchestrator.Globals.Constants.ReportParamsSessionVariable];
			m_includeExtraDetail = (m_reportParams.Get("ExtraDetail") == "Include");
			InitializeComponent();

            rptHeader rpt = new rptHeader();
            this.srHeader.Report = rpt;

            rptPageHeader rptPage = new rptPageHeader();
            this.srPageHeader.Report = rptPage;

            rptFooter rpt_ftr = new rptFooter();
            this.srFooter.Report = rpt_ftr;

            lblTradingAs.Text = System.Web.Configuration.WebConfigurationManager.AppSettings["TradingAs"];

        }

        public override void SetReportCulture()
        {
            DataSet dataSource = (DataSet)this.DataSource;
            CultureInfo culture = null;

            int LCID = 2057;

            foreach (DataRow dr in dataSource.Tables[0].Rows)
                if (dr["LCID"] != DBNull.Value && Convert.ToInt32(dr["LCID"]) != -1)
                {
                    if (LCID != Convert.ToInt32(dr["LCID"]))
                        LCID = Convert.ToInt32(dr["LCID"]);

                    break;
                }

            culture = new CultureInfo(LCID);

            Thread.CurrentThread.CurrentCulture = culture;
        }
		#endregion

		private void Detail_BeforePrint(object sender, System.EventArgs eArgs)
		{
            float maximumHeight = txtLoadNo.Height;

            if (txtDate.Height > maximumHeight)
                maximumHeight = txtDate.Height;
            if (txtExtraType.Height > maximumHeight)
                maximumHeight = txtExtraType.Height;
            if (txtDescription.Height > maximumHeight)
                maximumHeight = txtDescription.Height;
            if (txtClientContact.Height > maximumHeight)
                maximumHeight = txtClientContact.Height;
            if (txtExtraAmount.Height > maximumHeight)
                maximumHeight = txtExtraAmount.Height;

            txtDate.Height = maximumHeight;
            txtLoadNo.Height = maximumHeight;
            txtExtraType.Height = maximumHeight;
            txtDescription.Height = maximumHeight;
            txtClientContact.Height = maximumHeight;
            txtExtraAmount.Height = maximumHeight;

            // at this point the text value will be a number in the format of the current culture, 
            // we need to format it as a currency
            this.txtExtraAmount.Text = Decimal.Parse(this.txtExtraAmount.Text, NumberStyles.Currency).ToString("C");
		}

        private void PageHeader_Format(object sender, System.EventArgs eArgs)
        {
            _currentPage = this.PageNumber;
            PageHeader ph = sender as PageHeader;

            if (this.PageNumber == 1 || !this.showInvoicePageHeader)
            {
                this.srPageHeader.Visible = false;

                // On any page other that the first page, make give the page header more height.
                ph.Height -= 1.850f;
                foreach (ARControl control in ph.Controls)
                    control.Top -= 1.850f;
            }
            else
                this.srPageHeader.Visible = true;
        }

		#region ActiveReports Designer generated code
		private DataDynamics.ActiveReports.ReportHeader ReportHeader = null;
		private DataDynamics.ActiveReports.Label Label18 = null;
		private DataDynamics.ActiveReports.Label Label19 = null;
		private DataDynamics.ActiveReports.Label Label20 = null;
		private DataDynamics.ActiveReports.TextBox txtInvoiceNo = null;
		private DataDynamics.ActiveReports.TextBox txtInvoiceDate = null;
		private DataDynamics.ActiveReports.TextBox txtAccountNo = null;
		private DataDynamics.ActiveReports.Label lblInvoiceNotice = null;
		private DataDynamics.ActiveReports.TextBox txtCustomerName = null;
		private DataDynamics.ActiveReports.TextBox txtAddress = null;
		private DataDynamics.ActiveReports.SubReport srHeader = null;
		private DataDynamics.ActiveReports.Label lblOurVatNo = null;
		private DataDynamics.ActiveReports.TextBox txtOurVatNo = null;
		private DataDynamics.ActiveReports.PageHeader PageHeader = null;
		private DataDynamics.ActiveReports.Label lblExtraAmount = null;
		private DataDynamics.ActiveReports.Label lblExtraType = null;
		private DataDynamics.ActiveReports.Label Label1 = null;
		private DataDynamics.ActiveReports.Label lblLoadNo = null;
		private DataDynamics.ActiveReports.Label Label = null;
		private DataDynamics.ActiveReports.Label lblDate = null;
		private DataDynamics.ActiveReports.SubReport srPageHeader = null;
		private DataDynamics.ActiveReports.Detail Detail = null;
		private DataDynamics.ActiveReports.TextBox txtDescription = null;
		private DataDynamics.ActiveReports.TextBox txtLoadNo = null;
		private DataDynamics.ActiveReports.TextBox txtExtraAmount = null;
		private DataDynamics.ActiveReports.TextBox txtExtraType = null;
		private DataDynamics.ActiveReports.TextBox txtClientContact = null;
		private DataDynamics.ActiveReports.TextBox txtDate = null;
		private DataDynamics.ActiveReports.PageFooter PageFooter = null;
		private DataDynamics.ActiveReports.SubReport srFooter = null;
		private DataDynamics.ActiveReports.ReportFooter ReportFooter = null;
		private DataDynamics.ActiveReports.Label lblNote = null;
		private DataDynamics.ActiveReports.Label lblTotalAmount = null;
		private DataDynamics.ActiveReports.Label lblVAT = null;
		private DataDynamics.ActiveReports.Label lblInvoiceTotal = null;
		private DataDynamics.ActiveReports.TextBox txtTotalAmount = null;
		private DataDynamics.ActiveReports.TextBox txtVATAmount = null;
		private DataDynamics.ActiveReports.TextBox txtVATPercentage = null;
		private DataDynamics.ActiveReports.TextBox txtInvoiceTotal = null;
		private DataDynamics.ActiveReports.Line Line10 = null;
		private DataDynamics.ActiveReports.Line Line11 = null;
		private DataDynamics.ActiveReports.TextBox txtOverrideTotalAmount = null;
		private DataDynamics.ActiveReports.TextBox txtOverrideVATAmount = null;
		private DataDynamics.ActiveReports.TextBox txtOverrideInvoiceTotal = null;
		private DataDynamics.ActiveReports.TextBox txtInvoiceDetails = null;
		private DataDynamics.ActiveReports.Line Line12 = null;
		private DataDynamics.ActiveReports.Label lblTradingAs = null;
        private DataDynamics.ActiveReports.SubReport sbClientCustomerReportFooter = null;
		public void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptInvoiceExtra));
            this.Detail = new DataDynamics.ActiveReports.Detail();
            this.txtDescription = new DataDynamics.ActiveReports.TextBox();
            this.txtLoadNo = new DataDynamics.ActiveReports.TextBox();
            this.txtExtraAmount = new DataDynamics.ActiveReports.TextBox();
            this.txtExtraType = new DataDynamics.ActiveReports.TextBox();
            this.txtClientContact = new DataDynamics.ActiveReports.TextBox();
            this.txtDate = new DataDynamics.ActiveReports.TextBox();
            this.ReportHeader = new DataDynamics.ActiveReports.ReportHeader();
            this.Label18 = new DataDynamics.ActiveReports.Label();
            this.Label19 = new DataDynamics.ActiveReports.Label();
            this.Label20 = new DataDynamics.ActiveReports.Label();
            this.txtInvoiceNo = new DataDynamics.ActiveReports.TextBox();
            this.txtInvoiceDate = new DataDynamics.ActiveReports.TextBox();
            this.txtAccountNo = new DataDynamics.ActiveReports.TextBox();
            this.lblInvoiceNotice = new DataDynamics.ActiveReports.Label();
            this.txtCustomerName = new DataDynamics.ActiveReports.TextBox();
            this.txtAddress = new DataDynamics.ActiveReports.TextBox();
            this.srHeader = new DataDynamics.ActiveReports.SubReport();
            this.lblOurVatNo = new DataDynamics.ActiveReports.Label();
            this.txtOurVatNo = new DataDynamics.ActiveReports.TextBox();
            this.txtYourVatNo = new DataDynamics.ActiveReports.TextBox();
            this.lblYourVatNo = new DataDynamics.ActiveReports.Label();
            this.ReportFooter = new DataDynamics.ActiveReports.ReportFooter();
            this.lblNote = new DataDynamics.ActiveReports.Label();
            this.lblTotalAmount = new DataDynamics.ActiveReports.Label();
            this.lblVAT = new DataDynamics.ActiveReports.Label();
            this.lblInvoiceTotal = new DataDynamics.ActiveReports.Label();
            this.txtTotalAmount = new DataDynamics.ActiveReports.TextBox();
            this.txtVATAmount = new DataDynamics.ActiveReports.TextBox();
            this.txtVATPercentage = new DataDynamics.ActiveReports.TextBox();
            this.txtInvoiceTotal = new DataDynamics.ActiveReports.TextBox();
            this.Line10 = new DataDynamics.ActiveReports.Line();
            this.Line11 = new DataDynamics.ActiveReports.Line();
            this.txtOverrideTotalAmount = new DataDynamics.ActiveReports.TextBox();
            this.txtOverrideVATAmount = new DataDynamics.ActiveReports.TextBox();
            this.txtOverrideInvoiceTotal = new DataDynamics.ActiveReports.TextBox();
            this.txtInvoiceDetails = new DataDynamics.ActiveReports.TextBox();
            this.Line12 = new DataDynamics.ActiveReports.Line();
            this.lblTradingAs = new DataDynamics.ActiveReports.Label();
            this.sbClientCustomerReportFooter = new DataDynamics.ActiveReports.SubReport();
            this.txtCustomNotes = new DataDynamics.ActiveReports.RichTextBox();
            this.PageHeader = new DataDynamics.ActiveReports.PageHeader();
            this.lblExtraAmount = new DataDynamics.ActiveReports.Label();
            this.lblExtraType = new DataDynamics.ActiveReports.Label();
            this.Label1 = new DataDynamics.ActiveReports.Label();
            this.lblLoadNo = new DataDynamics.ActiveReports.Label();
            this.Label = new DataDynamics.ActiveReports.Label();
            this.lblDate = new DataDynamics.ActiveReports.Label();
            this.srPageHeader = new DataDynamics.ActiveReports.SubReport();
            this.PageFooter = new DataDynamics.ActiveReports.PageFooter();
            this.srFooter = new DataDynamics.ActiveReports.SubReport();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLoadNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExtraAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExtraType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtClientContact)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvoiceNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvoiceDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInvoiceNotice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustomerName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblOurVatNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOurVatNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYourVatNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblYourVatNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNote)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotalAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVAT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInvoiceTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVATAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVATPercentage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvoiceTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOverrideTotalAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOverrideVATAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOverrideInvoiceTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvoiceDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTradingAs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblExtraAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblExtraType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLoadNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.txtDescription,
            this.txtLoadNo,
            this.txtExtraAmount,
            this.txtExtraType,
            this.txtClientContact,
            this.txtDate});
            this.Detail.Height = 0.1875F;
            this.Detail.Name = "Detail";
            this.Detail.BeforePrint += new System.EventHandler(this.Detail_BeforePrint);
            // 
            // txtDescription
            // 
            this.txtDescription.Border.BottomColor = System.Drawing.Color.Black;
            this.txtDescription.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtDescription.Border.LeftColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.txtDescription.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtDescription.Border.RightColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.txtDescription.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtDescription.Border.TopColor = System.Drawing.Color.Black;
            this.txtDescription.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtDescription.DataField = "CustomDescription";
            this.txtDescription.Height = 0.2F;
            this.txtDescription.Left = 2.375F;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Style = "";
            this.txtDescription.Text = null;
            this.txtDescription.Top = 0F;
            this.txtDescription.Width = 2.375F;
            // 
            // txtLoadNo
            // 
            this.txtLoadNo.Border.BottomColor = System.Drawing.Color.Black;
            this.txtLoadNo.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtLoadNo.Border.LeftColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.txtLoadNo.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtLoadNo.Border.RightColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.txtLoadNo.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtLoadNo.Border.TopColor = System.Drawing.Color.Black;
            this.txtLoadNo.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtLoadNo.DataField = "LoadNo";
            this.txtLoadNo.Height = 0.2F;
            this.txtLoadNo.Left = 0.4375F;
            this.txtLoadNo.Name = "txtLoadNo";
            this.txtLoadNo.Style = "";
            this.txtLoadNo.Text = null;
            this.txtLoadNo.Top = 0F;
            this.txtLoadNo.Width = 1F;
            // 
            // txtExtraAmount
            // 
            this.txtExtraAmount.Border.BottomColor = System.Drawing.Color.Black;
            this.txtExtraAmount.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtExtraAmount.Border.LeftColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.txtExtraAmount.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtExtraAmount.Border.RightColor = System.Drawing.Color.Black;
            this.txtExtraAmount.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtExtraAmount.Border.TopColor = System.Drawing.Color.Black;
            this.txtExtraAmount.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtExtraAmount.DataField = "ForeignAmount";
            this.txtExtraAmount.Height = 0.2F;
            this.txtExtraAmount.Left = 6.5F;
            this.txtExtraAmount.Name = "txtExtraAmount";
            this.txtExtraAmount.OutputFormat = resources.GetString("txtExtraAmount.OutputFormat");
            this.txtExtraAmount.Style = "text-align: right; ";
            this.txtExtraAmount.Text = null;
            this.txtExtraAmount.Top = 0F;
            this.txtExtraAmount.Width = 1F;
            // 
            // txtExtraType
            // 
            this.txtExtraType.Border.BottomColor = System.Drawing.Color.Black;
            this.txtExtraType.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtExtraType.Border.LeftColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.txtExtraType.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtExtraType.Border.RightColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.txtExtraType.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtExtraType.Border.TopColor = System.Drawing.Color.Black;
            this.txtExtraType.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtExtraType.DataField = "ExtraType";
            this.txtExtraType.Height = 0.2F;
            this.txtExtraType.Left = 1.4375F;
            this.txtExtraType.Name = "txtExtraType";
            this.txtExtraType.Style = "";
            this.txtExtraType.Text = null;
            this.txtExtraType.Top = 0F;
            this.txtExtraType.Width = 0.9375F;
            // 
            // txtClientContact
            // 
            this.txtClientContact.Border.BottomColor = System.Drawing.Color.Black;
            this.txtClientContact.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtClientContact.Border.LeftColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.txtClientContact.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtClientContact.Border.RightColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.txtClientContact.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtClientContact.Border.TopColor = System.Drawing.Color.Black;
            this.txtClientContact.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtClientContact.DataField = "ClientContact";
            this.txtClientContact.Height = 0.2F;
            this.txtClientContact.Left = 4.75F;
            this.txtClientContact.Name = "txtClientContact";
            this.txtClientContact.Style = "";
            this.txtClientContact.Text = null;
            this.txtClientContact.Top = 0F;
            this.txtClientContact.Width = 1.75F;
            // 
            // txtDate
            // 
            this.txtDate.Border.BottomColor = System.Drawing.Color.Black;
            this.txtDate.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtDate.Border.LeftColor = System.Drawing.Color.Black;
            this.txtDate.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtDate.Border.RightColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.txtDate.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.txtDate.Border.TopColor = System.Drawing.Color.Black;
            this.txtDate.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtDate.DataField = "JobDate";
            this.txtDate.Height = 0.2F;
            this.txtDate.Left = 0F;
            this.txtDate.Name = "txtDate";
            this.txtDate.OutputFormat = resources.GetString("txtDate.OutputFormat");
            this.txtDate.Style = "";
            this.txtDate.Text = null;
            this.txtDate.Top = 0F;
            this.txtDate.Width = 0.4375F;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.Label18,
            this.Label19,
            this.Label20,
            this.txtInvoiceNo,
            this.txtInvoiceDate,
            this.txtAccountNo,
            this.lblInvoiceNotice,
            this.txtCustomerName,
            this.txtAddress,
            this.srHeader,
            this.lblOurVatNo,
            this.txtOurVatNo,
            this.txtYourVatNo,
            this.lblYourVatNo});
            this.ReportHeader.Height = 3.426389F;
            this.ReportHeader.Name = "ReportHeader";
            this.ReportHeader.Format += new System.EventHandler(this.ReportHeader_Format);
            // 
            // Label18
            // 
            this.Label18.Border.BottomColor = System.Drawing.Color.Black;
            this.Label18.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label18.Border.LeftColor = System.Drawing.Color.Black;
            this.Label18.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label18.Border.RightColor = System.Drawing.Color.Black;
            this.Label18.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label18.Border.TopColor = System.Drawing.Color.Black;
            this.Label18.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label18.Height = 0.1875F;
            this.Label18.HyperLink = null;
            this.Label18.Left = 5.125F;
            this.Label18.Name = "Label18";
            this.Label18.Style = "ddo-char-set: 1; font-weight: bold; ";
            this.Label18.Text = "Invoice No:";
            this.Label18.Top = 1.9375F;
            this.Label18.Width = 0.9375F;
            // 
            // Label19
            // 
            this.Label19.Border.BottomColor = System.Drawing.Color.Black;
            this.Label19.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label19.Border.LeftColor = System.Drawing.Color.Black;
            this.Label19.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label19.Border.RightColor = System.Drawing.Color.Black;
            this.Label19.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label19.Border.TopColor = System.Drawing.Color.Black;
            this.Label19.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label19.Height = 0.2F;
            this.Label19.HyperLink = null;
            this.Label19.Left = 5.125F;
            this.Label19.Name = "Label19";
            this.Label19.Style = "ddo-char-set: 1; font-weight: bold; ";
            this.Label19.Text = "Invoice Date:";
            this.Label19.Top = 2.125F;
            this.Label19.Width = 0.9375F;
            // 
            // Label20
            // 
            this.Label20.Border.BottomColor = System.Drawing.Color.Black;
            this.Label20.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label20.Border.LeftColor = System.Drawing.Color.Black;
            this.Label20.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label20.Border.RightColor = System.Drawing.Color.Black;
            this.Label20.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label20.Border.TopColor = System.Drawing.Color.Black;
            this.Label20.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Label20.Height = 0.1875F;
            this.Label20.HyperLink = null;
            this.Label20.Left = 5.125F;
            this.Label20.Name = "Label20";
            this.Label20.Style = "ddo-char-set: 1; font-weight: bold; ";
            this.Label20.Text = "Account No:";
            this.Label20.Top = 2.3125F;
            this.Label20.Width = 0.9375F;
            // 
            // txtInvoiceNo
            // 
            this.txtInvoiceNo.Border.BottomColor = System.Drawing.Color.Black;
            this.txtInvoiceNo.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtInvoiceNo.Border.LeftColor = System.Drawing.Color.Black;
            this.txtInvoiceNo.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtInvoiceNo.Border.RightColor = System.Drawing.Color.Black;
            this.txtInvoiceNo.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtInvoiceNo.Border.TopColor = System.Drawing.Color.Black;
            this.txtInvoiceNo.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtInvoiceNo.Height = 0.1875F;
            this.txtInvoiceNo.Left = 6.0625F;
            this.txtInvoiceNo.Name = "txtInvoiceNo";
            this.txtInvoiceNo.Style = "color: Red; ";
            this.txtInvoiceNo.Text = "To Be Issued";
            this.txtInvoiceNo.Top = 1.9375F;
            this.txtInvoiceNo.Width = 1.375F;
            // 
            // txtInvoiceDate
            // 
            this.txtInvoiceDate.Border.BottomColor = System.Drawing.Color.Black;
            this.txtInvoiceDate.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtInvoiceDate.Border.LeftColor = System.Drawing.Color.Black;
            this.txtInvoiceDate.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtInvoiceDate.Border.RightColor = System.Drawing.Color.Black;
            this.txtInvoiceDate.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtInvoiceDate.Border.TopColor = System.Drawing.Color.Black;
            this.txtInvoiceDate.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtInvoiceDate.Height = 0.1875F;
            this.txtInvoiceDate.Left = 6.0625F;
            this.txtInvoiceDate.Name = "txtInvoiceDate";
            this.txtInvoiceDate.Style = "";
            this.txtInvoiceDate.Text = null;
            this.txtInvoiceDate.Top = 2.125F;
            this.txtInvoiceDate.Width = 1.375F;
            // 
            // txtAccountNo
            // 
            this.txtAccountNo.Border.BottomColor = System.Drawing.Color.Black;
            this.txtAccountNo.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtAccountNo.Border.LeftColor = System.Drawing.Color.Black;
            this.txtAccountNo.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtAccountNo.Border.RightColor = System.Drawing.Color.Black;
            this.txtAccountNo.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtAccountNo.Border.TopColor = System.Drawing.Color.Black;
            this.txtAccountNo.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtAccountNo.Height = 0.1875F;
            this.txtAccountNo.Left = 6.0625F;
            this.txtAccountNo.Name = "txtAccountNo";
            this.txtAccountNo.Style = "vertical-align: top; ";
            this.txtAccountNo.Text = null;
            this.txtAccountNo.Top = 2.3125F;
            this.txtAccountNo.Width = 1.375F;
            // 
            // lblInvoiceNotice
            // 
            this.lblInvoiceNotice.Border.BottomColor = System.Drawing.Color.Black;
            this.lblInvoiceNotice.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblInvoiceNotice.Border.LeftColor = System.Drawing.Color.Black;
            this.lblInvoiceNotice.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblInvoiceNotice.Border.RightColor = System.Drawing.Color.Black;
            this.lblInvoiceNotice.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblInvoiceNotice.Border.TopColor = System.Drawing.Color.Black;
            this.lblInvoiceNotice.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblInvoiceNotice.Height = 0.375F;
            this.lblInvoiceNotice.HyperLink = null;
            this.lblInvoiceNotice.Left = 2.5F;
            this.lblInvoiceNotice.Name = "lblInvoiceNotice";
            this.lblInvoiceNotice.Style = "color: Red; text-align: center; ";
            this.lblInvoiceNotice.Text = "This invoice has not yet been saved, add invoice to allocate Invoice No.";
            this.lblInvoiceNotice.Top = 3F;
            this.lblInvoiceNotice.Visible = false;
            this.lblInvoiceNotice.Width = 2.859375F;
            // 
            // txtCustomerName
            // 
            this.txtCustomerName.Border.BottomColor = System.Drawing.Color.Black;
            this.txtCustomerName.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtCustomerName.Border.LeftColor = System.Drawing.Color.Black;
            this.txtCustomerName.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtCustomerName.Border.RightColor = System.Drawing.Color.Black;
            this.txtCustomerName.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtCustomerName.Border.TopColor = System.Drawing.Color.Black;
            this.txtCustomerName.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtCustomerName.Height = 0.2F;
            this.txtCustomerName.Left = 0.25F;
            this.txtCustomerName.Name = "txtCustomerName";
            this.txtCustomerName.Style = "ddo-char-set: 1; font-weight: bold; ";
            this.txtCustomerName.Text = null;
            this.txtCustomerName.Top = 1.9375F;
            this.txtCustomerName.Width = 3.53F;
            // 
            // txtAddress
            // 
            this.txtAddress.Border.BottomColor = System.Drawing.Color.Black;
            this.txtAddress.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtAddress.Border.LeftColor = System.Drawing.Color.Black;
            this.txtAddress.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtAddress.Border.RightColor = System.Drawing.Color.Black;
            this.txtAddress.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtAddress.Border.TopColor = System.Drawing.Color.Black;
            this.txtAddress.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtAddress.Height = 1.0625F;
            this.txtAddress.Left = 0.25F;
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Style = "ddo-char-set: 1; font-weight: bold; ";
            this.txtAddress.Text = null;
            this.txtAddress.Top = 2.125F;
            this.txtAddress.Width = 2.1875F;
            // 
            // srHeader
            // 
            this.srHeader.Border.BottomColor = System.Drawing.Color.Black;
            this.srHeader.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.srHeader.Border.LeftColor = System.Drawing.Color.Black;
            this.srHeader.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.srHeader.Border.RightColor = System.Drawing.Color.Black;
            this.srHeader.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.srHeader.Border.TopColor = System.Drawing.Color.Black;
            this.srHeader.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.srHeader.CloseBorder = false;
            this.srHeader.Height = 1.875F;
            this.srHeader.Left = 0F;
            this.srHeader.Name = "srHeader";
            this.srHeader.Report = null;
            this.srHeader.ReportName = "rptHeader";
            this.srHeader.Top = 0F;
            this.srHeader.Width = 7.552083F;
            // 
            // lblOurVatNo
            // 
            this.lblOurVatNo.Border.BottomColor = System.Drawing.Color.Black;
            this.lblOurVatNo.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblOurVatNo.Border.LeftColor = System.Drawing.Color.Black;
            this.lblOurVatNo.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblOurVatNo.Border.RightColor = System.Drawing.Color.Black;
            this.lblOurVatNo.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblOurVatNo.Border.TopColor = System.Drawing.Color.Black;
            this.lblOurVatNo.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblOurVatNo.Height = 0.1875F;
            this.lblOurVatNo.HyperLink = null;
            this.lblOurVatNo.Left = 5.125F;
            this.lblOurVatNo.Name = "lblOurVatNo";
            this.lblOurVatNo.Style = "ddo-char-set: 1; font-weight: bold; ";
            this.lblOurVatNo.Text = "Our VAT No:";
            this.lblOurVatNo.Top = 2.5F;
            this.lblOurVatNo.Width = 0.9375F;
            // 
            // txtOurVatNo
            // 
            this.txtOurVatNo.Border.BottomColor = System.Drawing.Color.Black;
            this.txtOurVatNo.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtOurVatNo.Border.LeftColor = System.Drawing.Color.Black;
            this.txtOurVatNo.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtOurVatNo.Border.RightColor = System.Drawing.Color.Black;
            this.txtOurVatNo.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtOurVatNo.Border.TopColor = System.Drawing.Color.Black;
            this.txtOurVatNo.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtOurVatNo.Height = 0.1875F;
            this.txtOurVatNo.Left = 6.0625F;
            this.txtOurVatNo.Name = "txtOurVatNo";
            this.txtOurVatNo.Style = "vertical-align: top; ";
            this.txtOurVatNo.Text = null;
            this.txtOurVatNo.Top = 2.5F;
            this.txtOurVatNo.Width = 1.375F;
            // 
            // txtYourVatNo
            // 
            this.txtYourVatNo.Border.BottomColor = System.Drawing.Color.Black;
            this.txtYourVatNo.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtYourVatNo.Border.LeftColor = System.Drawing.Color.Black;
            this.txtYourVatNo.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtYourVatNo.Border.RightColor = System.Drawing.Color.Black;
            this.txtYourVatNo.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtYourVatNo.Border.TopColor = System.Drawing.Color.Black;
            this.txtYourVatNo.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtYourVatNo.Height = 0.1875F;
            this.txtYourVatNo.Left = 6.0625F;
            this.txtYourVatNo.Name = "txtYourVatNo";
            this.txtYourVatNo.Style = "vertical-align: top; ";
            this.txtYourVatNo.Text = null;
            this.txtYourVatNo.Top = 2.6875F;
            this.txtYourVatNo.Width = 1.375F;
            // 
            // lblYourVatNo
            // 
            this.lblYourVatNo.Border.BottomColor = System.Drawing.Color.Black;
            this.lblYourVatNo.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblYourVatNo.Border.LeftColor = System.Drawing.Color.Black;
            this.lblYourVatNo.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblYourVatNo.Border.RightColor = System.Drawing.Color.Black;
            this.lblYourVatNo.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblYourVatNo.Border.TopColor = System.Drawing.Color.Black;
            this.lblYourVatNo.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblYourVatNo.Height = 0.1875F;
            this.lblYourVatNo.HyperLink = null;
            this.lblYourVatNo.Left = 5.125F;
            this.lblYourVatNo.Name = "lblYourVatNo";
            this.lblYourVatNo.Style = "ddo-char-set: 1; font-weight: bold; ";
            this.lblYourVatNo.Text = "Your VAT No:";
            this.lblYourVatNo.Top = 2.6875F;
            this.lblYourVatNo.Width = 0.9375F;
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.lblNote,
            this.lblTotalAmount,
            this.lblVAT,
            this.lblInvoiceTotal,
            this.txtTotalAmount,
            this.txtVATAmount,
            this.txtVATPercentage,
            this.txtInvoiceTotal,
            this.Line10,
            this.Line11,
            this.txtOverrideTotalAmount,
            this.txtOverrideVATAmount,
            this.txtOverrideInvoiceTotal,
            this.txtInvoiceDetails,
            this.Line12,
            this.lblTradingAs,
            this.sbClientCustomerReportFooter,
            this.txtCustomNotes});
            this.ReportFooter.Height = 1.625F;
            this.ReportFooter.Name = "ReportFooter";
            this.ReportFooter.PrintAtBottom = true;
            this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
            this.ReportFooter.BeforePrint += new System.EventHandler(this.ReportFooter_BeforePrint);
            // 
            // lblNote
            // 
            this.lblNote.Border.BottomColor = System.Drawing.Color.Black;
            this.lblNote.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblNote.Border.LeftColor = System.Drawing.Color.Black;
            this.lblNote.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblNote.Border.RightColor = System.Drawing.Color.Black;
            this.lblNote.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblNote.Border.TopColor = System.Drawing.Color.Black;
            this.lblNote.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblNote.Height = 0.2F;
            this.lblNote.HyperLink = null;
            this.lblNote.Left = 3.0625F;
            this.lblNote.Name = "lblNote";
            this.lblNote.Style = "color: DimGray; ddo-char-set: 0; text-align: center; font-weight: bold; font-styl" +
                "e: italic; font-size: 8.25pt; ";
            this.lblNote.Text = "Strictly 30 Days Nett.";
            this.lblNote.Top = 1F;
            this.lblNote.Width = 1.375F;
            // 
            // lblTotalAmount
            // 
            this.lblTotalAmount.Border.BottomColor = System.Drawing.Color.Black;
            this.lblTotalAmount.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblTotalAmount.Border.LeftColor = System.Drawing.Color.Black;
            this.lblTotalAmount.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblTotalAmount.Border.RightColor = System.Drawing.Color.Black;
            this.lblTotalAmount.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblTotalAmount.Border.TopColor = System.Drawing.Color.Black;
            this.lblTotalAmount.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblTotalAmount.Height = 0.2F;
            this.lblTotalAmount.HyperLink = null;
            this.lblTotalAmount.Left = 4.256945F;
            this.lblTotalAmount.Name = "lblTotalAmount";
            this.lblTotalAmount.Style = "ddo-char-set: 1; font-weight: bold; ";
            this.lblTotalAmount.Text = "TOTAL AMOUNT";
            this.lblTotalAmount.Top = 0.25F;
            this.lblTotalAmount.Width = 1.5625F;
            // 
            // lblVAT
            // 
            this.lblVAT.Border.BottomColor = System.Drawing.Color.Black;
            this.lblVAT.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblVAT.Border.LeftColor = System.Drawing.Color.Black;
            this.lblVAT.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblVAT.Border.RightColor = System.Drawing.Color.Black;
            this.lblVAT.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblVAT.Border.TopColor = System.Drawing.Color.Black;
            this.lblVAT.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblVAT.Height = 0.2F;
            this.lblVAT.HyperLink = null;
            this.lblVAT.Left = 4.256945F;
            this.lblVAT.Name = "lblVAT";
            this.lblVAT.Style = "ddo-char-set: 1; font-weight: bold; ";
            this.lblVAT.Text = "VAT @";
            this.lblVAT.Top = 0.4375F;
            this.lblVAT.Width = 0.5625F;
            // 
            // lblInvoiceTotal
            // 
            this.lblInvoiceTotal.Border.BottomColor = System.Drawing.Color.Black;
            this.lblInvoiceTotal.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblInvoiceTotal.Border.LeftColor = System.Drawing.Color.Black;
            this.lblInvoiceTotal.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblInvoiceTotal.Border.RightColor = System.Drawing.Color.Black;
            this.lblInvoiceTotal.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblInvoiceTotal.Border.TopColor = System.Drawing.Color.Black;
            this.lblInvoiceTotal.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblInvoiceTotal.Height = 0.2F;
            this.lblInvoiceTotal.HyperLink = null;
            this.lblInvoiceTotal.Left = 4.256945F;
            this.lblInvoiceTotal.Name = "lblInvoiceTotal";
            this.lblInvoiceTotal.Style = "ddo-char-set: 1; font-weight: bold; ";
            this.lblInvoiceTotal.Text = "INVOICE TOTAL";
            this.lblInvoiceTotal.Top = 0.75F;
            this.lblInvoiceTotal.Width = 1.25F;
            // 
            // txtTotalAmount
            // 
            this.txtTotalAmount.Border.BottomColor = System.Drawing.Color.Black;
            this.txtTotalAmount.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtTotalAmount.Border.LeftColor = System.Drawing.Color.Black;
            this.txtTotalAmount.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtTotalAmount.Border.RightColor = System.Drawing.Color.Black;
            this.txtTotalAmount.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtTotalAmount.Border.TopColor = System.Drawing.Color.Black;
            this.txtTotalAmount.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtTotalAmount.Height = 0.2F;
            this.txtTotalAmount.Left = 6.444445F;
            this.txtTotalAmount.Name = "txtTotalAmount";
            this.txtTotalAmount.OutputFormat = resources.GetString("txtTotalAmount.OutputFormat");
            this.txtTotalAmount.Style = "text-align: right; ";
            this.txtTotalAmount.Text = null;
            this.txtTotalAmount.Top = 0.25F;
            this.txtTotalAmount.Width = 1F;
            // 
            // txtVATAmount
            // 
            this.txtVATAmount.Border.BottomColor = System.Drawing.Color.Black;
            this.txtVATAmount.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtVATAmount.Border.LeftColor = System.Drawing.Color.Black;
            this.txtVATAmount.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtVATAmount.Border.RightColor = System.Drawing.Color.Black;
            this.txtVATAmount.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtVATAmount.Border.TopColor = System.Drawing.Color.Black;
            this.txtVATAmount.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtVATAmount.Height = 0.2F;
            this.txtVATAmount.Left = 6.444445F;
            this.txtVATAmount.Name = "txtVATAmount";
            this.txtVATAmount.OutputFormat = resources.GetString("txtVATAmount.OutputFormat");
            this.txtVATAmount.Style = "text-align: right; ";
            this.txtVATAmount.Text = null;
            this.txtVATAmount.Top = 0.4375F;
            this.txtVATAmount.Width = 1F;
            // 
            // txtVATPercentage
            // 
            this.txtVATPercentage.Border.BottomColor = System.Drawing.Color.Black;
            this.txtVATPercentage.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtVATPercentage.Border.LeftColor = System.Drawing.Color.Black;
            this.txtVATPercentage.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtVATPercentage.Border.RightColor = System.Drawing.Color.Black;
            this.txtVATPercentage.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtVATPercentage.Border.TopColor = System.Drawing.Color.Black;
            this.txtVATPercentage.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtVATPercentage.Height = 0.2F;
            this.txtVATPercentage.Left = 5.25F;
            this.txtVATPercentage.Name = "txtVATPercentage";
            this.txtVATPercentage.OutputFormat = resources.GetString("txtVATPercentage.OutputFormat");
            this.txtVATPercentage.Style = "ddo-char-set: 1; font-weight: bold; ";
            this.txtVATPercentage.Text = "17.5%";
            this.txtVATPercentage.Top = 0.4375F;
            this.txtVATPercentage.Width = 0.5625F;
            // 
            // txtInvoiceTotal
            // 
            this.txtInvoiceTotal.Border.BottomColor = System.Drawing.Color.Black;
            this.txtInvoiceTotal.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtInvoiceTotal.Border.LeftColor = System.Drawing.Color.Black;
            this.txtInvoiceTotal.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtInvoiceTotal.Border.RightColor = System.Drawing.Color.Black;
            this.txtInvoiceTotal.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtInvoiceTotal.Border.TopColor = System.Drawing.Color.Black;
            this.txtInvoiceTotal.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtInvoiceTotal.Height = 0.2F;
            this.txtInvoiceTotal.Left = 6.4375F;
            this.txtInvoiceTotal.Name = "txtInvoiceTotal";
            this.txtInvoiceTotal.OutputFormat = resources.GetString("txtInvoiceTotal.OutputFormat");
            this.txtInvoiceTotal.Style = "text-align: right; ";
            this.txtInvoiceTotal.Text = null;
            this.txtInvoiceTotal.Top = 0.75F;
            this.txtInvoiceTotal.Width = 1F;
            // 
            // Line10
            // 
            this.Line10.Border.BottomColor = System.Drawing.Color.Black;
            this.Line10.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Line10.Border.LeftColor = System.Drawing.Color.Black;
            this.Line10.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Line10.Border.RightColor = System.Drawing.Color.Black;
            this.Line10.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Line10.Border.TopColor = System.Drawing.Color.Black;
            this.Line10.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Line10.Height = 0F;
            this.Line10.Left = 4.256945F;
            this.Line10.LineWeight = 1F;
            this.Line10.Name = "Line10";
            this.Line10.Top = 0.75F;
            this.Line10.Width = 3.243055F;
            this.Line10.X1 = 4.256945F;
            this.Line10.X2 = 7.5F;
            this.Line10.Y1 = 0.75F;
            this.Line10.Y2 = 0.75F;
            // 
            // Line11
            // 
            this.Line11.Border.BottomColor = System.Drawing.Color.Black;
            this.Line11.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Line11.Border.LeftColor = System.Drawing.Color.Black;
            this.Line11.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Line11.Border.RightColor = System.Drawing.Color.Black;
            this.Line11.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Line11.Border.TopColor = System.Drawing.Color.Black;
            this.Line11.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Line11.Height = 0F;
            this.Line11.Left = 4.25F;
            this.Line11.LineWeight = 1F;
            this.Line11.Name = "Line11";
            this.Line11.Top = 0.9375F;
            this.Line11.Width = 3.25F;
            this.Line11.X1 = 4.25F;
            this.Line11.X2 = 7.5F;
            this.Line11.Y1 = 0.9375F;
            this.Line11.Y2 = 0.9375F;
            // 
            // txtOverrideTotalAmount
            // 
            this.txtOverrideTotalAmount.Border.BottomColor = System.Drawing.Color.Black;
            this.txtOverrideTotalAmount.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtOverrideTotalAmount.Border.LeftColor = System.Drawing.Color.Black;
            this.txtOverrideTotalAmount.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtOverrideTotalAmount.Border.RightColor = System.Drawing.Color.Black;
            this.txtOverrideTotalAmount.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtOverrideTotalAmount.Border.TopColor = System.Drawing.Color.Black;
            this.txtOverrideTotalAmount.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtOverrideTotalAmount.Height = 0.2F;
            this.txtOverrideTotalAmount.Left = 6.4375F;
            this.txtOverrideTotalAmount.Name = "txtOverrideTotalAmount";
            this.txtOverrideTotalAmount.OutputFormat = resources.GetString("txtOverrideTotalAmount.OutputFormat");
            this.txtOverrideTotalAmount.Style = "text-align: right; ";
            this.txtOverrideTotalAmount.Text = null;
            this.txtOverrideTotalAmount.Top = 0.25F;
            this.txtOverrideTotalAmount.Width = 1F;
            // 
            // txtOverrideVATAmount
            // 
            this.txtOverrideVATAmount.Border.BottomColor = System.Drawing.Color.Black;
            this.txtOverrideVATAmount.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtOverrideVATAmount.Border.LeftColor = System.Drawing.Color.Black;
            this.txtOverrideVATAmount.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtOverrideVATAmount.Border.RightColor = System.Drawing.Color.Black;
            this.txtOverrideVATAmount.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtOverrideVATAmount.Border.TopColor = System.Drawing.Color.Black;
            this.txtOverrideVATAmount.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtOverrideVATAmount.Height = 0.2000001F;
            this.txtOverrideVATAmount.Left = 6.444445F;
            this.txtOverrideVATAmount.Name = "txtOverrideVATAmount";
            this.txtOverrideVATAmount.OutputFormat = resources.GetString("txtOverrideVATAmount.OutputFormat");
            this.txtOverrideVATAmount.Style = "text-align: right; ";
            this.txtOverrideVATAmount.Text = null;
            this.txtOverrideVATAmount.Top = 0.4375F;
            this.txtOverrideVATAmount.Width = 1F;
            // 
            // txtOverrideInvoiceTotal
            // 
            this.txtOverrideInvoiceTotal.Border.BottomColor = System.Drawing.Color.Black;
            this.txtOverrideInvoiceTotal.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtOverrideInvoiceTotal.Border.LeftColor = System.Drawing.Color.Black;
            this.txtOverrideInvoiceTotal.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtOverrideInvoiceTotal.Border.RightColor = System.Drawing.Color.Black;
            this.txtOverrideInvoiceTotal.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtOverrideInvoiceTotal.Border.TopColor = System.Drawing.Color.Black;
            this.txtOverrideInvoiceTotal.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtOverrideInvoiceTotal.Height = 0.2F;
            this.txtOverrideInvoiceTotal.Left = 6.4375F;
            this.txtOverrideInvoiceTotal.Name = "txtOverrideInvoiceTotal";
            this.txtOverrideInvoiceTotal.OutputFormat = resources.GetString("txtOverrideInvoiceTotal.OutputFormat");
            this.txtOverrideInvoiceTotal.Style = "text-align: right; ";
            this.txtOverrideInvoiceTotal.Text = null;
            this.txtOverrideInvoiceTotal.Top = 0.75F;
            this.txtOverrideInvoiceTotal.Width = 1F;
            // 
            // txtInvoiceDetails
            // 
            this.txtInvoiceDetails.Border.BottomColor = System.Drawing.Color.Black;
            this.txtInvoiceDetails.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtInvoiceDetails.Border.LeftColor = System.Drawing.Color.Black;
            this.txtInvoiceDetails.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtInvoiceDetails.Border.RightColor = System.Drawing.Color.Black;
            this.txtInvoiceDetails.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtInvoiceDetails.Border.TopColor = System.Drawing.Color.Black;
            this.txtInvoiceDetails.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtInvoiceDetails.Height = 0.125F;
            this.txtInvoiceDetails.Left = 1.5625F;
            this.txtInvoiceDetails.Name = "txtInvoiceDetails";
            this.txtInvoiceDetails.Style = "text-align: center; ";
            this.txtInvoiceDetails.Text = null;
            this.txtInvoiceDetails.Top = 0F;
            this.txtInvoiceDetails.Visible = false;
            this.txtInvoiceDetails.Width = 4.1875F;
            // 
            // Line12
            // 
            this.Line12.Border.BottomColor = System.Drawing.Color.Black;
            this.Line12.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Line12.Border.LeftColor = System.Drawing.Color.Black;
            this.Line12.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Line12.Border.RightColor = System.Drawing.Color.Black;
            this.Line12.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Line12.Border.TopColor = System.Drawing.Color.Black;
            this.Line12.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Line12.Height = 0F;
            this.Line12.Left = 0F;
            this.Line12.LineWeight = 1F;
            this.Line12.Name = "Line12";
            this.Line12.Top = 0F;
            this.Line12.Width = 7.5625F;
            this.Line12.X1 = 0F;
            this.Line12.X2 = 7.5625F;
            this.Line12.Y1 = 0F;
            this.Line12.Y2 = 0F;
            // 
            // lblTradingAs
            // 
            this.lblTradingAs.Border.BottomColor = System.Drawing.Color.Black;
            this.lblTradingAs.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblTradingAs.Border.LeftColor = System.Drawing.Color.Black;
            this.lblTradingAs.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblTradingAs.Border.RightColor = System.Drawing.Color.Black;
            this.lblTradingAs.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblTradingAs.Border.TopColor = System.Drawing.Color.Black;
            this.lblTradingAs.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblTradingAs.Height = 0.2F;
            this.lblTradingAs.HyperLink = null;
            this.lblTradingAs.Left = 0.0625F;
            this.lblTradingAs.Name = "lblTradingAs";
            this.lblTradingAs.Style = "color: DimGray; ddo-char-set: 0; text-align: center; font-weight: bold; font-styl" +
                "e: italic; font-size: 8.25pt; ";
            this.lblTradingAs.Text = "Strictly 30 Days Nett.";
            this.lblTradingAs.Top = 1.1875F;
            this.lblTradingAs.Width = 7.375F;
            // 
            // sbClientCustomerReportFooter
            // 
            this.sbClientCustomerReportFooter.Border.BottomColor = System.Drawing.Color.Black;
            this.sbClientCustomerReportFooter.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.sbClientCustomerReportFooter.Border.LeftColor = System.Drawing.Color.Black;
            this.sbClientCustomerReportFooter.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.sbClientCustomerReportFooter.Border.RightColor = System.Drawing.Color.Black;
            this.sbClientCustomerReportFooter.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.sbClientCustomerReportFooter.Border.TopColor = System.Drawing.Color.Black;
            this.sbClientCustomerReportFooter.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.sbClientCustomerReportFooter.CloseBorder = false;
            this.sbClientCustomerReportFooter.Height = 1.3125F;
            this.sbClientCustomerReportFooter.Left = 0F;
            this.sbClientCustomerReportFooter.Name = "sbClientCustomerReportFooter";
            this.sbClientCustomerReportFooter.Report = null;
            this.sbClientCustomerReportFooter.Top = 1.1F;
            this.sbClientCustomerReportFooter.Visible = false;
            this.sbClientCustomerReportFooter.Width = 7.5625F;
            // 
            // txtCustomNotes
            // 
            this.txtCustomNotes.AutoReplaceFields = true;
            this.txtCustomNotes.BackColor = System.Drawing.Color.Transparent;
            this.txtCustomNotes.Border.BottomColor = System.Drawing.Color.Black;
            this.txtCustomNotes.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtCustomNotes.Border.LeftColor = System.Drawing.Color.Black;
            this.txtCustomNotes.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtCustomNotes.Border.RightColor = System.Drawing.Color.Black;
            this.txtCustomNotes.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtCustomNotes.Border.TopColor = System.Drawing.Color.Black;
            this.txtCustomNotes.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtCustomNotes.CanShrink = true;
            this.txtCustomNotes.Font = new System.Drawing.Font("Arial", 10F);
            this.txtCustomNotes.ForeColor = System.Drawing.Color.Black;
            this.txtCustomNotes.Height = 0.1875F;
            this.txtCustomNotes.Left = 0F;
            this.txtCustomNotes.Name = "txtCustomNotes";
            this.txtCustomNotes.RTF = resources.GetString("txtCustomNotes.RTF");
            this.txtCustomNotes.Top = 0.25F;
            this.txtCustomNotes.Width = 4.1875F;
            // 
            // PageHeader
            // 
            this.PageHeader.CanGrow = false;
            this.PageHeader.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.lblExtraAmount,
            this.lblExtraType,
            this.Label1,
            this.lblLoadNo,
            this.Label,
            this.lblDate,
            this.srPageHeader});
            this.PageHeader.Height = 2.082639F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
            // 
            // lblExtraAmount
            // 
            this.lblExtraAmount.Border.BottomColor = System.Drawing.Color.Black;
            this.lblExtraAmount.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblExtraAmount.Border.LeftColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.lblExtraAmount.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblExtraAmount.Border.RightColor = System.Drawing.Color.Black;
            this.lblExtraAmount.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblExtraAmount.Border.TopColor = System.Drawing.Color.Black;
            this.lblExtraAmount.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblExtraAmount.Height = 0.188F;
            this.lblExtraAmount.HyperLink = null;
            this.lblExtraAmount.Left = 6.5F;
            this.lblExtraAmount.Name = "lblExtraAmount";
            this.lblExtraAmount.Style = "text-align: right; font-weight: bold; background-color: White; ";
            this.lblExtraAmount.Text = "Extra Amount";
            this.lblExtraAmount.Top = 1.875F;
            this.lblExtraAmount.Width = 1F;
            // 
            // lblExtraType
            // 
            this.lblExtraType.Border.BottomColor = System.Drawing.Color.Black;
            this.lblExtraType.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblExtraType.Border.LeftColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.lblExtraType.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblExtraType.Border.RightColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.lblExtraType.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblExtraType.Border.TopColor = System.Drawing.Color.Black;
            this.lblExtraType.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblExtraType.Height = 0.1875F;
            this.lblExtraType.HyperLink = null;
            this.lblExtraType.Left = 1.4375F;
            this.lblExtraType.Name = "lblExtraType";
            this.lblExtraType.Style = "font-weight: bold; background-color: White; ";
            this.lblExtraType.Text = "Extra Type";
            this.lblExtraType.Top = 1.875F;
            this.lblExtraType.Width = 0.9375F;
            // 
            // Label1
            // 
            this.Label1.Border.BottomColor = System.Drawing.Color.Black;
            this.Label1.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.Label1.Border.LeftColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.Label1.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.Label1.Border.RightColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.Label1.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.Label1.Border.TopColor = System.Drawing.Color.Black;
            this.Label1.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.Label1.Height = 0.1875F;
            this.Label1.HyperLink = null;
            this.Label1.Left = 4.75F;
            this.Label1.Name = "Label1";
            this.Label1.Style = "font-weight: bold; background-color: White; ";
            this.Label1.Text = "Accepted By";
            this.Label1.Top = 1.875F;
            this.Label1.Width = 1.739583F;
            // 
            // lblLoadNo
            // 
            this.lblLoadNo.Border.BottomColor = System.Drawing.Color.Black;
            this.lblLoadNo.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblLoadNo.Border.LeftColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.lblLoadNo.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblLoadNo.Border.RightColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.lblLoadNo.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblLoadNo.Border.TopColor = System.Drawing.Color.Black;
            this.lblLoadNo.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblLoadNo.Height = 0.1875F;
            this.lblLoadNo.HyperLink = null;
            this.lblLoadNo.Left = 0.4479167F;
            this.lblLoadNo.Name = "lblLoadNo";
            this.lblLoadNo.Style = "font-weight: bold; background-color: White; ";
            this.lblLoadNo.Text = "Load No";
            this.lblLoadNo.Top = 1.875F;
            this.lblLoadNo.Width = 1F;
            // 
            // Label
            // 
            this.Label.Border.BottomColor = System.Drawing.Color.Black;
            this.Label.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.Label.Border.LeftColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.Label.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.Label.Border.RightColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.Label.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.Label.Border.TopColor = System.Drawing.Color.Black;
            this.Label.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.Label.Height = 0.1875F;
            this.Label.HyperLink = null;
            this.Label.Left = 2.375F;
            this.Label.Name = "Label";
            this.Label.Style = "font-weight: bold; background-color: White; ";
            this.Label.Text = "Description";
            this.Label.Top = 1.875F;
            this.Label.Width = 2.375F;
            // 
            // lblDate
            // 
            this.lblDate.Border.BottomColor = System.Drawing.Color.Black;
            this.lblDate.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblDate.Border.LeftColor = System.Drawing.Color.Black;
            this.lblDate.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblDate.Border.RightColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.lblDate.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblDate.Border.TopColor = System.Drawing.Color.Black;
            this.lblDate.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblDate.Height = 0.1875F;
            this.lblDate.HyperLink = null;
            this.lblDate.Left = 0F;
            this.lblDate.Name = "lblDate";
            this.lblDate.Style = "font-weight: bold; background-color: White; ";
            this.lblDate.Text = "Date";
            this.lblDate.Top = 1.875F;
            this.lblDate.Width = 0.4375F;
            // 
            // srPageHeader
            // 
            this.srPageHeader.Border.BottomColor = System.Drawing.Color.Black;
            this.srPageHeader.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.srPageHeader.Border.LeftColor = System.Drawing.Color.Black;
            this.srPageHeader.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.srPageHeader.Border.RightColor = System.Drawing.Color.Black;
            this.srPageHeader.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.srPageHeader.Border.TopColor = System.Drawing.Color.Black;
            this.srPageHeader.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.srPageHeader.CloseBorder = false;
            this.srPageHeader.Height = 1.8125F;
            this.srPageHeader.Left = 0F;
            this.srPageHeader.Name = "srPageHeader";
            this.srPageHeader.Report = null;
            this.srPageHeader.ReportName = "";
            this.srPageHeader.Top = 0F;
            this.srPageHeader.Width = 7.5F;
            // 
            // PageFooter
            // 
            this.PageFooter.CanShrink = true;
            this.PageFooter.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.srFooter});
            this.PageFooter.Height = 1.34375F;
            this.PageFooter.Name = "PageFooter";
            // 
            // srFooter
            // 
            this.srFooter.Border.BottomColor = System.Drawing.Color.Black;
            this.srFooter.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.srFooter.Border.LeftColor = System.Drawing.Color.Black;
            this.srFooter.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.srFooter.Border.RightColor = System.Drawing.Color.Black;
            this.srFooter.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.srFooter.Border.TopColor = System.Drawing.Color.Black;
            this.srFooter.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.srFooter.CloseBorder = false;
            this.srFooter.Height = 1.3125F;
            this.srFooter.Left = 0F;
            this.srFooter.Name = "srFooter";
            this.srFooter.Report = null;
            this.srFooter.Top = 0F;
            this.srFooter.Width = 7.5F;
            // 
            // rptInvoiceExtra
            // 
            this.MasterReport = false;
            this.PageSettings.Orientation = DataDynamics.ActiveReports.Document.PageOrientation.Portrait;
            this.PageSettings.PaperHeight = 11.69F;
            this.PageSettings.PaperWidth = 8.27F;
            this.PrintWidth = 7.552083F;
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                        "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLoadNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExtraAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExtraType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtClientContact)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvoiceNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvoiceDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccountNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInvoiceNotice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustomerName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblOurVatNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOurVatNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYourVatNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblYourVatNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNote)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotalAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVAT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInvoiceTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVATAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtVATPercentage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvoiceTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOverrideTotalAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOverrideVATAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOverrideInvoiceTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtInvoiceDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTradingAs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblExtraAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblExtraType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLoadNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		 }

		#endregion

		#region Section Event Handlers

		private void ReportHeader_Format(object sender, EventArgs e)
		{
			Facade.IInvoice facInv = new Facade.Invoice();
			
			DataSet ds = facInv.GetInvoiceDetailsForClientId(Convert.ToInt32(m_reportParams.Get("IdentityId")));
			
			try
			{
				if (ds != null)
				{
                    int identityID = Convert.ToInt32(m_reportParams.Get("IdentityId"));
                    Facade.IOrganisation facOrganisation = new Facade.Organisation();
                    txtCustomerName.Text = facOrganisation.GetDisplayNameForIdentityId(identityID);

                    string vatNo = facOrganisation.GetVATNumberForIdentityID(identityID);
                    // set the client Vat number
                    if (String.IsNullOrEmpty(vatNo))
                    {
                        this.txtYourVatNo.Visible = false;
                        this.lblYourVatNo.Visible = false;
                    }
                    else if (vatNo.ToLower().Trim() == "not set")
                    {
                        this.txtYourVatNo.Visible = false;
                        this.lblYourVatNo.Visible = false;
                    }
                    else
                    {
                        this.txtYourVatNo.Visible = true;
                        this.lblYourVatNo.Visible = true;
                        this.txtYourVatNo.Text = vatNo;
                    }

					// Client Address Details
					string mAddress = string.Empty;

                    if (Convert.ToString(ds.Tables[0].Rows[0]["AddressLine1"]) != "")
                        mAddress = Convert.ToString(ds.Tables[0].Rows[0]["AddressLine1"]) + "," + Environment.NewLine;

                    if (Convert.ToString(ds.Tables[0].Rows[0]["AddressLine2"]) != "")
                        mAddress += Convert.ToString(ds.Tables[0].Rows[0]["AddressLine2"]) + "," + Environment.NewLine;

                    if (Convert.ToString(ds.Tables[0].Rows[0]["AddressLine3"]) != "")
                        mAddress += Convert.ToString(ds.Tables[0].Rows[0]["AddressLine3"]) + "," + Environment.NewLine;

                    if (Convert.ToString(ds.Tables[0].Rows[0]["PostTown"]) != "")
                        mAddress += Convert.ToString(ds.Tables[0].Rows[0]["PostTown"]) + "," + Environment.NewLine;

                    if (Convert.ToString(ds.Tables[0].Rows[0]["County"]) != "")
                        mAddress += Convert.ToString(ds.Tables[0].Rows[0]["County"]) + "," + Environment.NewLine;

                    mAddress += Convert.ToString(ds.Tables[0].Rows[0]["PostCode"]);  

					txtAddress.Text = mAddress;

					txtAccountNo.Text = Convert.ToString(ds.Tables[0].Rows[0]["AccountCode"]);
				}
			}
			catch
			{ }

			if (m_reportParams.Get("InvoiceNo") != "0")  
			{
                txtInvoiceNo.Text = Configuration.InvoicingInvoicePrefix + m_reportParams.Get("InvoiceNo"); 
				txtInvoiceNo.ForeColor = Color.Black; 
				lblInvoiceNotice.Visible = false;
			}

			if (m_reportParams.Get("InvoiceDate") != null)
				txtInvoiceDate.Text = m_reportParams.Get("InvoiceDate");

			if (m_reportParams.Get("InvoiceDetails") != null)
			{
				txtInvoiceDetails.Text = m_reportParams.Get("InvoiceDetails");
				txtInvoiceDetails.Visible = true;
			}
            DataAccess.IOrganisationDefault DAL = new DataAccess.Organisation();
            DataSet dsDefaults = DAL.GetOrganisationDefaultsForIdentityId(int.Parse(m_reportParams.Get("IdentityId")));
            if (dsDefaults.Tables[0].Rows.Count > 0 && ((string)dsDefaults.Tables[0].Rows[0]["PaymentTerms"]).Length > 0)
                this.lblNote.Text = dsDefaults.Tables[0].Rows[0]["PaymentTerms"].ToString();
            else
                lblNote.Visible = false;

            // Show VAT Number
            bool showHeader = false;
            bool.TryParse(System.Configuration.ConfigurationManager.AppSettings["ShowInvoiceHeader"], out showHeader);

            lblOurVatNo.Visible = !showHeader;
            txtOurVatNo.Visible = !showHeader;
            txtOurVatNo.Text = System.Configuration.ConfigurationManager.AppSettings["VatNo"];
        }

		private void ReportFooter_Format(object sender, EventArgs e)
		{
            if (this.invoicePageFooterOnLastPageOnly)
            {
                sbClientCustomerReportFooter.Report = rptFooter.GetClientPageFooterSubReport(System.Configuration.ConfigurationManager.AppSettings["FooterReportName"].ToLower());
                if (sbClientCustomerReportFooter.Report != null)
                    sbClientCustomerReportFooter.Visible = true;
          }


			if (m_reportParams.Get("OverrideNET") != null)
			{
                txtOverrideTotalAmount.Text = Decimal.Parse(m_reportParams.Get("OverrideNET"),NumberStyles.Currency,Thread.CurrentThread.CurrentUICulture).ToString("C");
                txtOverrideVATAmount.Text = Decimal.Parse(m_reportParams.Get("OverrideVAT"), NumberStyles.Currency, Thread.CurrentThread.CurrentUICulture).ToString("C");
                txtOverrideInvoiceTotal.Text = Decimal.Parse(m_reportParams.Get("OverrideGross"), NumberStyles.Currency, Thread.CurrentThread.CurrentUICulture).ToString("C");
				
				txtTotalAmount.Visible = false;
				txtVATAmount.Visible = false;
				txtInvoiceTotal.Visible = false;
			}
			else
			{
                decimal totalAmountNET = Decimal.Parse(m_reportParams.Get("TotalAmountNET"), NumberStyles.Currency, Thread.CurrentThread.CurrentUICulture);
                txtTotalAmount.Text = totalAmountNET.ToString("C");
				//decimal vatAmount = totalAmountNET * (17.50M) / 100M;

                int vatNo = int.Parse(m_reportParams.Get("InvoiceExtraVatNo"));
                decimal vatRate = Decimal.Parse(m_reportParams.Get("InvoiceExtraVatRate"), NumberStyles.Float, Thread.CurrentThread.CurrentUICulture);
				decimal vatAmount = totalAmountNET * vatRate / 100M;

                this.txtVATPercentage.Text = (vatRate / 100).ToString("0.00%");
                this.txtVATAmount.Text = vatAmount.ToString("C");

				decimal invoiceTotal = totalAmountNET + vatAmount;
                txtInvoiceTotal.Text = invoiceTotal.ToString("C");
			}

            if (Globals.Configuration.InvoiceMessageForTypes.IndexOf("rptInvoiceExtra") > -1
                && Globals.Configuration.ShowInvoiceMessage)
            {
                this.txtCustomNotes.Text = String.Empty;
                this.txtCustomNotes.Html = Globals.Configuration.CustomInvoiceMessage;
            }
            else
            {
                this.txtCustomNotes.Html = String.Empty;
                this.txtCustomNotes.Text = String.Empty;
            }

            //if (m_reportParams.Get("InvoiceNo") != "0")
            //{
            //    int invoiceId = int.Parse(m_reportParams.Get("InvoiceNo"));
            //    Facade.IInvoice facInvoice = new Facade.Invoice();
            //    facInvoice.UpdateInvoiceTotals(
            //        invoiceId, 
            //        Decimal.Parse(txtTotalAmount.Text, System.Globalization.NumberStyles.Currency),
            //        Decimal.Parse(txtVATAmount.Text, System.Globalization.NumberStyles.Currency),
            //        Decimal.Parse(txtInvoiceTotal.Text, System.Globalization.NumberStyles.Currency),
            //        Decimal.Parse(txtTotalAmount.Text, System.Globalization.NumberStyles.Currency),
            //        0,
            //        ((Entities.CustomPrincipal)HttpContext.Current.User).UserName);
            //}
		}

		#endregion

        private void ReportFooter_BeforePrint(object sender, EventArgs e)
        {
            this.txtCustomNotes.Top = this.lblTotalAmount.Top;
        }
	}
}
