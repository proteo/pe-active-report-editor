using System;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;
using System.Configuration;

namespace Orchestrator.Reports.Invoicing
{
    public class rptHeader : DataDynamics.ActiveReports.ActiveReport3
	{
		public rptHeader()
		{
			InitializeComponent();

            bool showInvoiceHeader = false, showInvoicePageHeader = false;

            bool.TryParse(ConfigurationManager.AppSettings["ShowInvoiceHeader"], out showInvoiceHeader);
            bool.TryParse(ConfigurationManager.AppSettings["ShowInvoicePageHeader"], out showInvoicePageHeader);

            if ((!showInvoiceHeader && !showInvoicePageHeader) || (showInvoiceHeader && showInvoicePageHeader))
            {
                HideAllControls();
                return;
            }
            else if (showInvoicePageHeader && !showInvoiceHeader)
            {
                HideAllControls();
                sbImageHeader.Visible = true;

                switch (ConfigurationManager.AppSettings["HeaderReportName"].ToLower())
                {
                    case "firminsimageheader": //Alan Firmin
                        sbImageHeader.Report = new SubReports.FirminsImageHeader();
                        break;
                    case "nichollsimageheader"://Nicholls
                        sbImageHeader.Report = new SubReports.NichollsImageHeader();
                        break;
                    case "ffimageheader"://FF
                        sbImageHeader.Report = new SubReports.FFImageHeader();
                        break;
                    case "mstimageheader": // Mike Spence Transport
                        sbImageHeader.Report = new SubReports.MSTImageHeader();
                        break;
                    case "fandwimageheader" : //Fagan and Whalley
                        sbImageHeader.Report = new SubReports.FandWImageHeader();
                        break;
                    case "woodallimageheader": // Woodall
                        sbImageHeader.Report = new SubReports.WoodallImageHeader();
                        break;
                    case "knowlesimageheader": // Hub Partners (fomerly Wisbech Roadways)
                        sbImageHeader.Report = new SubReports.KnowlesImageHeader();
                        break;
                    case "demoimageheader": // Demo
                        sbImageHeader.Report = new SubReports.DemoImageHeader();
                        break;
                    case "kerseyimageheader": // Kersey
                        sbImageHeader.Report = new SubReports.KerseyImageHeader();
                        break;
                    case "williamsimageheader": // Williams
                        sbImageHeader.Report = new SubReports.WilliamsImageHeader();
                        break;
                    case "chilternimageheader": // Chiltern
                        sbImageHeader.Report = new SubReports.ChilternImageHeader();
                        break;
                    default:
                        sbImageHeader.Visible = false;
                        break;
                }
            }
            else
            {
                //Hide Subreport Control
                sbImageHeader.Visible = false;

                lblCompanyName.Text = ConfigurationManager.AppSettings["CompanyName"];
                lblAddressLine1.Text = ConfigurationManager.AppSettings["Address1.Line1"];
                lblAddressLine2.Text = ConfigurationManager.AppSettings["Address1.Line2"];
                lblAddressLine3.Text = ConfigurationManager.AppSettings["Address1.Line3"];

                lblAddress2Line1.Text = ConfigurationManager.AppSettings["Address2.Line1"];
                lblAddress2Line2.Text = ConfigurationManager.AppSettings["Address2.Line2"];
                lblAddress2Line3.Text = ConfigurationManager.AppSettings["Address2.Line3"];

                lblRegNo.Text = ConfigurationManager.AppSettings["RegistrationNumber"];
                lblVATNumber.Text = ConfigurationManager.AppSettings["VatNo"];

                if (ConfigurationManager.AppSettings["Address2.Line1"] == "")
                {
                    lblAddress2Line1.Visible = false;
                    lblAddress2Line2.Visible = false;
                    lblAddress2Line3.Visible = false;

                    lblAddress2Line1.Width = lblAddress2Line1.Width * 2;
                    lblAddress2Line2.Width = lblAddress2Line2.Width * 2;
                    lblAddress2Line3.Width = lblAddress2Line3.Width * 2;
                }
            }
		}

        private void HideAllControls()
        {
            foreach (DataDynamics.ActiveReports.ARControl cntl in this.ReportHeader.Controls)
            {
                cntl.Visible = false;
            }
        }

		#region ActiveReports Designer generated code
		private DataDynamics.ActiveReports.ReportHeader ReportHeader = null;
		private DataDynamics.ActiveReports.Label lblAddressLine1 = null;
        private DataDynamics.ActiveReports.Label lblAddressLine3 = null;
		private DataDynamics.ActiveReports.Label lblAddressLine2 = null;
		private DataDynamics.ActiveReports.Label lblAddress2Line1 = null;
		private DataDynamics.ActiveReports.Label lblAddress2Line3 = null;
		private DataDynamics.ActiveReports.Label lblAddress2Line2 = null;
		private DataDynamics.ActiveReports.Label lblRegNoTitle = null;
		private DataDynamics.ActiveReports.Label lblRegNo = null;
		private DataDynamics.ActiveReports.Label lblVATNumberTitle = null;
		private DataDynamics.ActiveReports.Label lblVATNumber = null;
        private DataDynamics.ActiveReports.Label lblCompanyName = null;
		private DataDynamics.ActiveReports.PageHeader PageHeader = null;
		private DataDynamics.ActiveReports.Detail Detail = null;
		private DataDynamics.ActiveReports.PageFooter PageFooter = null;
        private Label lblRHA;
        private SubReport sbImageHeader;
		private DataDynamics.ActiveReports.ReportFooter ReportFooter = null;
		public void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptHeader));
            this.Detail = new DataDynamics.ActiveReports.Detail();
            this.ReportHeader = new DataDynamics.ActiveReports.ReportHeader();
            this.lblRHA = new DataDynamics.ActiveReports.Label();
            this.lblAddressLine1 = new DataDynamics.ActiveReports.Label();
            this.lblAddressLine3 = new DataDynamics.ActiveReports.Label();
            this.lblAddressLine2 = new DataDynamics.ActiveReports.Label();
            this.lblAddress2Line1 = new DataDynamics.ActiveReports.Label();
            this.lblAddress2Line3 = new DataDynamics.ActiveReports.Label();
            this.lblAddress2Line2 = new DataDynamics.ActiveReports.Label();
            this.lblRegNoTitle = new DataDynamics.ActiveReports.Label();
            this.lblRegNo = new DataDynamics.ActiveReports.Label();
            this.lblVATNumberTitle = new DataDynamics.ActiveReports.Label();
            this.lblVATNumber = new DataDynamics.ActiveReports.Label();
            this.lblCompanyName = new DataDynamics.ActiveReports.Label();
            this.sbImageHeader = new DataDynamics.ActiveReports.SubReport();
            this.ReportFooter = new DataDynamics.ActiveReports.ReportFooter();
            this.PageHeader = new DataDynamics.ActiveReports.PageHeader();
            this.PageFooter = new DataDynamics.ActiveReports.PageFooter();
            ((System.ComponentModel.ISupportInitialize)(this.lblRHA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAddressLine1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAddressLine3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAddressLine2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAddress2Line1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAddress2Line3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAddress2Line2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRegNoTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRegNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVATNumberTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVATNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCompanyName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Height = 0F;
            this.Detail.Name = "Detail";
            // 
            // ReportHeader
            // 
            this.ReportHeader.CanShrink = true;
            this.ReportHeader.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.lblRHA,
            this.lblAddressLine1,
            this.lblAddressLine3,
            this.lblAddressLine2,
            this.lblAddress2Line1,
            this.lblAddress2Line3,
            this.lblAddress2Line2,
            this.lblRegNoTitle,
            this.lblRegNo,
            this.lblVATNumberTitle,
            this.lblVATNumber,
            this.lblCompanyName,
            this.sbImageHeader});
            this.ReportHeader.Height = 1.875F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // lblRHA
            // 
            this.lblRHA.Border.BottomColor = System.Drawing.Color.Black;
            this.lblRHA.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblRHA.Border.LeftColor = System.Drawing.Color.Black;
            this.lblRHA.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblRHA.Border.RightColor = System.Drawing.Color.Black;
            this.lblRHA.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblRHA.Border.TopColor = System.Drawing.Color.Black;
            this.lblRHA.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblRHA.Height = 0.1875F;
            this.lblRHA.HyperLink = null;
            this.lblRHA.Left = 0.2083333F;
            this.lblRHA.Name = "lblRHA";
            this.lblRHA.Style = "text-align: center; font-size: 8.25pt; ";
            this.lblRHA.Text = "All goods carried subject to RHA Conditions of Carriage 2009";
            this.lblRHA.Top = 1.375F;
            this.lblRHA.Width = 7.072917F;
            // 
            // lblAddressLine1
            // 
            this.lblAddressLine1.Border.BottomColor = System.Drawing.Color.Black;
            this.lblAddressLine1.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblAddressLine1.Border.LeftColor = System.Drawing.Color.Black;
            this.lblAddressLine1.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblAddressLine1.Border.RightColor = System.Drawing.Color.Black;
            this.lblAddressLine1.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblAddressLine1.Border.TopColor = System.Drawing.Color.Black;
            this.lblAddressLine1.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblAddressLine1.Height = 0.1875F;
            this.lblAddressLine1.HyperLink = null;
            this.lblAddressLine1.Left = 0.1875F;
            this.lblAddressLine1.Name = "lblAddressLine1";
            this.lblAddressLine1.Style = "text-align: center; font-size: 9pt; ";
            this.lblAddressLine1.Text = "Head Office :  2 Garrood Drive, Industrial Estate";
            this.lblAddressLine1.Top = 0.75F;
            this.lblAddressLine1.Width = 3F;
            // 
            // lblAddressLine3
            // 
            this.lblAddressLine3.Border.BottomColor = System.Drawing.Color.Black;
            this.lblAddressLine3.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblAddressLine3.Border.LeftColor = System.Drawing.Color.Black;
            this.lblAddressLine3.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblAddressLine3.Border.RightColor = System.Drawing.Color.Black;
            this.lblAddressLine3.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblAddressLine3.Border.TopColor = System.Drawing.Color.Black;
            this.lblAddressLine3.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblAddressLine3.Height = 0.1875F;
            this.lblAddressLine3.HyperLink = null;
            this.lblAddressLine3.Left = 0.1875F;
            this.lblAddressLine3.Name = "lblAddressLine3";
            this.lblAddressLine3.Style = "text-align: center; font-size: 9pt; ";
            this.lblAddressLine3.Text = "Tel: 01328 863 111 - Fax: 01328 851 026";
            this.lblAddressLine3.Top = 1.125F;
            this.lblAddressLine3.Width = 3F;
            // 
            // lblAddressLine2
            // 
            this.lblAddressLine2.Border.BottomColor = System.Drawing.Color.Black;
            this.lblAddressLine2.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblAddressLine2.Border.LeftColor = System.Drawing.Color.Black;
            this.lblAddressLine2.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblAddressLine2.Border.RightColor = System.Drawing.Color.Black;
            this.lblAddressLine2.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblAddressLine2.Border.TopColor = System.Drawing.Color.Black;
            this.lblAddressLine2.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblAddressLine2.Height = 0.1875F;
            this.lblAddressLine2.HyperLink = null;
            this.lblAddressLine2.Left = 0.1875F;
            this.lblAddressLine2.Name = "lblAddressLine2";
            this.lblAddressLine2.Style = "text-align: center; font-size: 9pt; ";
            this.lblAddressLine2.Text = "Fakenham, Norfolk, NR21 8NL";
            this.lblAddressLine2.Top = 0.9375F;
            this.lblAddressLine2.Width = 3F;
            // 
            // lblAddress2Line1
            // 
            this.lblAddress2Line1.Border.BottomColor = System.Drawing.Color.Black;
            this.lblAddress2Line1.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblAddress2Line1.Border.LeftColor = System.Drawing.Color.Black;
            this.lblAddress2Line1.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblAddress2Line1.Border.RightColor = System.Drawing.Color.Black;
            this.lblAddress2Line1.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblAddress2Line1.Border.TopColor = System.Drawing.Color.Black;
            this.lblAddress2Line1.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblAddress2Line1.Height = 0.1875F;
            this.lblAddress2Line1.HyperLink = null;
            this.lblAddress2Line1.Left = 4.3125F;
            this.lblAddress2Line1.Name = "lblAddress2Line1";
            this.lblAddress2Line1.Style = "text-align: center; font-size: 9pt; ";
            this.lblAddress2Line1.Text = "Also At: Griffiths Road, Lostock Gralam,";
            this.lblAddress2Line1.Top = 0.75F;
            this.lblAddress2Line1.Width = 3F;
            // 
            // lblAddress2Line3
            // 
            this.lblAddress2Line3.Border.BottomColor = System.Drawing.Color.Black;
            this.lblAddress2Line3.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblAddress2Line3.Border.LeftColor = System.Drawing.Color.Black;
            this.lblAddress2Line3.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblAddress2Line3.Border.RightColor = System.Drawing.Color.Black;
            this.lblAddress2Line3.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblAddress2Line3.Border.TopColor = System.Drawing.Color.Black;
            this.lblAddress2Line3.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblAddress2Line3.Height = 0.25F;
            this.lblAddress2Line3.HyperLink = null;
            this.lblAddress2Line3.Left = 4.3125F;
            this.lblAddress2Line3.Name = "lblAddress2Line3";
            this.lblAddress2Line3.Style = "text-align: center; font-size: 9pt; ";
            this.lblAddress2Line3.Text = "Tel: 01606 351 222 - Fax: 01606 351 444";
            this.lblAddress2Line3.Top = 1.125F;
            this.lblAddress2Line3.Width = 3F;
            // 
            // lblAddress2Line2
            // 
            this.lblAddress2Line2.Border.BottomColor = System.Drawing.Color.Black;
            this.lblAddress2Line2.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblAddress2Line2.Border.LeftColor = System.Drawing.Color.Black;
            this.lblAddress2Line2.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblAddress2Line2.Border.RightColor = System.Drawing.Color.Black;
            this.lblAddress2Line2.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblAddress2Line2.Border.TopColor = System.Drawing.Color.Black;
            this.lblAddress2Line2.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblAddress2Line2.Height = 0.1875F;
            this.lblAddress2Line2.HyperLink = null;
            this.lblAddress2Line2.Left = 4.3125F;
            this.lblAddress2Line2.Name = "lblAddress2Line2";
            this.lblAddress2Line2.Style = "text-align: center; font-size: 9pt; ";
            this.lblAddress2Line2.Text = "Northwich, Cheshire, CW9 7NU";
            this.lblAddress2Line2.Top = 0.9375F;
            this.lblAddress2Line2.Width = 3F;
            // 
            // lblRegNoTitle
            // 
            this.lblRegNoTitle.Border.BottomColor = System.Drawing.Color.Black;
            this.lblRegNoTitle.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblRegNoTitle.Border.LeftColor = System.Drawing.Color.Black;
            this.lblRegNoTitle.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblRegNoTitle.Border.RightColor = System.Drawing.Color.Black;
            this.lblRegNoTitle.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblRegNoTitle.Border.TopColor = System.Drawing.Color.Black;
            this.lblRegNoTitle.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblRegNoTitle.Height = 0.1875F;
            this.lblRegNoTitle.HyperLink = null;
            this.lblRegNoTitle.Left = 0.4375F;
            this.lblRegNoTitle.Name = "lblRegNoTitle";
            this.lblRegNoTitle.Style = "font-size: 8pt; ";
            this.lblRegNoTitle.Text = "Reg No:";
            this.lblRegNoTitle.Top = 1.375F;
            this.lblRegNoTitle.Width = 0.5F;
            // 
            // lblRegNo
            // 
            this.lblRegNo.Border.BottomColor = System.Drawing.Color.Black;
            this.lblRegNo.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblRegNo.Border.LeftColor = System.Drawing.Color.Black;
            this.lblRegNo.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblRegNo.Border.RightColor = System.Drawing.Color.Black;
            this.lblRegNo.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblRegNo.Border.TopColor = System.Drawing.Color.Black;
            this.lblRegNo.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblRegNo.Height = 0.1875F;
            this.lblRegNo.HyperLink = null;
            this.lblRegNo.Left = 0.875F;
            this.lblRegNo.Name = "lblRegNo";
            this.lblRegNo.Style = "font-size: 8pt; ";
            this.lblRegNo.Text = "1041518";
            this.lblRegNo.Top = 1.375F;
            this.lblRegNo.Width = 1.0625F;
            // 
            // lblVATNumberTitle
            // 
            this.lblVATNumberTitle.Border.BottomColor = System.Drawing.Color.Black;
            this.lblVATNumberTitle.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblVATNumberTitle.Border.LeftColor = System.Drawing.Color.Black;
            this.lblVATNumberTitle.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblVATNumberTitle.Border.RightColor = System.Drawing.Color.Black;
            this.lblVATNumberTitle.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblVATNumberTitle.Border.TopColor = System.Drawing.Color.Black;
            this.lblVATNumberTitle.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblVATNumberTitle.Height = 0.1875F;
            this.lblVATNumberTitle.HyperLink = null;
            this.lblVATNumberTitle.Left = 5.6875F;
            this.lblVATNumberTitle.Name = "lblVATNumberTitle";
            this.lblVATNumberTitle.Style = "font-size: 8pt; ";
            this.lblVATNumberTitle.Text = "VAT No:";
            this.lblVATNumberTitle.Top = 1.375F;
            this.lblVATNumberTitle.Width = 0.5F;
            // 
            // lblVATNumber
            // 
            this.lblVATNumber.Border.BottomColor = System.Drawing.Color.Black;
            this.lblVATNumber.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblVATNumber.Border.LeftColor = System.Drawing.Color.Black;
            this.lblVATNumber.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblVATNumber.Border.RightColor = System.Drawing.Color.Black;
            this.lblVATNumber.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblVATNumber.Border.TopColor = System.Drawing.Color.Black;
            this.lblVATNumber.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblVATNumber.Height = 0.1875F;
            this.lblVATNumber.HyperLink = null;
            this.lblVATNumber.Left = 6.125F;
            this.lblVATNumber.Name = "lblVATNumber";
            this.lblVATNumber.Style = "font-size: 8pt; ";
            this.lblVATNumber.Text = "676 8148 86";
            this.lblVATNumber.Top = 1.375F;
            this.lblVATNumber.Width = 0.75F;
            // 
            // lblCompanyName
            // 
            this.lblCompanyName.Border.BottomColor = System.Drawing.Color.Black;
            this.lblCompanyName.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblCompanyName.Border.LeftColor = System.Drawing.Color.Black;
            this.lblCompanyName.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblCompanyName.Border.RightColor = System.Drawing.Color.Black;
            this.lblCompanyName.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblCompanyName.Border.TopColor = System.Drawing.Color.Black;
            this.lblCompanyName.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblCompanyName.Height = 0.438F;
            this.lblCompanyName.HyperLink = null;
            this.lblCompanyName.Left = 0.1875F;
            this.lblCompanyName.Name = "lblCompanyName";
            this.lblCompanyName.Style = "ddo-char-set: 1; text-align: center; font-size: 25pt; ";
            this.lblCompanyName.Text = "Jack Richards & Son Ltd.";
            this.lblCompanyName.Top = 0.3125F;
            this.lblCompanyName.Width = 7.125F;
            // 
            // sbImageHeader
            // 
            this.sbImageHeader.Border.BottomColor = System.Drawing.Color.Black;
            this.sbImageHeader.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.sbImageHeader.Border.LeftColor = System.Drawing.Color.Black;
            this.sbImageHeader.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.sbImageHeader.Border.RightColor = System.Drawing.Color.Black;
            this.sbImageHeader.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.sbImageHeader.Border.TopColor = System.Drawing.Color.Black;
            this.sbImageHeader.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.sbImageHeader.CloseBorder = false;
            this.sbImageHeader.Height = 1.875F;
            this.sbImageHeader.Left = 0F;
            this.sbImageHeader.Name = "sbImageHeader";
            this.sbImageHeader.Report = null;
            this.sbImageHeader.Top = 0F;
            this.sbImageHeader.Width = 7.5F;
            // 
            // ReportFooter
            // 
            this.ReportFooter.Height = 0F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // PageHeader
            // 
            this.PageHeader.Height = 0F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // rptHeader
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11.69F;
            this.PageSettings.PaperWidth = 8.27F;
            this.PrintWidth = 7.510417F;
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                        "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.lblRHA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAddressLine1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAddressLine3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAddressLine2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAddress2Line1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAddress2Line3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAddress2Line2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRegNoTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRegNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVATNumberTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVATNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCompanyName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

		#endregion
	}
}
