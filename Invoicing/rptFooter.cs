using System;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;
using System.Configuration;

namespace Orchestrator.Reports.Invoicing
{
    public class rptFooter : DataDynamics.ActiveReports.ActiveReport3
	{
		public rptFooter()
		{
			InitializeComponent();

            bool showInvoicePageFooter = false;
            bool.TryParse(ConfigurationManager.AppSettings["ShowInvoicePageFooter"], out showInvoicePageFooter);

            if (showInvoicePageFooter)
            {
                ActiveReport3 subReport = GetClientPageFooterSubReport(ConfigurationManager.AppSettings["FooterReportName"].ToLower());
                if (subReport != null)
                    srImageFooter.Report = subReport;
                else
                    srImageFooter.Visible = false;
            }
		}

        public static ActiveReport3 GetClientPageFooterSubReport(string clientFooterName)
        {
            ActiveReport3 rptReturn = null;
            switch (clientFooterName)
            {
                case "firminsfooter": //Alan Firmin
                    rptReturn = new SubReports.FirminsFooter();
                    break;
                case "fffooter": //Freight Force
                    rptReturn = new SubReports.FFFooter();
                    break;
                case "mstfooter": // Mike Spence Transport
                    rptReturn = new SubReports.MSTFooter();
                    break;
                case "fandwfooter": // Fagan and Whalley
                    rptReturn = new SubReports.FandWFooter();
                    break;
                case "nichollsfooter": //Nicholls
                    rptReturn = new SubReports.NichollsFooter();
                    break;
                case "woodallfooter": //Nicholls
                    rptReturn = new SubReports.WoodallFooter();
                    break;
                case "demofooter": //Demo
                    rptReturn = new SubReports.DemoFooter();
                    break;
                case "kerseyfooter": //Demo
                    rptReturn = new SubReports.KerseyFooter();
                    break;

                default:
                    rptReturn = null;
                    break;
            }

            return rptReturn;
        }

		#region ActiveReports Designer generated code
		private DataDynamics.ActiveReports.PageHeader PageHeader = null;
		private DataDynamics.ActiveReports.Detail Detail = null;
		private DataDynamics.ActiveReports.SubReport srImageFooter = null;
		private DataDynamics.ActiveReports.PageFooter PageFooter = null;
		public void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptFooter));
            this.Detail = new DataDynamics.ActiveReports.Detail();
            this.srImageFooter = new DataDynamics.ActiveReports.SubReport();
            this.PageHeader = new DataDynamics.ActiveReports.PageHeader();
            this.PageFooter = new DataDynamics.ActiveReports.PageFooter();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.ColumnSpacing = 0F;
            this.Detail.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.srImageFooter});
            this.Detail.Height = 1.313F;
            this.Detail.Name = "Detail";
            // 
            // srImageFooter
            // 
            this.srImageFooter.Border.BottomColor = System.Drawing.Color.Black;
            this.srImageFooter.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.srImageFooter.Border.LeftColor = System.Drawing.Color.Black;
            this.srImageFooter.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.srImageFooter.Border.RightColor = System.Drawing.Color.Black;
            this.srImageFooter.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.srImageFooter.Border.TopColor = System.Drawing.Color.Black;
            this.srImageFooter.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.srImageFooter.CloseBorder = false;
            this.srImageFooter.Height = 1.3125F;
            this.srImageFooter.Left = 0F;
            this.srImageFooter.Name = "srImageFooter";
            this.srImageFooter.Report = null;
            this.srImageFooter.Top = 0F;
            this.srImageFooter.Width = 7.5625F;
            // 
            // PageHeader
            // 
            this.PageHeader.Height = 0F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // rptFooter
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11.69F;
            this.PageSettings.PaperWidth = 8.27F;
            this.PrintWidth = 7.520833F;
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-style: italic; font-variant: inherit; font-wei" +
                        "ght: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: inherit; font-style: inherit; font-variant: inherit; font-weight: bo" +
                        "ld; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit; ", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

		#endregion
	}
}
