using System;
using System.Web;

using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;
using Orchestrator.Globals;
using System.Configuration;
using System.Globalization;
using System.Threading;
using System.Data;

namespace Orchestrator.Reports
{
    /// <summary>
    /// Provides extra functionality used for all the HMS reports.
    /// </summary>
    public class ReportBase : DataDynamics.ActiveReports.ActiveReport3
    {
        #region Private Fields

        private PageOrientation m_reportOrientation = PageOrientation.Landscape;

        private string m_reportTitle = String.Empty;
        private float m_reportTitleHeight = 0.35F;
        private float m_reportTitleWidth = 3F;

        private bool m_showCreator = true;

        private float m_horizontalSpacer = 0.025F;
        private float m_verticalSpacer = 0.025F;

        protected bool showInvoiceHeader = false;
        protected bool showInvoicePageHeader = false;
        protected bool showInvoiceFooter = false;
        protected bool showInvoicePageFooter = false;
        protected bool invoicePageFooterOnLastPageOnly = false;
        private CultureInfo reportCulture = null;

        public CultureInfo ReportCulture
        {
            get { return reportCulture; }
            set { reportCulture = value; }
        }

        #endregion

        #region Constructors

        public ReportBase()
        {
            bool.TryParse(ConfigurationManager.AppSettings["ShowInvoiceHeader"], out this.showInvoiceHeader);
            bool.TryParse(ConfigurationManager.AppSettings["ShowInvoicePageHeader"], out this.showInvoicePageHeader);
            bool.TryParse(ConfigurationManager.AppSettings["ShowInvoiceFooter"], out this.showInvoiceFooter);
            bool.TryParse(ConfigurationManager.AppSettings["ShowInvoicePageFooter"], out this.showInvoicePageFooter);
            this.invoicePageFooterOnLastPageOnly = Orchestrator.Globals.Configuration.ShowInvoicePageFooterOnLastPageOnly;
            this.Document.Printer.Landscape = (ReportOrientation == PageOrientation.Landscape);
            this.ReportStart += new EventHandler(HMSActiveReport_ReportStart);
            //this.PageEnd += new EventHandler(ReportBase_PageEnd);


        }


      
        #endregion

        #region Property Interfaces

        protected PageOrientation ReportOrientation
        {
            get { return m_reportOrientation; }
            set { m_reportOrientation = value; }
        }

        protected string ReportTitle
        {
            get { return m_reportTitle; }
            set { m_reportTitle = value; }
        }

        protected float ReportTitleHeight
        {
            get { return m_reportTitleHeight; }
            set { m_reportTitleHeight = value; }
        }

        protected float ReportTitleWidth
        {
            get { return m_reportTitleWidth; }
            set { m_reportTitleWidth = value; }
        }

        protected bool ShowCreator
        {
            get { return m_showCreator; }
            set { m_showCreator = value; }
        }

        protected float HorizontalSpacer
        {
            get { return m_horizontalSpacer; }
            set { m_horizontalSpacer = value; }
        }

        protected float VerticalSpacer
        {
            get { return m_verticalSpacer; }
            set { m_verticalSpacer = value; }
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Add the standard HMS header to the report.  Can be overriden if the report requires it.
        /// </summary>
        protected virtual void CreateReportHeader()
        {
            Section reportHeader = Sections["ReportHeader"];
            if (reportHeader == null)
            {
                // Insert the report header
                Sections.InsertReportHF();
                reportHeader = Sections["ReportHeader"];
            }

            // HMS Logo

            DataDynamics.ActiveReports.Picture picHMSLogo = new DataDynamics.ActiveReports.Picture();
            picHMSLogo.Name = "hdr_picHMSLogo";
            picHMSLogo.Image = new System.Drawing.Bitmap(HttpContext.Current.Server.MapPath(Orchestrator.Globals.Configuration.InstallationReportImageLocation));
            picHMSLogo.Width = 5.50F;
            picHMSLogo.Height = 1.00F;
            picHMSLogo.Top = 0F;
            picHMSLogo.Left = PrintWidth - 4.00F;
            //picHMSLogo.Left = PrintWidth - picHMSLogo.Width;
            //picHMSLogo.SizeMode = SizeModes.Clip;

            Label lblReportTitle = null;
            if (ReportTitle.Trim() != String.Empty)
            {
                // Add the report title
                lblReportTitle = new Label();
                lblReportTitle.Top = 0.25F;
                lblReportTitle.Left = 0F;
                lblReportTitle.Width = ReportTitleWidth;
                lblReportTitle.Height = ReportTitleHeight;
                lblReportTitle.Name = "hdr_lblReportTitle";
                lblReportTitle.Style = "font-name: Arial; font-size: 18pt; font-weight: bold";
                lblReportTitle.Text = ReportTitle.Trim();
            }

            // Add the controls to the header
            if (reportHeader != null)
            {
                //reportHeader.Controls.AddRange(new ARControl[] {picHMSLogo});
                reportHeader.Controls.Add(picHMSLogo);

                if (lblReportTitle != null)
                    reportHeader.Controls.Add(lblReportTitle);
            }
        }

        protected virtual void CreateReportInvoiceHeader()
        {
            Section reportHeader = Sections["ReportHeader"];
            if (reportHeader == null)
            {
                // Insert the report header
                Sections.InsertReportHF();
                reportHeader = Sections["ReportHeader"];
            }

            // HMS Logo
            Picture picHMSLogo = new Picture();

            picHMSLogo.Image = new System.Drawing.Bitmap(HttpContext.Current.Server.MapPath("~/images/Orchestratorbanner.gif"));
            picHMSLogo.Name = "hdr_picHMSLogo";
            picHMSLogo.Width = 7.00F;
            picHMSLogo.Height = 1.60F;
            picHMSLogo.Top = 0F;
            picHMSLogo.Left = (PrintWidth / 2) - (picHMSLogo.Width / 2);
            picHMSLogo.SizeMode = SizeModes.Zoom;

            Label lblReportTitle = null;
            if (ReportTitle.Trim() != String.Empty)
            {
                // Add the report title
                lblReportTitle = new Label();
                lblReportTitle.Top = 0.25F;
                lblReportTitle.Left = 0F;
                lblReportTitle.Width = ReportTitleWidth;
                lblReportTitle.Height = ReportTitleHeight;
                lblReportTitle.Name = "hdr_lblReportTitle";
                lblReportTitle.Style = "font-name: Arial; font-size: 18pt; font-weight: bold";
                lblReportTitle.Text = ReportTitle.Trim();
            }

            // Add the controls to the header
            if (reportHeader != null)
            {
                reportHeader.Controls.AddRange(new ARControl[] { picHMSLogo });

                if (lblReportTitle != null)
                    reportHeader.Controls.Add(lblReportTitle);
            }
        }

        /// <summary>
        /// Add the standard HMS footer to the report.  Can be overriden if the report requires it.
        /// </summary>
        protected virtual void CreatePageFooter()
        {
            Section pageFooter = Sections["PageFooter"];
            if (pageFooter == null)
            {
                // Insert the page footer
                Sections.InsertPageHF();
                pageFooter = Sections["PageFooter"];
            }
            this.Sections["PageFooter"].BeforePrint += new EventHandler(HMSActiveReport_BeforePrint);

            // Created On Label
            Label lblCreatedOn = new Label();
            lblCreatedOn.Text = "Created On: " + DateTime.Now.ToString("dd/MM/yy HH:mm");
            lblCreatedOn.Top = 0F;
            lblCreatedOn.Left = 0F;
            lblCreatedOn.WordWrap = false;
            lblCreatedOn.Width = 2F;
            lblCreatedOn.Name = "ftr_lblCreatedOn";

            // Create By Label
            Label lblCreatedBy = null;
            if (ShowCreator)
            {
                lblCreatedBy = new Label();
                lblCreatedBy.Text = "Created By: " + ((Entities.CustomPrincipal)HttpContext.Current.User).UserName;
                lblCreatedBy.Top = lblCreatedOn.Height + VerticalSpacer;
                lblCreatedBy.Left = 0F;
                lblCreatedBy.Name = "ft_lblCreatedBy";
            }

            // P1 Logo
            //			Picture picP1Logo = new Picture();
            //			picP1Logo.Image = new System.Drawing.Bitmap(HttpContext.Current.Server.MapPath("~/images/p1Logo.gif"));
            //			picP1Logo.Width = 0.25F;
            //			picP1Logo.Height = 0.25F;
            //			picP1Logo.Top = 0F;
            //			picP1Logo.Left = (PrintWidth / 2)- (picP1Logo.Width / 2);
            //			picP1Logo.SizeMode = SizeModes.Zoom;

            // Page Number TextBox
            TextBox txtPageNumber = new TextBox();
            txtPageNumber.Name = "ftr_txtPageNumber";
            txtPageNumber.Text = "#";
            txtPageNumber.Visible = false;
            txtPageNumber.SummaryType = SummaryType.PageCount;

            // Page Count TextBox
            TextBox txtPageCount = new TextBox();
            txtPageCount.Name = "ftr_txtPageCount";
            txtPageCount.Text = "##";
            txtPageCount.Visible = false;
            txtPageCount.SummaryType = SummaryType.PageCount;
            txtPageCount.SummaryRunning = SummaryRunning.All;

            // Page Count Label
            Label lblPage = new Label();
            lblPage.Name = "ftr_lblPage";
            lblPage.Width = 5F;
            lblPage.Top = 0F;
            lblPage.Left = PrintWidth - lblPage.Width;
            lblPage.Alignment = TextAlignment.Right;

            // Add the controls to the footer
            if (pageFooter != null)
            {
                pageFooter.Controls.AddRange(new ARControl[] { lblCreatedOn, txtPageNumber, txtPageCount, lblPage });

                if (lblCreatedBy != null)
                    pageFooter.Controls.Add(lblCreatedBy);
            }
        }

        protected virtual void SetPageMargins()
        {
            this.PageSettings.Margins.Left = 0.4F;
            this.PageSettings.Margins.Right = 0.4F;
            this.PageSettings.Margins.Top = 0.1F;
            this.PageSettings.Margins.Bottom = 0.1F;
        }

        #endregion

        private void HMSActiveReport_ReportStart(object sender, EventArgs e)
        {
            CreateReportHeader();
            CreatePageFooter();
            //this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            SetPageMargins();
        }

        private void HMSActiveReport_BeforePrint(object sender, EventArgs e)
        {
            if (this.Sections["PageFooter"].Controls.Count > 0)
            {
                TextBox txtPageNumber = (TextBox)this.Sections["PageFooter"].Controls["ftr_txtPageNumber"];
                TextBox txtPageCount = (TextBox)this.Sections["PageFooter"].Controls["ftr_txtPageCount"];

                ((Label)this.Sections["PageFooter"].Controls["ftr_lblPage"]).Text = "Page " + txtPageCount.Text + " of " + txtPageNumber.Text;
            }
        }

        public virtual void SetReportCulture()
        {
            CultureInfo culture = new CultureInfo(2057); //default to english culture
            Thread.CurrentThread.CurrentCulture = culture;
        }
    }
}
