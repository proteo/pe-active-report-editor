﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Orchestrator
{

    public enum eEmptyRunningGrouping { ClientThenTrafficArea };
    public enum eInvoiceDisplayMethod { PerInvoice = 1, PerOrder };
    public enum eInvoiceType { Normal = 1, SelfBill, OneLiner, SubContract, SelfBillRemainder, Extra, ClientInvoicing, SubContractorSelfBill, SubContractorExtra, PFHubCharge, PFDepotCharge, PFSelfBillDeliveryPayment };

}
