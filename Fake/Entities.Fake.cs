﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Orchestrator.Entities
{
    public class Organisation
    {
        private int m_lcID = -1;

        public Organisation() { }

        public int LCID
        {
            get { return m_lcID; }
            set { m_lcID = value; }
        }
    }

    public class CustomPrincipal
    {
        public string UserName { get; set; }
    }

    public class Address
    {
        public string UserName { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public string PostTown { get; set; }
        public string County { get; set; }
        public string PostCode { get; set; }
    }

    public class PreInvoice
    {
        public string UserName { get; set; }
        public InvoiceGenerationParameters GenerationParameters { get; set; }
        public int IdentityID { get; set; }
        public int InvoiceID { get; set; }
        public List<int> ItemIDs { get; set; }
        public List<InvoiceNominalCodeSubTotal> InvoiceNominalCodeSubTotals { get; set; }
        public int PreInvoiceID { get; set; }
        public string debugInfo = string.Empty;
    }

    public class InvoiceGenerationParameters
    {
        public InvoiceOptions Options { get; set; }
        public int AddressID { get; set; }
        public DateTime InvoiceDate { get; set; }
        public string ClientInvoiceReference { get; set; }
        public string PurchaseOrderReference { get; set; }
        public decimal TaxRate { get; set; }
        public OverrideInvoiceAmounts Overrides { get; set; }
        public eInvoiceType InvoiceType { get; set; }
    }

    public class InvoiceOptions
    {
        public eInvoiceDisplayMethod ExtraDisplayMethod { get; set; }
        public bool ShowDetailsOfExtras  { get; set; }
        public bool UseHeadedPaper { get; set; }
        public bool IncludeReferences { get; set; }
        public bool IncludeInvoiceRunId { get; set; }
        public bool IncludeServiceLevel { get; set; }
        public bool IncludeGoodsRefusals { get; set; }
        public bool IncludePallets { get; set; }
        public bool IncludeWeights { get; set; }
        public bool IncludeSpaces { get; set; }
        public bool IncludeCases { get; set; }
        public bool IncludeSurcharges { get; set; }
        public bool IncludePalletType { get; set; }
        public bool ApplyFuelSurcharge { get; set; }
        public eInvoiceDisplayMethod FuelSurchargeDisplayMethod { get; set; }
        public bool IncludeVatTotal { get; set; }
        public bool IncludeFuelSurchargeInTotals { get; set; }
        public bool ShowVehicleRegInline { get; set; }
        public bool IncludeGoodsType { get; set; }
    }

    public static class Utilities
    {
        public static string GetCSV(IEnumerable<int> listOfInts) { return null; }
    }

    public class OverrideInvoiceAmounts
    {
        public bool OverrideAmounts { get; set; }
        public decimal NetAmount { get; set; }
        public decimal TaxAmount { get; set; }
        public decimal TotalAmount { get; set; }
    }

    public class InvoiceNominalCodeSubTotal
    {
        public decimal NetAmount { get; set; }
    }

}
