﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Orchestrator.Globals
{

    public class Constants
    {
        public const string ReportParamsSessionVariable = "";
    }

    public class Configuration
    {
        public static int NativeCulture { get; set; }
        public static string InstallationReportImageLocation { get; set; }
        public static bool ShowInvoicePageFooterOnLastPageOnly { get; set; }
        public static string InvoicingInvoicePrefix { get; set; }
        public static bool ShowSurchargesInInvoice { get; set; }
        public static string InvoiceMessageForTypes { get; set; }
        public static bool ShowInvoiceMessage { get; set; }
        public static string CustomInvoiceMessage { get; set; }
        public static string InstallationName { get; set; }
        public static string MailServer { get; set; }
        public static int MailPort { get; set; }
        public static string MailUsername { get; set; }
        public static string MailPassword { get; set; }
        public static string InvoicingSelfBillPrefix { get; set; }
    }

}
