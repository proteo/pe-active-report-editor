using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Orchestrator.Facade
{

    public interface IOrganisation : IDisposable
    {
        Entities.Organisation GetForIdentityId(int identityid);
        string GetDisplayNameForIdentityId(int identityID);
        System.Data.DataSet GetDefaultsForIdentityId(int identityId);
        string GetVATNumberForIdentityID(int identityId);
        string GetAccountCodeForIdentityId(int identityId);
    }

    public class Organisation : IOrganisation
    {
        public Organisation() { }
        public Entities.Organisation GetForIdentityId(int identityid) { return null; }
        public string GetDisplayNameForIdentityId(int id) { return null; }
        public System.Data.DataSet GetDefaultsForIdentityId(int identityId) { return null; }
        public string GetVATNumberForIdentityID(int identityId) { return null; }
        public string GetAccountCodeForIdentityId(int identityId) { return null; }
        void System.IDisposable.Dispose()
        {
        }
    }

    public interface IInvoice : IDisposable
    {
        System.Data.DataSet GetInvoiceDetailsForClientId(int clientId);
    }

    public class Invoice : IInvoice
    {
        public Invoice() { }
        public System.Data.DataSet GetInvoiceDetailsForClientId(int clientId) { return null; }
        void System.IDisposable.Dispose()
        {
        }
    }

    public class CustomPrincipal
    {
        public string UserName { get; set; }
    }

    public interface IAddress
    {
        Entities.Address GetForAddressID(int addressID);
    }

    public class Address
    {
        public string UserName { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public string PostTown { get; set; }
        public string County { get; set; }
        public string PostCode { get; set; }

    }

    public class PreInvoice
    {
        public string UserName { get; set; }
    }

    public interface IPoint
    {
    }

    public class Point : IPoint, IAddress
    {
        public Orchestrator.Entities.Address GetForAddressID(int addressID) { return null; }
    }
}
