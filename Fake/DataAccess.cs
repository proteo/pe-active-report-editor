﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Orchestrator.DataAccess
{
    public interface IOrganisationDefault
    {
        DataSet GetOrganisationDefaultsForIdentityId(int identityId);
    }

    public class Organisation : IOrganisationDefault
    {
        public Organisation() {}
        public DataSet GetOrganisationDefaultsForIdentityId(int identityId) { return null; }
    }
}