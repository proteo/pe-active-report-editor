﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Orchestrator.Reports
{
    public interface IDriverRunSheet { }
    public interface ICallInSheet { }
    public interface IDeliveryNote { }
}