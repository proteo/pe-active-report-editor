using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;
using System.Drawing.Drawing2D;
using System.Data;
using System.Web;

namespace Orchestrator.Reports.Labels
{
    /// <summary>
    /// Summary description for palletforcelabel.
    /// </summary>
    public partial class palletforcelabel : ReportBase
    {
        private const string TRACKING_NUMBER_FORMAT = "{0}{1}{2:d7}";
        private const string BARCODE_FORMAT = "506{0}{1}{2}{3:d2}";

        public palletforcelabel()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();

            this.ReportStart += new EventHandler(palletforcelabel_ReportStart);

            //this.PageSettings.DefaultPaperSize = 
        }

        private void detail_Format(object sender, EventArgs e)
        {
            if (this.txtCustomerOrderNumber.Text.Length > 25)
                this.txtCustomerOrderNumber.Text = this.txtCustomerOrderNumber.Text.Substring(0, 24);

            var useTrackingNoFromOrder = Entities.Utilities.ParseNullable<bool>(txtTrackingNoFromOrder.Text) ?? false;

            string trackingNumber =
                useTrackingNoFromOrder && !string.IsNullOrEmpty(txtTrackingNumber.Text)
                ? txtTrackingNumber.Text
                : string.Format(TRACKING_NUMBER_FORMAT,
                    lblRequestingDepot.Text,
                    lblCollectionDepot2.Text == lblRequestingDepot.Text ? "000" : lblCollectionDepot2.Text,
                    int.Parse(lblOrderId.Text));

            string barcode = string.Format(BARCODE_FORMAT,
                lblDeliveryDepot2.Text,
                lblService2.Text,
                trackingNumber,
                int.Parse(txtQuantity.Text));

            Barcode1.Text = barcode;
            Barcode2.Text = barcode;
            lblHumanReadableBarcode.Text = barcode;
            lblTrackingNumber.Text = trackingNumber;

            string originalPalletQuantity = txtQuantity.Text;
            txtQuantity.Text = txtQuantity.Text + "/" + lblPallets.Text;

            DateTime manifestDate = DateTime.MinValue;
            if (DateTime.TryParse(lblCollectionDateTime.Text, out manifestDate))
            {
                lblCollectionDateTime.Text = string.Format("{0:dd/MM/yyyy}", manifestDate);
            }

            string originalOrderId = lblOrderId.Text;
            lblOrderId.Text = string.Format("{0:d7}", int.Parse(lblOrderId.Text));
            lblOrderId2.Text = lblOrderId.Text;

            string surchargeCode;
            string surchargeDesc;
            
            // Only show the box surrounding the surcharge and surcharge description if there is a surcharge to display.
            lblPremiumSurchargeCode.Visible = lblPremiumSurchargeDescription.Visible = ParsePremiumSurcharge(lblSurcharges.Text, out surchargeCode, out surchargeDesc);

            lblPremiumSurchargeCode.Text = surchargeCode;
            lblPremiumSurchargeDescription.Text = surchargeDesc;

            lblHubTwo.Visible = txtPrintOnLabel.Text == "True";
            lblHubTwoSmall.Visible = txtPrintOnLabel.Text == "True";

            label6.ForeColor = lblDeliveryDepot2.ForeColor = txtPrintOnLabel.Text == "True" ? Color.White : Color.Black;
            label6.BackColor = lblDeliveryDepot2.BackColor = txtPrintOnLabel.Text == "True" ? Color.Black : Color.White;
        }

        private const string SURCHARGE_CODE_AM= "AM";
        private const string SURCHARGE_CODE_TB = "TB";
        private const string SURCHARGE_CODE_TA = "TA";
        private const string SURCHARGE_CODE_SA = "SA";

        private const string SURCHARGE_DESC_AM = "AM DELIVERY";
        private const string SURCHARGE_DESC_TB = "TIMED BOOKING";
        private const string SURCHARGE_DESC_TA = "10AM DELIVERY";
        private const string SURCHARGE_DESC_SA = "SATURDAY DELIVERY";

        public bool ParsePremiumSurcharge(string surcharges, out string code, out string description)
        {
            code = string.Empty;
            description = string.Empty;

            if (string.IsNullOrEmpty(surcharges))
                return false;

            if (surcharges.Contains(SURCHARGE_CODE_AM))
            {
                code = SURCHARGE_CODE_AM;
                description = SURCHARGE_DESC_AM; 
                return true;
            }

            if (surcharges.Contains(SURCHARGE_CODE_TB))
            {
                code = SURCHARGE_CODE_TB;
                description = SURCHARGE_DESC_TB; 
                return true;
            }

            if (surcharges.Contains(SURCHARGE_CODE_TA))
            {
                code = SURCHARGE_CODE_TA;
                description = SURCHARGE_DESC_TA; 
                return true;
            }
            if (surcharges.Contains(SURCHARGE_CODE_SA))
            {
                code = SURCHARGE_CODE_SA;
                description = SURCHARGE_DESC_SA;
                return true;
            }

            return false;
        }

        protected override void CreateReportHeader()
        {
            //Do Nothing and prevent base from doing anything
            //base.CreateReportHeader();
        }

        protected override void CreatePageFooter()
        {
            //Do Nothing and prevent base from doing anything
            //base.CreatePageFooter();
        }

        private void palletforcelabel_ReportStart(object sender, EventArgs e)
        {
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Custom;
            this.PageSettings.Orientation = PageOrientation.Landscape;
            this.PageSettings.PaperHeight = 8.0f;
            this.PageSettings.PaperWidth = 4.0f;
            this.PageSettings.Gutter = 0f;
            //this.PageSettings.PaperName = "Palletforce Label";
            this.PageSettings.Margins.Top = 0.0f;
            this.PageSettings.Margins.Bottom = 0.0f;
            this.PageSettings.Margins.Left = 0.0f;
            this.PageSettings.Margins.Right = 0.0f;
        }

        /// <summary>
        /// method to rotate an image either clockwise or counter-clockwise
        /// </summary>
        /// <param name="img">the image to be rotated</param>
        /// <param name="rotationAngle">the angle (in degrees).
        /// NOTE: 
        /// Positive values will rotate clockwise
        /// negative values will rotate counter-clockwise
        /// </param>
        /// <returns></returns>
        public static Image RotateImage(Image img, float rotationAngle)
        {
            //create an empty Bitmap image
            Bitmap bmp = new Bitmap(img.Width, img.Height);

            //turn the Bitmap into a Graphics object
            Graphics gfx = Graphics.FromImage(bmp);

            //now we set the rotation point to the center of our image
            gfx.TranslateTransform((float)bmp.Width / 2, (float)bmp.Height / 2);

            //now rotate the image
            gfx.RotateTransform(rotationAngle);

            gfx.TranslateTransform(-(float)bmp.Width / 2, -(float)bmp.Height / 2);

            //set the InterpolationMode to HighQualityBicubic so to ensure a high
            //quality image once it is transformed to the specified size
            gfx.InterpolationMode = InterpolationMode.HighQualityBicubic;

            //now draw our new image onto the graphics object
            gfx.DrawImage(img, new Point(0, 0));

            //dispose of our Graphics object
            gfx.Dispose();

            //return the image
            return bmp;
        }
    }
}
