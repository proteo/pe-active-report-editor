namespace Orchestrator.Reports.Labels
{
    /// <summary>
    /// Summary description for palletforcelabel.
    /// </summary>
    partial class palletforcelabel
    {
        private DataDynamics.ActiveReports.Detail detail;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(palletforcelabel));
            this.detail = new DataDynamics.ActiveReports.Detail();
            this.label3 = new DataDynamics.ActiveReports.Label();
            this.lblPremiumSurchargeDescription = new DataDynamics.ActiveReports.Label();
            this.label6 = new DataDynamics.ActiveReports.Label();
            this.lblRequestingDepot = new DataDynamics.ActiveReports.Label();
            this.txtDeliveryPostcode = new DataDynamics.ActiveReports.TextBox();
            this.lblService2 = new DataDynamics.ActiveReports.Label();
            this.lblConsignmentNumber = new DataDynamics.ActiveReports.Label();
            this.lblReqDepot = new DataDynamics.ActiveReports.Label();
            this.lblQuantity = new DataDynamics.ActiveReports.Label();
            this.lblWeight = new DataDynamics.ActiveReports.Label();
            this.lblDeliveryDate = new DataDynamics.ActiveReports.Label();
            this.lblPalletforceServiceLevel = new DataDynamics.ActiveReports.Label();
            this.lblServiceDescription = new DataDynamics.ActiveReports.Label();
            this.Barcode1 = new DataDynamics.ActiveReports.Barcode();
            this.label11 = new DataDynamics.ActiveReports.Label();
            this.label12 = new DataDynamics.ActiveReports.Label();
            this.lblOrderId = new DataDynamics.ActiveReports.Label();
            this.label15 = new DataDynamics.ActiveReports.Label();
            this.Barcode2 = new DataDynamics.ActiveReports.Barcode();
            this.lblHumanReadableBarcode = new DataDynamics.ActiveReports.Label();
            this.lblCollectionDepot2 = new DataDynamics.ActiveReports.Label();
            this.lblDeliveryDepot2 = new DataDynamics.ActiveReports.Label();
            this.txtQuantity = new DataDynamics.ActiveReports.TextBox();
            this.txtWeight = new DataDynamics.ActiveReports.TextBox();
            this.txtDeliveryDate = new DataDynamics.ActiveReports.TextBox();
            this.lblWeight2 = new DataDynamics.ActiveReports.Label();
            this.txtNote1 = new DataDynamics.ActiveReports.TextBox();
            this.txtNote2 = new DataDynamics.ActiveReports.TextBox();
            this.txtCustomerName = new DataDynamics.ActiveReports.TextBox();
            this.txtCustomerOrderNumber = new DataDynamics.ActiveReports.TextBox();
            this.lblPallets = new DataDynamics.ActiveReports.Label();
            this.lblCollectionDateTime = new DataDynamics.ActiveReports.Label();
            this.lblOrderId2 = new DataDynamics.ActiveReports.Label();
            this.txtDeliveryName = new DataDynamics.ActiveReports.TextBox();
            this.txtAddress1 = new DataDynamics.ActiveReports.TextBox();
            this.lblSurcharges = new DataDynamics.ActiveReports.Label();
            this.txtTrackingNumber = new DataDynamics.ActiveReports.TextBox();
            this.txtTrackingNoFromOrder = new DataDynamics.ActiveReports.TextBox();
            this.lblPremiumSurchargeCode = new DataDynamics.ActiveReports.Label();
            this.label4 = new DataDynamics.ActiveReports.Label();
            this.label5 = new DataDynamics.ActiveReports.Label();
            this.label7 = new DataDynamics.ActiveReports.Label();
            this.label8 = new DataDynamics.ActiveReports.Label();
            this.lblTrackingNumber = new DataDynamics.ActiveReports.Label();
            this.label10 = new DataDynamics.ActiveReports.Label();
            this.label13 = new DataDynamics.ActiveReports.Label();
            this.label14 = new DataDynamics.ActiveReports.Label();
            this.label16 = new DataDynamics.ActiveReports.Label();
            this.lblNotes1 = new DataDynamics.ActiveReports.Label();
            this.lblHubTwo = new DataDynamics.ActiveReports.Label();
            this.lblHubTwoSmall = new DataDynamics.ActiveReports.Label();
            this.txtPrintOnLabel = new DataDynamics.ActiveReports.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPremiumSurchargeDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRequestingDepot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDeliveryPostcode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblService2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblConsignmentNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReqDepot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblQuantity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblWeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDeliveryDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPalletforceServiceLevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblServiceDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblOrderId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHumanReadableBarcode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCollectionDepot2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDeliveryDepot2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQuantity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDeliveryDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblWeight2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNote1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNote2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustomerName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustomerOrderNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPallets)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCollectionDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblOrderId2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDeliveryName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSurcharges)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTrackingNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTrackingNoFromOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPremiumSurchargeCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTrackingNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNotes1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHubTwo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHubTwoSmall)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrintOnLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // detail
            // 
            this.detail.CanGrow = false;
            this.detail.ColumnSpacing = 0F;
            this.detail.Controls.AddRange(new DataDynamics.ActiveReports.ARControl[] {
            this.label3,
            this.lblPremiumSurchargeDescription,
            this.label6,
            this.lblRequestingDepot,
            this.txtDeliveryPostcode,
            this.lblService2,
            this.lblConsignmentNumber,
            this.lblReqDepot,
            this.lblQuantity,
            this.lblWeight,
            this.lblDeliveryDate,
            this.lblPalletforceServiceLevel,
            this.lblServiceDescription,
            this.Barcode1,
            this.label11,
            this.label12,
            this.lblOrderId,
            this.label15,
            this.Barcode2,
            this.lblHumanReadableBarcode,
            this.lblCollectionDepot2,
            this.lblDeliveryDepot2,
            this.txtQuantity,
            this.txtWeight,
            this.txtDeliveryDate,
            this.lblWeight2,
            this.txtNote1,
            this.txtNote2,
            this.txtCustomerName,
            this.txtCustomerOrderNumber,
            this.lblPallets,
            this.lblCollectionDateTime,
            this.lblOrderId2,
            this.txtDeliveryName,
            this.txtAddress1,
            this.lblSurcharges,
            this.txtTrackingNumber,
            this.txtTrackingNoFromOrder,
            this.lblPremiumSurchargeCode,
            this.label4,
            this.label5,
            this.label7,
            this.label8,
            this.lblTrackingNumber,
            this.label10,
            this.label13,
            this.label14,
            this.label16,
            this.lblNotes1,
            this.lblHubTwo,
            this.lblHubTwoSmall,
            this.txtPrintOnLabel});
            this.detail.Height = 4F;
            this.detail.Name = "detail";
            this.detail.NewPage = DataDynamics.ActiveReports.NewPage.After;
            this.detail.Format += new System.EventHandler(this.detail_Format);
            // 
            // label3
            // 
            this.label3.Border.BottomColor = System.Drawing.Color.Black;
            this.label3.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.label3.Border.LeftColor = System.Drawing.Color.Black;
            this.label3.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.label3.Border.RightColor = System.Drawing.Color.Black;
            this.label3.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.label3.Border.TopColor = System.Drawing.Color.Black;
            this.label3.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.label3.Height = 0.1875F;
            this.label3.HyperLink = null;
            this.label3.Left = 1.4375F;
            this.label3.Name = "label3";
            this.label3.Style = "text-decoration: none; ddo-char-set: 0; font-size: 8.25pt; ";
            this.label3.Text = "Del";
            this.label3.Top = 1.5625F;
            this.label3.Width = 0.375F;
            // 
            // lblPremiumSurchargeDescription
            // 
            this.lblPremiumSurchargeDescription.Border.BottomColor = System.Drawing.Color.Black;
            this.lblPremiumSurchargeDescription.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblPremiumSurchargeDescription.Border.LeftColor = System.Drawing.Color.Black;
            this.lblPremiumSurchargeDescription.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblPremiumSurchargeDescription.Border.RightColor = System.Drawing.Color.Black;
            this.lblPremiumSurchargeDescription.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblPremiumSurchargeDescription.Border.TopColor = System.Drawing.Color.Black;
            this.lblPremiumSurchargeDescription.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblPremiumSurchargeDescription.Height = 0.1875F;
            this.lblPremiumSurchargeDescription.HyperLink = null;
            this.lblPremiumSurchargeDescription.Left = 1.25F;
            this.lblPremiumSurchargeDescription.Name = "lblPremiumSurchargeDescription";
            this.lblPremiumSurchargeDescription.Style = "text-decoration: none; ddo-char-set: 0; text-align: left; font-size: 9.75pt; ";
            this.lblPremiumSurchargeDescription.Text = "AM DELIVERY";
            this.lblPremiumSurchargeDescription.Top = 1.375F;
            this.lblPremiumSurchargeDescription.Width = 1.5F;
            // 
            // label6
            // 
            this.label6.Border.BottomColor = System.Drawing.Color.Black;
            this.label6.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.label6.Border.LeftColor = System.Drawing.Color.Black;
            this.label6.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.label6.Border.RightColor = System.Drawing.Color.Black;
            this.label6.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.label6.Border.TopColor = System.Drawing.Color.Black;
            this.label6.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.label6.DataField = "DeliveryDepot";
            this.label6.Height = 0.6875F;
            this.label6.HyperLink = null;
            this.label6.Left = 1.4375F;
            this.label6.Name = "label6";
            this.label6.Style = "ddo-char-set: 0; font-weight: bold; font-size: 48pt; vertical-align: middle; ";
            this.label6.Text = "099";
            this.label6.Top = 1.6875F;
            this.label6.Width = 1.25F;
            // 
            // lblRequestingDepot
            // 
            this.lblRequestingDepot.Border.BottomColor = System.Drawing.Color.Black;
            this.lblRequestingDepot.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblRequestingDepot.Border.LeftColor = System.Drawing.Color.Black;
            this.lblRequestingDepot.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblRequestingDepot.Border.RightColor = System.Drawing.Color.Black;
            this.lblRequestingDepot.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblRequestingDepot.Border.TopColor = System.Drawing.Color.Black;
            this.lblRequestingDepot.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblRequestingDepot.DataField = "RequestingDepot";
            this.lblRequestingDepot.Height = 0.6875F;
            this.lblRequestingDepot.HyperLink = null;
            this.lblRequestingDepot.Left = 0.1875F;
            this.lblRequestingDepot.Name = "lblRequestingDepot";
            this.lblRequestingDepot.Style = "ddo-char-set: 0; font-weight: bold; font-size: 48pt; vertical-align: middle; ";
            this.lblRequestingDepot.Text = "001";
            this.lblRequestingDepot.Top = 1.6875F;
            this.lblRequestingDepot.Width = 1.1875F;
            // 
            // txtDeliveryPostcode
            // 
            this.txtDeliveryPostcode.Border.BottomColor = System.Drawing.Color.Black;
            this.txtDeliveryPostcode.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtDeliveryPostcode.Border.LeftColor = System.Drawing.Color.Black;
            this.txtDeliveryPostcode.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtDeliveryPostcode.Border.RightColor = System.Drawing.Color.Black;
            this.txtDeliveryPostcode.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtDeliveryPostcode.Border.TopColor = System.Drawing.Color.Black;
            this.txtDeliveryPostcode.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtDeliveryPostcode.CanGrow = false;
            this.txtDeliveryPostcode.DataField = "PostCode";
            this.txtDeliveryPostcode.Height = 0.625F;
            this.txtDeliveryPostcode.Left = 2.875F;
            this.txtDeliveryPostcode.MultiLine = false;
            this.txtDeliveryPostcode.Name = "txtDeliveryPostcode";
            this.txtDeliveryPostcode.Style = "ddo-char-set: 0; text-align: left; font-weight: bold; font-size: 48pt; white-spac" +
                "e: nowrap; vertical-align: middle; ";
            this.txtDeliveryPostcode.Text = "AB24 2BQ";
            this.txtDeliveryPostcode.Top = 1F;
            this.txtDeliveryPostcode.Width = 3.5F;
            // 
            // lblService2
            // 
            this.lblService2.Angle = 900;
            this.lblService2.Border.BottomColor = System.Drawing.Color.Black;
            this.lblService2.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblService2.Border.LeftColor = System.Drawing.Color.Black;
            this.lblService2.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblService2.Border.RightColor = System.Drawing.Color.Black;
            this.lblService2.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblService2.Border.TopColor = System.Drawing.Color.Black;
            this.lblService2.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblService2.DataField = "ShortServiceLevel";
            this.lblService2.Height = 0.25F;
            this.lblService2.HyperLink = null;
            this.lblService2.Left = 6.625F;
            this.lblService2.Name = "lblService2";
            this.lblService2.Style = "text-align: right; font-weight: bold; font-size: 12pt; vertical-align: bottom; ";
            this.lblService2.Text = "A";
            this.lblService2.Top = 1.375F;
            this.lblService2.Width = 0.25F;
            // 
            // lblConsignmentNumber
            // 
            this.lblConsignmentNumber.Border.BottomColor = System.Drawing.Color.Black;
            this.lblConsignmentNumber.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblConsignmentNumber.Border.LeftColor = System.Drawing.Color.Black;
            this.lblConsignmentNumber.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblConsignmentNumber.Border.RightColor = System.Drawing.Color.Black;
            this.lblConsignmentNumber.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblConsignmentNumber.Border.TopColor = System.Drawing.Color.Black;
            this.lblConsignmentNumber.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblConsignmentNumber.Height = 0.1875F;
            this.lblConsignmentNumber.HyperLink = null;
            this.lblConsignmentNumber.Left = 3.75F;
            this.lblConsignmentNumber.Name = "lblConsignmentNumber";
            this.lblConsignmentNumber.Style = "text-decoration: none; ddo-char-set: 0; font-size: 8.25pt; vertical-align: top; ";
            this.lblConsignmentNumber.Text = "Con No.";
            this.lblConsignmentNumber.Top = 0.3125F;
            this.lblConsignmentNumber.Width = 0.5F;
            // 
            // lblReqDepot
            // 
            this.lblReqDepot.Border.BottomColor = System.Drawing.Color.Black;
            this.lblReqDepot.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblReqDepot.Border.LeftColor = System.Drawing.Color.Black;
            this.lblReqDepot.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblReqDepot.Border.RightColor = System.Drawing.Color.Black;
            this.lblReqDepot.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblReqDepot.Border.TopColor = System.Drawing.Color.Black;
            this.lblReqDepot.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblReqDepot.Height = 0.1875F;
            this.lblReqDepot.HyperLink = null;
            this.lblReqDepot.Left = 0.1875F;
            this.lblReqDepot.Name = "lblReqDepot";
            this.lblReqDepot.Style = "ddo-char-set: 0; text-decoration: none; font-size: 8.25pt; ";
            this.lblReqDepot.Text = "Req";
            this.lblReqDepot.Top = 1.5625F;
            this.lblReqDepot.Width = 0.375F;
            // 
            // lblQuantity
            // 
            this.lblQuantity.Border.BottomColor = System.Drawing.Color.Black;
            this.lblQuantity.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblQuantity.Border.LeftColor = System.Drawing.Color.Black;
            this.lblQuantity.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblQuantity.Border.RightColor = System.Drawing.Color.Black;
            this.lblQuantity.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblQuantity.Border.TopColor = System.Drawing.Color.Black;
            this.lblQuantity.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblQuantity.Height = 0.1875F;
            this.lblQuantity.HyperLink = null;
            this.lblQuantity.Left = 4.3125F;
            this.lblQuantity.Name = "lblQuantity";
            this.lblQuantity.Style = "text-decoration: none; ddo-char-set: 0; text-align: left; font-size: 9pt; ";
            this.lblQuantity.Text = "Qty";
            this.lblQuantity.Top = 1.875F;
            this.lblQuantity.Width = 0.375F;
            // 
            // lblWeight
            // 
            this.lblWeight.Border.BottomColor = System.Drawing.Color.Black;
            this.lblWeight.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblWeight.Border.LeftColor = System.Drawing.Color.Black;
            this.lblWeight.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblWeight.Border.RightColor = System.Drawing.Color.Black;
            this.lblWeight.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblWeight.Border.TopColor = System.Drawing.Color.Black;
            this.lblWeight.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblWeight.Height = 0.1875F;
            this.lblWeight.HyperLink = null;
            this.lblWeight.Left = 4.8125F;
            this.lblWeight.Name = "lblWeight";
            this.lblWeight.Style = "ddo-char-set: 0; text-decoration: none; text-align: left; font-size: 9pt; ";
            this.lblWeight.Text = "Kg";
            this.lblWeight.Top = 1.875F;
            this.lblWeight.Width = 0.4375F;
            // 
            // lblDeliveryDate
            // 
            this.lblDeliveryDate.Border.BottomColor = System.Drawing.Color.Black;
            this.lblDeliveryDate.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblDeliveryDate.Border.LeftColor = System.Drawing.Color.Black;
            this.lblDeliveryDate.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblDeliveryDate.Border.RightColor = System.Drawing.Color.Black;
            this.lblDeliveryDate.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblDeliveryDate.Border.TopColor = System.Drawing.Color.Black;
            this.lblDeliveryDate.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblDeliveryDate.Height = 0.1875F;
            this.lblDeliveryDate.HyperLink = null;
            this.lblDeliveryDate.Left = 5.375F;
            this.lblDeliveryDate.Name = "lblDeliveryDate";
            this.lblDeliveryDate.Style = "ddo-char-set: 0; text-decoration: none; text-align: left; font-size: 9pt; ";
            this.lblDeliveryDate.Text = "Del Date";
            this.lblDeliveryDate.Top = 1.875F;
            this.lblDeliveryDate.Width = 0.6875F;
            // 
            // lblPalletforceServiceLevel
            // 
            this.lblPalletforceServiceLevel.Border.BottomColor = System.Drawing.Color.Black;
            this.lblPalletforceServiceLevel.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblPalletforceServiceLevel.Border.LeftColor = System.Drawing.Color.Black;
            this.lblPalletforceServiceLevel.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblPalletforceServiceLevel.Border.RightColor = System.Drawing.Color.Black;
            this.lblPalletforceServiceLevel.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblPalletforceServiceLevel.Border.TopColor = System.Drawing.Color.Black;
            this.lblPalletforceServiceLevel.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblPalletforceServiceLevel.DataField = "ShortServiceLevel";
            this.lblPalletforceServiceLevel.Height = 1.0625F;
            this.lblPalletforceServiceLevel.HyperLink = null;
            this.lblPalletforceServiceLevel.Left = 0.1875F;
            this.lblPalletforceServiceLevel.MultiLine = false;
            this.lblPalletforceServiceLevel.Name = "lblPalletforceServiceLevel";
            this.lblPalletforceServiceLevel.Style = "ddo-char-set: 0; text-align: left; font-weight: bold; font-size: 72pt; vertical-a" +
                "lign: bottom; ";
            this.lblPalletforceServiceLevel.Text = "A";
            this.lblPalletforceServiceLevel.Top = 0.5F;
            this.lblPalletforceServiceLevel.Width = 0.8125F;
            // 
            // lblServiceDescription
            // 
            this.lblServiceDescription.Border.BottomColor = System.Drawing.Color.Black;
            this.lblServiceDescription.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblServiceDescription.Border.LeftColor = System.Drawing.Color.Black;
            this.lblServiceDescription.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblServiceDescription.Border.RightColor = System.Drawing.Color.Black;
            this.lblServiceDescription.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblServiceDescription.Border.TopColor = System.Drawing.Color.Black;
            this.lblServiceDescription.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblServiceDescription.DataField = "ServiceLevel";
            this.lblServiceDescription.Height = 0.1875F;
            this.lblServiceDescription.HyperLink = null;
            this.lblServiceDescription.Left = 0.1875F;
            this.lblServiceDescription.MultiLine = false;
            this.lblServiceDescription.Name = "lblServiceDescription";
            this.lblServiceDescription.Style = "ddo-char-set: 0; text-decoration: none; text-align: left; font-size: 9.75pt; whit" +
                "e-space: nowrap; ";
            this.lblServiceDescription.Text = "24HRS";
            this.lblServiceDescription.Top = 1.375F;
            this.lblServiceDescription.Width = 1.0625F;
            // 
            // Barcode1
            // 
            this.Barcode1.Border.BottomColor = System.Drawing.Color.Black;
            this.Barcode1.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Barcode1.Border.LeftColor = System.Drawing.Color.Black;
            this.Barcode1.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Barcode1.Border.RightColor = System.Drawing.Color.Black;
            this.Barcode1.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Barcode1.Border.TopColor = System.Drawing.Color.Black;
            this.Barcode1.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Barcode1.Font = new System.Drawing.Font("Courier New", 8F);
            this.Barcode1.Height = 0.75F;
            this.Barcode1.Left = 0.25F;
            this.Barcode1.Name = "Barcode1";
            this.Barcode1.Style = DataDynamics.ActiveReports.BarCodeStyle.Code_128auto;
            this.Barcode1.Text = "506099A001000004771601";
            this.Barcode1.Top = 2.875F;
            this.Barcode1.Width = 5.875F;
            // 
            // label11
            // 
            this.label11.Border.BottomColor = System.Drawing.Color.Black;
            this.label11.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.label11.Border.LeftColor = System.Drawing.Color.Black;
            this.label11.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.label11.Border.RightColor = System.Drawing.Color.Black;
            this.label11.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.label11.Border.TopColor = System.Drawing.Color.Black;
            this.label11.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.label11.Height = 0.1875F;
            this.label11.HyperLink = null;
            this.label11.Left = 3.625F;
            this.label11.Name = "label11";
            this.label11.Style = "text-decoration: none; ddo-char-set: 0; font-size: 8.25pt; ";
            this.label11.Text = "Sender: ";
            this.label11.Top = 2.4375F;
            this.label11.Width = 0.5625F;
            // 
            // label12
            // 
            this.label12.Border.BottomColor = System.Drawing.Color.Black;
            this.label12.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.label12.Border.LeftColor = System.Drawing.Color.Black;
            this.label12.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.label12.Border.RightColor = System.Drawing.Color.Black;
            this.label12.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.label12.Border.TopColor = System.Drawing.Color.Black;
            this.label12.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.label12.Height = 0.1875F;
            this.label12.HyperLink = null;
            this.label12.Left = 3.625F;
            this.label12.Name = "label12";
            this.label12.Style = "ddo-char-set: 0; text-decoration: none; font-size: 8.25pt; vertical-align: bottom" +
                "; ";
            this.label12.Text = "Customer Ref1: ";
            this.label12.Top = 2.5625F;
            this.label12.Width = 0.9375F;
            // 
            // lblOrderId
            // 
            this.lblOrderId.Border.BottomColor = System.Drawing.Color.Black;
            this.lblOrderId.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblOrderId.Border.LeftColor = System.Drawing.Color.Black;
            this.lblOrderId.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblOrderId.Border.RightColor = System.Drawing.Color.Black;
            this.lblOrderId.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblOrderId.Border.TopColor = System.Drawing.Color.Black;
            this.lblOrderId.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblOrderId.DataField = "OrderId";
            this.lblOrderId.Height = 0.5625F;
            this.lblOrderId.HyperLink = null;
            this.lblOrderId.Left = 4.1875F;
            this.lblOrderId.Name = "lblOrderId";
            this.lblOrderId.Style = "ddo-char-set: 0; font-weight: bold; font-size: 32.25pt; vertical-align: bottom; ";
            this.lblOrderId.Text = "4072791";
            this.lblOrderId.Top = 0.125F;
            this.lblOrderId.Width = 2.0625F;
            // 
            // label15
            // 
            this.label15.Border.BottomColor = System.Drawing.Color.Black;
            this.label15.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.label15.Border.LeftColor = System.Drawing.Color.Black;
            this.label15.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.label15.Border.RightColor = System.Drawing.Color.Black;
            this.label15.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.label15.Border.TopColor = System.Drawing.Color.Black;
            this.label15.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.label15.Height = 0.1875F;
            this.label15.HyperLink = null;
            this.label15.Left = 0.1875F;
            this.label15.Name = "label15";
            this.label15.Style = "text-decoration: none; ddo-char-set: 0; font-size: 8.25pt; ";
            this.label15.Text = "Notes:";
            this.label15.Top = 2.3125F;
            this.label15.Width = 0.5F;
            // 
            // Barcode2
            // 
            this.Barcode2.Border.BottomColor = System.Drawing.Color.Black;
            this.Barcode2.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Barcode2.Border.LeftColor = System.Drawing.Color.Black;
            this.Barcode2.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Barcode2.Border.RightColor = System.Drawing.Color.Black;
            this.Barcode2.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Barcode2.Border.TopColor = System.Drawing.Color.Black;
            this.Barcode2.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.Barcode2.Direction = DataDynamics.ActiveReports.BarCodeDirection.BottomToTop;
            this.Barcode2.Font = new System.Drawing.Font("Courier New", 8F);
            this.Barcode2.Height = 3.5F;
            this.Barcode2.Left = 7.5625F;
            this.Barcode2.Name = "Barcode2";
            this.Barcode2.Style = DataDynamics.ActiveReports.BarCodeStyle.Code_128auto;
            this.Barcode2.Text = "506099A001000004771601";
            this.Barcode2.Top = 0.25F;
            this.Barcode2.Width = 0.375F;
            // 
            // lblHumanReadableBarcode
            // 
            this.lblHumanReadableBarcode.Border.BottomColor = System.Drawing.Color.Black;
            this.lblHumanReadableBarcode.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblHumanReadableBarcode.Border.LeftColor = System.Drawing.Color.Black;
            this.lblHumanReadableBarcode.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblHumanReadableBarcode.Border.RightColor = System.Drawing.Color.Black;
            this.lblHumanReadableBarcode.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblHumanReadableBarcode.Border.TopColor = System.Drawing.Color.Black;
            this.lblHumanReadableBarcode.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblHumanReadableBarcode.Height = 0.1968505F;
            this.lblHumanReadableBarcode.HyperLink = null;
            this.lblHumanReadableBarcode.Left = 1.9375F;
            this.lblHumanReadableBarcode.Name = "lblHumanReadableBarcode";
            this.lblHumanReadableBarcode.Style = "text-decoration: none; ddo-char-set: 1; text-align: center; font-size: 12pt; ";
            this.lblHumanReadableBarcode.Text = "506061A001000003851601";
            this.lblHumanReadableBarcode.Top = 3.6875F;
            this.lblHumanReadableBarcode.Width = 2.731299F;
            // 
            // lblCollectionDepot2
            // 
            this.lblCollectionDepot2.Angle = 900;
            this.lblCollectionDepot2.Border.BottomColor = System.Drawing.Color.Black;
            this.lblCollectionDepot2.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblCollectionDepot2.Border.LeftColor = System.Drawing.Color.Black;
            this.lblCollectionDepot2.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblCollectionDepot2.Border.RightColor = System.Drawing.Color.Black;
            this.lblCollectionDepot2.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblCollectionDepot2.Border.TopColor = System.Drawing.Color.Black;
            this.lblCollectionDepot2.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblCollectionDepot2.DataField = "CollectionDepot";
            this.lblCollectionDepot2.Height = 0.375F;
            this.lblCollectionDepot2.HyperLink = null;
            this.lblCollectionDepot2.Left = 6.625F;
            this.lblCollectionDepot2.Name = "lblCollectionDepot2";
            this.lblCollectionDepot2.Style = "text-align: right; font-weight: bold; font-size: 12pt; vertical-align: bottom; ";
            this.lblCollectionDepot2.Text = "001";
            this.lblCollectionDepot2.Top = 3.5F;
            this.lblCollectionDepot2.Width = 0.25F;
            // 
            // lblDeliveryDepot2
            // 
            this.lblDeliveryDepot2.Angle = 900;
            this.lblDeliveryDepot2.Border.BottomColor = System.Drawing.Color.Black;
            this.lblDeliveryDepot2.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblDeliveryDepot2.Border.LeftColor = System.Drawing.Color.Black;
            this.lblDeliveryDepot2.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblDeliveryDepot2.Border.RightColor = System.Drawing.Color.Black;
            this.lblDeliveryDepot2.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblDeliveryDepot2.Border.TopColor = System.Drawing.Color.Black;
            this.lblDeliveryDepot2.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblDeliveryDepot2.DataField = "DeliveryDepot";
            this.lblDeliveryDepot2.Height = 0.375F;
            this.lblDeliveryDepot2.HyperLink = null;
            this.lblDeliveryDepot2.Left = 6.625F;
            this.lblDeliveryDepot2.Name = "lblDeliveryDepot2";
            this.lblDeliveryDepot2.Style = "text-align: right; font-weight: bold; font-size: 12pt; vertical-align: bottom; ";
            this.lblDeliveryDepot2.Text = "099";
            this.lblDeliveryDepot2.Top = 3.0625F;
            this.lblDeliveryDepot2.Width = 0.25F;
            // 
            // txtQuantity
            // 
            this.txtQuantity.Border.BottomColor = System.Drawing.Color.Black;
            this.txtQuantity.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtQuantity.Border.LeftColor = System.Drawing.Color.Black;
            this.txtQuantity.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtQuantity.Border.RightColor = System.Drawing.Color.Black;
            this.txtQuantity.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtQuantity.Border.TopColor = System.Drawing.Color.Black;
            this.txtQuantity.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtQuantity.DataField = "PalletCount";
            this.txtQuantity.Height = 0.25F;
            this.txtQuantity.Left = 4.3125F;
            this.txtQuantity.Name = "txtQuantity";
            this.txtQuantity.Style = "ddo-char-set: 0; text-align: left; font-weight: bold; font-size: 12pt; ";
            this.txtQuantity.Text = "1/3";
            this.txtQuantity.Top = 2.0625F;
            this.txtQuantity.Width = 0.375F;
            // 
            // txtWeight
            // 
            this.txtWeight.Border.BottomColor = System.Drawing.Color.Black;
            this.txtWeight.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtWeight.Border.LeftColor = System.Drawing.Color.Black;
            this.txtWeight.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtWeight.Border.RightColor = System.Drawing.Color.Black;
            this.txtWeight.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtWeight.Border.TopColor = System.Drawing.Color.Black;
            this.txtWeight.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtWeight.DataField = "Weight";
            this.txtWeight.Height = 0.25F;
            this.txtWeight.Left = 4.8125F;
            this.txtWeight.Name = "txtWeight";
            this.txtWeight.Style = "ddo-char-set: 0; text-align: left; font-weight: bold; font-size: 12pt; ";
            this.txtWeight.Text = "1000";
            this.txtWeight.Top = 2.0625F;
            this.txtWeight.Width = 0.5F;
            // 
            // txtDeliveryDate
            // 
            this.txtDeliveryDate.Border.BottomColor = System.Drawing.Color.Black;
            this.txtDeliveryDate.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtDeliveryDate.Border.LeftColor = System.Drawing.Color.Black;
            this.txtDeliveryDate.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtDeliveryDate.Border.RightColor = System.Drawing.Color.Black;
            this.txtDeliveryDate.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtDeliveryDate.Border.TopColor = System.Drawing.Color.Black;
            this.txtDeliveryDate.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtDeliveryDate.DataField = "DeliveryDateTime";
            this.txtDeliveryDate.Height = 0.25F;
            this.txtDeliveryDate.Left = 5.375F;
            this.txtDeliveryDate.Name = "txtDeliveryDate";
            this.txtDeliveryDate.OutputFormat = resources.GetString("txtDeliveryDate.OutputFormat");
            this.txtDeliveryDate.Style = "ddo-char-set: 0; text-align: left; font-weight: bold; font-size: 12pt; ";
            this.txtDeliveryDate.Text = "02/02/2010";
            this.txtDeliveryDate.Top = 2.0625F;
            this.txtDeliveryDate.Width = 0.875F;
            // 
            // lblWeight2
            // 
            this.lblWeight2.Angle = 900;
            this.lblWeight2.Border.BottomColor = System.Drawing.Color.Black;
            this.lblWeight2.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblWeight2.Border.LeftColor = System.Drawing.Color.Black;
            this.lblWeight2.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblWeight2.Border.RightColor = System.Drawing.Color.Black;
            this.lblWeight2.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblWeight2.Border.TopColor = System.Drawing.Color.Black;
            this.lblWeight2.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblWeight2.DataField = "Weight";
            this.lblWeight2.Height = 0.625F;
            this.lblWeight2.HyperLink = null;
            this.lblWeight2.Left = 6.625F;
            this.lblWeight2.Name = "lblWeight2";
            this.lblWeight2.Style = "text-align: right; font-weight: bold; font-size: 12pt; vertical-align: bottom; ";
            this.lblWeight2.Text = "1000";
            this.lblWeight2.Top = 1.6875F;
            this.lblWeight2.Width = 0.25F;
            // 
            // txtNote1
            // 
            this.txtNote1.Border.BottomColor = System.Drawing.Color.Black;
            this.txtNote1.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtNote1.Border.LeftColor = System.Drawing.Color.Black;
            this.txtNote1.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtNote1.Border.RightColor = System.Drawing.Color.Black;
            this.txtNote1.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtNote1.Border.TopColor = System.Drawing.Color.Black;
            this.txtNote1.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtNote1.CanGrow = false;
            this.txtNote1.DataField = "Note1";
            this.txtNote1.Height = 0.1875F;
            this.txtNote1.Left = 0.1875F;
            this.txtNote1.MultiLine = false;
            this.txtNote1.Name = "txtNote1";
            this.txtNote1.Style = "font-size: 8.25pt; white-space: nowrap; ";
            this.txtNote1.Text = "DEL WEDNESDAY 1ST";
            this.txtNote1.Top = 2.4375F;
            this.txtNote1.Width = 3.25F;
            // 
            // txtNote2
            // 
            this.txtNote2.Border.BottomColor = System.Drawing.Color.Black;
            this.txtNote2.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtNote2.Border.LeftColor = System.Drawing.Color.Black;
            this.txtNote2.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtNote2.Border.RightColor = System.Drawing.Color.Black;
            this.txtNote2.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtNote2.Border.TopColor = System.Drawing.Color.Black;
            this.txtNote2.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtNote2.CanGrow = false;
            this.txtNote2.DataField = "Note2";
            this.txtNote2.Height = 0.1875F;
            this.txtNote2.Left = 0.1875F;
            this.txtNote2.MultiLine = false;
            this.txtNote2.Name = "txtNote2";
            this.txtNote2.Style = "font-size: 8.25pt; white-space: nowrap; vertical-align: bottom; ";
            this.txtNote2.Text = "TAIL LIFT";
            this.txtNote2.Top = 2.5625F;
            this.txtNote2.Width = 3.25F;
            // 
            // txtCustomerName
            // 
            this.txtCustomerName.Border.BottomColor = System.Drawing.Color.Black;
            this.txtCustomerName.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtCustomerName.Border.LeftColor = System.Drawing.Color.Black;
            this.txtCustomerName.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtCustomerName.Border.RightColor = System.Drawing.Color.Black;
            this.txtCustomerName.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtCustomerName.Border.TopColor = System.Drawing.Color.Black;
            this.txtCustomerName.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtCustomerName.CanGrow = false;
            this.txtCustomerName.DataField = "Customer";
            this.txtCustomerName.Height = 0.1875F;
            this.txtCustomerName.Left = 4.1875F;
            this.txtCustomerName.MultiLine = false;
            this.txtCustomerName.Name = "txtCustomerName";
            this.txtCustomerName.Style = "font-size: 8.25pt; white-space: nowrap; vertical-align: top; ";
            this.txtCustomerName.Text = "S.M.Corp";
            this.txtCustomerName.Top = 2.4375F;
            this.txtCustomerName.Width = 2.0625F;
            // 
            // txtCustomerOrderNumber
            // 
            this.txtCustomerOrderNumber.Border.BottomColor = System.Drawing.Color.Black;
            this.txtCustomerOrderNumber.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtCustomerOrderNumber.Border.LeftColor = System.Drawing.Color.Black;
            this.txtCustomerOrderNumber.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtCustomerOrderNumber.Border.RightColor = System.Drawing.Color.Black;
            this.txtCustomerOrderNumber.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtCustomerOrderNumber.Border.TopColor = System.Drawing.Color.Black;
            this.txtCustomerOrderNumber.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtCustomerOrderNumber.CanGrow = false;
            this.txtCustomerOrderNumber.DataField = "CustomerOrderNumber";
            this.txtCustomerOrderNumber.Height = 0.1875F;
            this.txtCustomerOrderNumber.Left = 4.5625F;
            this.txtCustomerOrderNumber.MultiLine = false;
            this.txtCustomerOrderNumber.Name = "txtCustomerOrderNumber";
            this.txtCustomerOrderNumber.Style = "font-size: 8.25pt; white-space: nowrap; vertical-align: bottom; ";
            this.txtCustomerOrderNumber.Text = "ANNON";
            this.txtCustomerOrderNumber.Top = 2.5625F;
            this.txtCustomerOrderNumber.Width = 1.6875F;
            // 
            // lblPallets
            // 
            this.lblPallets.Angle = 900;
            this.lblPallets.Border.BottomColor = System.Drawing.Color.Black;
            this.lblPallets.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblPallets.Border.LeftColor = System.Drawing.Color.Black;
            this.lblPallets.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblPallets.Border.RightColor = System.Drawing.Color.Black;
            this.lblPallets.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblPallets.Border.TopColor = System.Drawing.Color.Black;
            this.lblPallets.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblPallets.DataField = "NoPallets";
            this.lblPallets.Height = 0.25F;
            this.lblPallets.HyperLink = null;
            this.lblPallets.Left = 6.625F;
            this.lblPallets.Name = "lblPallets";
            this.lblPallets.Style = "text-align: right; font-weight: bold; font-size: 12pt; vertical-align: bottom; ";
            this.lblPallets.Text = "99";
            this.lblPallets.Top = 2.375F;
            this.lblPallets.Width = 0.25F;
            // 
            // lblCollectionDateTime
            // 
            this.lblCollectionDateTime.Angle = 900;
            this.lblCollectionDateTime.Border.BottomColor = System.Drawing.Color.Black;
            this.lblCollectionDateTime.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblCollectionDateTime.Border.LeftColor = System.Drawing.Color.Black;
            this.lblCollectionDateTime.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblCollectionDateTime.Border.RightColor = System.Drawing.Color.Black;
            this.lblCollectionDateTime.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblCollectionDateTime.Border.TopColor = System.Drawing.Color.Black;
            this.lblCollectionDateTime.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblCollectionDateTime.DataField = "CollectionDateTime";
            this.lblCollectionDateTime.Height = 0.8125F;
            this.lblCollectionDateTime.HyperLink = null;
            this.lblCollectionDateTime.Left = 7.0625F;
            this.lblCollectionDateTime.Name = "lblCollectionDateTime";
            this.lblCollectionDateTime.Style = "text-align: left; font-weight: bold; font-size: 9.75pt; vertical-align: bottom; ";
            this.lblCollectionDateTime.Text = "01/02/2010";
            this.lblCollectionDateTime.Top = 1.125F;
            this.lblCollectionDateTime.Width = 0.1875F;
            // 
            // lblOrderId2
            // 
            this.lblOrderId2.Angle = 900;
            this.lblOrderId2.Border.BottomColor = System.Drawing.Color.Black;
            this.lblOrderId2.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblOrderId2.Border.LeftColor = System.Drawing.Color.Black;
            this.lblOrderId2.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblOrderId2.Border.RightColor = System.Drawing.Color.Black;
            this.lblOrderId2.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblOrderId2.Border.TopColor = System.Drawing.Color.Black;
            this.lblOrderId2.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblOrderId2.DataField = "OrderId";
            this.lblOrderId2.Height = 0.625F;
            this.lblOrderId2.HyperLink = null;
            this.lblOrderId2.Left = 7.0625F;
            this.lblOrderId2.Name = "lblOrderId2";
            this.lblOrderId2.Style = "font-weight: bold; font-size: 9pt; vertical-align: bottom; ";
            this.lblOrderId2.Text = "4072791";
            this.lblOrderId2.Top = 3.25F;
            this.lblOrderId2.Width = 0.1875F;
            // 
            // txtDeliveryName
            // 
            this.txtDeliveryName.Border.BottomColor = System.Drawing.Color.Black;
            this.txtDeliveryName.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtDeliveryName.Border.LeftColor = System.Drawing.Color.Black;
            this.txtDeliveryName.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtDeliveryName.Border.RightColor = System.Drawing.Color.Black;
            this.txtDeliveryName.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtDeliveryName.Border.TopColor = System.Drawing.Color.Black;
            this.txtDeliveryName.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtDeliveryName.CanGrow = false;
            this.txtDeliveryName.DataField = "CustomerClient";
            this.txtDeliveryName.Height = 0.25F;
            this.txtDeliveryName.Left = 2.875F;
            this.txtDeliveryName.MultiLine = false;
            this.txtDeliveryName.Name = "txtDeliveryName";
            this.txtDeliveryName.Style = "ddo-char-set: 0; text-align: left; font-weight: bold; font-size: 15.75pt; white-s" +
                "pace: nowrap; ";
            this.txtDeliveryName.Text = "CEF - Beckenham (part)";
            this.txtDeliveryName.Top = 0.5625F;
            this.txtDeliveryName.Width = 3.375F;
            // 
            // txtAddress1
            // 
            this.txtAddress1.Border.BottomColor = System.Drawing.Color.Black;
            this.txtAddress1.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtAddress1.Border.LeftColor = System.Drawing.Color.Black;
            this.txtAddress1.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtAddress1.Border.RightColor = System.Drawing.Color.Black;
            this.txtAddress1.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtAddress1.Border.TopColor = System.Drawing.Color.Black;
            this.txtAddress1.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtAddress1.CanGrow = false;
            this.txtAddress1.DataField = "AddressLine1";
            this.txtAddress1.Height = 0.1875F;
            this.txtAddress1.Left = 2.875F;
            this.txtAddress1.MultiLine = false;
            this.txtAddress1.Name = "txtAddress1";
            this.txtAddress1.Style = "white-space: nowrap; ";
            this.txtAddress1.Text = "Address1";
            this.txtAddress1.Top = 0.8125F;
            this.txtAddress1.Width = 3.375F;
            // 
            // lblSurcharges
            // 
            this.lblSurcharges.Angle = 900;
            this.lblSurcharges.Border.BottomColor = System.Drawing.Color.Black;
            this.lblSurcharges.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblSurcharges.Border.LeftColor = System.Drawing.Color.Black;
            this.lblSurcharges.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblSurcharges.Border.RightColor = System.Drawing.Color.Black;
            this.lblSurcharges.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblSurcharges.Border.TopColor = System.Drawing.Color.Black;
            this.lblSurcharges.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblSurcharges.DataField = "Surcharges";
            this.lblSurcharges.Height = 1.25F;
            this.lblSurcharges.HyperLink = null;
            this.lblSurcharges.Left = 6.6875F;
            this.lblSurcharges.Name = "lblSurcharges";
            this.lblSurcharges.Style = "text-align: right; font-weight: bold; font-size: 9.75pt; vertical-align: bottom; " +
                "";
            this.lblSurcharges.Text = "EB";
            this.lblSurcharges.Top = 0.125F;
            this.lblSurcharges.Width = 0.1875F;
            // 
            // txtTrackingNumber
            // 
            this.txtTrackingNumber.Border.BottomColor = System.Drawing.Color.Black;
            this.txtTrackingNumber.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtTrackingNumber.Border.LeftColor = System.Drawing.Color.Black;
            this.txtTrackingNumber.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtTrackingNumber.Border.RightColor = System.Drawing.Color.Black;
            this.txtTrackingNumber.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtTrackingNumber.Border.TopColor = System.Drawing.Color.Black;
            this.txtTrackingNumber.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtTrackingNumber.DataField = "TrackingNumber";
            this.txtTrackingNumber.Height = 0.1979167F;
            this.txtTrackingNumber.Left = 4.8125F;
            this.txtTrackingNumber.Name = "txtTrackingNumber";
            this.txtTrackingNumber.Style = "";
            this.txtTrackingNumber.Text = "TrackingNumber";
            this.txtTrackingNumber.Top = 3.6875F;
            this.txtTrackingNumber.Visible = false;
            this.txtTrackingNumber.Width = 1F;
            // 
            // txtTrackingNoFromOrder
            // 
            this.txtTrackingNoFromOrder.Border.BottomColor = System.Drawing.Color.Black;
            this.txtTrackingNoFromOrder.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtTrackingNoFromOrder.Border.LeftColor = System.Drawing.Color.Black;
            this.txtTrackingNoFromOrder.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtTrackingNoFromOrder.Border.RightColor = System.Drawing.Color.Black;
            this.txtTrackingNoFromOrder.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtTrackingNoFromOrder.Border.TopColor = System.Drawing.Color.Black;
            this.txtTrackingNoFromOrder.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtTrackingNoFromOrder.DataField = "TrackingNoFromOrder";
            this.txtTrackingNoFromOrder.Height = 0.1979167F;
            this.txtTrackingNoFromOrder.Left = 0.9375F;
            this.txtTrackingNoFromOrder.Name = "txtTrackingNoFromOrder";
            this.txtTrackingNoFromOrder.Style = "text-align: center; ";
            this.txtTrackingNoFromOrder.Text = "TrackingNoFromOrder";
            this.txtTrackingNoFromOrder.Top = 3.6875F;
            this.txtTrackingNoFromOrder.Visible = false;
            this.txtTrackingNoFromOrder.Width = 1F;
            // 
            // lblPremiumSurchargeCode
            // 
            this.lblPremiumSurchargeCode.Border.BottomColor = System.Drawing.Color.Black;
            this.lblPremiumSurchargeCode.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblPremiumSurchargeCode.Border.LeftColor = System.Drawing.Color.Black;
            this.lblPremiumSurchargeCode.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblPremiumSurchargeCode.Border.RightColor = System.Drawing.Color.Black;
            this.lblPremiumSurchargeCode.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblPremiumSurchargeCode.Border.TopColor = System.Drawing.Color.Black;
            this.lblPremiumSurchargeCode.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.Solid;
            this.lblPremiumSurchargeCode.Height = 1.0625F;
            this.lblPremiumSurchargeCode.HyperLink = null;
            this.lblPremiumSurchargeCode.Left = 1F;
            this.lblPremiumSurchargeCode.MultiLine = false;
            this.lblPremiumSurchargeCode.Name = "lblPremiumSurchargeCode";
            this.lblPremiumSurchargeCode.Style = "ddo-char-set: 0; text-align: center; font-weight: bold; font-size: 72pt; white-sp" +
                "ace: nowrap; vertical-align: bottom; ";
            this.lblPremiumSurchargeCode.Text = "AM";
            this.lblPremiumSurchargeCode.Top = 0.5F;
            this.lblPremiumSurchargeCode.Width = 1.6875F;
            // 
            // label4
            // 
            this.label4.Angle = 900;
            this.label4.Border.BottomColor = System.Drawing.Color.Black;
            this.label4.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.label4.Border.LeftColor = System.Drawing.Color.Black;
            this.label4.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.label4.Border.RightColor = System.Drawing.Color.Black;
            this.label4.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.label4.Border.TopColor = System.Drawing.Color.Black;
            this.label4.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.label4.Height = 0.25F;
            this.label4.HyperLink = null;
            this.label4.Left = 6.5F;
            this.label4.Name = "label4";
            this.label4.Style = "ddo-char-set: 0; text-decoration: none; font-size: 8.25pt; vertical-align: bottom" +
                "; ";
            this.label4.Text = "Del";
            this.label4.Top = 3.1875F;
            this.label4.Width = 0.1875F;
            // 
            // label5
            // 
            this.label5.Angle = 900;
            this.label5.Border.BottomColor = System.Drawing.Color.Black;
            this.label5.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.label5.Border.LeftColor = System.Drawing.Color.Black;
            this.label5.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.label5.Border.RightColor = System.Drawing.Color.Black;
            this.label5.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.label5.Border.TopColor = System.Drawing.Color.Black;
            this.label5.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.label5.Height = 0.25F;
            this.label5.HyperLink = null;
            this.label5.Left = 6.5F;
            this.label5.Name = "label5";
            this.label5.Style = "text-decoration: none; ddo-char-set: 0; text-align: left; font-size: 8.25pt; vert" +
                "ical-align: bottom; ";
            this.label5.Text = "Req";
            this.label5.Top = 3.625F;
            this.label5.Width = 0.1875F;
            // 
            // label7
            // 
            this.label7.Angle = 900;
            this.label7.Border.BottomColor = System.Drawing.Color.Black;
            this.label7.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.label7.Border.LeftColor = System.Drawing.Color.Black;
            this.label7.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.label7.Border.RightColor = System.Drawing.Color.Black;
            this.label7.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.label7.Border.TopColor = System.Drawing.Color.Black;
            this.label7.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.label7.Height = 0.25F;
            this.label7.HyperLink = null;
            this.label7.Left = 6.5F;
            this.label7.Name = "label7";
            this.label7.Style = "text-decoration: none; ddo-char-set: 0; font-size: 8.25pt; vertical-align: bottom" +
                "; ";
            this.label7.Text = "Qty";
            this.label7.Top = 2.375F;
            this.label7.Width = 0.1875F;
            // 
            // label8
            // 
            this.label8.Angle = 900;
            this.label8.Border.BottomColor = System.Drawing.Color.Black;
            this.label8.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.label8.Border.LeftColor = System.Drawing.Color.Black;
            this.label8.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.label8.Border.RightColor = System.Drawing.Color.Black;
            this.label8.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.label8.Border.TopColor = System.Drawing.Color.Black;
            this.label8.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.label8.Height = 0.5625F;
            this.label8.HyperLink = null;
            this.label8.Left = 6.875F;
            this.label8.Name = "label8";
            this.label8.Style = "ddo-char-set: 0; text-decoration: none; text-align: right; font-size: 8.25pt; ver" +
                "tical-align: bottom; ";
            this.label8.Text = "Con No.";
            this.label8.Top = 3.3125F;
            this.label8.Width = 0.1875F;
            // 
            // lblTrackingNumber
            // 
            this.lblTrackingNumber.Angle = 900;
            this.lblTrackingNumber.Border.BottomColor = System.Drawing.Color.Black;
            this.lblTrackingNumber.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblTrackingNumber.Border.LeftColor = System.Drawing.Color.Black;
            this.lblTrackingNumber.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblTrackingNumber.Border.RightColor = System.Drawing.Color.Black;
            this.lblTrackingNumber.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblTrackingNumber.Border.TopColor = System.Drawing.Color.Black;
            this.lblTrackingNumber.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblTrackingNumber.Height = 1.0625F;
            this.lblTrackingNumber.HyperLink = null;
            this.lblTrackingNumber.Left = 7.0625F;
            this.lblTrackingNumber.Name = "lblTrackingNumber";
            this.lblTrackingNumber.Style = "ddo-char-set: 0; text-decoration: none; text-align: center; font-weight: bold; fo" +
                "nt-size: 9.75pt; vertical-align: bottom; ";
            this.lblTrackingNumber.Text = "0010000038516";
            this.lblTrackingNumber.Top = 2.0625F;
            this.lblTrackingNumber.Width = 0.1875F;
            // 
            // label10
            // 
            this.label10.Angle = 900;
            this.label10.Border.BottomColor = System.Drawing.Color.Black;
            this.label10.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.label10.Border.LeftColor = System.Drawing.Color.Black;
            this.label10.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.label10.Border.RightColor = System.Drawing.Color.Black;
            this.label10.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.label10.Border.TopColor = System.Drawing.Color.Black;
            this.label10.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.label10.Height = 0.6875F;
            this.label10.HyperLink = null;
            this.label10.Left = 6.875F;
            this.label10.Name = "label10";
            this.label10.Style = "text-decoration: none; ddo-char-set: 0; text-align: right; font-size: 8.25pt; ver" +
                "tical-align: bottom; ";
            this.label10.Text = "Tracking No.";
            this.label10.Top = 2.4375F;
            this.label10.Width = 0.1875F;
            // 
            // label13
            // 
            this.label13.Angle = 900;
            this.label13.Border.BottomColor = System.Drawing.Color.Black;
            this.label13.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.label13.Border.LeftColor = System.Drawing.Color.Black;
            this.label13.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.label13.Border.RightColor = System.Drawing.Color.Black;
            this.label13.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.label13.Border.TopColor = System.Drawing.Color.Black;
            this.label13.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.label13.Height = 0.6875F;
            this.label13.HyperLink = null;
            this.label13.Left = 6.875F;
            this.label13.Name = "label13";
            this.label13.Style = "ddo-char-set: 0; text-decoration: none; text-align: right; font-size: 8.25pt; ver" +
                "tical-align: bottom; ";
            this.label13.Text = "Col. Date";
            this.label13.Top = 1.25F;
            this.label13.Width = 0.1875F;
            // 
            // label14
            // 
            this.label14.Angle = 900;
            this.label14.Border.BottomColor = System.Drawing.Color.Black;
            this.label14.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.label14.Border.LeftColor = System.Drawing.Color.Black;
            this.label14.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.label14.Border.RightColor = System.Drawing.Color.Black;
            this.label14.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.label14.Border.TopColor = System.Drawing.Color.Black;
            this.label14.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.label14.Height = 0.25F;
            this.label14.HyperLink = null;
            this.label14.Left = 6.5F;
            this.label14.Name = "label14";
            this.label14.Style = "text-decoration: none; ddo-char-set: 0; font-size: 8.25pt; vertical-align: bottom" +
                "; ";
            this.label14.Text = "Kg";
            this.label14.Top = 2.0625F;
            this.label14.Width = 0.1875F;
            // 
            // label16
            // 
            this.label16.Angle = 900;
            this.label16.Border.BottomColor = System.Drawing.Color.Black;
            this.label16.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.label16.Border.LeftColor = System.Drawing.Color.Black;
            this.label16.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.label16.Border.RightColor = System.Drawing.Color.Black;
            this.label16.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.label16.Border.TopColor = System.Drawing.Color.Black;
            this.label16.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.label16.Height = 0.625F;
            this.label16.HyperLink = null;
            this.label16.Left = 6.5F;
            this.label16.Name = "label16";
            this.label16.Style = "text-decoration: none; ddo-char-set: 0; font-size: 8.25pt; vertical-align: bottom" +
                "; ";
            this.label16.Text = "Service";
            this.label16.Top = 0.75F;
            this.label16.Width = 0.1875F;
            // 
            // lblNotes1
            // 
            this.lblNotes1.Angle = 900;
            this.lblNotes1.Border.BottomColor = System.Drawing.Color.Black;
            this.lblNotes1.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblNotes1.Border.LeftColor = System.Drawing.Color.Black;
            this.lblNotes1.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblNotes1.Border.RightColor = System.Drawing.Color.Black;
            this.lblNotes1.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblNotes1.Border.TopColor = System.Drawing.Color.Black;
            this.lblNotes1.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblNotes1.DataField = "Note1";
            this.lblNotes1.Height = 3.5F;
            this.lblNotes1.HyperLink = null;
            this.lblNotes1.Left = 7.25F;
            this.lblNotes1.MultiLine = false;
            this.lblNotes1.Name = "lblNotes1";
            this.lblNotes1.Style = "text-decoration: none; ddo-char-set: 0; text-align: center; font-size: 9pt; white" +
                "-space: nowrap; vertical-align: bottom; ";
            this.lblNotes1.Text = "DEL WDENESDAY 1ST";
            this.lblNotes1.Top = 0.375F;
            this.lblNotes1.Width = 0.1875F;
            // 
            // lblHubTwo
            // 
            this.lblHubTwo.Border.BottomColor = System.Drawing.Color.Black;
            this.lblHubTwo.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblHubTwo.Border.LeftColor = System.Drawing.Color.Black;
            this.lblHubTwo.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblHubTwo.Border.RightColor = System.Drawing.Color.Black;
            this.lblHubTwo.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblHubTwo.Border.TopColor = System.Drawing.Color.Black;
            this.lblHubTwo.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblHubTwo.DataField = "HubIdentifier";
            this.lblHubTwo.Height = 0.75F;
            this.lblHubTwo.HyperLink = null;
            this.lblHubTwo.Left = 2.8125F;
            this.lblHubTwo.Name = "lblHubTwo";
            this.lblHubTwo.Style = "color: White; text-align: center; font-weight: bold; background-color: Black; fon" +
                "t-size: 48pt; vertical-align: middle; ";
            this.lblHubTwo.Text = "W";
            this.lblHubTwo.Top = 1.6875F;
            this.lblHubTwo.Width = 0.6875F;
            // 
            // lblHubTwoSmall
            // 
            this.lblHubTwoSmall.Angle = 900;
            this.lblHubTwoSmall.Border.BottomColor = System.Drawing.Color.Black;
            this.lblHubTwoSmall.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblHubTwoSmall.Border.LeftColor = System.Drawing.Color.Black;
            this.lblHubTwoSmall.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblHubTwoSmall.Border.RightColor = System.Drawing.Color.Black;
            this.lblHubTwoSmall.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblHubTwoSmall.Border.TopColor = System.Drawing.Color.Black;
            this.lblHubTwoSmall.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.lblHubTwoSmall.DataField = "HubIdentifier";
            this.lblHubTwoSmall.Height = 0.375F;
            this.lblHubTwoSmall.HyperLink = null;
            this.lblHubTwoSmall.Left = 6.5625F;
            this.lblHubTwoSmall.Name = "lblHubTwoSmall";
            this.lblHubTwoSmall.Style = "color: White; text-align: center; font-weight: bold; background-color: Black; fon" +
                "t-size: 20pt; vertical-align: middle; ";
            this.lblHubTwoSmall.Text = "W";
            this.lblHubTwoSmall.Top = 2.625F;
            this.lblHubTwoSmall.Width = 0.3125F;
            // 
            // txtPrintOnLabel
            // 
            this.txtPrintOnLabel.Border.BottomColor = System.Drawing.Color.Black;
            this.txtPrintOnLabel.Border.BottomStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtPrintOnLabel.Border.LeftColor = System.Drawing.Color.Black;
            this.txtPrintOnLabel.Border.LeftStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtPrintOnLabel.Border.RightColor = System.Drawing.Color.Black;
            this.txtPrintOnLabel.Border.RightStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtPrintOnLabel.Border.TopColor = System.Drawing.Color.Black;
            this.txtPrintOnLabel.Border.TopStyle = DataDynamics.ActiveReports.BorderLineStyle.None;
            this.txtPrintOnLabel.DataField = "PrintOnLabel";
            this.txtPrintOnLabel.Height = 0.1875F;
            this.txtPrintOnLabel.Left = 6.375F;
            this.txtPrintOnLabel.Name = "txtPrintOnLabel";
            this.txtPrintOnLabel.Style = "";
            this.txtPrintOnLabel.Text = "X";
            this.txtPrintOnLabel.Top = 0.125F;
            this.txtPrintOnLabel.Visible = false;
            this.txtPrintOnLabel.Width = 0.1875F;
            // 
            // palletforcelabel
            // 
            this.MasterReport = false;
            this.PageSettings.DefaultPaperSize = false;
            this.PageSettings.Margins.Bottom = 0F;
            this.PageSettings.Margins.Left = 0F;
            this.PageSettings.Margins.Right = 0F;
            this.PageSettings.Margins.Top = 0F;
            this.PageSettings.PaperHeight = 4F;
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Custom;
            this.PageSettings.PaperName = "Custom paper";
            this.PageSettings.PaperWidth = 8F;
            this.PrintWidth = 8F;
            this.Sections.Add(this.detail);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" +
                        "l; font-size: 10pt; color: Black; ddo-char-set: 204; ", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold; ", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" +
                        "lic; ", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold; ", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPremiumSurchargeDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRequestingDepot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDeliveryPostcode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblService2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblConsignmentNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReqDepot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblQuantity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblWeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDeliveryDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPalletforceServiceLevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblServiceDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblOrderId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHumanReadableBarcode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCollectionDepot2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDeliveryDepot2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQuantity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDeliveryDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblWeight2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNote1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNote2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustomerName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCustomerOrderNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPallets)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCollectionDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblOrderId2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDeliveryName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSurcharges)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTrackingNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTrackingNoFromOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPremiumSurchargeCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTrackingNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNotes1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHubTwo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHubTwoSmall)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrintOnLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private DataDynamics.ActiveReports.Label lblConsignmentNumber;
        private DataDynamics.ActiveReports.Label lblReqDepot;
        private DataDynamics.ActiveReports.Label lblQuantity;
        private DataDynamics.ActiveReports.Label lblWeight;
        private DataDynamics.ActiveReports.Label lblDeliveryDate;
        private DataDynamics.ActiveReports.Label label6;
        private DataDynamics.ActiveReports.Label lblPalletforceServiceLevel;
        private DataDynamics.ActiveReports.Label lblServiceDescription;
        private DataDynamics.ActiveReports.Barcode Barcode1;
        private DataDynamics.ActiveReports.Label label11;
        private DataDynamics.ActiveReports.Label label12;
        private DataDynamics.ActiveReports.Label lblOrderId;
        private DataDynamics.ActiveReports.Label lblRequestingDepot;
        private DataDynamics.ActiveReports.Label label15;
        private DataDynamics.ActiveReports.Barcode Barcode2;
        private DataDynamics.ActiveReports.Label lblHumanReadableBarcode;
        private DataDynamics.ActiveReports.Label lblCollectionDepot2;
        private DataDynamics.ActiveReports.Label lblDeliveryDepot2;
        private DataDynamics.ActiveReports.TextBox txtQuantity;
        private DataDynamics.ActiveReports.TextBox txtWeight;
        private DataDynamics.ActiveReports.TextBox txtDeliveryDate;
        private DataDynamics.ActiveReports.Label lblWeight2;
        private DataDynamics.ActiveReports.TextBox txtNote1;
        private DataDynamics.ActiveReports.TextBox txtNote2;
        private DataDynamics.ActiveReports.TextBox txtCustomerName;
        private DataDynamics.ActiveReports.TextBox txtCustomerOrderNumber;
        private DataDynamics.ActiveReports.Label lblPallets;
        private DataDynamics.ActiveReports.Label lblCollectionDateTime;
        private DataDynamics.ActiveReports.Label lblOrderId2;
        private DataDynamics.ActiveReports.Label lblService2;
        private DataDynamics.ActiveReports.TextBox txtDeliveryName;
        private DataDynamics.ActiveReports.TextBox txtDeliveryPostcode;
        private DataDynamics.ActiveReports.TextBox txtAddress1;
        private DataDynamics.ActiveReports.Label lblSurcharges;
        private DataDynamics.ActiveReports.TextBox txtTrackingNumber;
        private DataDynamics.ActiveReports.TextBox txtTrackingNoFromOrder;
        private DataDynamics.ActiveReports.Label lblPremiumSurchargeCode;
        private DataDynamics.ActiveReports.Label lblPremiumSurchargeDescription;
        private DataDynamics.ActiveReports.Label label3;
        private DataDynamics.ActiveReports.Label label4;
        private DataDynamics.ActiveReports.Label label5;
        private DataDynamics.ActiveReports.Label label7;
        private DataDynamics.ActiveReports.Label label8;
        private DataDynamics.ActiveReports.Label lblTrackingNumber;
        private DataDynamics.ActiveReports.Label label10;
        private DataDynamics.ActiveReports.Label label13;
        private DataDynamics.ActiveReports.Label label14;
        private DataDynamics.ActiveReports.Label label16;
        private DataDynamics.ActiveReports.Label lblNotes1;
        private DataDynamics.ActiveReports.Label lblHubTwo;
        private DataDynamics.ActiveReports.Label lblHubTwoSmall;
        private DataDynamics.ActiveReports.TextBox txtPrintOnLabel;
    }
}
