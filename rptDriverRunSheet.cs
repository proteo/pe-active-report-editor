using System;
using System.Data;
using System.Web;
using System.Collections.Specialized;
using System.Drawing;
using System.Drawing.Imaging;

using DataDynamics.ActiveReports;
using DataDynamics.ActiveReports.Document;
using Orchestrator.Globals;


namespace Orchestrator.Reports
{
    /// <summary>
    /// Summary description for rptMultiManifet.
    /// </summary>
    public partial class rptDriverRunSheet : Orchestrator.Reports.ReportBase, IDriverRunSheet
    {
        //----------------------------------------------------------------------------------

        NameValueCollection reportParams = null;
        bool UsePlannedTimes = false;
        string InsertText = string.Empty;
        public enum eReportSettingPortion { GeneralInstructions, ContactNumbers };


        //----------------------------------------------------------------------------------

        public rptDriverRunSheet()
        {
            NameValueCollection reportParams = (NameValueCollection)HttpContext.Current.Session[Orchestrator.Globals.Constants.ReportParamsSessionVariable];
            ReportOrientation = PageOrientation.Landscape;
            PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            UsePlannedTimes = Convert.ToBoolean(reportParams["UsePlannedTimes"].ToString());
            InsertText = UsePlannedTimes ? " at " : " by ";            

            //InitializeComponent();

            this.PageSettings.Margins.Left = 0.0F;
            this.PageSettings.Margins.Right = 0.0F;
            this.PageSettings.Margins.Bottom = 0.4F;
            this.PageSettings.Margins.Top = 0.0F;
        }

        //----------------------------------------------------------------------------------

        private void Detail_BeforePrint(object sender, System.EventArgs eArgs)
        {
            //float maxHeight = 0F;
            //foreach (ARControl control in this.Detail.Controls)
            //{
            //    if (control.Height > maxHeight)
            //        maxHeight = control.Height;
            //}

            //foreach (ARControl control in this.Detail.Controls)
            //    if (!(control is Line))
            //        control.Height = maxHeight;
        }

        //----------------------------------------------------------------------------------

        private void Detail_Format(object sender, EventArgs e)
        {
            //if (!string.IsNullOrEmpty(this.txtInstruction.Text) && !string.IsNullOrEmpty(this.txtAT.Text))
            //{
            //    if ( UsePlannedTimes || bool.Parse(txtIsTimed.Text) )
            //        this.txtInstructionAt.Text = this.txtInstruction.Text + " at " + Convert.ToDateTime(this.txtAT.Text).ToString("dd/MM/yy HH:mm");
            //    else if ( !bool.Parse(txtIsTimed.Text) && !bool.Parse(txtIsAnyTime.Text) )
            //        this.txtInstructionAt.Text = this.txtInstruction.Text + " by " + Convert.ToDateTime(this.txtAT.Text).ToString("dd/MM/yy HH:mm");
            //    else if ( bool.Parse(txtIsAnyTime.Text) )
            //        this.txtInstructionAt.Text = this.txtInstruction.Text + " on " + Convert.ToDateTime(this.txtAT.Text).ToShortDateString() + " AnyTime";
            //}
            //else
            //    this.txtInstructionAt.Text = "";

            //if (!string.IsNullOrEmpty(txtSurcharges.Text))
            //    this.txtInstructionAt.Text = this.txtInstructionAt.Text + Environment.NewLine + txtSurcharges.Text;
            
            //// get the orderDetail as rows by spliting by a comma
            //string[] orderRows = this.txtOrders.Text.Split(new string[] { "^^" }, StringSplitOptions.RemoveEmptyEntries);

            //subReport.rptsbOrders subrep = new Orchestrator.Reports.subReport.rptsbOrders();
            //subrep.DataSource = orderRows;

            //this.sbOrders.Report = subrep;
        }

        //----------------------------------------------------------------------------------
        protected override void CreateReportHeader()
        {
            ///remove the header images etc
            //this.PageSettings.Margins.Left = 0.2F;
            //this.PageSettings.Margins.Right = 0.4F;
            //this.PageSettings.Margins.Top = 0.4F;
            //this.PageSettings.Margins.Bottom = 0.4F;
        }

        protected override void CreatePageFooter()
        {
            //// Do not cause the created by, date and page count to be displayed.


            //// Show report settings information.
            //Facade.IReportSetting facReportSetting = new Facade.ReportSetting();
            //DataSet dsGeneralInstructions = facReportSetting.GetReportSettings(eReportType.MultiDriverManifest, Globals.Configuration.IdentityId, (int)eReportSettingPortion.GeneralInstructions);
            //txtInstructions.Text = string.Empty;
            //foreach (DataRow row in dsGeneralInstructions.Tables[0].Rows)
            //    txtInstructions.Text += ((string)row["Data"]) + Environment.NewLine;

            //DataSet dsContactNumbers = facReportSetting.GetReportSettings(eReportType.MultiDriverManifest, Globals.Configuration.IdentityId, (int)eReportSettingPortion.ContactNumbers);

        }

        //----------------------------------------------------------------------------------
        private void rptMultiDriverManifest_ReportStart(object sender, EventArgs e)
        {

        }

        private void PageFooter_Format(object sender, EventArgs e)
        {
            //string organisationName = Globals.Configuration.InstallationCompanyName;
            //Facade.IOrganisation facOrg = new Facade.Organisation();
            //Entities.Organisation org = facOrg.GetForIdentityId(Globals.Configuration.IdentityId);
            //string phoneNumber = string.Empty;
            //if (org.Locations != null && org.Locations.Count > 0)
            //    phoneNumber = org.Locations[0].TelephoneNumber;

            //rtbDriverInstructions.Text = rtbDriverInstructions.Text.Replace("{0}", organisationName);
            //rtbDriverInstructions.Text = rtbDriverInstructions.Text.Replace("{1}", phoneNumber);

        }

        private void PageHeader_Format(object sender, EventArgs e)
        {
            //if (HttpContext.Current.Session != null && HttpContext.Current.Session["rptParamsSessionVariable"] != null)
            //{
            //    reportParams = (NameValueCollection)HttpContext.Current.Session["rptParamsSessionVariable"];
            //    this.lblManifestNumber.Text = "Manifest Id: " + reportParams["ManifestID"];
            //    this.lblManifestName.Text = reportParams["ManifestName"];

            //    // Note: the dataset field JobId actually contains the order id!
            //    string resourceManifestId = reportParams["ManifestID"].ToString();
            //    Entities.FormType formType = null;
            //    Facade.Form facForm = new Orchestrator.Facade.Form();
            //    formType = facForm.GetForFormTypeId((int)eFormTypeId.Manifest);
            //    string docType = formType.BarcodePrefix;

            //    string resourceManifestIdWithLeadingZeros = string.Empty;
            //    int len = resourceManifestId.ToString().Length;
            //    int padding = 8 - len;
            //    for (int i = 0; i < padding; i++)
            //        resourceManifestIdWithLeadingZeros = string.Concat("0", resourceManifestIdWithLeadingZeros);
            //    resourceManifestIdWithLeadingZeros = string.Concat(resourceManifestIdWithLeadingZeros, resourceManifestId);

            //    // All Orchestrator barcodes start with "99". The next 2 digits refer to the document type, e.g. "01" means "POD".
            //    string barcode = string.Format("99{0}{1}", docType, resourceManifestIdWithLeadingZeros);

            //    // Set up the barcode.
            //    this.barcode1.AutoSize = true;
            //    this.barcode1.BarWidth = 0.0f;
            //    this.barcode1.CheckSumEnabled = false;
            //    this.barcode1.Direction = BarCodeDirection.LeftToRight;
            //    this.barcode1.ForeColor = System.Drawing.Color.Black;
            //    this.barcode1.Style = BarCodeStyle.EAN_13;
            //    this.barcode1.Text = barcode;
            //    lblBarcode.Text = barcode;
            //}
        }

        private void DriverGroupHeader_BeforePrint(object sender, EventArgs e)
        {
            //if (this.PageNumber > 1)
            //    this.lblStartTime.Visible = false;
        }

        void DriverGroupFooter_BeforePrint(object sender, System.EventArgs e)
        {
            //txtTotalPallets.Text = txtTotalPallets.Text + txtPalletSpaces.Text;

            //if (txtTotalOrders.Text.Length == 0 && txtTotalPallets.Text.Length == 0 && txtTotalWeight.Text.Length == 0)
            //    lblTotals.Visible = false;
        }

        private void PageFooter_BeforePrint(object sender, EventArgs e)
        {
            //txtPageNumber.Text = "Page " + txtPage.Text + " of " + txtPageCount.Text;
        }

        //----------------------------------------------------------------------------------
    }

    //----------------------------------------------------------------------------------

}
